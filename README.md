Upspeak Android Project
================
Introduction
--------------
This is the Upspeak Android App Project.

Getting Started
--------------
This Project uses the Gradle build system. 

Clone repository
```
git clone git@github.com:voctag/voctag_android.git
```

Workflow
----------------------
Here you can find information about the workflow, branches, and releasing new versions: https://github.com/voctag/voctag_android/wiki/Workflow-Guide

CI/CD
---------------------
For CI, we use GitHub Actions. There are two GitHub Actions:
**pull_request.yml**
* Runs on making a pull request
* Lints the app
* In the Futur: Runs the tests

**create_release.yml**
* Runs on pushing a tag to master
* Lints the app
* Build a production version apk
* Signs the apk
* Uploads the apk to Google Play alpha track
* Create a release on GitHub

Code Formatting
--------------
Please use: https://github.com/aosp-mirror/platform_development/blob/master/ide/intellij/codestyles/AndroidStyle.xml for the preferred code formatting
