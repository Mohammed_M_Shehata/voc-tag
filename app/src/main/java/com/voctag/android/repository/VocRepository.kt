package com.voctag.android.repository

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import com.voctag.android.db.DatabaseRoomManager
import com.voctag.android.db.daos.ClientDao
import com.voctag.android.db.daos.DownloadedVocsDao
import com.voctag.android.db.daos.VocsDao
import com.voctag.android.db.entities.DownloadedVoc
import com.voctag.android.db.relations.VocClient
import com.voctag.android.model.Voc

class VocRepository(context: Context) : BaseRepository(context) {

    private val downloadedVocsDao by lazy { DatabaseRoomManager.get(context, this).downloadedVocsDao() }

    fun saveDownloadedVoc(voc: Voc) {
        InsertVocAsyncTask(vocsDao, clientDao, downloadedVocsDao).execute(voc)
    }

    fun deleteDownloadedVoc(vocId: Int) {
        DeleteAsyncTask(vocsDao, downloadedVocsDao).execute(vocId)
    }

    fun getDownloadedVocs(): LiveData<List<VocClient>> {
        return downloadedVocsDao.getDownloadedVocsList()
    }

    fun saveDownloadedVocs(vocs: List<Voc>) {
        InsertVocsListAsyncTask(vocsDao, clientDao, downloadedVocsDao).execute(vocs)
    }

    fun getVocById(vocID: Int, onFetchVocListener: (voc: Voc?) -> Unit) {
        GetVocByIdAsyncTask(vocsDao, onFetchVocListener).execute(vocID)
    }

    fun updateCurrentVocIfInDatabase(voc: Voc) {
        UpdateVocAsyncTask(vocsDao).execute(voc)
    }

    class UpdateVocAsyncTask(private val vocsDao: VocsDao) : AsyncTask<Voc, Void, Void>() {
        override fun doInBackground(vararg params: Voc): Void? {
            val voc = params[0]
            val vocStoredInDatabase = vocsDao.getVocById(voc.vocID)

            vocStoredInDatabase?.let { databaseVoc ->
                voc.id = databaseVoc.id
                voc.clientSlug = databaseVoc.clientSlug
                vocsDao.insert(voc)
            }
            return null
        }
    }

    class GetVocByIdAsyncTask(private val vocsDao: VocsDao, private val onFetchVocListener: (voc: Voc?) -> Unit) : AsyncTask<Any, Void, Void>() {
        override fun doInBackground(vararg params: Any?): Void? {
            val vocId = params[0] as Int
            val voc = vocsDao.getVocById(vocId)

            onFetchVocListener(voc)
            return null
        }
    }

    class InsertVocsListAsyncTask(private val vocsDao: VocsDao, private val clientDao: ClientDao, private val downloadedVocsDao: DownloadedVocsDao) : AsyncTask<Any, Void, Void>() {
        override fun doInBackground(vararg params: Any): Void? {
            val vocsList = params[0] as List<Voc>
            val downloadedVocs = ArrayList<DownloadedVoc>()
            val vocsRowsIds = vocsDao.insertVocsIntoDatabase(vocsList, clientDao)
            vocsRowsIds.forEach { vocRowId ->
                downloadedVocs.add(DownloadedVoc(vocId = vocRowId.toInt()))
            }

            downloadedVocsDao.insert(downloadedVocs)

            return null
        }
    }

    class InsertVocAsyncTask(private val vocsDao: VocsDao, private val clientDao: ClientDao, private val downloadedVocsDao: DownloadedVocsDao) : AsyncTask<Any, Void, Void>() {
        override fun doInBackground(vararg params: Any): Void? {
            val voc = params[0] as Voc

            voc.parentClient?.let {
                voc.clientSlug = it.slug
                clientDao.insert(it)
            }

            val vocInDB = vocsDao.getVocById(voc.vocID)
            vocInDB?.let { voc.id = it.id }

            val vocId = vocsDao.insert(voc)
            downloadedVocsDao.insert(DownloadedVoc(vocId = vocId.toInt()))

            return null
        }
    }

    class DeleteAsyncTask(private val vocsDao: VocsDao, private val downloadedVocsDao: DownloadedVocsDao) : AsyncTask<Any, Void, Void>() {
        override fun doInBackground(vararg params: Any): Void? {
            val vocId = params[0] as Int
            val voc = vocsDao.getVocById(vocId) as Voc
            val deletedDownloadedVocsCount = downloadedVocsDao.deleteDownloadedVoc(voc.id)
            val deletedVocCount = vocsDao.deleteVoc(vocId)

            Log.d("com.test", "DeleteDownloadedVocsCount>${deletedDownloadedVocsCount}")
            Log.d("com.test", "DeletedVocsCount>${deletedVocCount}")
            return null
        }
    }
}