package com.voctag.android.repository

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.voctag.android.db.DatabaseRoomManager
import com.voctag.android.db.daos.ClientDao
import com.voctag.android.db.daos.PlaListVocsJoinDao
import com.voctag.android.db.daos.PlayListDao
import com.voctag.android.db.daos.VocsDao
import com.voctag.android.db.entities.PlaListVocJoin
import com.voctag.android.db.entities.PlayList
import com.voctag.android.model.Voc

class PlayListRepository(context: Context) : BaseRepository(context) {

    companion object {
        val TAG: String = PlayListRepository::class.java.simpleName
    }

    private val playListDao by lazy { DatabaseRoomManager.get(context, this).playListDao() }
    private val plaListVocsJoinDao by lazy { DatabaseRoomManager.get(context, this).playListVocJoinDao() }

    fun savePlayList(vocs: ArrayList<Voc>, lastVocIndex: Int, lastVocTimeSeek: Int, autoPlayNextEnabled: Boolean = false) {
        val playList = PlayList(PlayList.CURRENT_PLAY_LIST, lastVocIndex, lastVocTimeSeek, autoPlayNextEnabled)
        InsertAsyncTask(playListDao, vocsDao, clientDao, plaListVocsJoinDao).execute(playList, vocs)
    }

    fun getCurrentPlayList(onFetchedCurrentPlayList: ((ArrayList<Voc>, Int, Int, Boolean) -> Unit)) {
        GetAsyncTask(plaListVocsJoinDao, playListDao, onFetchedCurrentPlayList).execute()
    }

    fun updateCurrentPlayList(currentIndex: Int?, progress: Int, autoPlayNextEnabled: Boolean) {
        UpdateAsyncTask(playListDao, autoPlayNextEnabled).execute(currentIndex, progress)
    }

    class InsertAsyncTask(private val playListDao: PlayListDao, private val vocsDao: VocsDao, private val clientDao: ClientDao, private val joinDao: PlaListVocsJoinDao) : AsyncTask<Any, Void, Void>() {

        override fun doInBackground(vararg params: Any): Void? {
            val joinList = ArrayList<PlaListVocJoin>()

            val playList = params[0] as PlayList
            val vocsList = params[1] as ArrayList<Voc>

            playListDao.insert(playList)
            val vocsRowsIds = vocsDao.insertVocsIntoDatabase(vocsList, clientDao)

            for (i in 0 until vocsList.size) {
                joinList.add(PlaListVocJoin(playList.id, vocsRowsIds.get(i).toInt(), i))
            }

            joinDao.clear(playList.id)
            joinDao.insert(joinList)

            Log.d(TAG, "Saving new play list")
            return null
        }
    }

    class UpdateAsyncTask(private val playListDao: PlayListDao, private val autoPlayNextEnabled: Boolean) : AsyncTask<Int, Void, Void>() {
        override fun doInBackground(vararg params: Int?): Void? {
            val currentIndex = params[0] ?: 0
            val progress = params[1] ?: 0

            Log.d(TAG, "update current play list> $currentIndex, $progress")
            playListDao.updateCurrentPlayList(PlayList(PlayList.CURRENT_PLAY_LIST, currentIndex, progress, autoPlayNextEnabled))
            return null
        }
    }

    class GetAsyncTask(private val playListJoinDao: PlaListVocsJoinDao, private val playListDao: PlayListDao, val onFetchedCurrentPlayList: ((ArrayList<Voc>, Int, Int, Boolean) -> Unit)) : AsyncTask<Void, ArrayList<Voc>, ArrayList<Voc>>() {

        override fun doInBackground(vararg params: Void?): ArrayList<Voc>? {
            val currentPlayList = playListDao.getCurrentPlayList(PlayList.CURRENT_PLAY_LIST)
            if (currentPlayList == null) return null


            val vocsOfPlayList = playListJoinDao.getCurrentVocsOfPlayList(PlayList.CURRENT_PLAY_LIST)
            val vocsList: ArrayList<Voc> = ArrayList()

            vocsOfPlayList.forEach {
                it.voc.parentClient = it.clientList?.get(0)
                vocsList.add(it.voc)
            }

            onFetchedCurrentPlayList(vocsList, currentPlayList.lastVocIndex, currentPlayList.lastVocSeekTime, currentPlayList.autoPlayNextEnabled)
            return vocsList
        }
    }
}