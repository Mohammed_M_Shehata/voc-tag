package com.voctag.android.repository

import android.content.Context
import android.util.Log
import com.voctag.android.db.DatabaseRoomManager

open class BaseRepository(context: Context) : DatabaseRoomManager.DatabaseListener {

    protected val vocsDao by lazy { DatabaseRoomManager.get(context, this).vocsDao() }
    protected val clientDao by lazy { DatabaseRoomManager.get(context, this).clientDao() }

    override fun onDatabaseCreated() {
        Log.d(PlayListRepository.TAG, "on database created")
    }
}