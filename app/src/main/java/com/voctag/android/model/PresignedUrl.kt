package com.voctag.android.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PresignedUrl : Serializable {

    var url: String? = null
    @SerializedName("url_fields")
    var urlFields: UrlFields? = null

}

class UrlFields : Serializable {
    var key: String? = null
    @SerializedName("success_action_status")
    var successActionStatus: String? = null
    var acl: String? = null
    var policy: String? = null
    @SerializedName("x-amz-credential")
    var xAmzCredential: String? = null
    @SerializedName("x-amz-algorithm")
    var xAmzAlgorithm: String? = null
    @SerializedName("x-amz-date")
    var xAmzDate: String? = null
    @SerializedName("x-amz-signature")
    var xAmzSignature: String? = null
}
