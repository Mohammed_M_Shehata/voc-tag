package com.voctag.android.model

import androidx.room.Ignore
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import org.json.JSONObject

class Banner @Ignore constructor(jsonObject: JSONObject) {

    companion object {

        fun decode(json: String): Banner {
            // Deserialization
            return Gson().fromJson(json, Banner::class.java)
        }

        fun createArray(jsonObject: String): ArrayList<Banner> {
            val gson = Gson()
            val type = object : TypeToken<ArrayList<Banner>>() {}.type
            return gson.fromJson(jsonObject, type)
        }
    }

    @SerializedName("id")
    var id: Int = 0

    @SerializedName("banner_type")
    lateinit var type: BannerType

    @SerializedName("image_url")
    lateinit var imageUrl: String

    @SerializedName("extras")
    lateinit var extras: Extras


    inner class Extras {

        @SerializedName("client_slug")
        lateinit var clientSlug: String

        @SerializedName("voc_id")
        lateinit var vocId: String

        @SerializedName("web_url")
        lateinit var webUrl: String

        @SerializedName("webview_url")
        lateinit var webviewUrl: String

    }

    enum class BannerType {
        @SerializedName("0")
        CLIENT,

        @SerializedName("1")
        CONFERENCE,

        @SerializedName("2")
        VOC,

        @SerializedName("3")
        WEB,

        @SerializedName("4")
        WEBVIEW
    }

}
