package com.voctag.android.model

data class IntroScreen(val textResId: Int, val imageResId: Int)