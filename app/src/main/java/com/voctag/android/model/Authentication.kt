package com.voctag.android.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.voctag.android.helper.VoctagKeys
import org.json.JSONObject

class Authentication(jsonObject: JSONObject) {

    companion object {
        fun decode(json: String): Authentication {
            // Deserialization
            return Gson().fromJson(json, Authentication::class.java)
        }
    }

    @SerializedName("user_id")
    var userID = 0
    @SerializedName("auth_token")
    var authToken: String? = null
    @SerializedName("refresh_token")
    var refreshToken: String? = null
    var type: String? = null
    @SerializedName("expires_at")
    var expiresAt: Long = 0
    var nickname: String? = null
    @SerializedName("is_new_user")
    var isNewUser: Boolean = false

    init {
        userID = jsonObject.getInt(VoctagKeys.AUTHENTICATION_USER_ID)
        authToken = jsonObject.getString(VoctagKeys.AUTHENTICATION_AUTH_TOKEN)
        refreshToken = jsonObject.getString(VoctagKeys.AUTHENTICATION_REFRESH_TOKEN)
        expiresAt = jsonObject.optLong(VoctagKeys.AUTHENTICATION_EXPIRES_AT, 0)
        isNewUser = jsonObject.optBoolean(VoctagKeys.AUTHENTICATION_IS_NEW_USER, false)
        nickname = jsonObject.optString(VoctagKeys.AUTHENTICATION_NICKNAME, "")
        type = jsonObject.optString(VoctagKeys.AUTHENTICATION_TYPE, "")

    }

    fun encode(): String {
        // Serialization
        return Gson().toJson(this)
    }
}
