package com.voctag.android.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Settings : Serializable {

    var notifications: NotificationsSettings = NotificationsSettings()

    class NotificationsSettings : Serializable {
        var speak: Boolean = false
        var podcast: Boolean = false
        @SerializedName("open_client")
        var openClient: Boolean = false
        var reply: Boolean = false
        @SerializedName("reply_followed")
        var replyFollowed: Boolean = false
        @SerializedName("reply_voted")
        var replyVoted: Boolean = false
        @SerializedName("user_published")
        var userPublished: Boolean = false
        @SerializedName("user_published_private")
        var userPublishedPrivate: Boolean = false
    }
}