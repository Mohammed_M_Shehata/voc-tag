package com.voctag.android.model

import com.google.gson.annotations.SerializedName
import com.voctag.android.helper.Utils
import java.io.Serializable

class PaymentInfo : Serializable {

    companion object {
        const val PAYMENT_STATUS_ANSWERED = "answered"
        const val PAYMENT_STATUS_TIMEOUT = "timed_out"
        const val PAYMENT_STATUS_WAITING = "waiting"

        fun isQuestionStillActive(questionStatus: String?): Boolean {
            return questionStatus != null && questionStatus == PAYMENT_STATUS_WAITING
        }
    }

    @SerializedName("payment_status")
    lateinit var paymentStatus: String
    @SerializedName("charged?")
    var charged: Boolean = false
    @SerializedName("remaining_to_block")
    var remaningToBlock: Int = 0
    @SerializedName("client_earned")
    var clientEarned: String = "00.00"
    val clientEarnedFormatted: String
        get() = Utils.formatPriceLocale(clientEarned.toFloat())

}