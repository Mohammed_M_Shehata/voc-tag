package com.voctag.android.model

import android.text.Spannable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.voctag.android.R
import com.voctag.android.helper.LanguageHelper
import com.voctag.android.helper.LoggingHelper
import com.voctag.android.helper.Utils
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.ui.VoctagApp
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@Entity
class Voc : Serializable {

    companion object {

        fun decode(json: String): Voc {
            // Deserialization
            return Gson().fromJson(json, Voc::class.java)
        }

        fun createArray(jsonArray: JSONArray): ArrayList<Voc> {
            val vocs = arrayListOf<Voc>()
            for (i in 0 until jsonArray.length()) {
                vocs.add(Voc(jsonArray.get(i) as JSONObject))
            }
            return vocs
        }
    }

    @Ignore
    private val VOC_DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

    @PrimaryKey(autoGenerate = true)
    var id = 0
    var vocID = 0
    var userNickname: String? = null
    var userID = 0
    var repliesCount = 0
    var title: String? = null
    var playCount = 0
    var audioURL: String? = null
    var externalAudioURL: String? = null
    var duration = 0
    var webURL = ""
    var city: String? = null
    var score = 0
    var transcript = ""

    var parentID: Int? = null
    var voted = 0
    var secret = false
    var secretRepliesAllowed = false
    var following = false
    @Ignore
    var replies = arrayListOf<Voc>()
    var repliesFetched = false

    var createdAt = Date()

    var createdByClientOwner = false
    @Ignore
    var parentClient: Client? = null
    @Ignore
    var paymentInfo: PaymentInfo? = null
    var clientName: String? = null

    var newRepliesCount: Int = 0
    var slug: String? = null
    var userClientSlug: String? = null
    var userClientThemeColor: String? = null
    var userClientLogoMedium: String? = null
    var vocClientMember: Boolean = false
    var isClientReply: Boolean = false
    var replyCreatedAt = Date()
    var replyCity: String = ""
    var clientUserNickname: String? = null

    // it is the foreign key to the client in database
    var clientSlug: String? = null

    @SerializedName("is_podcast")
    var isPodcast: Boolean = false

    // used by Room
    constructor()

    @Ignore
    constructor(vocID: Int) {
        this.vocID = vocID
    }

    @Ignore
    constructor(vocID: Int, audioURL: String) {
        this.vocID = vocID
        this.audioURL = audioURL
    }

    @Ignore
    constructor(jsonObject: JSONObject) {
        this.vocID = jsonObject.getInt(VoctagKeys.VOC_ID)
        this.userID = jsonObject.getInt(VoctagKeys.VOC_USER_ID)
        this.repliesCount = jsonObject.getInt(VoctagKeys.VOC_REPLIES_COUNT)
        this.playCount = jsonObject.getInt(VoctagKeys.VOC_PLAY_COUNT)
        this.duration = (jsonObject.getDouble(VoctagKeys.VOC_DURATION) * 1000).toInt()
        this.webURL = jsonObject.getString(VoctagKeys.VOC_WEB_URL)
        this.score = jsonObject.getInt(VoctagKeys.VOC_SCORE)
        this.transcript = jsonObject.getString(VoctagKeys.VOC_TRANSCRIPT)

        if (jsonObject.has(VoctagKeys.VOC_SLUG)) {
            this.slug = jsonObject.optString(VoctagKeys.VOC_SLUG)
        }
        if (jsonObject.has(VoctagKeys.PAYMENT_INFO)) {
            paymentInfo = Gson().fromJson(jsonObject.getJSONObject(VoctagKeys.PAYMENT_INFO).toString(), PaymentInfo::class.java)
        }
        if (jsonObject.has(VoctagKeys.VOC_CLIENT_MEMBER)) {
            vocClientMember = jsonObject.getBoolean(VoctagKeys.VOC_CLIENT_MEMBER)
        }
        //those parameters are optional
        if (!jsonObject.isNull(VoctagKeys.VOC_AUDIO_URL)) this.audioURL = jsonObject.getString(VoctagKeys.VOC_AUDIO_URL)
        if (!jsonObject.isNull(VoctagKeys.VOC_EXTERNAL_AUDIO_URL)) this.externalAudioURL = jsonObject.getString(VoctagKeys.VOC_EXTERNAL_AUDIO_URL)

        if (!jsonObject.isNull(VoctagKeys.VOC_TITLE)) this.title = jsonObject.getString(VoctagKeys.VOC_TITLE)
        if (!jsonObject.isNull(VoctagKeys.VOC_CITY)) this.city = jsonObject.getString(VoctagKeys.VOC_CITY)
        if (!jsonObject.isNull(VoctagKeys.VOC_PARENT_ID)) this.parentID = jsonObject.getInt(VoctagKeys.VOC_PARENT_ID)
        if (!jsonObject.isNull(VoctagKeys.VOC_VOTED)) this.voted = jsonObject.getInt(VoctagKeys.VOC_VOTED)
        if (!jsonObject.isNull(VoctagKeys.VOC_SECRET)) this.secret = jsonObject.getBoolean(VoctagKeys.VOC_SECRET)
        if (!jsonObject.isNull(VoctagKeys.VOC_SECRET_REPLIES_ALLOWED)) this.secretRepliesAllowed = jsonObject.getBoolean(VoctagKeys.VOC_SECRET_REPLIES_ALLOWED)
        if (!jsonObject.isNull(VoctagKeys.VOC_FOLLOWING)) this.following = jsonObject.getBoolean(VoctagKeys.VOC_FOLLOWING)


        if (!jsonObject.isNull(VoctagKeys.VOC_REPLIES)) {
            this.replies = Voc.createArray(jsonObject.getJSONArray(VoctagKeys.VOC_REPLIES))
            repliesFetched = true
        }

        if (!jsonObject.isNull(VoctagKeys.VOC_USER_NICKNAME)) this.userNickname = jsonObject.getString(VoctagKeys.VOC_USER_NICKNAME)
        if (!jsonObject.isNull(VoctagKeys.VOC_CREATED_BY_CLIENT_OWNER)) this.createdByClientOwner = jsonObject.getBoolean(VoctagKeys.VOC_CREATED_BY_CLIENT_OWNER)
        if (!jsonObject.isNull(VoctagKeys.VOC_PARENT_CLIENT)) {
            this.parentClient = Client(jsonObject.getJSONObject(VoctagKeys.VOC_PARENT_CLIENT))
        } else if (!jsonObject.isNull(VoctagKeys.VOC_CLIENT_SLUG) && !jsonObject.isNull(VoctagKeys.VOC_CLIENT_THEMECOLOR)) {
            this.parentClient = Client(jsonObject.getString(VoctagKeys.VOC_CLIENT_SLUG), jsonObject.getString(VoctagKeys.VOC_CLIENT_THEMECOLOR))

            if (!jsonObject.isNull(VoctagKeys.VOC_CLIENT_SMALL_LOGO)) {
                parentClient?.smallIconURL = jsonObject.getString(VoctagKeys.VOC_CLIENT_SMALL_LOGO)
            }
            if (!jsonObject.isNull(VoctagKeys.VOC_CLIENT_MEDIUM_LOGO)) {
                parentClient?.mediumIconURL = jsonObject.getString(VoctagKeys.VOC_CLIENT_MEDIUM_LOGO)
            }
        }

        if ((!jsonObject.isNull(VoctagKeys.VOC_CLIENT_NAME))) {
            clientName = jsonObject.getString(VoctagKeys.VOC_CLIENT_NAME)
        }

        if (!jsonObject.isNull(VoctagKeys.VOC_USER_CLIENT_SLUG)) {
            this.userClientSlug = jsonObject.optString(VoctagKeys.VOC_USER_CLIENT_SLUG)
        }

        if (!jsonObject.isNull(VoctagKeys.VOC_CLIENT_USER_NICKNAME)) {
            this.clientUserNickname = jsonObject.optString(VoctagKeys.VOC_CLIENT_USER_NICKNAME)
        }

        if (!jsonObject.isNull(VoctagKeys.VOC_USER_CLIENT_THEME_COLOR)) {
            this.userClientThemeColor = jsonObject.optString(VoctagKeys.VOC_USER_CLIENT_THEME_COLOR)
        }

        if (!jsonObject.isNull(VoctagKeys.VOC_USER_CLIENT_LOGO_MEDIUM)) {
            this.userClientLogoMedium = jsonObject.optString(VoctagKeys.VOC_USER_CLIENT_LOGO_MEDIUM)
        }

        if (jsonObject.has(VoctagKeys.NEW_VOCS_COUNT)) {
            this.newRepliesCount = jsonObject.getInt(VoctagKeys.NEW_VOCS_COUNT)
        }

        if (jsonObject.has(VoctagKeys.VOC_CLIENT_IS_TOP_MENTOR) && !jsonObject.isNull(VoctagKeys.VOC_CLIENT_IS_TOP_MENTOR)) {
            parentClient?.isTopMentor = jsonObject.getBoolean(VoctagKeys.VOC_CLIENT_IS_TOP_MENTOR)
        }

        // Voc date format:
        // 2016-10-25T08:42:46.293Z
        val format = SimpleDateFormat(VOC_DATEFORMAT, Locale.getDefault())
        format.timeZone = TimeZone.getTimeZone("UTC-1")
        try {
            this.createdAt = format.parse(jsonObject.getString(VoctagKeys.VOC_CREATED_AT))
            if (!jsonObject.isNull(VoctagKeys.VOC_REPLY_CREATED_AT))
                this.replyCreatedAt = format.parse(jsonObject.getString(VoctagKeys.VOC_REPLY_CREATED_AT))
        } catch (e: ParseException) {
            LoggingHelper.logErrorForClass(Voc::class.java, e.localizedMessage)
        }

        if (!jsonObject.isNull(VoctagKeys.VOC_IS_CLIENT_REPLY)) this.isClientReply = jsonObject.getBoolean(VoctagKeys.VOC_IS_CLIENT_REPLY)
        if (!jsonObject.isNull(VoctagKeys.VOC_REPLY_CITY)) this.replyCity = jsonObject.getString(VoctagKeys.VOC_REPLY_CITY)

        if (jsonObject.has(VoctagKeys.IS_PODCAST)) isPodcast = jsonObject.getBoolean(VoctagKeys.IS_PODCAST)
    }

    fun encode(): String {
        // Serialization
        return Gson().toJson(this)
    }

    // MARK: - ViewModel

    val didVote: Boolean
        get() {
            return voted != 0
        }

    val didVoteUp: Boolean
        get() {
            return voted == 1
        }

    val vocAudioURL: String?
        get() {
            if (externalAudioURL != null) {
                return externalAudioURL!!
            } else {
                return audioURL
            }
        }

    val playsString: String
        get() {
            return "${playCount} Plays"
        }

    val repliesString: String
        get() {
            repliesCount = if (repliesFetched) replies.size else repliesCount

            return if (repliesCount == 1) {
                "${repliesCount} ${VoctagApp.getTranslation(R.string.reply_title)}"
            } else {
                "${repliesCount} ${VoctagApp.getTranslation(R.string.replies_title)}"
            }
        }

    val titleString: String
        get() {
            return title ?: ""
        }

    val shareTitleString: String
        get() {
            return if (titleString.isEmpty()) {
                "Audio"
            } else {
                titleString
            }
        }

    val scoreString: String
        get() {
            return "${score}"
        }

    val locationString: String
        get() {
            return if (city.isNullOrEmpty() || city.equals("Limbo", true)) return "" else city!!
        }

    val replyLocationString: String
        get() {
            return if (replyCity.isNullOrEmpty() || replyCity.equals("Limbo", true)) return "" else replyCity
        }

    val transcriptString: String
        get() {
            //Erster Buchstabe soll groß sein
            return if (transcript.isEmpty()) {
                transcript
            } else {
                transcript.substring(0, 1).toUpperCase() + transcript.substring(1)
            }
        }

    val shareWebURL: String
        get() {
            return if (webURL.isNotEmpty()) {
                val lang = LanguageHelper.getCurrentLanguageCode()
                val components = webURL.split(".de")
                components[0] + ".de/" + lang + components[1]
            } else {
                ""
            }
        }

    val dateString: String
        get() {
            return formatDate(createdAt)
        }
    val replyDateString: String
        get() {
            return formatDate(replyCreatedAt)
        }

    private fun formatDate(createdAt: Date): String {
        val now = Calendar.getInstance().time
        val str = VoctagApp.getTranslation(R.string.date_before)
        val diff = now.time - createdAt.time

        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        val days = hours / 24
        val months = days / 31
        val years = days / 365

        if (years > 0) {
            return str.replace("%%", "${years}Y")
        }

        if (months > 0) {
            return str.replace("%%", "${months}M")
        }

        if (days > 0) {
            return str.replace("%%", "${days}d")
        }

        if (hours > 0) {
            return str.replace("%%", "${hours}h")
        }

        if (minutes > 0) {
            return str.replace("%%", "${minutes}m")
        }

        if (seconds > 0) {
            return str.replace("%%", "${seconds}s")
        }

        return str.replace("%%", "<1s")
    }

    val dateLocationString: String
        get() {
            return "$dateString ${if (locationString.isNotEmpty()) "in $locationString" else ""}"
        }

    val replyDateLocationString: String
        get() {
            return "${replyDateString} ${if (replyLocationString.isNotEmpty()) "in $replyLocationString" else ""}"
        }

    val attributedTranscript: Spannable
        get() {
            val source = "<span style=\"font-family: 'HelveticaNeue'\">%s</span>".format(this.transcript)

            return Utils.getTextFromRawText(source) as Spannable
        }

    fun voteUp(up: Boolean) {
        if (up) {
            voted = 1
            score += 1
        } else {
            voted = -1
            score -= 1
        }
    }

    fun isNotOpenFeed(): Boolean {
        return parentClient != null && parentClient?.isNotOpenFeed()!!
    }

    override fun hashCode(): Int {
        return vocID
    }

    override fun equals(other: Any?): Boolean {
        if (other is Voc) {
            return this.vocID == other.vocID
        }
        return false
    }

    fun isMain(): Boolean = vocID < 0

}
