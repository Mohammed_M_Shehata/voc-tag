package com.voctag.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Error {

    public static final String ERROR_CODE_FACEBOOK = "facebook";
    public static final String ERROR_CODE_INVALID = "invalid";
    public static final String ERROR_CODE_TAKEN = "taken";
    public static final String ERROR_ATTRIBUTE_EMAIL = "email";
    public static final String ERROR_ATTRIBUTE_FACEBOOK_EMAIL = "facebook_email";
    public static final String ERROR_ATTRIBUTE_FACEBOOK_TOKEN = "facebook_token";
    public static final String ERROR_ATTRIBUTE_TOKEN = "token";

    @SerializedName("attribute")
    @Expose
    private String attribute;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
