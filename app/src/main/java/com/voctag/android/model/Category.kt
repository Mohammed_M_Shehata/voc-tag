package com.voctag.android.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.util.*

class Category {
    lateinit var name: String

    @SerializedName("name_translations")
    lateinit var nameTranslations: CategoryTranslation

    fun getNameTranslation(): String {
        if (Locale.getDefault().language.equals(Locale.ENGLISH.language)) {
            return nameTranslations.en
        }

        return nameTranslations.de
    }


    class CategoryTranslation {
        lateinit var en: String
        lateinit var de: String
    }

    companion object {
        fun createArray(jsonObject: String): ArrayList<Category> {
            val gson = Gson()
            val type = object : TypeToken<ArrayList<Category>>() {

            }.type
            var categories: ArrayList<Category> = gson.fromJson(jsonObject, type)
            return categories
        }
    }
}