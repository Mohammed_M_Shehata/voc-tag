package com.voctag.android.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.voctag.android.helper.Utils
import com.voctag.android.helper.VoctagKeys
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable
import kotlin.math.absoluteValue

@Entity
class Client : Serializable {

    companion object {

        fun decode(json: String): Client {
            // Deserialization
            return Gson().fromJson(json, Client::class.java)
        }

        fun createArray(jsonArray: JSONArray): ArrayList<Client> {
            val clients = arrayListOf<Client>()
            for (i in 0 until jsonArray.length()) {
                clients.add(Client(jsonArray.get(i) as JSONObject))
            }
            return clients
        }
    }

    // used by Room
    constructor()

    @PrimaryKey
    var slug = ""
    var name = ""
    var description = ""
    var memberships_count = 0
    var smallIconURL = ""
    var mediumIconURL = ""
    @ColumnInfo(name = "client_audio_url")
    var audioURL = ""
    var themeColor = ""
    @ColumnInfo(name = "client_following")
    var following = false
    var vocsCount = 0

    var isClosed = false
    var hasAccess = false
    var salesURL = ""

    var mentorFor: String? = null
    var isOfficial = false

    var secretVocPrice: String = "00.00"
    var isSecretVocEnabled: Boolean = false
    var newVocsCount: Int = 0
    var isTopMentor: Boolean = false
    var mainVocId: Int = 0
    var isConference: Boolean = false
    var webViewLink: String? = null

    @Ignore
    constructor(clientSlug: String, themeColor: String) {
        this.slug = clientSlug
        this.themeColor = themeColor
    }

    @Ignore
    constructor(clientSlug: String) {
        this.slug = clientSlug
    }

    @Ignore
    constructor(jsonObject: JSONObject) {
        this.slug = jsonObject.getString(VoctagKeys.CLIENT_SLUG)
        this.name = jsonObject.getString(VoctagKeys.CLIENT_NAME)
        this.description = jsonObject.getString(VoctagKeys.CLIENT_DESCRIPTION)
        this.memberships_count = jsonObject.getInt(VoctagKeys.CLIENT_MEMBERSHIPS_COUNT)
        this.themeColor = jsonObject.getString(VoctagKeys.CLIENT_THEMECOLOR)
        this.vocsCount = jsonObject.getInt(VoctagKeys.CLIENT_VOCS_COUNT)

        if (jsonObject.has(VoctagKeys.CLIENT_IS_CONFERENCE)) {
            isConference = jsonObject.getBoolean(VoctagKeys.CLIENT_IS_CONFERENCE)
        }
        if (jsonObject.has(VoctagKeys.CLIENT_WEB_VIEW_LINK)) {
            webViewLink = jsonObject.getString(VoctagKeys.CLIENT_WEB_VIEW_LINK)
        }
        if (jsonObject.has(VoctagKeys.CLIENT_IS_TOP_MENTOR) && !jsonObject.isNull(VoctagKeys.CLIENT_IS_TOP_MENTOR)) {
            this.isTopMentor = jsonObject.getBoolean(VoctagKeys.CLIENT_IS_TOP_MENTOR)
        }
        if (jsonObject.has(VoctagKeys.SECRET_VOC_PRICE)) {

            this.secretVocPrice = Utils.formatPriceLocale(jsonObject.getDouble(VoctagKeys.SECRET_VOC_PRICE).toFloat())
        }
        if (jsonObject.has(VoctagKeys.IS_SECRET_VOC_ENABLED)) {
            this.isSecretVocEnabled = jsonObject.getBoolean(VoctagKeys.IS_SECRET_VOC_ENABLED)
        }

        if (jsonObject.has(VoctagKeys.CLIENT_FOLLOWING)) {
            this.following = jsonObject.getBoolean(VoctagKeys.CLIENT_FOLLOWING)
        }

        if (jsonObject.has(VoctagKeys.CLIENT_AUDIO_URL)) {
            val audioURL = jsonObject.getString(VoctagKeys.CLIENT_AUDIO_URL)
            if (audioURL.isNotEmpty() && audioURL != "null") {
                this.audioURL = audioURL
            }
        }

        //get image urls
        val urls = jsonObject.get(VoctagKeys.CLIENT_ICON_URLS) as JSONObject
        if (urls.has(VoctagKeys.CLIENT_ICON_SMALL) && urls.has(VoctagKeys.CLIENT_ICON_MEDIUM)) {
            this.smallIconURL = urls.getString(VoctagKeys.CLIENT_ICON_SMALL)
            this.mediumIconURL = urls.getString(VoctagKeys.CLIENT_ICON_MEDIUM)
        }

        if (jsonObject.has(VoctagKeys.CLIENT_IS_CLOSED)) {
            this.isClosed = jsonObject.getBoolean(VoctagKeys.CLIENT_IS_CLOSED)
        }

        if (jsonObject.has(VoctagKeys.CLIENT_HAS_ACCESS)) {
            this.hasAccess = jsonObject.getBoolean(VoctagKeys.CLIENT_HAS_ACCESS)
        }

        if (jsonObject.has(VoctagKeys.CLIENT_SALES_LINK)) {
            val salesLink = jsonObject.getString(VoctagKeys.CLIENT_SALES_LINK)
            if (salesLink.isNotEmpty()) {
                this.salesURL = salesLink
            }
        }

        if (jsonObject.has(VoctagKeys.CLIENT_MENTOR_FOR)) {
            this.mentorFor = jsonObject.getString(VoctagKeys.CLIENT_MENTOR_FOR)
        }

        if (jsonObject.has(VoctagKeys.CLIENT_IS_OFFICIAL)) {
            this.isOfficial = jsonObject.getBoolean(VoctagKeys.CLIENT_IS_OFFICIAL)
        }

        if (jsonObject.has(VoctagKeys.NEW_VOCS_COUNT)) {
            this.newVocsCount = jsonObject.getInt(VoctagKeys.NEW_VOCS_COUNT)
        }

        mainVocId = slug.hashCode().absoluteValue * -1
    }

    fun isNotOpenFeed() = slug != VoctagKeys.COMMUNITY_SLUG

    fun encode(): String {
        // Serialization
        return Gson().toJson(this)
    }

    // MARK: - ViewModel

    val membershipsCountString: String
        get() {
            return "$memberships_count"
        }

    val themeColorString: String
        get() {
            return "#$themeColor"
        }

    val mainAudioFakeVoc: Voc?
        get() {
            return if (this.audioURL.isNotEmpty()) {
                val voc = Voc(mainVocId, this.audioURL)
                voc.parentClient = this
                voc
            } else {
                null
            }
        }

    override fun hashCode(): Int {
        return slug.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return other is Client && this.slug == other.slug
    }
}