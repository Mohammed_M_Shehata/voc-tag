package com.voctag.android.model

import com.google.gson.Gson
import com.voctag.android.helper.VoctagKeys
import org.json.JSONArray
import org.json.JSONObject

class User {

    companion object {
        fun decode(json: String): User {
            // Deserialization
            return Gson().fromJson(json, User::class.java)
        }
    }

    var id = 0
    var acceptedTerms = false
    var points = 0
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var nickname: String? = null
    var vocs = arrayListOf<Voc>()
    var replies = arrayListOf<Voc>()
    var favorites = arrayListOf<Voc>()
    var owningChannels = arrayListOf<String>()

    var settings: Settings? = null

    constructor(userID: Int) {
        this.id = userID
    }

    constructor(userID: Int, nickname: String?) {
        this.id = userID
        this.nickname = nickname
    }

    constructor(jsonObject: JSONObject) {
        this.id = jsonObject.getInt(VoctagKeys.USER_ID)
        this.acceptedTerms = jsonObject.getBoolean(VoctagKeys.USER_ACCEPTED_TERMS)
        this.points = jsonObject.getInt(VoctagKeys.USER_POINTS)

        if (jsonObject.has(VoctagKeys.USER_VOCS)) {
            this.vocs = Voc.createArray(jsonObject.getJSONArray(VoctagKeys.USER_VOCS))
        }
        if (jsonObject.has(VoctagKeys.USER_REPLIES)) {
            this.replies = Voc.createArray(jsonObject.getJSONArray(VoctagKeys.USER_REPLIES))
        }
        if (jsonObject.has(VoctagKeys.USER_FAVORITES)) {
            this.favorites = Voc.createArray(jsonObject.getJSONArray(VoctagKeys.USER_FAVORITES))
        }

        if (jsonObject.has(VoctagKeys.USER_OWNING_CHANNELS)) {
            this.owningChannels = createOwningChannelsArray(jsonObject.getJSONArray(VoctagKeys.USER_OWNING_CHANNELS))
        }

        if (jsonObject.has(VoctagKeys.USER_NICKNAME)) {
            val nickname = jsonObject.getString(VoctagKeys.USER_NICKNAME)
            if (!nickname.isNullOrEmpty()) {
                this.nickname = nickname
            }
        }
        if (jsonObject.has(VoctagKeys.SETTINGS)) {
            settings = Gson().fromJson(jsonObject.getString(VoctagKeys.SETTINGS), Settings::class.java)
        }
    }

    fun encode(): String {
        // Serialization
        return Gson().toJson(this)
    }

    private fun createOwningChannelsArray(jsonArray: JSONArray): ArrayList<String> {
        val arr = arrayListOf<String>()
        //if (jsonArray.length() != 0) {
        for (i in 0 until jsonArray.length()) {
            arr.add(jsonArray.get(i) as String)
        }
        //}
        return arr
    }

    fun isOwnerOf(slug: String): Boolean {
        return owningChannels.contains(slug)
    }

    // MARK: - ViewModel

    val pointsString: String
        get() {
            return "${points}"
        }

    val isOfficial: Boolean
        get() {
            return !this.owningChannels.isEmpty()
        }

    val hasLocation: Boolean
        get() {
            return latitude != 0.0 && longitude != 0.0
        }
}
