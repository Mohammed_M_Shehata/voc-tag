package com.voctag.android.model

import com.google.gson.Gson

class Filter {

    companion object {
        fun decode(json: String): Filter {
            // Deserialization
            return Gson().fromJson(json, Filter::class.java)
        }
    }

    var orderType = OrderType.Newest

    enum class OrderType(var rawValue: String) {
        Newest("newest"), MostPlayed("most_played"), MostReplies("most_replies"), MostUpVotes("most_up_votes")
    }

    fun encode(): String {
        // Serialization
        return Gson().toJson(this)
    }

}
