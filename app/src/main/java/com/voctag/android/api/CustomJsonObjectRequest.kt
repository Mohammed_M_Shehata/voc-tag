package com.voctag.android.api

import com.android.volley.NetworkResponse
import com.android.volley.ParseError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.voctag.android.helper.VoctagKeys
import org.json.JSONObject
import java.nio.charset.Charset
import java.util.*

/**
 * Created by jan.maron on 23.10.17.
 */

class CustomJsonObjectRequest(private val requestType: RequestHelper.RequestType, url: String, jsonBody: JSONObject, listener: Response.Listener<JSONObject?>, errorListener: Response.ErrorListener) : JsonObjectRequest(requestType.method, url, jsonBody, listener, errorListener) {

    override fun getHeaders(): MutableMap<String, String> {
        val applicationTypeString = "application/json"
        val header = HashMap<String, String>()
        header.put(VoctagKeys.ACCEPT, applicationTypeString)

        if (method == Method.POST) {
            header.put(VoctagKeys.CONTENT_TYPE, applicationTypeString)
        }

        when (requestType) {
            // No auth required
            RequestHelper.RequestType.refresh, RequestHelper.RequestType.register -> {
            }
            else -> {
                val authToken = AuthenticationHelper.shared.getAuthToken()
                if (!authToken.isNullOrEmpty()) {
                    header.put(VoctagKeys.AUTHORIZATION, authToken)
                }
            }
        }
        return header
    }

    override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject> {
        return try {
            val jsonString = String(response?.data
                    ?: ByteArray(0), Charset.forName(HttpHeaderParser.parseCharset(response?.headers)))

            var result: JSONObject? = null
            if (jsonString.isNotEmpty()) {
                result = JSONObject(jsonString)
            }

            Response.success(result, HttpHeaderParser.parseCacheHeaders(response))
        } catch (e: Exception) {
            Response.error(ParseError(e))
        }
    }
}
