package com.voctag.android.api.firebase

import android.content.Context
import android.os.Bundle
import com.facebook.appevents.AppEventsConstants
import com.facebook.appevents.AppEventsLogger
import com.google.firebase.analytics.FirebaseAnalytics


class AnalyticsLoggerUtils {

    companion object {
        const val AUTH_METHOD_EMAIL = "email"
        const val GET_STARTED_SCREEN = "getting_started"
        const val SETTINGS_SCREEN = "settings"
        private const val LOGIN_SCREEN = "login"
        private const val CONTENT_TYPE_CLIENT = "client"

        private const val PARAM_SEARCH_TEXT = "search_text"
        private const val PARAM_CLIENT_SLUG = "client_slug"
        private const val PARAM_VOC_ID = "voc_id"
        private const val PARAM_SCREEN = "screen"
        private const val PARAM_NOTIFICATION_TYPE = "type"
        private const val PARAM_NOTIFICATION_MESSAGE = "message"

        private const val EVENT_FORGET_PASSWORD = "forget_password"
        private const val EVENT_PRIVACY_POLICY = "privacy"
        private const val EVENT_TERMS_OF_SERVICE = "terms"
        private const val EVENT_MENTOR_SEARCH = "mentor_search"
        private const val EVENT_MENTOR_FOLLOW = "mentor_follow"
        private const val EVENT_MENTOR_UNFOLLOW = "mentor_unfollow"
        private const val EVENT_MENTOR_PLAY_MAIN_VOC = "mentor_play_main_audio"
        private const val EVENT_MENTOR_PUBLISH_VOC = "mentor_publish_voc"
        private const val EVENT_VOC_PLAY = "voc_play"
        private const val EVENT_VOC_SHARE = "voc_share"
        private const val EVENT_VOC_VOTE_UP = "voc_vote_up"
        private const val EVENT_VOC_VOTE_DOWN = "voc_vote_down"
        private const val EVENT_VOC_LONG_TAB = "voc_long_tab"
        private const val EVENT_NOTIFICATION_USER_OPEN = "notification_user_open"
        private const val EVENT_APP_OPEN = "app_open"

        private lateinit var firebaseAnalytics: FirebaseAnalytics
        private lateinit var logger: AppEventsLogger

        private var instance: AnalyticsLoggerUtils? = null

        fun getInstance(): AnalyticsLoggerUtils {
            if (instance == null) {
                instance = AnalyticsLoggerUtils()
            }
            return instance!!
        }
    }

    private fun logFirebase(event: String, params: HashMap<String, String>?) {
        var bundle: Bundle? = null
        if (params != null) {
            bundle = Bundle()
            params.forEach { bundle.putString(it.key, it.value) }
        }

        firebaseAnalytics.logEvent(event, bundle)
    }

    private fun logFacebook(event: String, params: HashMap<String, String>) {
        val bundle = Bundle()
        params.forEach { bundle.putString(it.key, it.value) }
        logger.logEvent(event, bundle)
    }

    fun init(context: Context) {
        firebaseAnalytics = FirebaseAnalytics.getInstance(context)
        logger = AppEventsLogger.newLogger(context)
    }

    fun logSignUp(method: String) {
        logFirebase(FirebaseAnalytics.Event.SIGN_UP, hashMapOf(FirebaseAnalytics.Param.METHOD to method))
        logFacebook(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, hashMapOf(FirebaseAnalytics.Param.METHOD to method))
    }

    fun logLogin(method: String) {
        logFirebase(FirebaseAnalytics.Event.LOGIN, hashMapOf(FirebaseAnalytics.Param.METHOD to method))
        logFacebook(FirebaseAnalytics.Event.LOGIN, hashMapOf(FirebaseAnalytics.Param.METHOD to method))
    }

    private fun logButtonClicked(event: String, screenName: String) {
        val params = hashMapOf(PARAM_SCREEN to screenName)
        logFirebase(event, params)
        logFacebook(event, params)
    }

    fun logForgetPassword() {
        logButtonClicked(EVENT_FORGET_PASSWORD, LOGIN_SCREEN)
    }

    fun logPrivacyClicked(screenName: String) {
        logButtonClicked(EVENT_PRIVACY_POLICY, screenName)
    }

    fun logTermsClicked(screenName: String) {
        logButtonClicked(EVENT_TERMS_OF_SERVICE, screenName)
    }

    fun logMentorSearch(searchText: String) {
        val params = hashMapOf(PARAM_SEARCH_TEXT to searchText)

        logFacebook(EVENT_MENTOR_SEARCH, params)
        logFirebase(EVENT_MENTOR_SEARCH, params)
    }

    private fun logFollowEvents(event: String, clientSlug: String) {
        val params = hashMapOf(PARAM_CLIENT_SLUG to clientSlug)
        logFirebase(event, params)
        logFacebook(event, params)
    }

    fun logFollowMentor(clientSlug: String) {
        logFollowEvents(EVENT_MENTOR_FOLLOW, clientSlug)
    }

    fun logUnFollowMentor(clientSlug: String) {
        logFollowEvents(EVENT_MENTOR_UNFOLLOW, clientSlug)
    }

    fun logMentorShare(clientSlug: String) {
        val params = hashMapOf(FirebaseAnalytics.Param.CONTENT_TYPE to CONTENT_TYPE_CLIENT
                , FirebaseAnalytics.Param.ITEM_ID to clientSlug)

        logFirebase(FirebaseAnalytics.Event.SHARE, params)
        logFacebook(FirebaseAnalytics.Event.SHARE, params)
    }

    fun logMentorPlayMainAudio(clientSlug: String) {
        val params = hashMapOf(PARAM_CLIENT_SLUG to clientSlug)
        logFacebook(EVENT_MENTOR_PLAY_MAIN_VOC, params)
        logFirebase(EVENT_MENTOR_PLAY_MAIN_VOC, params)
    }

    private fun logVocEvents(event: String, vocId: Int, clientSlug: String) {
        val params = hashMapOf(PARAM_VOC_ID to vocId.toString(), PARAM_CLIENT_SLUG to clientSlug)
        logFacebook(event, params)
        logFirebase(event, params)
    }

    fun logMentorPublishVoc(clientSlug: String, vocId: Int) {
        logVocEvents(EVENT_MENTOR_PUBLISH_VOC, vocId, clientSlug)
    }

    fun logVocPlay(clientSlug: String, vocId: Int) {
        logVocEvents(EVENT_VOC_PLAY, vocId, clientSlug)
    }

    fun logVocShare(clientSlug: String, vocId: Int) {
        logVocEvents(EVENT_VOC_SHARE, vocId, clientSlug)
    }

    fun logVocLike(clientSlug: String, vocId: Int) {
        logVocEvents(EVENT_VOC_VOTE_UP, vocId, clientSlug)
    }

    fun logVocUnLike(clientSlug: String, vocId: Int) {
        logVocEvents(EVENT_VOC_VOTE_DOWN, vocId, clientSlug)
    }

    fun logVocLongTab(clientSlug: String, vocId: Int) {
        logVocEvents(EVENT_VOC_LONG_TAB, vocId, clientSlug)
    }

    fun logUserOpenNotification(type: String, message: String) {
        val params = hashMapOf(PARAM_NOTIFICATION_TYPE to type, PARAM_NOTIFICATION_MESSAGE to message)
        logFirebase(EVENT_NOTIFICATION_USER_OPEN, params)
        logFacebook(EVENT_NOTIFICATION_USER_OPEN, params)
    }

    fun logAppOpen() {
        logFirebase(EVENT_APP_OPEN, null)
    }
}