package com.voctag.android.api.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.voctag.android.R
import com.voctag.android.api.AuthenticationHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.LoggingHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.NotificationsRedirectActivity
import com.voctag.android.ui.VoctagApp
import com.voctag.android.ui.main.mentor.detail.conference.MentorDetailConferenceActivity
import com.voctag.android.ui.main.mentor.detail.open.MentorDetailActivity
import org.json.JSONObject

class CustomFirebaseMessagingService : FirebaseMessagingService() {

    companion object {
        private const val TAG = "FBMessagingService"

        private const val REPLY_ID = 1
        private const val REPLY_FOLLOWED_ID = 2
        private const val CLIENT_OWNER_PUBLISHED_ID = 3
        private const val USER_PUBLISHED_ID = 4
        private const val PRIVATE_QUESTION_ID = 5
        private const val REPLY_VOTED_ID = 6
        private const val OPEN_CLIENT_ID = 7
        private const val NEW_PODCAST_ID = 8
        private const val OPEN_WEBVIEW_ID = 9
        private const val OPEN_WEB_ID = 10

        private const val CHANNEL_ID = "default"
        private const val EXTRA_TITLE = "voc_title"
        private const val EXTRA_CLIENT_NAME = "client_name"
        private const val EXTRA_NICKNAME = "nickname"
        private const val EXTRA_EARNED_AMOUNT = "earned_amount"
        private const val EXTRA_CLIENT_THEME_COLOR = "client_theme_color"
        private const val EXTRA_CLIENT_DESCRIPTION = "client_description"
        private const val EXTRA_CLIENT_MAIN_AUDIO_URL = "client_main_audio_url"
        private const val EXTRA_CLIENT_LOGO_URLs = "client_logo_urls"
        private const val EXTRA_CLIENT_IS_CONFERENCE = "is_conference"
        private const val EXTRA_CONFERENCE_WEBVIEW_LINK = "webview_link"
        private const val EXTRA_VOC_ID = "voc_id"
        const val EXTRA_WEB_URL = "web_url"
        const val EXTRA_WEBVIEW_URL = "webview_url"
        const val EXTRA_CLIENT_SLUG = "client_slug"
    }

    private var notificationType: String? = null
    private var message: String = ""

    override fun onNewToken(token: String) {
        Log.d(TAG, "New Token: $token")

        // Update/save Firebase token -> on the next refresh this token will be updated in the backend
        AuthenticationHelper.shared.saveFirebaseToken(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "From: ${remoteMessage.from}")

        var notificationID = 1
        var title = "Upspeak"

        val data = remoteMessage.data

        // check if message contains a data payload
        data.isNotEmpty().let {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)

            // check if data contains "type"
            if (data.containsKey(VoctagKeys.NOTIFICATION_TYPE)) {

                // get some values if set
                val vocTitle = data[EXTRA_TITLE] ?: ""
                val clientName = data[EXTRA_CLIENT_NAME] ?: ""
                val nickname = data[EXTRA_NICKNAME] ?: ""
                val webUrl = data[EXTRA_WEB_URL] ?: ""
                val webviewUrl = data[EXTRA_WEBVIEW_URL] ?: ""

                notificationType = data[VoctagKeys.NOTIFICATION_TYPE] as String
                when (notificationType) {
                    VoctagKeys.NOTIFICATION_TYPE_REPLY -> {
                        Log.d(TAG, "onMessageReceived: ${VoctagKeys.NOTIFICATION_TYPE_REPLY}")
                        notificationID = REPLY_ID
                        message = getMessageForID(REPLY_ID, nickname, null, vocTitle)
                    }
                    VoctagKeys.NOTIFICATION_TYPE_REPLY_FOLLOWED -> {
                        Log.d(TAG, "onMessageReceived: ${VoctagKeys.NOTIFICATION_TYPE_REPLY_FOLLOWED}")
                        notificationID = REPLY_FOLLOWED_ID
                        message = getMessageForID(REPLY_FOLLOWED_ID, nickname, null, vocTitle)
                    }
                    VoctagKeys.NOTIFICATION_TYPE_CLIENT_OWNER_PUBLISHED -> {
                        Log.d(TAG, "onMessageReceived: ${VoctagKeys.NOTIFICATION_TYPE_CLIENT_OWNER_PUBLISHED}")
                        title = clientName
                        notificationID = CLIENT_OWNER_PUBLISHED_ID
                        message = getMessageForID(CLIENT_OWNER_PUBLISHED_ID, nickname, clientName, vocTitle)
                    }
                    VoctagKeys.NOTIFICATION_TYPE_PODCAST -> {
                        Log.d(TAG, "onMessageReceived: ${VoctagKeys.NOTIFICATION_TYPE_PODCAST}")
                        title = clientName
                        notificationID = NEW_PODCAST_ID
                        message = getMessageForID(NEW_PODCAST_ID, nickname, clientName, vocTitle)
                    }
                    VoctagKeys.NOTIFICATION_TYPE_USER_PUBLISHED -> {
                        Log.d(TAG, "onMessageReceived: ${VoctagKeys.NOTIFICATION_TYPE_USER_PUBLISHED}")
                        notificationID = USER_PUBLISHED_ID
                        message = getMessageForID(notificationID, nickname, null, vocTitle)
                    }
                    VoctagKeys.NOTIFICATION_TYPE_PRIVATE_QUESTION -> {
                        Log.d(TAG, "onMessageReceived: ${VoctagKeys.NOTIFICATION_TYPE_PRIVATE_QUESTION}")
                        notificationID = PRIVATE_QUESTION_ID
                        val amount = data[EXTRA_EARNED_AMOUNT] ?: ""
                        message = getMessageForID(notificationID, nickname, null, vocTitle, earnedAmount = amount)
                    }
                    VoctagKeys.NOTIFICATION_TYPE_REPLY_VOTED -> {
                        Log.d(TAG, "onMessageReceived: ${VoctagKeys.NOTIFICATION_TYPE_REPLY_VOTED}")
                        notificationID = REPLY_VOTED_ID
                        message = getMessageForID(notificationID, nickname, clientName, vocTitle)
                    }
                    VoctagKeys.NOTIFICATION_TYPE_OPEN_CLIENT -> {
                        Log.d(TAG, "onMessageReceived: ${VoctagKeys.NOTIFICATION_TYPE_OPEN_CLIENT}")
                        notificationID = OPEN_CLIENT_ID
                        title = remoteMessage.notification?.title.toString()
                        message = remoteMessage.notification?.body.toString()
                    }
                    VoctagKeys.NOTIFICATION_TYPE_OPEN_WEB -> {
                        Log.d(TAG, "onMessageReceived: ${VoctagKeys.NOTIFICATION_TYPE_OPEN_WEB}")
                        notificationID = OPEN_WEB_ID
                        title = remoteMessage.notification?.title.toString()
                        message = remoteMessage.notification?.body.toString()
                    }
                    VoctagKeys.NOTIFICATION_TYPE_OPEN_WEBVIEW -> {
                        Log.d(TAG, "onMessageReceived: ${VoctagKeys.NOTIFICATION_TYPE_OPEN_WEBVIEW}")
                        notificationID = OPEN_WEBVIEW_ID
                        title = remoteMessage.notification?.title.toString()
                        message = remoteMessage.notification?.body.toString()
                    }
                }

                // Do some more things based on notification type
                when {
                    // Client Notification with client data
                    isClientTypeNotification(notificationType) && hasClientData(data) -> {
                        val client = Client(data[EXTRA_CLIENT_SLUG]!!, data[EXTRA_CLIENT_THEME_COLOR]!!)
                        client.name = data[EXTRA_CLIENT_NAME]!!
                        client.description = data[EXTRA_CLIENT_DESCRIPTION]!!
                        client.following = true

                        if (data.containsKey(EXTRA_CLIENT_MAIN_AUDIO_URL)) {
                            val audioURL = data[EXTRA_CLIENT_MAIN_AUDIO_URL]!!
                            if (audioURL.isNotEmpty() && audioURL != "null") {
                                client.audioURL = audioURL
                            }
                        }

                        if (data.containsKey(EXTRA_CLIENT_IS_CONFERENCE)) {
                            client.isConference = data[EXTRA_CLIENT_IS_CONFERENCE]?.toBoolean()!!
                            client.webViewLink = data[EXTRA_CONFERENCE_WEBVIEW_LINK]
                        }

                        if (data.containsKey(EXTRA_CLIENT_LOGO_URLs)) {
                            try {
                                val urls = JSONObject(data[EXTRA_CLIENT_LOGO_URLs])
                                client.smallIconURL = urls.getString(VoctagKeys.CLIENT_ICON_SMALL)
                                client.mediumIconURL = urls.getString(VoctagKeys.CLIENT_ICON_MEDIUM)
                            } catch (e: Exception) {
                                LoggingHelper.logErrorForClass(CustomFirebaseMessagingService::class.java, e.localizedMessage)
                            }
                        }

                        showNotification(notificationID, title, message, null, client, webUrl, webviewUrl)
                    }

                    // Voc Notification and voc data
                    isVocTypeNotification(notificationType) && data.containsKey(EXTRA_VOC_ID) -> {
                        val voc = Voc(Integer.parseInt(data[EXTRA_VOC_ID]!!))
                        if (hasParentClient(data)) {
                            voc.parentClient = Client(data[EXTRA_CLIENT_SLUG]!!, data[EXTRA_CLIENT_THEME_COLOR]!!)
                        }
                        showNotification(notificationID, title, message, voc, null, webUrl, webviewUrl)
                    }

                    // Default Notification
                    else -> {
                        showNotification(notificationID, title, message, null, null, webUrl, webviewUrl)
                    }
                }
            }
        }
    }


    /* ----- Helper methods ------ */

    private fun hasParentClient(data: Map<String, String>): Boolean {
        return data.containsKey(EXTRA_CLIENT_SLUG) && data.containsKey(EXTRA_CLIENT_THEME_COLOR)
    }

    private fun hasClientData(data: Map<String, String>): Boolean {
        return data.containsKey(EXTRA_CLIENT_NAME) && data.containsKey(EXTRA_CLIENT_DESCRIPTION) && data.containsKey(EXTRA_CLIENT_THEME_COLOR)
    }

    private fun isVocTypeNotification(notificationType: String?): Boolean {
        return notificationType == VoctagKeys.NOTIFICATION_TYPE_REPLY
                || notificationType == VoctagKeys.NOTIFICATION_TYPE_REPLY_FOLLOWED
                || notificationType == VoctagKeys.NOTIFICATION_TYPE_REPLY_VOTED
    }

    private fun isClientTypeNotification(notificationType: String?): Boolean {
        return notificationType == VoctagKeys.NOTIFICATION_TYPE_CLIENT_OWNER_PUBLISHED
                || notificationType == VoctagKeys.NOTIFICATION_TYPE_PODCAST
                || notificationType == VoctagKeys.NOTIFICATION_TYPE_PRIVATE_QUESTION
                || notificationType == VoctagKeys.NOTIFICATION_TYPE_USER_PUBLISHED
                || notificationType == VoctagKeys.NOTIFICATION_TYPE_OPEN_CLIENT
    }

    /* ----- Methods for showing the notification ------ */

    private fun showNotification(notificationID: Int, title: String, message: String, voc: Voc?, client: Client?, webUrl: String, webviewUrl: String) {

        // check if the user has enabled the notification type. If not do not show the notification
        if (!isNotificationTypeEnabled()) {
            return
        }

        // create notification channel
        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        createNotificationChannel(manager)

        // create notification builder
        val builder = createNotificationBuilder(title, message)

        // create notification intent
        val notificationIntent = createNotificationIntent(notificationID, voc, client, webUrl, webviewUrl)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        // create/build the notification
        builder.setContentIntent(pendingIntent)
        manager.notify(System.currentTimeMillis().toInt(), builder.build())
    }

    private fun isNotificationTypeEnabled(): Boolean {
        var valid = true

        // check weather the user has enabled the notification type
        val currentUser = PersistDataHelper.shared.loadUser()
        currentUser?.settings?.notifications?.let {
            when (notificationType) {
                VoctagKeys.NOTIFICATION_TYPE_REPLY -> {
                    valid = it.reply
                }
                VoctagKeys.NOTIFICATION_TYPE_REPLY_FOLLOWED -> {
                    valid = it.replyFollowed
                }
                VoctagKeys.NOTIFICATION_TYPE_CLIENT_OWNER_PUBLISHED -> {
                    valid = it.speak
                }
                VoctagKeys.NOTIFICATION_TYPE_PODCAST -> {
                    valid = it.podcast
                }
                VoctagKeys.NOTIFICATION_TYPE_USER_PUBLISHED -> {
                    valid = it.userPublished
                }
                VoctagKeys.NOTIFICATION_TYPE_PRIVATE_QUESTION -> {
                    valid = it.userPublishedPrivate
                }
                VoctagKeys.NOTIFICATION_TYPE_REPLY_VOTED -> {
                    valid = it.replyVoted
                }
                VoctagKeys.NOTIFICATION_TYPE_OPEN_CLIENT -> {
                    valid = it.openClient
                }
            }
        }
        return valid
    }

    private fun createNotificationChannel(manager: NotificationManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = VoctagApp.getTranslation(R.string.notification_channel_description)
            manager.createNotificationChannel(channel)
        }
    }

    private fun createNotificationIntent(notificationID: Int, voc: Voc?, client: Client?, webUrl: String, webviewUrl: String): Intent {
        return Intent(this, NotificationsRedirectActivity::class.java).apply {
            putExtra(VoctagKeys.NOTIFICATION_TYPE, notificationType)
            putExtra(VoctagKeys.NOTIFICATION_MESSAGE, message)

            // add extras when voc, client, webview or web
            when {
                // extra for voc
                voc != null -> {
                    putExtra(VoctagKeys.EXTRA_VOC, voc.encode())
                }
                // extra for client
                client != null -> {
                    putExtra(VoctagKeys.EXTRA_CLIENT, client.encode())
                    putExtra(VoctagKeys.EXTRA_RELOAD_CLIENT, true)

                    when (notificationID) {
                        CLIENT_OWNER_PUBLISHED_ID, NEW_PODCAST_ID -> putExtra(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, MentorDetailActivity.SPEAKS_PAGE)
                        USER_PUBLISHED_ID -> putExtra(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, MentorDetailActivity.QUESTIONS_PAGE)
                        PRIVATE_QUESTION_ID -> putExtra(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, MentorDetailActivity.QUESTIONS_PAGE)
                        OPEN_CLIENT_ID -> {
                            if (client.isConference) {
                                putExtra(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, MentorDetailConferenceActivity.STAGE_PAGE)
                            }
                        }
                    }
                }
                // extra for webUrl
                webUrl.isNotEmpty() -> {
                    putExtra(VoctagKeys.EXTRA_WEB_URL, webUrl)
                }
                // extra for webviewUrl
                webviewUrl.isNotEmpty() -> {
                    putExtra(VoctagKeys.EXTRA_WEBVIEW_URL, webviewUrl)
                }
            }
        }
    }

    private fun createNotificationBuilder(title: String, message: String): NotificationCompat.Builder {
        return NotificationCompat.Builder(applicationContext, CHANNEL_ID)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVibrate(longArrayOf(500, 250, 500))
                .setSmallIcon(R.drawable.notification_logo)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
    }

    private fun getMessageForID(id: Int, nickname: String, clientName: String?, vocTitle: String, earnedAmount: String = ""): String {
        return when (id) {
            REPLY_FOLLOWED_ID -> getString(R.string.REPLY_FOLLOWED, nickname, vocTitle)
            CLIENT_OWNER_PUBLISHED_ID -> getString(R.string.CLIENT_OWNER_PUBLISHED, nickname, clientName!!, vocTitle)
            NEW_PODCAST_ID -> getString(R.string.POCAST_NOTIFICATION_MESSAGE, nickname, clientName!!, vocTitle)
            REPLY_ID -> getString(R.string.REPLY, nickname, vocTitle)
            USER_PUBLISHED_ID -> getString(R.string.notification_public_question_published, vocTitle)
            PRIVATE_QUESTION_ID -> getString(R.string.notification_private_question_published, earnedAmount)
            else -> getString(R.string.notification_reply_to_upvoted_post, clientName, vocTitle)
        }
    }
}
