package com.voctag.android.api

import android.content.Context
import android.util.Log
import com.koushikdutta.ion.Ion
import com.voctag.android.model.PresignedUrl
import java.io.File

object S3Manager {

    fun uploadFile(context: Context, presignedUrl: PresignedUrl, file: File, onUploadListener: OnUploadListener) {

        Ion.with(context)
                .load("POST", presignedUrl.url)
                .uploadProgressHandler { downloaded, total ->
                    Log.d("com.test", "progress")
                    if (downloaded == total) {
                        onUploadListener.onComplete()
                    } else {
                        val progress = downloaded / total.toDouble() * 100.0
                        onUploadListener.onProgress(progress.toInt())
                    }
                }
                .setMultipartParameter("key", presignedUrl.urlFields?.key.toString())
                .setMultipartParameter("success_action_status", presignedUrl.urlFields?.successActionStatus.toString())
                .setMultipartParameter("acl", presignedUrl.urlFields?.acl.toString())
                .setMultipartParameter("policy", presignedUrl.urlFields?.policy.toString())
                .setMultipartParameter("x-amz-credential", presignedUrl.urlFields?.xAmzCredential.toString())
                .setMultipartParameter("x-amz-algorithm", presignedUrl.urlFields?.xAmzAlgorithm.toString())
                .setMultipartParameter("x-amz-signature", presignedUrl.urlFields?.xAmzSignature.toString())
                .setMultipartParameter("x-amz-date", presignedUrl.urlFields?.xAmzDate.toString())
                .setMultipartFile("file", file)
                .asString()
                .setCallback { e, result ->
                    Log.d("com.test", "result> ${result}")
                    Log.d("com.test", "message> ${e?.message}")
                    if (e != null) {
                        onUploadListener.onError(e.message)
                    }
                }
    }

    interface OnUploadListener {
        fun onError(message: String?)
        fun onComplete()
        fun onProgress(percentage: Int)
    }
}