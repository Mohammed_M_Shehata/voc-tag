package com.voctag.android.api

import com.pddstudio.preferences.encrypted.EncryptedPreferences
import com.voctag.android.BuildConfig
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.ui.VoctagApp
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm

/**
 * Created by jan.maron on 28.08.17.
 */

class AuthenticationHelper {

    companion object {
        val shared = AuthenticationHelper()
    }

    fun getAuthToken(): String? {
        val encryptedPreferences = EncryptedPreferences.Builder(VoctagApp.context).withEncryptionPassword(BuildConfig.ENCRYPTION_PASSWORD).build()
        val token = encryptedPreferences.getString(VoctagKeys.AUTHENTICATION_AUTH_TOKEN_ENCRYPTION_ACCESS, VoctagKeys.EMPTY)
        return if (token.isNullOrEmpty()) {
            null
        } else {
            token
        }
    }

    fun saveAuthToken(token: String) {
        val encryptedPreferences = EncryptedPreferences.Builder(VoctagApp.context).withEncryptionPassword(BuildConfig.ENCRYPTION_PASSWORD).build()
        encryptedPreferences.edit().putString(VoctagKeys.AUTHENTICATION_AUTH_TOKEN_ENCRYPTION_ACCESS, token).apply()
    }

    fun getFirebaseToken(): String? {
        val encryptedPreferences = EncryptedPreferences.Builder(VoctagApp.context).withEncryptionPassword(BuildConfig.ENCRYPTION_PASSWORD).build()
        val token = encryptedPreferences.getString(VoctagKeys.AUTHENTICATION_FIREBASE_DEVICE_TOKEN, VoctagKeys.EMPTY)
        return if (token.isNullOrEmpty()) {
            null
        } else {
            token
        }
    }

    fun saveFirebaseToken(token: String) {
        val encryptedPreferences = EncryptedPreferences.Builder(VoctagApp.context).withEncryptionPassword(BuildConfig.ENCRYPTION_PASSWORD).build()
        encryptedPreferences.edit().putString(VoctagKeys.AUTHENTICATION_FIREBASE_DEVICE_TOKEN, token).apply()
    }

    fun encrypt(params: HashMap<String, Any>): String {
        val user = PersistDataHelper.shared.loadUser()
        if (user != null) {
            if (user.latitude != 0.0 && user.longitude != 0.0) {
                params.put(VoctagKeys.LATITUDE, user.latitude.toString())
                params.put(VoctagKeys.LONGITUDE, user.longitude.toString())
            }
        }

        val firebaseToken = getFirebaseToken()
        if (firebaseToken != null) {
            params.put(VoctagKeys.AUTHENTICATION_FIREBASE_DEVICE_TOKEN, firebaseToken)
        }

        return Jwts.builder().setHeaderParam(VoctagKeys.TYP, "JWT").setClaims(params).signWith(SignatureAlgorithm.HS256, BuildConfig.API_SHARED_SECRET.toByteArray()).compact()
    }

    fun encryptLogoutToken(deviceId: String, refreshToken: String): String {
        val params = HashMap<String, Any>()
        params.put(VoctagKeys.DEVICE_ID, deviceId)
        params.put(VoctagKeys.REFRESH_TOKEN, refreshToken)
        return Jwts.builder().setHeaderParam(VoctagKeys.TYP, "JWT").setClaims(params).signWith(SignatureAlgorithm.HS256, BuildConfig.API_SHARED_SECRET.toByteArray()).compact()
    }
}
