package com.voctag.android.api

import android.app.Activity
import android.provider.Settings
import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.voctag.android.BuildConfig
import com.voctag.android.R
import com.voctag.android.api.firebase.AnalyticsLoggerUtils
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.LanguageHelper
import com.voctag.android.helper.LoggingHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.LocalVocsManager
import com.voctag.android.model.*
import com.voctag.android.ui.VoctagApp
import org.json.JSONObject
import java.net.URLEncoder
import java.nio.charset.Charset
import java.util.*
import kotlin.collections.HashMap


/**
 * Created by jan.maron on 29.08.17.
 */

class RequestHelper {

    companion object {
        val shared = RequestHelper()
        val TAG: String = RequestHelper::class.java.simpleName
    }

    enum class RequestType {
        register, login, logout, refresh, loadUser, updateNickname, acceptTerms, loadVocDetail, upload, search, played, upVote, downVote, follow, unfollow, report, delete, loadClientDetail, loadClientVocs, loadClientVocDetail, uploadClientVoc, followClient, unfollowClient, redeemAccessCode, searchClients, getPresignedUrl, getCategories, fetchMyFeeds, updateNotificationsSettings, readAllNotifications, searchDownloadedVocs, loginWithFacebook, updateUser, getBanner;

        var vocID: Int = 0
        var clientSlug: String = ""

        val url: String
            get() {
                return when (this) {
                    register -> "v3/auth/register"
                    login -> "v3/auth/login"
                    loginWithFacebook -> "v3/auth/login_facebook"
                    logout -> "v3/auth/logout"
                    refresh -> "v3/auth/refresh"
                    loadUser -> "v3/users/me"
                    updateUser -> "v3/users/me"
                    acceptTerms -> "v3/users/me/accept_terms"
                    updateNickname -> "v3/users/me"
                    loadVocDetail -> "v2/vocs/${this.vocID}"
                    upload -> "v3/vocs"
                    search -> "v2/vocs/search"
                    searchDownloadedVocs -> "v3/vocs/search_by_ids"
                    played -> "v3/vocs/${this.vocID}/notify_played"
                    upVote -> "v3/vocs/${this.vocID}/vote_up"
                    downVote -> "v3/vocs/${this.vocID}/vote_down"
                    follow -> "v3/vocs/${this.vocID}/follow"
                    unfollow -> "v3/vocs/${this.vocID}/unfollow"
                    report -> "v3/vocs/${this.vocID}/report"
                    delete -> "v3/vocs/${this.vocID}"
                    loadClientDetail -> "v3/clients/${this.clientSlug}"
                    loadClientVocs -> "v3/clients/${this.clientSlug}/vocs"
                    loadClientVocDetail -> "v3/clients/${this.clientSlug}/vocs/${this.vocID}"
                    uploadClientVoc -> "v3/clients/${this.clientSlug}/vocs"
                    followClient -> "v3/clients/${this.clientSlug}/membership"
                    unfollowClient -> "v3/clients/${this.clientSlug}/membership"
                    redeemAccessCode -> "v3/clients/${this.clientSlug}/access"
                    searchClients -> "v3/clients/search"
                    getPresignedUrl -> "v3/clients/${this.clientSlug}/vocs/presigned_url"
                    getCategories -> "v3/categories"
                    fetchMyFeeds -> "v3/vocs/search"
                    updateNotificationsSettings -> "v3/users/me/notifications"
                    readAllNotifications -> "v3/notifications/read_all"
                    getBanner -> "v3/banners"
                }
            }

        val method: Int
            get() {
                return when (this) {
                    updateNickname, acceptTerms, downVote, follow, unfollow, played, report, upVote, updateUser -> Request.Method.PATCH
                    register, login, logout, refresh, search, upload, uploadClientVoc, followClient, redeemAccessCode
                        , searchClients, fetchMyFeeds, readAllNotifications, searchDownloadedVocs, loginWithFacebook -> Request.Method.POST
                    loadClientDetail, loadVocDetail, loadClientVocs, loadUser, loadClientVocDetail, getPresignedUrl, getCategories, getBanner -> Request.Method.GET
                    delete, unfollowClient -> Request.Method.DELETE
                    updateNotificationsSettings -> Request.Method.PUT
                }
            }
    }

    private val TIMEOUT_INTERVAL = 30000 //milliseconds

    // --- Auth Requests --- //
    /**
     * Register a new User
     */
    fun register(nickname: String, email: String, password: String, successListener: () -> Unit, errorListener: (error: VolleyError) -> Unit, context: Activity) {
        //add parameters of the request
        val deviceID = Settings.System.getString(VoctagApp.context.contentResolver, Settings.Secure.ANDROID_ID)

        val parameters = HashMap<String, String>()
        parameters[VoctagKeys.REG_EMAIL] = email
        parameters[VoctagKeys.REG_PASSWORD] = password
        parameters[VoctagKeys.REG_NICKNAME] = nickname
        parameters[VoctagKeys.REG_TOKEN] = AuthenticationHelper.shared.encrypt(hashMapOf(VoctagKeys.DEVICE_ID to deviceID))
        parameters[VoctagKeys.REG_LOCALE] = LanguageHelper.getCurrentLanguageCode()

        //request
        request(RequestType.register, parameters, Response.Listener { json ->
            if (json != null) {
                //Save authentication to shared preferences
                cacheAuthentication(Authentication(json.getJSONObject(VoctagKeys.DATA)))
                //refresh to set the firebase token for push notifications
                refresh(RequestType.register, parameters, null, null, context)

                AnalyticsLoggerUtils.getInstance().logSignUp(AnalyticsLoggerUtils.AUTH_METHOD_EMAIL)

                //call the successListener
                successListener()
                return@Listener
            }
        }, Response.Listener {
            errorListener(it)
        }, context)
    }

    private fun cacheAuthentication(auth: Authentication) {
        PersistDataHelper.shared.saveAuthentication(auth)
        PersistDataHelper.shared.saveUser(User(auth.userID, auth.nickname))
    }

    fun updateUser(newsletter: Boolean, successListener: () -> Unit, errorListener: (errorMessageId: Int) -> Unit, context: Activity) {
        val parameters = HashMap<String, String>()
        parameters[VoctagKeys.REG_NEWSLETTER] = newsletter.toString()

        request(RequestType.updateUser, parameters, Response.Listener { json ->
            successListener()
        }, Response.Listener {
            handleErrorResponse(it, errorListener)
        }, context)

    }

    fun loginWithFacebook(facebookToken: String, successListener: (isNewUser: Boolean) -> Unit, errorListener: (errorMessageId: Int) -> Unit, context: Activity) {
        val parameters = HashMap<String, String>()
        parameters[VoctagKeys.PARAM_FACEBOOK_TOKEN] = facebookToken

        val deviceID = Settings.System.getString(VoctagApp.context.contentResolver, Settings.Secure.ANDROID_ID)
        parameters[VoctagKeys.REG_TOKEN] = AuthenticationHelper.shared.encrypt(hashMapOf(VoctagKeys.DEVICE_ID to deviceID))
        parameters[VoctagKeys.REG_LOCALE] = LanguageHelper.getCurrentLanguageCode()

        request(RequestType.loginWithFacebook, parameters, Response.Listener { json ->
            if (json != null) {
                //Save authentication to shared preferences
                val auth = Authentication(json.getJSONObject(VoctagKeys.DATA))
                cacheAuthentication(auth)

                //refresh to set the firebase facebookToken for push notifications
                refresh(RequestType.loginWithFacebook, parameters, null, null, context)
                //call the successListener
                successListener(auth.isNewUser)
                return@Listener
            }
        }, Response.Listener {
            handleErrorResponse(it, errorListener)
        }, context)
    }

    /**
     * Login an User
     */
    fun login(email: String, password: String, successListener: () -> Unit, errorListener: (errorMessageId: Int) -> Unit, context: Activity) {
        val deviceID = Settings.System.getString(VoctagApp.context.contentResolver, Settings.Secure.ANDROID_ID)

        val parameters = HashMap<String, String>()
        parameters[VoctagKeys.LOGIN_EMAIL] = email
        parameters[VoctagKeys.LOGIN_PASSWORD] = password

        parameters[VoctagKeys.LOGIN_TOKEN] = AuthenticationHelper.shared.encrypt(hashMapOf(VoctagKeys.DEVICE_ID to deviceID))
        parameters[VoctagKeys.LOGIN_LOCALE] = LanguageHelper.getCurrentLanguageCode()

        request(RequestType.login, parameters, Response.Listener {
            if (it != null) {
                cacheAuthentication(Authentication(it.getJSONObject(VoctagKeys.DATA)))
                AnalyticsLoggerUtils.getInstance().logLogin(AnalyticsLoggerUtils.AUTH_METHOD_EMAIL)
                successListener()
                return@Listener
            }
        }, Response.Listener {
            handleErrorResponse(it, errorListener)
        }, context)
    }

    /**
     * Function to logout a user
     */
    fun logout(successListener: () -> Unit, errorListener: (taken: Boolean, json: String) -> Unit, context: Activity) {
        val parameters = HashMap<String, String>()
        val deviceID = Settings.System.getString(VoctagApp.context.contentResolver, Settings.Secure.ANDROID_ID)
        val refreshToken = PersistDataHelper.shared.loadAuthentication()?.refreshToken!!
        parameters[VoctagKeys.REG_TOKEN] = AuthenticationHelper.shared.encryptLogoutToken(deviceID, refreshToken)

        request(RequestType.logout, parameters, Response.Listener {
            successListener()
        }, Response.Listener {
            if (it.networkResponse.statusCode == 401) {
                val jsonString = String(it.networkResponse?.data
                        ?: ByteArray(0), Charset.forName(HttpHeaderParser.parseCharset(it.networkResponse?.headers)))
                errorListener(true, jsonString)
            } else {
                errorListener(false, it.networkResponse.toString() + "")
            }
        }, context)
    }

    /**
     * Function to refresh a auth token
     */
    private fun refresh(requestType: RequestType, parameters: HashMap<String, String>?, successListener: Response.Listener<JSONObject?>?, errorListener: Response.Listener<VolleyError>?, context: Activity?) {
        //check if refresh token is stored
        val refreshToken = PersistDataHelper.shared.loadAuthentication()?.refreshToken
        if (refreshToken == null) {
            LoggingHelper.logErrorForClass(RequestHelper::class.java, "No refresh token")
            return
        }

        val deviceID = Settings.System.getString(VoctagApp.context.contentResolver, Settings.Secure.ANDROID_ID)
        val encryptedRefreshToken = AuthenticationHelper.shared.encrypt(hashMapOf(VoctagKeys.REFRESH_TOKEN to refreshToken, VoctagKeys.DEVICE_ID to deviceID))
        val refreshParameters = HashMap<String, String>()
        refreshParameters.put(VoctagKeys.TOKEN, encryptedRefreshToken)

        val queue = Volley.newRequestQueue(VoctagApp.context)
        queue.add(buildRequest(RequestType.refresh, refreshParameters, Response.Listener { json ->
            if (json != null) {
                val auth = Authentication(json.getJSONObject(VoctagKeys.DATA))
                PersistDataHelper.shared.saveAuthentication(auth)
                request(requestType, parameters, successListener, errorListener, context)
                return@Listener
            }
            errorListener?.onResponse(null)
        }, Response.ErrorListener { error ->
            VolleyLog.e(requestType.url, error.message)
            val statusCode = error.networkResponse.statusCode
            if (statusCode == 401) {
                //refreshtoken is invalid and need to be refreshed
                LoggingHelper.logErrorForClass(RequestHelper::class.java, "Invalid refresh token")
                if (context != null) {
                    PersistDataHelper.shared.clearAllData(context)
                }
                return@ErrorListener
            }
            errorListener?.onResponse(error)
        }))
    }

    // --- User Requests --- //

    //Load User and save to persist data
    fun loadUser(withVocs: Boolean, successListener: (user: User) -> Unit, errorListener: (error: VolleyError) -> Unit, context: Activity?) {
        var parameters: HashMap<String, String>? = null
        if (withVocs) {
            parameters = hashMapOf("scope" to "all")
        }

        request(RequestType.loadUser, parameters, Response.Listener { json ->
            if (json != null) {
                val user = User(json.getJSONObject(VoctagKeys.DATA))
                PersistDataHelper.shared.saveUser(user)
                successListener(user)
            }
        }, Response.Listener {
            errorListener(it)
        }, context)
    }

    //update nickname
    fun updateNickname(nickname: String, successListener: (user: User) -> Unit, errorListener: (error: VolleyError) -> Unit, context: Activity) {
        val parameters = HashMap<String, String>()
        parameters[VoctagKeys.USER_NICKNAME] = URLEncoder.encode(nickname)
        request(RequestType.updateNickname, parameters, Response.Listener { json ->
            if (json != null) {
                val user = User(json.getJSONObject(VoctagKeys.DATA))
                PersistDataHelper.shared.saveUser(user)
                successListener(user)
            }
        }, Response.Listener {
            errorListener(it)
        }, context)
    }

    // --- Voc Requests --- //

    //Load Voc Detail
    fun loadVocDetail(vocID: Int, completionListener: (completion: Voc?) -> Unit, context: Activity) {
        val requestType = RequestType.loadVocDetail
        requestType.vocID = vocID
        request(requestType, null, Response.Listener { json ->
            if (json != null) {
                completionListener(Voc(json.getJSONObject(VoctagKeys.DATA)))
            } else {
                completionListener(null)
            }
        }, Response.Listener {
            completionListener(null)
        }, context)
    }

    //upload Voc
    fun uploadVoc(parameters: HashMap<String, String>, successListener: (success: Voc) -> Unit, errorListener: () -> Unit, context: Activity) {
        request(RequestType.upload, parameters, Response.Listener { json ->
            if (json != null) {
                val voc = Voc(json.getJSONObject(VoctagKeys.DATA))
                logMentorVoc(voc)
                successListener(voc)
            } else {
                errorListener()
            }
        }, Response.Listener {
            errorListener()
        }, context)
    }

    //search Vocs
    fun searchVocs(parameters: HashMap<String, String>, completionListener: (success: ArrayList<Voc>?, errorHappened: Boolean) -> Unit, context: Activity?, myFeeds: Boolean = false) {
        var requestType = RequestType.search
        if (myFeeds){
            requestType = RequestType.fetchMyFeeds
        }

        request(requestType, parameters, Response.Listener { json ->
            if (json != null) {
                completionListener(Voc.createArray(json.getJSONArray(VoctagKeys.DATA)), false)
            } else {
                completionListener(null, false)
            }
        }, Response.Listener {
            completionListener(null, true)
        }, context)
    }

    //Voc played
    fun playedVoc(voc: Voc, context: Activity?) {
        val requestType = RequestType.played
        requestType.vocID = voc.vocID
        request(requestType, null, null, null, context)
    }

    private fun isMentorMainAudio(voc: Voc): Boolean {
        return voc.isNotOpenFeed() && voc.parentClient?.mainVocId == voc.vocID
    }

    fun logPlayVocEvent(voc: Voc) {
        if (isMentorMainAudio(voc)) {
            AnalyticsLoggerUtils.getInstance().logMentorPlayMainAudio(voc.parentClient?.slug!!)
        } else {
            AnalyticsLoggerUtils.getInstance().logVocPlay(voc.parentClient?.slug.toString(), voc.vocID)
        }
    }

    //upVote Voc
    fun upVoteVoc(vocID: Int, clientSlug: String, context: Activity) {
        val requestType = RequestType.upVote
        requestType.vocID = vocID
        request(requestType, null, Response.Listener {
            AnalyticsLoggerUtils.getInstance().logVocLike(clientSlug, vocID)
        }, null, context)
    }

    //downVote Voc
    fun downVoteVoc(vocID: Int, clientSlug: String, context: Activity) {
        val requestType = RequestType.downVote
        requestType.vocID = vocID
        request(requestType, null, Response.Listener {
            AnalyticsLoggerUtils.getInstance().logVocUnLike(clientSlug, vocID)
        }, null, context)
    }

    //Follow Voc
    fun followVoc(vocID: Int, context: Activity) {
        val requestType = RequestType.follow
        requestType.vocID = vocID
        request(requestType, null, null, null, context)
    }

    //Unfollow Voc
    fun unfollowVoc(vocID: Int, context: Activity) {
        val requestType = RequestType.unfollow
        requestType.vocID = vocID
        request(requestType, null, null, null, context)
    }

    //Report Voc
    fun reportVoc(vocID: Int, reason: String, context: Activity) {
        val parameters = hashMapOf(VoctagKeys.REASON to reason)
        val requestType = RequestType.report
        requestType.vocID = vocID
        request(requestType, parameters, null, null, context)
    }

    //Delete Voc
    fun deleteVoc(vocID: Int, completionListener: (completion: Boolean) -> Unit, context: Activity) {
        val requestType = RequestType.delete
        requestType.vocID = vocID
        request(requestType, null, Response.Listener {
            completionListener(true)
        }, Response.Listener {
            completionListener(false)
        }, context)
    }

    // --- Client Requests --- //

    //Load Vocs from Client
    fun loadClientVocs(clientSlug: String, parameters: HashMap<String, String>, completionListener: (vocs: ArrayList<Voc>?, errorHappen: Boolean) -> Unit, context: Activity) {
        val requestType = RequestType.loadClientVocs
        requestType.clientSlug = clientSlug
        request(requestType, parameters, Response.Listener { json ->
            if (json != null) {
                completionListener(Voc.createArray(json.getJSONArray(VoctagKeys.DATA)), false)
            } else {
                completionListener(null, false)
            }
        }, Response.Listener {
            completionListener(null, true)
        }, context)
    }

    //load client Detail
    fun loadClientDetail(clientSlug: String, completionListener: (completion: Client?) -> Unit, context: Activity) {
        val requestType = RequestType.loadClientDetail
        requestType.clientSlug = clientSlug
        request(requestType, null, Response.Listener { json ->
            if (json != null) {
                completionListener(Client(json.getJSONArray(VoctagKeys.DATA).get(0) as JSONObject))
            } else {
                completionListener(null)
            }
        }, Response.Listener {
            completionListener(null)
        }, context)
    }

    //Load Client Voc Detail
    fun loadClientVocDetail(clientSlug: String, vocID: Int, completionListener: (completion: Voc?) -> Unit, context: Activity) {
        val requestType = RequestType.loadClientVocDetail
        requestType.clientSlug = clientSlug
        requestType.vocID = vocID
        request(requestType, null, Response.Listener { json ->
            if (json != null) {
                completionListener(Voc(json.getJSONObject(VoctagKeys.DATA)))
            } else {
                completionListener(null)
            }
        }, Response.Listener {
            completionListener(null)
        }, context)
    }

    //Search Clients
    fun searchClients(parameters: HashMap<String, String>, completionListener: (clients: ArrayList<Client>?, errorHappended: Boolean, categoryName: String?) -> Unit, context: Activity?, categoryName: String? = null) {
        request(RequestType.searchClients, parameters, Response.Listener { json ->
            if (json != null) {
                completionListener(Client.createArray(json.getJSONArray(VoctagKeys.DATA)), false, categoryName)
            } else {
                completionListener(null, false, categoryName)
            }
            parameters[VoctagKeys.FILTER_QUERY]?.let { query ->
                AnalyticsLoggerUtils.getInstance().logMentorSearch(query)
            }
        }, Response.Listener {
            completionListener(null, true, categoryName)
        }, context)
    }

    //Search DownloadedVocs
    fun searchForDownloadedVocs(parameters: HashMap<String, String>, completionListener: (vocs: ArrayList<Voc>?, errorHappened: Boolean) -> Unit, context: Activity?) {
        request(RequestType.searchDownloadedVocs, parameters, Response.Listener { json ->
            if (json != null) {
                completionListener(Voc.createArray(json.getJSONArray(VoctagKeys.DATA)), false)
            } else {
                completionListener(null, false)
            }

        }, Response.Listener {
            completionListener(null, true)
        }, context)
    }

    // fetch Categories
    fun fetCategories(completionListener: (clients: ArrayList<Category>?, errorHappended: Boolean) -> Unit, context: Activity?) {
        request(RequestType.getCategories, null, Response.Listener { json ->
            if (json != null) {
                completionListener(Category.createArray(json.get(VoctagKeys.DATA).toString()), false)
            } else {
                completionListener(null, false)
            }
        }, Response.Listener {
            completionListener(null, true)
        }, context)
    }

    // get Banners
    fun getBanner(completionListener: (bannerList: ArrayList<Banner>?, errorHappended: Boolean) -> Unit, context: Activity?) {
        request(RequestType.getBanner, null, Response.Listener { json ->
            if (json != null) {
                completionListener(Banner.createArray(json.get(VoctagKeys.DATA).toString()), false)
            } else {
                completionListener(null, false)
            }
        }, Response.Listener {
            completionListener(null, true)
        }, context)
    }


    //upload Client Voc
    fun getPresignedUrl(clientSlug: String, successListener: (success: PresignedUrl) -> Unit, errorListener: () -> Unit, context: Activity) {

        val requestType = RequestType.getPresignedUrl
        requestType.clientSlug = clientSlug
        request(requestType, null, Response.Listener { json ->
            if (json != null) {
                val gson = Gson()
                val presignedUrl = gson.fromJson<PresignedUrl>(json.getJSONObject(VoctagKeys.DATA).toString(), PresignedUrl::class.java)
                successListener(presignedUrl)
            } else {
                errorListener()
            }
        }, Response.Listener {
            errorListener()
        }, context)
    }

    //upload Client Voc
    fun uploadClientVoc(clientSlug: String, parameters: HashMap<String, String>, successListener: (success: Voc) -> Unit, errorListener: () -> Unit, context: Activity) {
        val requestType = RequestType.uploadClientVoc
        requestType.clientSlug = clientSlug
        request(requestType, parameters, Response.Listener { json ->
            if (json != null) {
                val voc = Voc(json.getJSONObject(VoctagKeys.DATA))
                logMentorVoc(voc)
                successListener(voc)
            } else {
                errorListener()
            }
        }, Response.Listener {
            errorListener()
        }, context)
    }

    private fun logMentorVoc(voc: Voc) {
        if (voc.createdByClientOwner) {
            AnalyticsLoggerUtils.getInstance().logMentorPublishVoc(voc.parentClient?.slug!!, voc.vocID)
        }
    }

    //Follow Client
    fun followClient(clientSlug: String, completionListener: (success: Boolean) -> Unit, context: Activity) {
        val requestType = RequestType.followClient
        requestType.clientSlug = clientSlug
        request(requestType, null, Response.Listener { _ ->
            AnalyticsLoggerUtils.getInstance().logFollowMentor(clientSlug)
            completionListener(true)
        }, Response.Listener {
            completionListener(false)
        }, context)
    }

    //unfollow Client
    fun unfollowClient(clientSlug: String, completionListener: (success: Boolean) -> Unit, context: Activity) {
        val requestType = RequestType.unfollowClient
        requestType.clientSlug = clientSlug
        request(requestType, null, Response.Listener { _ ->
            AnalyticsLoggerUtils.getInstance().logUnFollowMentor(clientSlug)

            completionListener(true)
        }, Response.Listener {
            completionListener(false)
        }, context)
    }

    //Redeem Access Code
    fun redeemAccessCode(clientSlug: String, accessCode: String, successListener: () -> Unit, errorListener: (invalid: Boolean) -> Unit, context: Activity) {
        val parameters = HashMap<String, String>()
        parameters[VoctagKeys.CLIENT_ACCESS_CODE] = accessCode

        val requestType = RequestType.redeemAccessCode
        requestType.clientSlug = clientSlug

        request(requestType, parameters, Response.Listener { _ ->
            successListener()
        }, Response.Listener {
            if (it.networkResponse.statusCode == 422 || it.networkResponse.statusCode == 423) {
                errorListener(true)
            } else {
                errorListener(false)
            }
        }, context)
    }

    fun updateNotificationSettings(notificationSettings: com.voctag.android.model.Settings.NotificationsSettings, errorListener: (error: VolleyError) -> Unit, context: Activity) {

        val gson = Gson()
        val type = object : TypeToken<HashMap<String?, String>>() {}.type
        val parameters: HashMap<String, String> = gson.fromJson(gson.toJson(notificationSettings), type)

        request(RequestType.updateNotificationsSettings, parameters, Response.Listener { json ->

            Log.d(TAG, "notification settings is updated successfully")
        }, Response.Listener {
            errorListener(it)
        }, context)
    }

    fun readAllNotifications(context: Activity?) {

        val requestType = RequestType.readAllNotifications
        request(requestType, null, Response.Listener {
            Log.d(TAG, "all notifications are read")
        }, null, context)
    }


    // --- Private Methods --- //

    private fun request(requestType: RequestType, parameters: HashMap<String, String>?, successListener: Response.Listener<JSONObject?>?, errorListener: Response.Listener<VolleyError>?, context: Activity?) {
        val queue = Volley.newRequestQueue(VoctagApp.context)
        queue.add(buildRequest(requestType, parameters, Response.Listener { response ->
            successListener?.onResponse(response)
        }, Response.ErrorListener { error ->
            VolleyLog.e(requestType.url, error.message)
            if (error.networkResponse != null) {
                val statusCode = error.networkResponse.statusCode
                if (statusCode == 401 && !isAuthenticationRequest(requestType)) {
                    //auth token is invalid and need to be refreshed
                    refresh(requestType, parameters, successListener, errorListener, context)
                } else {
                    errorListener?.onResponse(error)
                }
            } else {
                errorListener?.onResponse(error)
            }
        }))
    }
    private fun isAuthenticationRequest(requestType: RequestType): Boolean {
        return requestType.name == RequestType.register.name
                || requestType.name == RequestType.login.name
                || requestType.name == RequestType.loginWithFacebook.name
    }
    /**
     * Function to build the request. BASE_URL + REQUESTTYPE_URL + Parameter or Body
     */
    private fun buildRequest(requestType: RequestType, parameters: HashMap<String, String>?, successListener: Response.Listener<JSONObject?>, errorListener: Response.ErrorListener): CustomJsonObjectRequest {
        var url = BuildConfig.API_BASE_URL + requestType.url

        var jsonBody = JSONObject()
        if (parameters != null) {
            // add parameters to the url if get request
            if (!(requestType.name.equals(RequestType.upload.name) || requestType.name.equals(RequestType.uploadClientVoc.name)
                            || requestType.name.equals(RequestType.register.name) || requestType.name.equals(RequestType.login.name)
                            || requestType.name.equals(RequestType.searchDownloadedVocs.name))) {
                url = addParametersToURL(url, parameters)
            } else if (requestType.name.equals(RequestType.searchDownloadedVocs.name)) {
                jsonBody = JSONObject(parameters[LocalVocsManager.VOCS_IDS_KEY])
            } else {
                jsonBody = JSONObject(parameters)
            }
        }

        val request = CustomJsonObjectRequest(requestType, url, jsonBody, successListener, errorListener)
        request.retryPolicy = DefaultRetryPolicy(TIMEOUT_INTERVAL, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        Log.d("com.test", "requestHeaders > ${request.headers.values}")
        return request
    }

    private fun addParametersToURL(requestURL: String, parameters: HashMap<String, String>): String {
        var url = requestURL

        var i = 0
        val arr = parameters.keys zip parameters.values
        arr.forEach {
            if (i == 0) {
                url += "?${it.first}=${it.second}"
            } else {
                url += "&${it.first}=${it.second}"
            }

            i += 1
        }

        return url
    }

    private fun handleErrorResponse(error: VolleyError?, errorListener: (errorMessageId: Int) -> Unit) {
        error?.let {
            if (it is NoConnectionError || it.networkResponse == null) {
                errorListener(R.string.nointernet)
                return
            }

            when (it.networkResponse.statusCode) {
                401 -> {
                    val errorResponse = parseVolleyNetworkError(it)[0]
                    when {
                        errorResponse.attribute == Error.ERROR_ATTRIBUTE_EMAIL && errorResponse.code == Error.ERROR_CODE_INVALID -> {
                            errorListener(R.string.wrong_email_or_password)
                        }
                        errorResponse.attribute == Error.ERROR_ATTRIBUTE_EMAIL && errorResponse.code == Error.ERROR_CODE_FACEBOOK -> {
                            errorListener(R.string.email_assigned_to_facebook_account)
                        }
                        errorResponse.attribute == Error.ERROR_ATTRIBUTE_FACEBOOK_EMAIL && errorResponse.code == Error.ERROR_CODE_TAKEN -> {
                            errorListener(R.string.email_assigned_to_email_account)
                        }
                        else -> errorListener(R.string.nointernet)
                    }
                }
                500 -> errorListener(R.string.server_error)
                else -> errorListener(R.string.nointernet)
            }
        }
    }

    private fun parseVolleyNetworkError(error: VolleyError): List<Error> {
        val errorResponse = String(error.networkResponse?.data
                ?: ByteArray(0), Charset.forName(HttpHeaderParser.parseCharset(error.networkResponse?.headers)))

        return Gson().fromJson(errorResponse, ErrorHandler::class.java).errors
    }

    private fun parseNetworkError(error: VolleyError): String {
        return String(error.networkResponse?.data
                ?: ByteArray(0), Charset.forName(HttpHeaderParser.parseCharset(error.networkResponse?.headers)))
    }
}
