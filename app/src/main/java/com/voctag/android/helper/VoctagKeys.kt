package com.voctag.android.helper

interface VoctagKeys {

    companion object {

        // MARK: - Integers

        val TITLE_CHARACTER_MAX = 150
        val NICKNAME_CHARACTER_MAX = 30
        val NICKNAME_CHARACTER_MIN = 2
        val PASSWORD_CHARACTER_MIN = 6
        val UPDATE_INTERVAL = 40

        // MARK: - Strings

        val EXTRA_AMOUNT: String = "EXTRA_AMOUNT"
        val EXTRA_FINISH_DETAILS_ON_BACK: String = "EXTRA_FINISH_DETAILS_ON_BACK"
        val EMPTY = ""
        val DATA = "data"
        val DEVICE_ID = "device_id"
        val TOKEN = "token"
        val REFRESH_TOKEN = "refresh_token"
        val LATITUDE = "latitude"
        val LONGITUDE = "longitude"
        val TYP = "typ"
        val REASON = "reason"
        val AUDIO = "audio"
        val TITLE = "title"
        val PARENT_ID = "parent_id"
        val SECRET = "secret"
        val PLACEHOLDER_DIVIDER_TOP_CLIENTS = "placeholder_divider_top_clients"
        val PLACEHOLDER_DIVIDER_SUGGESTIONS = "placeholder_divider_suggestions"
        val PLACEHOLDER_DIVIDER_MYCLIENTS = "placeholder_divider_myclients"
        val PLACEHOLDER_DIVIDER_OWNER_CLIENTS = "placeholder_divider_ownerclients"
        val PLACEHOLDER_DIVIDER_NEW_CONGRESSES = "placeholder_divider_new_congresses"
        val SHOW_IN_FEED = "show_in_feed"

        // MARK: - Intent Extras

        val EXTRA_VOC = "voc"
        val EXTRA_VOC_ID = "voc_id"
        val EXTRA_CLIENT = "client"
        val EXTRA_CLIENTS = "clients"
        val EXTRA_TOP_CLIENTS = "top_clients"
        val EXTRA_MEMBER_CLIENTS = "members_clients"
        val EXTRA_VOCS = "vocs"
        val EXTRA_SHOULDSCROLLTOVOC = "shouldscrolltovoc"
        val EXTRA_CLIENTSPEAKSSECTION = "clientspeakssection"
        val EXTRA_SECRET = "EXTRA_SECRET"
        val EXTRA_AUDIO_DURATION = "EXTRA_AUDIO_DURATION"
        val EXTRA_WEBVIEW_URL = "EXTRA_WEBVIEW_URL"
        val EXTRA_WEB_URL = "EXTRA_WEB_URL"

        const val EXTRA_CLIENT_SLUG = "EXTRA_CLIENT_SLUG"

        const val EXTRA_CLIENT_TAB_TO_OPEN: String = "EXTRA_CLIENT_TAB_TO_OPEN"
        const val EXTRA_FOLLOW_CLIENT = "EXTRA_FOLLOW_CLIENT"
        const val EXTRA_RELOAD_CLIENT = "EXTRA_RELOAD_CLIENT"
        const val EXTRA_SHOW_CONGRATULATIONS_DIALOG = "EXTRA_SHOW_CONGRATULATIONS_DIALOG"
        // MARK: - Notifications

        const val NOTIFICATION_MESSAGE = "message"
        const val NOTIFICATION_TYPE = "type"
        const val NOTIFICATION_TYPE_REPLY = "REPLY"
        const val NOTIFICATION_TYPE_REPLY_FOLLOWED = "REPLY_FOLLOWED"
        const val NOTIFICATION_TYPE_CLIENT_OWNER_PUBLISHED = "CLIENT_OWNER_PUBLISHED"
        const val NOTIFICATION_TYPE_USER_PUBLISHED: String = "USER_PUBLISHED"
        const val NOTIFICATION_TYPE_PRIVATE_QUESTION: String = "PRIVATE_QUESTION"
        const val NOTIFICATION_TYPE_REPLY_VOTED: String = "REPLY_VOTED"
        const val NOTIFICATION_TYPE_OPEN_CLIENT: String = "OPEN_CLIENT"
        const val NOTIFICATION_TYPE_PODCAST: String = "PODCAST"
        const val NOTIFICATION_TYPE_OPEN_WEBVIEW: String = "OPEN_WEBVIEW"
        const val NOTIFICATION_TYPE_OPEN_WEB: String = "OPEN_WEB"

        // MARK: - Request Header

        val ACCEPT = "Accept"
        val CONTENT_TYPE = "Content-Type"
        val AUTHORIZATION = "Authorization"

        // MARK: - Persist Data

        val PREFERENCES = "preferences"
        val PREFERENCES_VOC_COUNTS = "preferences_voc_counts"
        val USER = "user"
        val AUTHENTICATION = "auth"
        val CURRENT_FILTER = "current_filter"
        const val IS_PRIVATE_QUESTION = "IS_PRIVATE_QUESTION"

        // MARK: - Authentication Object

        const val AUTHENTICATION_USER_ID = "user_id"
        const val AUTHENTICATION_AUTH_TOKEN = "auth_token"
        const val AUTHENTICATION_AUTH_TOKEN_ENCRYPTION_ACCESS = "auth_token"
        const val AUTHENTICATION_FIREBASE_DEVICE_TOKEN = "firebase_device_token"
        const val AUTHENTICATION_REFRESH_TOKEN = "refresh_token"
        const val AUTHENTICATION_EXPIRES_AT = "expires_at"
        const val AUTHENTICATION_IS_NEW_USER = "is_new_user"
        const val AUTHENTICATION_NICKNAME = "nickname"
        const val AUTHENTICATION_TYPE = "type"


        // MARK: - OnboardinTypes

        val ONBOARDING_TYPE_COMMUNITY = "community"
        val ONBOARDING_TYPE_MENTORS = "mentors"
        val ONBOARDING_TYPE_PROFILE = "profile"

        // MARK: - Filter Object

        val FILTER_ORDER = "order"
        val FILTER_QUERY = "query"
        val FILTER_PAGE = "page"
        val FILTER_SCOPE = "scope"
        val FILTER_CATEGORY = "category"

        // MARK: - User Object

        val USER_ID = "id"
        val USER_ACCEPTED_TERMS = "accepted_terms"
        val USER_POINTS = "points"
        val USER_VOCS = "vocs"
        val USER_NICKNAME = "nickname"
        val SETTINGS = "settings"
        val USER_REPLIES = "reply_vocs"
        val USER_FAVORITES = "followed_vocs"
        val USER_OWNING_CHANNELS = "owner_of"

        // MARK: - Voc Object

        val VOC_ID = "id"
        val VOC_USER_ID = "user_id"
        val VOC_USER_NICKNAME = "user_nickname"
        val VOC_REPLIES_COUNT = "replies_count"
        val VOC_TITLE = "title"
        val VOC_PLAY_COUNT = "play_count"
        val VOC_AUDIO_URL = "audio_url"
        val VOC_AUDIO_DURATION = "audio_duration"
        val VOC_EXTERNAL_AUDIO_URL = "external_audio_url"
        val VOC_DURATION = "duration"
        val VOC_WEB_URL = "web_url"
        val VOC_CITY = "city"
        val VOC_SCORE = "score"
        val VOC_CREATED_AT = "created_at"
        val VOC_TRANSCRIPT = "transcript"

        val VOC_PARENT_ID = "parent_id"
        val VOC_VOTED = "voted"
        val VOC_SECRET = "secret"
        val VOC_SECRET_REPLIES_ALLOWED = "secret_replies_allowed"
        val VOC_FOLLOWING = "following"
        val VOC_REPLIES = "replies"
        val VOC_CREATED_BY_CLIENT_OWNER = "created_by_channel_owner"
        val VOC_PARENT_CLIENT = "client"

        val VOC_CLIENT_SLUG = "client_slug"
        val VOC_CLIENT_THEMECOLOR = "client_theme_color"
        val VOC_CLIENT_SMALL_LOGO = "client_logo_small"
        val VOC_CLIENT_MEDIUM_LOGO = "client_logo_medium"
        val VOC_SLUG = "slug"

        const val PAYMENT_INFO = "payment_info"
        const val VOC_CLIENT_NAME = "client_name"
        const val COMMUNITY_SLUG: String = "open-feed-de"
        const val VOC_CLIENT_MEMBER = "client_member"
        const val VOC_CLIENT_IS_TOP_MENTOR = "client_is_top"
        const val VOC_USER_CLIENT_SLUG = "user_client_slug"
        const val VOC_USER_CLIENT_THEME_COLOR = "user_client_theme_color"
        const val VOC_USER_CLIENT_LOGO_MEDIUM: String = "user_client_logo_medium"
        const val VOC_CLIENT_USER_NICKNAME = "client_user_nickname"
        const val IS_PODCAST = "is_podcast"

        val BANNER_ID = "id"
        val BANNER_TYPE = "type"
        val BANNER_IMAGE_URL = "image_url"
        val BANNER_CLIENT_SLUG = "client_slug"
        val BANNER_VOC_ID = "voc_id"
        val BANNER_WEBVIEW_URL = "webview_url"
        val BANNER_WEB_URL = "web_url"

        // MARK: - Client Object

        val CLIENT_SLUG = "slug"
        val CLIENT_NAME = "name"
        val CLIENT_DESCRIPTION = "description"
        val CLIENT_MEMBERSHIPS_COUNT = "memberships_count"
        val CLIENT_ICON_URLS = "logo_urls"
        val CLIENT_ICON_SMALL = "small"
        val CLIENT_ICON_MEDIUM = "medium"
        val CLIENT_AUDIO_URL = "main_audio_url"
        val CLIENT_THEMECOLOR = "theme_color"
        val CLIENT_FOLLOWING = "member"
        val CLIENT_VOCS_COUNT = "vocs_count"
        val SECRET_VOC_PRICE = "secret_voc_price"
        val IS_SECRET_VOC_ENABLED = "is_secret_voc_enabled"

        val CLIENT_IS_CLOSED = "is_closed"
        val CLIENT_HAS_ACCESS = "has_access"
        val CLIENT_SALES_LINK = "sales_link"
        val CLIENT_ACCESS_CODE = "access_code"

        val CLIENT_IS_OFFICIAL = "is_official"
        val CLIENT_MENTOR_FOR = "mentor_for"
        const val CLIENT_IS_TOP_MENTOR = "is_top"

        val LOGIN_EMAIL = "email"
        val LOGIN_PASSWORD = "password"
        val LOGIN_TOKEN = "token"
        val LOGIN_LOCALE = "locale"

        val REG_EMAIL = "email"
        val REG_PASSWORD = "password"
        val REG_NICKNAME = "nickname"
        val REG_NEWSLETTER = "newsletter"
        val REG_TOKEN = "token"
        val REG_LOCALE = "locale"
        val NEW_VOCS_COUNT = "new_voc_count"
        const val CLIENT_IS_CONFERENCE = "is_conference"
        const val CLIENT_WEB_VIEW_LINK = "webview_link"

        // Record Activity
        const val PAYMENT_METHOD_NONCE = "payment_method_nonce"

        const val CHARGE_USER = "charge_user"
        const val DOWNLOADED_VOCS = "downloaded_vocs"
        const val DOWNLOADED_VOCS_IDS = "downloaded_vocs_IDS"
        val VOC_REPLY_CREATED_AT = "reply_created_at"
        val VOC_IS_CLIENT_REPLY = "is_client_reply"
        val VOC_REPLY_CITY = "reply_city"

        val PLAYER_SPEED = "PLAYER_SPEED"

        const val PARAM_FACEBOOK_TOKEN = "facebook_token"
    }

}
