package com.voctag.android.helper

import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.voctag.android.R

object UiUtils {

    fun showSnackbar(view: View?, messageId: Int, duration: Int = Snackbar.LENGTH_SHORT, onRetry: (() -> Unit?)? = null): Snackbar? {
        view?.let {
            return showSnackbar(it, it.context.getString(messageId), duration, onRetry)
        }
        return null
    }

    fun showSnackbar(view: View?, message: String, duration: Int = Snackbar.LENGTH_SHORT, onRetry: (() -> Unit?)? = null): Snackbar? {
        view?.let {
            val snackbar = Snackbar.make(it, message, duration)
            if (onRetry != null) {
                snackbar.setAction(R.string.retry) {
                    onRetry()
                }
            }
            snackbar.show()
            return snackbar
        }
        return null
    }
}