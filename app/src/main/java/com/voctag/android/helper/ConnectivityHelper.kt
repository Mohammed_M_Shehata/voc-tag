package com.voctag.android.helper

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class ConnectivityHelper {

    companion object {
        /**
         * This function check Mobile Data or WiFi is switched on or not..
         * It will be return true when switched on and return false when switched off.
         */
        fun isOnline(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
            return if (connectivityManager is ConnectivityManager) {
                val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
                networkInfo?.isConnected ?: false
            } else false
        }
    }

}
