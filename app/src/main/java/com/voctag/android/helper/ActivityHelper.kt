package com.voctag.android.helper

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.ShortDynamicLink
import com.voctag.android.BuildConfig
import com.voctag.android.R
import com.voctag.android.api.firebase.AnalyticsLoggerUtils
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.auth.register.WelcomeActivity
import com.voctag.android.ui.main.MainActivity
import com.voctag.android.ui.main.mentor.detail.MentorAccessActivity
import com.voctag.android.ui.main.mentor.detail.conference.MentorDetailConferenceActivity
import com.voctag.android.ui.main.mentor.detail.open.MentorDetailActivity
import com.voctag.android.ui.main.publish.PaymentActivity
import com.voctag.android.ui.main.voc.single.VocSingleActivity
import kotlinx.android.synthetic.main.dialog_congratulation.view.*
import org.jetbrains.anko.*

class ActivityHelper {

    companion object {
        const val PAYMENT_REQUEST_CODE: Int = 21

        fun openClient(activity: Activity, client: Client
                       , followingListener: ((client: Client) -> Unit)?
                       , currentTab: Int = MentorDetailActivity.SPEAKS_PAGE
                       , followClient: Boolean? = null
                       , extras: Bundle? = null
                       , enableSingleActivityMode: Boolean = true) {


            MentorDetailActivity.onFollowingChanged = followingListener
            if (!isClientClosed(client, activity)) {
                val Bundle = Bundle().apply {
                    putInt(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, currentTab)
                    if (followClient != null) {
                        putBoolean(VoctagKeys.EXTRA_FOLLOW_CLIENT, followClient)
                    }

                    if (extras != null) {
                        putAll(extras)
                    }
                }
                openClient(activity, client, Bundle, enableSingleActivityMode)
            }
        }

        private fun isClientClosed(client: Client, activity: Activity): Boolean {
            if (client.isClosed && !client.hasAccess) {
                val intent = Intent(activity, MentorAccessActivity::class.java).apply {
                    putExtra(VoctagKeys.EXTRA_CLIENT, client.encode())
                }

                activity.startActivity(intent)
                return true
            }
            return false
        }

        fun openClient(activity: Activity, client: Client, extras: Bundle, enableSingleActivityMode: Boolean = true) {
            val intent = Intent(activity, getActivityToOpen(client.isConference)).apply {
                putExtra(VoctagKeys.EXTRA_CLIENT, client.encode())
                putExtras(extras)

                if (enableSingleActivityMode) {
                    flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                }

                if (client.isConference) {
                    putExtra(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, MentorDetailConferenceActivity.STAGE_PAGE)
                }
            }
            activity.startActivity(intent)
        }

        fun getActivityToOpen(conference: Boolean): Class<*> {
            return if (conference) MentorDetailConferenceActivity::class.java
            else MentorDetailActivity::class.java
        }

        fun openConference(activity: Activity, client: Client, onFollowingChanged: ((clientSlug: Client) -> Unit)?) {
            MentorDetailConferenceActivity.onFollowingChanged = onFollowingChanged
            if (!isClientClosed(client, activity)) {

                val extras = Bundle()
                val intent = Intent(activity, MentorDetailConferenceActivity::class.java).apply {
                    putExtra(VoctagKeys.EXTRA_CLIENT, client.encode())
                    putExtras(extras)
                }
                activity.startActivity(intent)
            }
        }

        fun openVoc(activity: Activity, voc: Voc, shouldScrollToVoc: Voc?, deletedVocListener: (() -> Unit)?) {
            VocSingleActivity.onDeletedVoc = deletedVocListener
            val intent = Intent(activity, VocSingleActivity::class.java).apply {
                putExtra(VoctagKeys.EXTRA_VOC, voc.encode())
                if (shouldScrollToVoc != null) {
                    putExtra(VoctagKeys.EXTRA_SHOULDSCROLLTOVOC, shouldScrollToVoc.encode())
                }
            }
            activity.startActivity(intent)
        }

        /**
         * Function to hide the Soft Keyboard
         */
        fun hideSoftKeyboard(activity: Activity) {
            val view = activity.currentFocus
            if (view != null) {
                val inputManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }
        }

        fun onPublishingError(activity: Activity, errorMessage: String, nonce: String?, publish: (nonce: String?) -> Unit) {
            val dialog = AlertDialog.Builder(activity).create()
            dialog.setMessage(errorMessage)
            dialog.setButton(AlertDialog.BUTTON_POSITIVE, activity.getString(com.voctag.android.R.string.retry)) { dialogInterface: DialogInterface, i: Int ->
                publish(nonce)
            }
            dialog.show()
        }

        fun showCongratulationDialog(activity: Activity, congratulationText: String, descriptionText: String, actionButtonText: String) {
            val dialog = AlertDialog.Builder(activity).create()
            val view = activity.layoutInflater.inflate(R.layout.dialog_congratulation, null)
            view.congratulationTextView.text = congratulationText
            view.descriptionTexView.text = descriptionText
            view.actionButton.text = actionButtonText
            view.actionButton.setOnClickListener {
                dialog.dismiss()
            }
            dialog.setView(view)
            dialog.show()
        }

        fun showProgressDialog(activity: Activity, progressText: String, progressBarColor: Int): AlertDialog {
            val dialog = AlertDialog.Builder(activity).create()
            val view = activity.layoutInflater.inflate(R.layout.dialog_progress, null)

            view.find<TextView>(R.id.textviewProgress).text = progressText
            view.find<ProgressBar>(R.id.progressbar).indeterminateDrawable.setTint(progressBarColor)

            dialog?.setView(view)
            dialog?.setCanceledOnTouchOutside(false)
            dialog?.show()
            return dialog
        }

        fun payForPublishQuestion(activity: Activity, amount: String?) {
            val intent = Intent(activity, PaymentActivity::class.java).apply {
                putExtra(PaymentActivity.AMOUNT, amount)
            }
            activity.startActivityForResult(intent, PAYMENT_REQUEST_CODE)
        }

        fun onPaymentActivityResult(requestCode: Int, resultCode: Int, data: Intent?, publish: (nonce: String?) -> Unit) {
            if (requestCode == PAYMENT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                val nonce = data?.getStringExtra(PaymentActivity.PAYMENT_NONCE)
                publish(nonce)
            }
        }

        fun openAskPlanChooser(activity: Activity, client: Client, currentTabIndex: Int, amount: String?) {

            val choosePlanDialog = AlertDialog.Builder(activity).create()
            choosePlanDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val dialogView = activity.layoutInflater.inflate(R.layout.dialog_choose_ask_mentor_plan, null, false)
            val color = Color.parseColor(client.themeColorString)

            val freeAskPlanTextView = dialogView.findViewById<TextView>(R.id.freeAskPlanTextView)
            val privateAskPlanPriceTextView = dialogView.findViewById<TextView>(R.id.privateAskPlanPriceTextView)
            val freeAskPlanDescriptionTextView = dialogView.findViewById<TextView>(R.id.freeAskPlanDescriptionTextView)
            val privateAskPlanDescriptionTextView = dialogView.findViewById<TextView>(R.id.privateAskPlanDescriptionTextView)
            val moreInfoText = dialogView.findViewById<TextView>(R.id.dialog_moreinfo)
            val gradientDrawable = dialogView.findViewById<View>(R.id.view_choose_plan).background as GradientDrawable

            //general
            gradientDrawable.mutate()
            gradientDrawable.color = ColorStateList.valueOf(color)
            dialogView.findViewById<ImageView>(R.id.publicImageView).setColorFilter(color)
            dialogView.findViewById<ImageView>(R.id.privateImageView).setColorFilter(color)

            //public
            freeAskPlanDescriptionTextView.text = activity.getString(R.string.ask_mentor_public_plan_description, client.name)
            freeAskPlanTextView.textColor = color

            // private
            privateAskPlanPriceTextView.text = activity.getString(R.string.private_question_price, client.secretVocPrice)
            privateAskPlanPriceTextView.textColor = color
            privateAskPlanDescriptionTextView.text = activity.getString(R.string.ask_mentor_private_plan_description, client.name)

            //on click listener
            val openAskReplyViewListener: (View) -> Unit = { view ->
                val secret = view.id != R.id.publicAskPlanView
                VocCreationManager.shared.openAskReplyView(activity, currentTabIndex, secret, amount)
                choosePlanDialog.dismiss()
            }
            moreInfoText.setOnClickListener { view ->
                val message = SpannableStringBuilder(activity.getString(R.string.more_info_message))
                showMessageDialog(activity, activity.getString(R.string.more_info_title), message)
            }

            dialogView.findViewById<View>(R.id.publicAskPlanView).setOnClickListener(openAskReplyViewListener)
            dialogView.findViewById<View>(R.id.privateAskPlanView).setOnClickListener(openAskReplyViewListener)
            choosePlanDialog.setView(dialogView)
            choosePlanDialog.show()
        }

        fun shareVocLink(activity: Activity, voc: Voc? = null, client: Client? = null) {

            val progressDialog = showProgressDialog(activity, activity.getString(R.string.progress_loading), getProgressColor(activity, voc, client))
            progressDialog.show()

            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.type = "text/plain"
            createFirebaseDynamicLink(activity, voc, client, OnSuccessListener { result ->
                val shortLink = result.shortLink
                Log.d("com.test>", "  shortenLink> $shortLink")
                progressDialog.dismiss()

                sendIntent.putExtra(Intent.EXTRA_TEXT, shortLink.toString())
                activity.startActivity(Intent.createChooser(sendIntent, shortLink.toString()))
            })
        }

        private fun getProgressColor(activity: Activity, voc: Voc?, client: Client?): Int {
            return when {
                client != null -> Color.parseColor(client.themeColorString)
                else -> {
                    if (voc != null && voc.isNotOpenFeed()) {
                        Color.parseColor(voc.parentClient?.themeColorString)
                    } else {
                        ContextCompat.getColor(activity, R.color.main)
                    }
                }
            }
        }

        private fun createFirebaseDynamicLink(activity: Activity, voc: Voc? = null, client: Client? = null, successListener: OnSuccessListener<ShortDynamicLink>) {
            var deepLinkBuilder = Uri.parse(activity.getString(R.string.deep_link_authority))
                    .buildUpon()

            var vocWebUrl: Uri? = null

            if (voc != null && voc.isNotOpenFeed()) {
                val clientSlug = voc.parentClient?.slug as String
                AnalyticsLoggerUtils.getInstance().logVocShare(clientSlug, voc.vocID)
                vocWebUrl = createClientVocWebUrl(voc, deepLinkBuilder)
                deepLinkBuilder = buildClientVocDeepLink(voc, vocWebUrl)
            } else if (voc != null && voc.parentClient == null) {
                AnalyticsLoggerUtils.getInstance().logVocShare(VoctagKeys.COMMUNITY_SLUG, voc.vocID)
                vocWebUrl = createCommunityVocWebUrl(voc, deepLinkBuilder)
                deepLinkBuilder = buildVocDeepLink(voc, vocWebUrl)
            } else if (client != null) {
                AnalyticsLoggerUtils.getInstance().logMentorShare(client.slug)
                deepLinkBuilder.appendPath(client.slug)
            }

            buildFirebaseDynamicLinkWithParameters(activity, deepLinkBuilder.build()
                    , buildSocialParameters(activity, voc, client)
                    , vocWebUrl
                    , successListener)
        }

        private fun buildFirebaseDynamicLinkWithParameters(activity: Activity, deepLinkUrl: Uri
                                                           , socialParameters: DynamicLink.SocialMetaTagParameters
                                                           , vocWebUrl: Uri?
                                                           , successListener: OnSuccessListener<ShortDynamicLink>) {

            FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(deepLinkUrl)
                    .setDomainUriPrefix(activity.getString(R.string.domain_uri_prefix))
                    .setAndroidParameters(buildAndroidParameters(vocWebUrl))
                    .setSocialMetaTagParameters(socialParameters)
                    .setIosParameters(buildIosParameters(vocWebUrl, activity.getString(R.string.dynamic_links_ios_bundle), activity.getString(R.string.dynamic_links_ios_store_id)))
                    .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT)
                    .addOnSuccessListener(successListener)
        }

        private fun buildSocialParameters(activity: Activity, voc: Voc?, client: Client?): DynamicLink.SocialMetaTagParameters {
            var title: String? = null
            var description: String? = null
            var imageUrl: String? = null

            if (voc != null) {
                title = voc.titleString
                imageUrl = activity.getString(R.string.voc_social_share_image_url)
                description = Utils.getTextFromRawText(voc.transcript).toString()
            } else if (client != null) {
                title = client.name
                description = Utils.getTextFromRawText(client.description).toString()
                imageUrl = client.mediumIconURL
            }
            val builder = DynamicLink.SocialMetaTagParameters.Builder()

            if (!title.isNullOrEmpty()) builder.setTitle(title)
            if (!description.isNullOrEmpty()) builder.setDescription(description)
            if (!imageUrl.isNullOrEmpty()) builder.setImageUrl(Uri.parse(imageUrl))

            return builder.build()
        }

        private fun buildIosParameters(vocWebUrl: Uri?, iosBundleId: String, appStoreId: String): DynamicLink.IosParameters {
            val builder = DynamicLink.IosParameters.Builder(iosBundleId).setAppStoreId(appStoreId)

            if (vocWebUrl != null) {
                builder.setFallbackUrl(vocWebUrl)
            }
            return builder.build()
        }

        private fun buildAndroidParameters(vocWebUrl: Uri?): DynamicLink.AndroidParameters {
            val builder = DynamicLink.AndroidParameters.Builder(BuildConfig.APPLICATION_ID)
            if (vocWebUrl != null) {
                builder.setFallbackUrl(vocWebUrl)
            }
            return builder.build()
        }

        private fun createCommunityVocWebUrl(voc: Voc, builder: Uri.Builder): Uri {
            return builder.appendPath(VoctagKeys.COMMUNITY_SLUG)
                    .appendPath(voc.slug)
                    .build()
        }

        private fun createClientVocWebUrl(voc: Voc, builder: Uri.Builder): Uri {
            return builder.appendPath(voc.parentClient?.slug)
                    .appendPath(voc.slug)
                    .build()
        }

        private fun buildVocDeepLink(voc: Voc, vocWebUrl: Uri): Uri.Builder {
            return vocWebUrl.buildUpon()
                    .appendQueryParameter(VoctagKeys.VOC_ID, voc.vocID.toString())
        }

        private fun buildClientVocDeepLink(voc: Voc, vocWebUrl: Uri): Uri.Builder {
            return buildVocDeepLink(voc, vocWebUrl)
                    .appendQueryParameter(VoctagKeys.VOC_CLIENT_THEMECOLOR, voc.parentClient?.themeColor)
        }

        fun showMessageDialog(activity: Activity, title: String, message: Spannable) {
            val builder = AlertDialog.Builder(activity)

            val messageTextView = TextView(activity)
            messageTextView.text = message
            Linkify.addLinks(messageTextView, Linkify.WEB_URLS)
            messageTextView.movementMethod = LinkMovementMethod.getInstance()
            messageTextView.padding = activity.dimen(R.dimen.dialog_message_padding)

            builder.setView(messageTextView)
            builder.setTitle(title)
            builder.setPositiveButton(R.string.feed_cell_action_done, null)
            builder.create().show()
        }

        fun getVocTransparentBadgeImageResource(isTopMentor: Boolean?): Int {
            return when (isTopMentor) {
                true -> R.drawable.ic_top_mentor_header
                else -> R.drawable.ic_mentor_header
            }
        }

        fun getVocBadgeImageResource(isTopMentor: Boolean?): Int {
            return when (isTopMentor) {
                true -> R.drawable.ic_mentor_top_cell
                else -> R.drawable.ic_mentor_cell
            }
        }

        private fun showHintDialog(context: Context, messageId: Int, hintKey: String, color: String) {
            val preferencesManager = PersistDataHelper.shared
            if (preferencesManager.getCachedBoolean(hintKey)){
                return
            }

            // show hint dialog
            val builder = AlertDialog.Builder(context)
            builder.setMessage(messageId)
            builder.setCancelable(false)
            builder.setPositiveButton(R.string.onboarding_confirm_title) { _, _ ->
                preferencesManager.cacheBoolean(hintKey, true)
            }
            val dialog: AlertDialog = builder.create()
            dialog.setOnShowListener {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor(color))
            }

            dialog.show()
        }

        fun showQuestionsAnswersHint(context: Context, secretVocEnabled: Boolean, clientSlug: String, color: String) {
            if (secretVocEnabled) {
                showHintDialog(context, R.string.public_private_questions_hint, clientSlug, color)
            } else {
                showHintDialog(context, R.string.public_questions_hint, clientSlug, color)
            }
        }

        fun showConfirmationDialog(context: Context, title: Int, message: Int, positveBtnLabel: Int, onPositiveBtnClicked: () -> Unit) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setPositiveButton(positveBtnLabel) { _, _ ->

                onPositiveBtnClicked()
            }
            builder.setNegativeButton(R.string.record_discard_button_cancel, null)
            val dialog = builder.create()
            dialog.setOnShowListener { dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED) }
            dialog.show()
        }

        fun openMainView(activity: Activity, fromRegistration: Boolean = false) {
            val intent = Intent(activity, MainActivity::class.java)
            intent.putExtra(MainActivity.FROM_REGISTRATION, fromRegistration)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            activity.finish()
            activity.startActivity(intent)
            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        fun openWelcomeView(activity: Activity) {
            activity.finish()
            activity.startActivity(activity.intentFor<WelcomeActivity>().newTask().clearTask())
            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        fun makeStatusBarTransparent(activity: Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                activity.window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                activity.window.setStatusBarColor(0x00000000) // transparent
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                val flags = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                activity.window.addFlags(flags)
            }
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)

        }

        fun setMarginTop(view: View, marginTop: Int) {
            val menuLayoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
            menuLayoutParams.setMargins(0, marginTop, 0, 0)
            view.layoutParams = menuLayoutParams
        }
    }
}