package com.voctag.android.helper

class TimeHelper {

    companion object {
        fun getProgressString(progress: Int, duration: Int, withCharacters: Boolean = false): String {
            val time = (duration - progress) / 1000
            val hours = time / 3600
            val minutes = (time % 3600) / 60
            val seconds = time % 60

            return if (withCharacters) {
                when {
                    hours > 0 -> "%dh %dm %ds".format(hours, minutes, seconds)
                    minutes > 0 -> "%dm %ds".format(minutes, seconds)
                    else -> "%ds".format(seconds)
                }
            } else {
                if (hours > 0) {
                    "%d:%02d:%02d".format(hours, minutes, seconds)
                } else {
                    "%d:%02d".format(minutes, seconds)
                }
            }
        }

        fun getDurationString(duration: Int): String {
            val time = duration / 1000
            val hours = time / 3600
            val minutes = (time % 3600) / 60
            val seconds = time % 60

            return when {
                hours > 0 -> "%dh %dm %ds".format(hours, minutes, seconds)
                minutes > 0 -> "%dm %ds".format(minutes, seconds)
                else -> "%ds".format(seconds)
            }
        }
    }

}