package com.voctag.android.helper

import android.app.Activity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.model.Voc

class VocReportHelper {

    companion object {
        val shared = VocReportHelper()
    }

    private var itemClicked = 0

    private enum class ReportType {
        myvoc, copyright, racism, porn, spam, other;

        val rawValue: String
            get() {
                return when (this) {
                    myvoc -> "support"
                    copyright -> "copyright"
                    racism -> "racism"
                    porn -> "porn"
                    spam -> "spam"
                    other -> "other"
                }
            }
    }

    fun showReportDialog(activity: Activity, voc: Voc) {
        MaterialAlertDialogBuilder(activity)
                .setTitle(R.string.report_action_sheet_title)
                .setSingleChoiceItems(arrayOf<CharSequence>(activity.getString(R.string.report_type_myvoc), activity.getString(R.string.report_type_copyright), activity.getString(R.string.report_type_racism), activity.getString(R.string.report_type_porn), activity.getString(R.string.report_type_spam), activity.getString(R.string.report_type_other)), itemClicked) { _, which -> itemClicked = which }
                .setPositiveButton(R.string.report_action_sheet_title) { _, _ -> RequestHelper.shared.reportVoc(voc.vocID, ReportType.values()[itemClicked].rawValue,activity) }
                .setNegativeButton(R.string.feed_cell_action_cancel, null)
                .show();
    }
}
