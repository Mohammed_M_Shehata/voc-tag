package com.voctag.android.helper

import android.os.Build
import android.text.Html
import android.text.Spanned
import java.util.*

class Utils {

    companion object {
        fun formatPriceLocale(price: Float): String {
            return String.format(Locale.getDefault(), "%.2f", price)
        }

        fun getTextFromRawText(text: String): Spanned? {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
            else Html.fromHtml(text)
        }
    }
}