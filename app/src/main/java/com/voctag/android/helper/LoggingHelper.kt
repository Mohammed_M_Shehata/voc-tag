package com.voctag.android.helper

import android.util.Log

object LoggingHelper {
    fun logErrorForClass(errorClass: Class<*>, message: String) {
        val errorDomain = "com.voctag.error." + errorClass.name
        Log.e(errorDomain, message)
    }
}
