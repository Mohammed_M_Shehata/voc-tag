package com.voctag.android.helper

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.paginate.recycler.LoadingListItemCreator


object PaginateHelper {

    fun getLoadingListCreator(color: Int): CustomLoadingListItemCreator {
        return CustomLoadingListItemCreator(color)
    }

    fun getLoadingListCreatorVocs(): CustomLoadingListItemCreatorVocs {
        return CustomLoadingListItemCreatorVocs()
    }

    fun getLoadingListCreatorMentorList(): CustomLoadingListItemCreatorMentorList {
        return CustomLoadingListItemCreatorMentorList()
    }

    fun getLoadingListCreatorMyFeed(): CustomLoadingListItemCreatorMyFeed {
        return CustomLoadingListItemCreatorMyFeed()
    }
}

class CustomLoadingListItemCreatorVocs : LoadingListItemCreator {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(com.voctag.android.R.layout.item_custom_loading_list_vocs, parent, false)
        return PaginateViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    }
}

class CustomLoadingListItemCreatorMyFeed : LoadingListItemCreator {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(com.voctag.android.R.layout.item_custom_loading_list_my_feed, parent, false)
        return PaginateViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    }
}


class CustomLoadingListItemCreatorMentorList : LoadingListItemCreator {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(com.voctag.android.R.layout.item_custom_loading_list_mentor, parent, false)
        return PaginateViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    }
}

class CustomLoadingListItemCreator(val color: Int) : LoadingListItemCreator {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(com.voctag.android.R.layout.item_custom_loading_list, parent, false)

        val progressBar: ProgressBar = view.findViewById(com.voctag.android.R.id.progressbar);
        progressBar.indeterminateTintList = ColorStateList.valueOf(color)
        return PaginateViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    }
}

private class PaginateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}
