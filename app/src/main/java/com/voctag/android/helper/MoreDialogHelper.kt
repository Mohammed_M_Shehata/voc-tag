package com.voctag.android.helper

import android.app.Activity
import android.graphics.Color
import android.text.TextUtils
import androidx.appcompat.app.AlertDialog
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.manager.LocalVocsManager
import com.voctag.android.model.Voc
import com.voctag.android.ui.VoctagApp
import com.voctag.android.ui.main.player.PlayerActivity
import com.voctag.android.ui.main.voc.single.VocSingleActivity

class MoreDialogHelper(val activity: Activity, val voc: Voc) {

    companion object {
        fun newInstance(activity: Activity, voc: Voc): MoreDialogHelper {
            return MoreDialogHelper(activity, voc)
        }
    }

    fun showMoreDialog() {
        val builder = AlertDialog.Builder(activity)
        val items = ArrayList<CustomItem>()

        //follow
        var followTitle = VoctagApp.getTranslation(R.string.feed_detail_more_unfollow_title)
        if (!voc.following) {
            followTitle = VoctagApp.getTranslation(R.string.feed_detail_more_follow_title)
        }
        items.add(CustomItem(followTitle) {
            followVoc(activity)
        })

        if (!TextUtils.isEmpty(voc.vocAudioURL)) {
            //transcript
            if (!voc.transcript.isNullOrEmpty()){
                items.add(CustomItem(VoctagApp.getTranslation(R.string.feed_detail_more_transcript_title)) {
                    showTranscript()
                })
            }

            // download

            val downloadLabel = if (LocalVocsManager.newInstance(activity).isVocDownloaded(voc.vocID))
                VoctagApp.getTranslation(R.string.feed_detail_more_delete_downloaded_file)
            else
                VoctagApp.getTranslation(R.string.feed_detail_more_download_file)

            items.add(CustomItem(downloadLabel) {
                if (activity is VocSingleActivity)
                    activity.onDownloadBtnClicked()
                else if (activity is PlayerActivity)
                    activity.onDownloadBtnClicked()
            })
        }

        if (!voc.secret) {
            // Share
            items.add(CustomItem(VoctagApp.getTranslation(R.string.feed_detail_more_share_title)) {
                shareVoc()
            })
        }

        // Report
        items.add(CustomItem(VoctagApp.getTranslation(R.string.feed_detail_more_report_title)) {
            VocReportHelper.shared.showReportDialog(activity, voc)
        })

        // DELETE
        val user = PersistDataHelper.shared.loadUser()
        if (!voc.secret && user != null && voc.userID == user.id) {
            items.add(CustomItem(VoctagApp.getTranslation(R.string.feed_detail_more_delete_title)) {
                showDeleteDialog()
            })
        }

        builder.setItems(CustomItem.getStringArray(items).toTypedArray()) { _, which ->
            if (which < items.size) {
                items[which].action()
            }
        }

        builder.setNegativeButton(R.string.feed_cell_action_cancel, null)
        builder.create().show()
    }

    private fun showDeleteDialog() {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(R.string.delete_voc_alert_title)
        builder.setMessage(R.string.delete_voc_alert_message)
        builder.setPositiveButton(R.string.delete_voc_alert_action_delete) { _, _ ->
            RequestHelper.shared.deleteVoc(voc.vocID, { success ->
                if (success) {
                    VocSingleActivity.onDeletedVoc?.invoke()
                    activity.finish()
                }
            }, activity)
        }
        builder.setNegativeButton(R.string.delete_voc_alert_action_cancel, null)

        val dialog = builder.create()
        dialog.setOnShowListener { dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED) }
        dialog.show()
    }

    private fun shareVoc() {
        ActivityHelper.shareVocLink(activity, voc = voc)
    }

    private fun followVoc(activity: Activity) {
        if (voc.following) {
            voc.following = false
            RequestHelper.shared.unfollowVoc(voc.vocID, activity)
        } else {
            voc.following = true
            RequestHelper.shared.followVoc(voc.vocID, activity)
        }
    }

    private fun showTranscript() {
        if (voc.transcript.isEmpty()) {
            return
        }
        ActivityHelper.showMessageDialog(activity, voc.titleString, voc.attributedTranscript)
    }

    class CustomItem(val label: String, val action: () -> Unit) {
        companion object {
            fun getStringArray(items: ArrayList<CustomItem>): ArrayList<String> {

                val result = ArrayList<String>();
                for (item in items) {
                    result.add(item.label)
                }
                return result
            }
        }
    }

}