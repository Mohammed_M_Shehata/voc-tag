package com.voctag.android.helper;

import java.util.*

class LanguageHelper {

    companion object {
        fun getCurrentLanguageCode(): String {
            return if (Locale.getDefault().language == "de") {
                "de"
            } else {
                "en"
            }
        }
    }
}
