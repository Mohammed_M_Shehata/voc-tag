package com.voctag.android.ui.main.profile.settings

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.facebook.login.LoginManager
import com.google.firebase.iid.FirebaseInstanceId
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.api.firebase.AnalyticsLoggerUtils
import com.voctag.android.db.DatabaseRoomManager
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.manager.AudioService
import com.voctag.android.ui.VoctagApp
import com.voctag.android.ui.main.profile.NicknameEditActivity
import kotlinx.android.synthetic.main.activity_settings.*
import org.jetbrains.anko.find
import org.jetbrains.anko.intentFor
import java.io.IOException

class SettingsActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "SettingsActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        setupToolbar()
        setupSettings()
    }

    private fun setupToolbar() {
        val toolbar: Toolbar = find(R.id.toolbar)
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val title: TextView = toolbar.find(R.id.toolbarTitle)
        title.text = VoctagApp.getTranslation(R.string.settings_title)
    }

    private fun setupSettings() {
        //Update Nickname
        if (PersistDataHelper.shared.loadUser()?.owningChannels?.isNotEmpty()!!) {
            settingNickname.visibility = View.GONE
        } else {
            settingNickname.visibility = View.VISIBLE
        }
        settingNickname.setOnClickListener {
            startActivity(intentFor<NicknameEditActivity>())
        }

        //settings
        notification_settings.setOnClickListener {
            startActivity(Intent(this, NotificationSettingsActivity::class.java))
        }

        //Rating
        settingRating.setOnClickListener { openPlayStore() }

        //Become Mentor
        if (PersistDataHelper.shared.loadUser()?.owningChannels?.isNotEmpty()!!) {
            settingTerms0.visibility = View.GONE
        } else {
            settingTerms0.visibility = View.VISIBLE
        }
        settingTerms0.setOnClickListener { openURL(getString(R.string.voctag_web_url) + "partner") }

        //Terms
        settingTerms2.setOnClickListener {
            AnalyticsLoggerUtils.getInstance().logTermsClicked(AnalyticsLoggerUtils.SETTINGS_SCREEN)
            openURL(getString(R.string.voctag_url) + "terms")
        }

        //Privacy
        settingTerms3.setOnClickListener {
            AnalyticsLoggerUtils.getInstance().logPrivacyClicked(AnalyticsLoggerUtils.SETTINGS_SCREEN)
            openURL(getString(R.string.voctag_url) + "privacy")
        }

        //Imprint
        settingTerms4.setOnClickListener { openURL(getString(R.string.voctag_url) + "imprint") }

        //Support
        settingSupport.setOnClickListener { openSupportEmail() }

        //logout
        settingLogout.setOnClickListener {
            RequestHelper.shared.logout(successListener = {
                logoutFromFCM()
                LoginManager.getInstance().logOut()
                PersistDataHelper.shared.clearAllData(this)
                AudioService.signOut(this)
                DatabaseRoomManager.get(this, null).singOut()
            }, errorListener = { taken: Boolean, message: String ->
                settingLogout.isEnabled = true
                if (taken) {
                    Toast.makeText(this@SettingsActivity, message, Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this@SettingsActivity, message, Toast.LENGTH_LONG).show()
                }
            }, context = this@SettingsActivity)
        }
    }

    private fun openPlayStore() {
        Log.d(TAG, "Open Play Store: $packageName")
        val uri = Uri.parse("market://details?id=$packageName")
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        try {
            startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + packageName)))
        }
    }

    private fun logoutFromFCM() {
        Thread(Runnable {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId()
                FirebaseInstanceId.getInstance().instanceId
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }).start()
    }

    private fun openSupportEmail() {
        val intent = Intent(Intent.ACTION_VIEW)
        val data = Uri.parse("mailto:support@voctag.com" + "?body=" + deviceInfoForSupportMail())
        intent.data = data
        startActivity(intent)
    }

    private fun deviceInfoForSupportMail(): String {
        val model = Build.MODEL
        val version = Build.VERSION.RELEASE
        return "\n\nSupport Info\n$model - Android $version"
    }

    private fun openURL(url: String) {
        val i = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(i)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

}