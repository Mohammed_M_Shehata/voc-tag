package com.voctag.android.ui.main.publish

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.material.snackbar.Snackbar
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.api.S3Manager
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.LoggingHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.LocationManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.PresignedUrl
import com.voctag.android.model.Voc
import com.voctag.android.ui.VoctagApp
import kotlinx.android.synthetic.main.activity_publish.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor
import java.io.File

class PublishActivity : AppCompatActivity(), ActivityCompat.OnRequestPermissionsResultCallback, View.OnClickListener {

    companion object {
        val TAG: String = PublishActivity::class.java.simpleName
        var onPublished: ((secret: Boolean) -> Unit)? = null

        private const val PUBLISH_VOC_INITIAL_STATE = 100
        private const val PRESIGNED_URL_FETCHED = 101
        private const val VOC_UPLOADED_TO_S3 = 102
        private const val PUBLISH_VOC_FINAL_STATE = 103
    }

    private var amount: String? = null
    private lateinit var player: MediaPlayer
    private var isSpeak = false
    private var isSecret = false
    private var chargeUser = false
    private var audioDuration: Float = 0f
    private var nonce: String? = null
    private var presignedUrl: PresignedUrl? = null
    private var currentPublishVocState = PUBLISH_VOC_INITIAL_STATE
    private var voc: Voc? = null
    private var client: Client? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_publish)

        //get params from intent
        if (intent.hasExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION)) isSpeak = intent.getBooleanExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION, false)
        if (intent.hasExtra(VoctagKeys.EXTRA_SECRET)) isSecret = intent.getBooleanExtra(VoctagKeys.EXTRA_SECRET, false)
        if (intent.hasExtra(VoctagKeys.EXTRA_AMOUNT)) amount = intent.getStringExtra(VoctagKeys.EXTRA_AMOUNT)
        if (intent.hasExtra(VoctagKeys.EXTRA_AUDIO_DURATION)) audioDuration = intent.getFloatExtra(VoctagKeys.EXTRA_AUDIO_DURATION, 0f)

        voc = VocCreationManager.shared.parentVoc
        client = VocCreationManager.shared.parentClient

        setupToolbar()
        setupColor()
        setupPublishButton()
        setupPlayerViews()

        bindProgressButton(btn_publish_default)
        bindProgressButton(btn_publish_secretuser)
        bindProgressButton(btn_publish_secretmentor)
        bindProgressButton(btn_publish_secretmentor_charge)
        btn_publish_default.setOnClickListener(this@PublishActivity)
        btn_publish_secretuser.setOnClickListener(this@PublishActivity)
        btn_publish_secretmentor.setOnClickListener(this@PublishActivity)
        btn_publish_secretmentor_charge.setOnClickListener(this@PublishActivity)

        LocationManager.shared.getLocationIfPossible(this)
    }

    override fun onPause() {
        super.onPause()
        if (player.isPlaying) {
            player.pause()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        onPublished = null
    }

    private fun setupToolbar() {
        setSupportActionBar(tb_publish)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.elevation = 0F

        if (isSpeak) {
            tv_publish_title.text = getString(R.string.publish_title_speak)
        } else {
            if (voc != null) {
                if (isSecret) {
                    tv_publish_title.text = getString(R.string.record_title_private_reply)
                } else {
                    tv_publish_title.text = getString(R.string.publish_title_reply)
                }
            } else {
                if (isSecret) {
                    tv_publish_title.text = getString(R.string.ask_mentor_privately)
                } else {
                    tv_publish_title.text = getString(R.string.publish_title_ask)
                }
            }
        }
    }

    private fun setupColor() {
        // update color when it's not the community/open feed
        if (VocCreationManager.shared.isNotOpenFeed()) {
            val color = Color.parseColor(client?.themeColorString)
            window.statusBarColor = color
            cl_publish_layout.backgroundColor = color
            btn_publish_default.textColor = color
            if (isSecret) {
                btn_publish_secretuser.textColor = color
                btn_publish_secretmentor.textColor = color
                btn_publish_secretmentor_charge.textColor = color
            }
        }
    }

    private fun setupPublishButton() {
        if (isSecret) {
            val parentVoc = VocCreationManager.shared.parentVoc
            // if its a reply, it's an indicator that the mentor publishes
            if (parentVoc != null) {
                group_publish_secretuser.visibility = View.GONE
                group_publish_mentor.visibility = View.VISIBLE
            } else {
                group_publish_secretuser.visibility = View.VISIBLE
                group_publish_mentor.visibility = View.GONE
            }
            group_publish_default.visibility = View.GONE
        } else {
            group_publish_secretuser.visibility = View.GONE
            group_publish_mentor.visibility = View.GONE
            group_publish_default.visibility = View.VISIBLE
        }
    }

    private fun setupPlayerViews() {
        btn_publish_play.setOnClickListener {
            if (player.isPlaying) {
                player.pause()
            } else {
                player.start()
            }
            updatePlayerViews()
        }

        sb_publish_player.setup { updateSeekbar() }
        sb_publish_player.max = VocCreationManager.shared.recordDuration
        sb_publish_player.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    player.seekTo(progress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        setupMediaPlayer()
    }

    private fun setupMediaPlayer() {
        val fileURL = VocCreationManager.shared.recordedAudioFileURL()
        if (!fileURL.isNullOrEmpty() && File(fileURL).exists()) {
            player = MediaPlayer()
            player.isLooping = false
            player.setOnCompletionListener { updatePlayerViews() }
            player.setDataSource(fileURL)
            player.prepare()
        } else {
            LoggingHelper.logErrorForClass(PublishActivity::class.java, "recordURL not found")
            finish()
        }
    }

    private fun updatePlayerViews() {
        if (player.isPlaying) {
            btn_publish_play.background = VoctagApp.getDrawable(R.drawable.ic_pause_circle_filled)
        } else {
            btn_publish_play.background = VoctagApp.getDrawable(R.drawable.ic_play_circle_filled)
        }
    }

    private fun updateSeekbar() {
        sb_publish_player.progress = player.currentPosition
    }

    private fun publish(nonce: String? = null) {
        ActivityHelper.hideSoftKeyboard(this)
        if (VocCreationManager.shared.isNotOpenFeed()) {
            if (chargeUser) {
                showProgressSecretMentor(true)
                showProgressSecretUser()
            } else {
                showProgress()
                showProgressSecretMentor(false)
            }
        } else {
            showProgress()
        }

        //title
        if (et_publish_title.text.toString().isNotEmpty()) {
            VocCreationManager.shared.title = et_publish_title.text.toString()
        }

        if (isSecret) {
            this.nonce = nonce
        }

        if (VocCreationManager.shared.isNotOpenFeed()) {
            goToPublishVocNextState(false)
        } else {
            publishVoc()
        }
    }

    private fun pay() {
        ActivityHelper.payForPublishQuestion(this, amount)
    }

    private fun updatePublishVocCurrentState() {
        val prevState = currentPublishVocState
        currentPublishVocState = when (prevState) {
            PUBLISH_VOC_INITIAL_STATE -> PRESIGNED_URL_FETCHED
            PRESIGNED_URL_FETCHED -> VOC_UPLOADED_TO_S3
            else -> PUBLISH_VOC_FINAL_STATE
        }
    }

    private fun goToPublishVocNextState(updateVocCurrentState: Boolean = true) {
        if (updateVocCurrentState) {
            updatePublishVocCurrentState()
        }

        when (currentPublishVocState) {
            PUBLISH_VOC_INITIAL_STATE -> getPresignedUrl()
            PRESIGNED_URL_FETCHED -> uploadAudioToS3()
            VOC_UPLOADED_TO_S3 -> publishVoc()
        }
    }

    private fun getPresignedUrl() {
        val client = VocCreationManager.shared.parentClient!!
        RequestHelper.shared.getPresignedUrl(client.slug, { presignedUrl: PresignedUrl ->
            Log.d(TAG, "presigned url> ${presignedUrl.url}/${presignedUrl.urlFields?.key} - ${presignedUrl.urlFields?.acl}")
            onFetchPresignedUrlListener.invoke(true, presignedUrl)
        }, {
            onFetchPresignedUrlListener.invoke(false, null)
        }, this)
    }

    private fun uploadAudioToS3() {
        S3Manager.uploadFile(this, presignedUrl as PresignedUrl,
                File(VocCreationManager.shared.audioRecorder.recordAudioURL),
                object : S3Manager.OnUploadListener {
                    override fun onError(message: String?) {
                        onUploadVocToS3Listener.invoke(false)
                    }

                    override fun onComplete() {
                        onUploadVocToS3Listener.invoke(true)
                    }

                    override fun onProgress(percentage: Int) {
                        Log.i(TAG, "progress for uploading file> $percentage")
                    }
                })
    }

    private fun publishVoc() {
        val fileUrl = "${presignedUrl?.url}/${presignedUrl?.urlFields?.key}".takeIf { presignedUrl != null }
        publishVoc(fileUrl) { success ->
            if (success) {
                VocCreationManager.shared.onPublished?.invoke()
                onPublished?.invoke(showCongratulationDialog())
                finish()
            } else {
                hideProgress()
                hideProgressSecretUser()
                hideProgressScretMentor(false)
                hideProgressScretMentor(true)
                onPublishingError()
            }
        }
    }

    private fun publishVoc(fileUrl: String?, completionListener: (success: Boolean) -> Unit) {
        val reply = VocCreationManager.shared.parentVoc != null
        val params = VocCreationManager.shared.publishingParameters(fileUrl, audioDuration, isSecret, reply, chargeUser, nonce, isSpeak)
        val successListener: (voc: Voc) -> Unit = { voc ->
            val title = voc.shareTitleString
            VocCreationManager.shared.reset()
            completionListener(true)
        }

        if (VocCreationManager.shared.isNotOpenFeed()) {
            val client = VocCreationManager.shared.parentClient as Client
            RequestHelper.shared.uploadClientVoc(client.slug, params, successListener, {
                completionListener(false)
            }, this@PublishActivity)
        } else {
            RequestHelper.shared.uploadVoc(params, successListener, {
                completionListener(false)
            }, this@PublishActivity)
        }
    }

    private fun showCongratulationDialog(): Boolean {
        return isSecret && chargeUser
    }

    private fun onPublishingError() {
        val errorMessage = if (isSpeak) getString(R.string.pulishing_speak_error_message) else getString(R.string.pulishing_question_error_message)
        Snackbar.make(cl_publish_layout, errorMessage, Snackbar.LENGTH_LONG).show()
    }

    private val onUploadVocToS3Listener = { success: Boolean ->
        if (success) {
            goToPublishVocNextState()
        } else {
            onPublishingError()
            hideProgress()
        }
    }

    private val onFetchPresignedUrlListener = { success: Boolean, presignedUrl: PresignedUrl? ->
        if (success) {
            this.presignedUrl = presignedUrl
            goToPublishVocNextState()
        } else {
            onPublishingError()
            hideProgress()
        }
    }

    private fun showProgress() {
        btn_publish_default.isEnabled = false
        if (client != null) {
            btn_publish_default.showProgress {
                progressColor = Color.parseColor(client?.themeColorString)
                textMarginRes = R.dimen.keyline_0
            }
        } else {
            btn_publish_default.showProgress {
                progressColorRes = R.color.main
                textMarginRes = R.dimen.keyline_0
            }
        }

    }

    private fun hideProgress() {
        btn_publish_default.isEnabled = true
        btn_publish_default.hideProgress(R.string.publish_button)
    }

    private fun showProgressSecretUser() {
        btn_publish_secretuser.isEnabled = false
        btn_publish_secretuser.showProgress {
            progressColor = Color.parseColor(client?.themeColorString)
            textMarginRes = R.dimen.keyline_0
        }
    }

    private fun hideProgressSecretUser() {
        btn_publish_secretuser.isEnabled = true
        btn_publish_secretuser.hideProgress(R.string.send_question_charge_description)
    }

    private fun showProgressSecretMentor(charge: Boolean) {
        if (charge) {
            btn_publish_secretmentor_charge.isEnabled = false
            btn_publish_secretmentor_charge.showProgress {
                progressColor = Color.parseColor(client?.themeColorString)
                textMarginRes = R.dimen.keyline_0
            }
        } else {
            btn_publish_secretmentor.isEnabled = false
            btn_publish_secretmentor.showProgress {
                progressColor = Color.parseColor(client?.themeColorString)
                textMarginRes = R.dimen.keyline_0
            }
        }

    }

    private fun hideProgressScretMentor(charge: Boolean) {
        if (charge) {
            btn_publish_secretmentor_charge.isEnabled = true
            btn_publish_secretmentor_charge.hideProgress(R.string.send_answer_charge_user)
        } else {
            btn_publish_secretmentor.isEnabled = true
            btn_publish_secretmentor.hideProgress(R.string.send_answer_don_t_charge_user)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LocationManager.LOCATION_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                LocationManager.shared.getLocationIfPossible(this)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ActivityHelper.onPaymentActivityResult(requestCode, resultCode, data, ::publish)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_publish_default -> {
                publish()
            }
            R.id.btn_publish_secretuser -> {
                chargeUser = true
                pay()
            }
            R.id.btn_publish_secretmentor -> {
                chargeUser = false
                publish()
            }
            R.id.btn_publish_secretmentor_charge -> {
                chargeUser = true
                publish()
            }
        }
    }
}
