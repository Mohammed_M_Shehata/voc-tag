package com.voctag.android.ui.main.community

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.paginate.Paginate
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.PaginateHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.FeedFilterManager
import com.voctag.android.manager.OnboardingManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.MainActivity
import kotlinx.android.synthetic.main.fragment_community.*
import kotlinx.android.synthetic.main.fragment_community.view.*
import kotlinx.android.synthetic.main.layout_audio_visualizer.*


open class CommunityFragment : Fragment(), View.OnClickListener, Paginate.Callbacks {

    private lateinit var mainActivity: MainActivity
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: CommunityAdapter
    private var vocs = ArrayList<Voc>()
    private lateinit var paginate: Paginate
    private var loading = false
    private var allItemsLoaded = false
    private var shouldReload = false
    private var pageIndex: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity = activity as MainActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_community, container, false)

        // init the swipe to refresh
        swipeRefreshLayout = view.srl_community
        swipeRefreshLayout.setColorSchemeResources(R.color.main)
        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            reloadData()
        }

        // init adapter and recyclerview for vocs
        linearLayoutManager = LinearLayoutManager(mainActivity, LinearLayoutManager.VERTICAL, false)
        adapter = CommunityAdapter(mainActivity, vocs)
        recyclerView = view.rv_community
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter

        // view should reloaded when voc is beeing deleted
        adapter.onDeletedVoc = {
            shouldReload = true
        }

        // pagination
        paginate = Paginate.with(recyclerView, this)
                .setLoadingTriggerThreshold(2)
                .setLoadingListItemCreator(PaginateHelper.getLoadingListCreatorVocs())
                .build()
        paginate.setHasMoreDataToLoad(!allItemsLoaded)

        // init click listeners
        view.btn_community_add.setOnClickListener(this@CommunityFragment)
        view.btn_community_search.setOnClickListener(this@CommunityFragment)

        // show onboarding
        showOnBoardingDialog()

        return view
    }

    override fun onResume() {
        super.onResume()
        adapter.updateOnPlaylistDidChange()

        VocCreationManager.shared.parentClient = null
        VocCreationManager.shared.parentVoc = null
        VocCreationManager.shared.onPublished = {
            shouldReload = true
        }

        // Reload when flag is set
        if (shouldReload) {
            reloadData()
        }

        mainActivity.addVisualizer(layout_visualizer)
    }

    private fun showOnBoardingDialog() {
        if (!OnboardingManager.shared.hasUserSeenOnboarding(OnboardingManager.OnboardingType.Community)) {
            MaterialAlertDialogBuilder(this.context)
                    .setMessage(getString(R.string.onboarding_text_community))
                    .setPositiveButton(getString(R.string.onboarding_confirm_title), null)
                    .show();

            OnboardingManager.shared.userSeenOnboarding(OnboardingManager.OnboardingType.Community)
        }
    }

    private fun loadMore(isCompleteReload: Boolean) {
        loading = true
        val parameters = FeedFilterManager.shared.searchParametersForCurrentFilter(null, pageIndex)
        RequestHelper.shared.searchVocs(parameters, { response, errorHappened ->
            swipeRefreshLayout.isRefreshing = false
            loading = false
            if (cl_community_layout != null) {
                if (response != null) {
                    pageIndex++
                    if (isCompleteReload) {
                        vocs = response
                    } else {
                        vocs.addAll(response)
                    }

                    adapter.setVocs(vocs)
                }

                if (!errorHappened && response.isNullOrEmpty()) {
                    allItemsLoaded = true
                    adapter.notifyDataSetChanged()
                }
                if (errorHappened) {
                    Snackbar.make(cl_community_layout, getString(R.string.community_no_data_snackbar), Snackbar.LENGTH_LONG).show()
                }
            }
        }, context = activity as? Activity, myFeeds = false)
    }

    private fun showSearchView() {
        val intent = Intent(mainActivity, CommunitySearchActivity::class.java).apply {
            putExtra(VoctagKeys.EXTRA_VOCS, vocs)
        }
        mainActivity.startActivity(intent)
        mainActivity.overridePendingTransition(0, 0)
    }

    private fun reloadData() {
        pageIndex = 1
        allItemsLoaded = false
        loadMore(true)
    }

    fun getPlayList(): ArrayList<Voc> {
        return vocs
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_community_add -> VocCreationManager.shared.openAskReplyView(mainActivity)
            R.id.btn_community_search -> showSearchView()
        }
    }

    override fun onLoadMore() {
        loadMore(false)
    }

    override fun isLoading(): Boolean {
        return loading
    }

    override fun hasLoadedAllItems(): Boolean {
        return allItemsLoaded
    }
}

