package com.voctag.android.ui.main.mentor.list

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.google.android.material.snackbar.Snackbar
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.UiUtils
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.FeedFilterManager
import com.voctag.android.model.Client
import kotlinx.android.synthetic.main.activity_mentor_search.*


class MentorSearchActivity : AppCompatActivity() {

    private lateinit var mentorListAdapter: MentorListAdapter
    private var members: ArrayList<Client>? = null
    private var clients: ArrayList<Client>? = null
    private var topTen: ArrayList<Client>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mentor_search)

        members = intent.getSerializableExtra(VoctagKeys.EXTRA_MEMBER_CLIENTS) as ArrayList<Client>
        clients = intent.getSerializableExtra(VoctagKeys.EXTRA_CLIENTS) as ArrayList<Client>
        topTen = intent.getSerializableExtra(VoctagKeys.EXTRA_TOP_CLIENTS) as ArrayList<Client>

        setupToolbar()
        showEmptyViewIfNecessary(false)

        //Set the adapter
        mentorListAdapter = MentorListAdapter(this)
        mentorListAdapter.update(members!!, clients!!, topTen!!)
        mentorListAdapter.notifyDataSetChanged()
        recyclerviewMentorsearch.adapter = mentorListAdapter

        //searchview settings
        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
        searchView.setIconifiedByDefault(false)
        searchView.isFocusable = true
        searchView.requestFocusFromTouch()
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                if (s.length > 1) {
                    search(s)
                } else {
                    if (members != null && clients != null && topTen != null){
                        notifyClientsList(members!!, clients!!, topTen!!)
                    }
                }
                return true
            }
        })
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbarLoadingView.visibility = View.GONE
    }

    private fun showEmptyViewIfNecessary(empty: Boolean) {
        if (empty) {
            emptyviewMentorsearch.visibility = View.VISIBLE
        } else {
            emptyviewMentorsearch.visibility = View.GONE
        }
    }

    private fun search(query: String) {
        searchClients(query)
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            toolbarLoadingView.visibility = View.VISIBLE
        } else {
            toolbarLoadingView.visibility = View.GONE
        }
    }

    private fun searchClients(query: String) {
        showLoading(true)

        val parameters = FeedFilterManager.shared.searchParametersForSearchClients(query)
        RequestHelper.shared.searchClients(parameters, { clients, errorHappened, categoryName ->
            showLoading(false)

            if (clients != null) {
                setClients(clients)
            }
            if (errorHappened) {
                UiUtils.showSnackbar(this.layout_mentor_search, getString(R.string.nointernet), Snackbar.LENGTH_LONG)
            }
        }, this@MentorSearchActivity)
    }

    private fun setClients(searchClients: ArrayList<Client>) {
        val members = ArrayList<Client>()
        val topTen = ArrayList<Client>()
        val others = ArrayList<Client>()
        for (client in searchClients) {
            var isAdded = false
            if (this.topTen?.indexOf(client).let { it != null && it >= 0 }) {
                topTen.add(client)
                isAdded = true
            }
            if (client.following and !client.isConference) {
                members.add(client)
                isAdded = true
            }
            if (!isAdded) {
                others.add(client)
            }
        }
        notifyClientsList(members, others, topTen)
    }

    private fun notifyClientsList(members: ArrayList<Client>, others: ArrayList<Client>, topTen: ArrayList<Client>) {
        mentorListAdapter.update(members, others, topTen)
        mentorListAdapter.notifyDataSetChanged()
        showEmptyViewIfNecessary(members.isEmpty() && others.isEmpty() && topTen.isEmpty())
    }

    private fun close() {
        finish()
        overridePendingTransition(0, 0)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        close()
    }
}
