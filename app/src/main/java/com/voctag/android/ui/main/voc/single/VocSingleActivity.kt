package com.voctag.android.ui.main.voc.single

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.text.util.Linkify
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.appbar.AppBarLayout
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.ConnectivityHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.DownloadVocsHelper
import com.voctag.android.manager.LocalVocsManager
import com.voctag.android.manager.PlayListHelper
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.PaymentInfo
import com.voctag.android.model.User
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.MainActivity
import com.voctag.android.ui.VoctagApp
import kotlinx.android.synthetic.main.activity_voc_single.*


class VocSingleActivity : AppCompatActivity(), View.OnClickListener {

    // MARK: - Reactive Variables

    companion object {
        var onDeletedVoc: (() -> Unit)? = null
        val TAG: String = VocSingleActivity::class.java.simpleName
        const val FINISH_FEED_DETAILS: String = "FINISH_FEED_DETAILS"
    }

    private val finishBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            this@VocSingleActivity.finish()
        }
    }

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: VocRepliesAdapter
    private lateinit var voc: Voc
    private var vocId: Int = 0
    private lateinit var client: Client
    private lateinit var clientSlug: String
    private lateinit var currentUser: User
    private var shouldScrollToVoc: Voc? = null
    private var vocDetail: Voc? = null
    private var shouldReload = false
    private var willOpenVoting = false
    private var secret: Boolean = false
    private lateinit var downloadVocsHelper: DownloadVocsHelper
    private lateinit var vocSingleHeader: VocSingleHeader

    private val playListHelper = PlayListHelper(object : PlayListHelper.PlayListListener {
        override fun isAutoPlayNextEnabled(): Boolean {
            return false
        }

        override fun getPlayList(vocId: Int): ArrayList<Voc> {
            return (if (vocId != voc.vocID) {
                getVocAudioReplies()
            } else ArrayList())
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voc_single)

        // Deeplink
        val data = intent.data
        if (data != null) {
            val params = data.pathSegments
            if (params != null) {
                val vocID = Integer.valueOf(params[1])
                voc = Voc(vocID)
            }
        }

        // get params from intent
        if (intent.hasExtra(VoctagKeys.EXTRA_VOC)) voc = Voc.decode(intent.getStringExtra(VoctagKeys.EXTRA_VOC))
        if (intent.hasExtra(VoctagKeys.EXTRA_SHOULDSCROLLTOVOC)) shouldScrollToVoc = Voc.decode(intent.getStringExtra(VoctagKeys.EXTRA_SHOULDSCROLLTOVOC))
        if (intent.hasExtra(VoctagKeys.EXTRA_VOC_ID)) {
            vocId = Integer.parseInt(intent.getStringExtra(VoctagKeys.EXTRA_VOC_ID))
        }
        if (intent.hasExtra(VoctagKeys.EXTRA_CLIENT_SLUG)) {
            clientSlug = intent.getStringExtra(VoctagKeys.EXTRA_CLIENT_SLUG)
        }

        if (!this::voc.isInitialized) {
            voc = Voc()
            voc.vocID = vocId
            if (voc.parentClient != null) {
                val client = voc.parentClient as Client
                clientSlug = client.slug
            }
        }

        if (!this::clientSlug.isInitialized) {
            if (voc.parentClient != null) {
                val client = voc.parentClient as Client
                clientSlug = client.slug
            }

        }

        // set the swipe to refresh
        swipeRefreshLayout = srl_vocsingle
        swipeRefreshLayout.setOnRefreshListener { loadData() }

        // Set the adapter and the recyclerview
        adapter = VocRepliesAdapter(this, getReplies(), voc.parentClient)
        adapter.onDeletedVoc = {
            shouldReload = true
        }
        recyclerView = rv_vocsingle_replies
        recyclerView.adapter = adapter

        //set on click listener
        btn_vocsingle_ask.setOnClickListener(this)
        btn_vocsingle_share.setOnClickListener(this)

        // setup download
        LocalBroadcastManager.getInstance(this).registerReceiver(finishBroadcastReceiver, IntentFilter(FINISH_FEED_DETAILS))
        downloadVocsHelper = DownloadVocsHelper(this, btn_vocsingle_download, tv_vocsingle_downloadprogress)

        //setup views
        setupToolbar()
        setupViews()
        setupColors()
    }

    override fun onResume() {
        super.onResume()
        adapter.updateOnPlaylistDidChange()
        updateSharedParentsData()
        VocCreationManager.shared.onPublished = {
            shouldReload = true
        }

        if (secret) {
            secret = false
            ActivityHelper.showCongratulationDialog(this
                    , getString(R.string.congratulation)
                    , getString(R.string.you_have_just_earned, voc.paymentInfo?.clientEarnedFormatted)
                    , getString(R.string.cool))
        }

        if (vocDetail == null || shouldReload) {
            loadData()
        } else {
            setVoc(vocDetail!!)
        }

        playListHelper.onResume(this)
    }

    override fun onStart() {
        super.onStart()
        playListHelper.addVisualizer(layout_audio_visualizer)
    }

    override fun onStop() {
        super.onStop()
        downloadVocsHelper.onStop()
        playListHelper.onStop(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        onDeletedVoc = null
        LocalBroadcastManager.getInstance(this).unregisterReceiver(finishBroadcastReceiver)
    }

    private fun setupToolbar() {
        setSupportActionBar(tb_vocsingle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.elevation = 0F

        abl_vocsingle.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
            swipeRefreshLayout.isEnabled = verticalOffset == 0
            val total = abl_vocsingle.height - tb_vocsingle.height
            val offset = (verticalOffset + total).toFloat() / total
            if (offset < 0.15) {
                btn_vocsingle_play.visibility = View.INVISIBLE
            } else {
                if (!voc.vocAudioURL.isNullOrEmpty()) {
                    btn_vocsingle_play.visibility = View.VISIBLE
                    btn_vocsingle_play.setVoc(voc)
                }
            }
        })

        // download
        if (!TextUtils.isEmpty(voc.vocAudioURL)) {
            rl_vocsingle_download.visibility = View.VISIBLE
        } else {
            rl_vocsingle_download.visibility = View.GONE
        }
    }

    private fun setupToolbarButtons() {
        if (!voc.secret) {
            btn_vocsingle_share.visibility = View.VISIBLE
        } else {
            btn_vocsingle_share.visibility = View.GONE
        }
        downloadVocsHelper.onResume(voc)
    }

    private fun setupViews() {
        val currentUser = PersistDataHelper.shared.loadUser()!!

        //set header info
        updateVocSingleHeader()

        //setup replies
        updateReplies(voc)

        // setup playbutton
        setupPlaybutton()

        this.voc.parentClient?.apply {
            // setup scret text
            if ((!voc.secret) || (voc.secret && currentUser.isOwnerOf(slug) && PaymentInfo.isQuestionStillActive(voc.paymentInfo?.paymentStatus))) {
                btn_vocsingle_ask.visibility = View.VISIBLE
            } else {
                btn_vocsingle_ask.visibility = View.GONE
            }

            if (!voc.secret) {
                tv_vocsingle_emptyview.text = getString(R.string.feed_detail_no_replies)
            } else {
                if (PaymentInfo.isQuestionStillActive(voc.paymentInfo?.paymentStatus)) {
                    if (currentUser.isOwnerOf(slug)) {
                        tv_vocsingle_private.text = getBoldTextFromHtml(getString(R.string.not_answered_yet, voc.paymentInfo?.remaningToBlock, voc.paymentInfo?.clientEarnedFormatted))
                    } else {
                        tv_vocsingle_private.text = getBoldTextFromHtml(getString(R.string.waiting_reply, voc.clientName, voc.paymentInfo?.remaningToBlock))
                    }

                } else if (voc.paymentInfo?.paymentStatus.equals(PaymentInfo.PAYMENT_STATUS_ANSWERED)) {
                    if (currentUser.isOwnerOf(slug)) {
                        tv_vocsingle_private.text = getBoldTextFromHtml(getString(R.string.mentor_question_answered, voc.paymentInfo?.clientEarnedFormatted)).takeIf { voc.paymentInfo?.charged!! }
                                ?: getBoldTextFromHtml(getString(R.string.mentor_question_answered_not_charged))
                    } else {
                        tv_vocsingle_private.text = getBoldTextFromHtml(getString(R.string.answer_received)).takeIf { voc.paymentInfo?.charged!! }
                                ?: getBoldTextFromHtml(getString(R.string.answer_received_not_charged, voc.clientName))
                        Linkify.addLinks(tv_vocsingle_private, Linkify.EMAIL_ADDRESSES)
                    }
                } else {
                    if (currentUser.isOwnerOf(slug)) {
                        tv_vocsingle_private.text = getBoldTextFromHtml(getString(R.string.mentor_question_timed_out))
                    } else {
                        tv_vocsingle_private.text = getBoldTextFromHtml(getString(R.string.question_timed_out, voc.clientName))
                    }
                }
            }
        }
    }

    private fun setupPlaybutton() {
        btn_vocsingle_play.setVoc(voc)
        if (!voc.vocAudioURL.isNullOrEmpty()) {
            btn_vocsingle_play.visibility = View.VISIBLE
        } else {
            btn_vocsingle_play.visibility = View.GONE
        }
    }

    private fun updateReplies(voc: Voc) {
        val count = voc.repliesCount
        when {
            count == 1 -> {
                tv_vocsingle_replies.text = getString(R.string.voc_single_replies_text_one, count)
            }
            count > 1 -> {
                tv_vocsingle_replies.text = getString(R.string.voc_single_replies_text_multiple, count)
            }
            else -> {
                tv_vocsingle_replies.visibility = View.INVISIBLE
            }
        }
    }

    private fun setupColors() {
        // set swipe to refresh color
        if (voc.isNotOpenFeed()) {
            val color = Color.parseColor(voc.parentClient!!.themeColorString)
            swipeRefreshLayout.setColorSchemeColors(color)
            window.statusBarColor = color
            tb_vocsingle.setBackgroundColor(color)
            btn_vocsingle_ask.background.setTint(color)
        } else {
            swipeRefreshLayout.setColorSchemeColors(VoctagApp.getColor(R.color.main))
        }
    }

    private fun updateVocSingleHeader() {
        cl_vocsingle.post {
            vocSingleHeader = VocSingleHeader(cl_vocsingle)
            vocSingleHeader.setup(voc, this)
        }
    }

    private fun getBoldTextFromHtml(waitingAnswerText: String): Spanned? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(waitingAnswerText, Html.FROM_HTML_MODE_LEGACY)
        else Html.fromHtml(waitingAnswerText)
    }

    private fun updateSharedParentsData() {
        VocCreationManager.shared.parentClient = voc.parentClient
        VocCreationManager.shared.parentVoc = voc
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let {
            if (intent.hasExtra(VoctagKeys.EXTRA_VOC)) {
                val newVoc = Voc.decode(intent.getStringExtra(VoctagKeys.EXTRA_VOC))
                if (newVoc.vocID != voc.vocID) {
                    ActivityHelper.openVoc(this, newVoc, null, onDeletedVoc)
                } else {
                    shouldReload = true
                }
            }
        }
    }

    private fun getReplies(): ArrayList<Voc> {
        val replies = ArrayList<Voc>()
        if (vocDetail != null) {
            replies.addAll(vocDetail!!.replies)
        }
        return replies
    }

    private fun loadData() {
        swipeRefreshLayout.isRefreshing = true
        // if offline use the offline voc
        if (!ConnectivityHelper.isOnline(this)) {
            LocalVocsManager(this).getVocById(voc.vocID) { voc ->
                if (voc != null && voc.isNotOpenFeed()) {
                    updateViewAfterClientVocLoaded(voc)
                } else {
                    setVoc(voc)
                }
            }
        } else if (voc.isNotOpenFeed() || this::clientSlug.isInitialized) {
            RequestHelper.shared.loadClientVocDetail(clientSlug, voc.vocID, { voc ->
                updateViewAfterClientVocLoaded(voc)
            }, this@VocSingleActivity)
        } else {
            RequestHelper.shared.loadVocDetail(voc.vocID, { voc ->
                updateViewAfterClientVocLoaded(voc)
            }, this@VocSingleActivity)
        }
    }

    private fun setVoc(voc: Voc?) {
        vocDetail = voc
        shouldReload = false
        swipeRefreshLayout.isRefreshing = false
        val replies = getReplies()
        adapter.update(replies)

        if (replies.count() == 0) {
            recyclerView.visibility = View.GONE
        } else {
            recyclerView.visibility = View.VISIBLE
        }
        showEmptyViewIfNecessary()
        scrollToVocIfNecessary()
        setupToolbarButtons()
        setupViews()

        // TODO move to repository
        if (ConnectivityHelper.isOnline(this)) {
            voc?.let { LocalVocsManager(this).updateCurrentVocIfInDatabase(voc) }
        }
    }

    private fun updateViewAfterClientVocLoaded(voc: Voc?) {
        swipeRefreshLayout.isRefreshing = false
        voc?.let {
            this.voc = voc
            updateSharedParentsData()
            setupToolbar()
            setupViews()
            setupColors()
            handleFollowClient()
        }
        setVoc(voc)
    }

    private fun handleFollowClient() {
        val client = voc.parentClient
        if (!voc.vocClientMember && client != null) {
            RequestHelper.shared.followClient(client.slug, { success ->
                Log.d(TAG, "handle follow success")
            }, this)
        }
    }

    private fun scrollToVocIfNecessary() {
        if (vocDetail == null || vocDetail!!.replies.isEmpty() || shouldScrollToVoc == null) {
            return
        }

        rv_vocsingle_replies.scrollToPosition(getRowForVoc(shouldScrollToVoc!!))
        shouldScrollToVoc = null
    }

    private fun getRowForVoc(voc: Voc): Int {
        var i = 0
        for (v in vocDetail!!.replies) {
            if (v.vocID == voc.vocID) {
                break
            } else {
                i += 1
            }
        }
        return i
    }

    private fun getVocAudioReplies(): ArrayList<Voc> {
        val vocReplies = ArrayList<Voc>()
        vocDetail?.replies?.forEach { reply ->
            reply.takeUnless { reply.vocAudioURL.isNullOrEmpty() }?.let {
                vocReplies.add(it)
            }
        }
        return vocReplies
    }

    private fun showEmptyViewIfNecessary() {
        vocDetail?.let { voc ->
            if (!voc.secret && voc.replies.isEmpty()) {
                tv_vocsingle_emptyview.visibility = View.VISIBLE
            } else {
                tv_vocsingle_emptyview.visibility = View.GONE
            }
            if (voc.secret) {
                tv_vocsingle_private.visibility = View.VISIBLE
            } else {
                tv_vocsingle_private.visibility = View.GONE
            }
        }
    }

    fun onDownloadBtnClicked() {
        downloadVocsHelper.onDownloadBtnClicked()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isTaskRoot) {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        secret = data?.getBooleanExtra(VoctagKeys.EXTRA_SECRET, false) ?: false
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_vocsingle_ask -> {
                if (voc.secret) {
                    VocCreationManager.shared.openRecordView(this, secret = true)
                } else {
                    VocCreationManager.shared.openAskReplyView(this)
                }
            }
            R.id.btn_vocsingle_share -> {
                ActivityHelper.shareVocLink(this, voc = voc)
            }
        }
    }

}
