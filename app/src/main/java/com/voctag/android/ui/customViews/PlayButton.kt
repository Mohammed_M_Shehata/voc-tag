package com.voctag.android.ui.customViews

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.voctag.android.R
import com.voctag.android.manager.AudioPlayer
import com.voctag.android.manager.PlaylistManager
import com.voctag.android.model.Voc
import com.voctag.android.ui.VoctagApp
import kotlinx.android.synthetic.main.view_playbutton.view.*
import org.jetbrains.anko.backgroundDrawable

class PlayButton : FrameLayout {

    private var voc: Voc? = null

    constructor(context: Context) : super(context) {
        setup()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setup()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setup()
    }

    private fun setup() {
        val view = View.inflate(context, R.layout.view_playbutton, this)
        view.setOnClickListener {
            LocalBroadcastManager.getInstance(context).sendBroadcast(PlaylistManager.getPlayIntent(voc!!))
        }
    }

    fun setVoc(voc: Voc) {
        this.voc = voc
        if (voc.isNotOpenFeed()) {
            val parentClient = voc.parentClient
            if(parentClient != null){
                iv_playbutton_background.backgroundTintList = ColorStateList.valueOf(Color.parseColor(parentClient.themeColorString))
            }
        } else {
            iv_playbutton_background.backgroundTintList = null
            iv_playbutton_background.backgroundDrawable = VoctagApp.getDrawable(R.drawable.playbutton_gradient)
        }

        updateButtonState()
    }

    private fun updateButtonState() {
        if (voc == null) {
            hideDownloadAnimation()
            iv_playbutton_icon.background = VoctagApp.getDrawable(R.drawable.ic_play)
            return
        }

        if (AudioPlayer.shared.isPreparingVoc(voc!!.vocID)) {
            showDownloadAnimation()
        } else {
            hideDownloadAnimation()
            if (AudioPlayer.shared.isPlayingVoc(voc!!.vocID)) {
                iv_playbutton_icon.background = VoctagApp.getDrawable(R.drawable.ic_pause)
            } else {
                iv_playbutton_icon.background = VoctagApp.getDrawable(R.drawable.ic_play)
            }
        }
    }

    private fun showDownloadAnimation() {
        pb_playbutton_progress.visibility = View.VISIBLE
        iv_playbutton_icon.visibility = View.GONE
    }

    private fun hideDownloadAnimation() {
        pb_playbutton_progress.visibility = View.GONE
        iv_playbutton_icon.visibility = View.VISIBLE
    }

}
