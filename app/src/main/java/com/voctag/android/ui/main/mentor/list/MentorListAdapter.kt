package com.voctag.android.ui.main.mentor.list

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.voctag.android.R
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Client
import com.voctag.android.model.User
import com.voctag.android.ui.VoctagApp
import org.jetbrains.anko.find

class MentorListAdapter(private val activity: Activity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    // MARK: - Reactive Variables

    var onFollowingChanged: ((client: Client) -> Unit)? = null

    // MARK: - ClientListAdapter

    private class DividerCellViewHolder(view: View, text: String) : RecyclerView.ViewHolder(view) {
        val titleTextView: TextView = view.find(R.id.textview)
        init {
            titleTextView.text = text
        }
    }

    private val DIVIDER_VIEW_NEW_CONGRESSES = 0
    private val DIVIDER_VIEW_MYCLIENTS = 1
    private val DIVIDER_VIEW_SUGGESTIONS = 2
    private val DIVIDER_VIEW_OWNER_CLIENTS = 3
    private val DIVIDER_VIEW_TOP_CLIENTS = 4
    private val CLIENT_VIEW_CELL = 5
    private val CONGRESSES_VIEW_CELL = 6

    private var pageIndex = 1
    private var clients = ArrayList<Client>()
    private var members = ArrayList<Client>()
    private var topTen = ArrayList<Client>()
    private var ownerClients = ArrayList<Any>()
    private var followingClients = ArrayList<Any>()
    private var topClients = ArrayList<Any>()
    private var otherClients = ArrayList<Any>()
    private var conferenceList = ArrayList<Any>()
    private var completeArray = ArrayList<Any>()

    private val user: User = PersistDataHelper.shared.loadUser()!!

    override fun getItemViewType(position: Int): Int {
        val obj = completeArray[position]
        return if (obj is String) {
            if (obj == VoctagKeys.PLACEHOLDER_DIVIDER_OWNER_CLIENTS) {
                DIVIDER_VIEW_OWNER_CLIENTS
            } else if (obj == VoctagKeys.PLACEHOLDER_DIVIDER_MYCLIENTS) {
                DIVIDER_VIEW_MYCLIENTS
            } else if (obj == VoctagKeys.PLACEHOLDER_DIVIDER_SUGGESTIONS) {
                DIVIDER_VIEW_SUGGESTIONS
            } else if (obj == VoctagKeys.PLACEHOLDER_DIVIDER_NEW_CONGRESSES) {
                DIVIDER_VIEW_NEW_CONGRESSES
            } else {
                DIVIDER_VIEW_TOP_CLIENTS
            }
        } else if (obj is Client) {
            if (obj.isConference) {
                CONGRESSES_VIEW_CELL
            } else {
                CLIENT_VIEW_CELL
            }
        } else {
            super.getItemViewType(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            DIVIDER_VIEW_MYCLIENTS -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mentor_list_devider, parent, false)
                DividerCellViewHolder(view, VoctagApp.getTranslation(R.string.community_divider_myclients))
            }
            DIVIDER_VIEW_SUGGESTIONS -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mentor_list_devider, parent, false)
                DividerCellViewHolder(view, VoctagApp.getTranslation(R.string.community_divider_suggestions))
            }
            DIVIDER_VIEW_OWNER_CLIENTS -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mentor_list_devider, parent, false)
                DividerCellViewHolder(view, VoctagApp.getTranslation(R.string.my_mentoring_community))
            }
            DIVIDER_VIEW_TOP_CLIENTS -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mentor_list_devider, parent, false)
                DividerCellViewHolder(view, VoctagApp.getTranslation(R.string.top_mentors))
            }
            DIVIDER_VIEW_NEW_CONGRESSES -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mentor_list_devider, parent, false)
                DividerCellViewHolder(view, VoctagApp.getTranslation(R.string.new_congresses))
            }
            CLIENT_VIEW_CELL -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mentor_list, parent, false)
                MentorListViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mentor_list_congress, parent, false)
                MentorListConferenceViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is MentorListConferenceViewHolder) {
            val client = completeArray[position] as Client

            val lastElement = listOf(ownerClients.size - 1,
                    ownerClients.size + followingClients.size - 1,
                    ownerClients.size + followingClients.size + topClients.size - 1)
                    .contains(position)

            viewHolder.setup(client, isOwner(client), activity, lastElement)
            viewHolder.onPressedViewHolder = {
                client.newVocsCount = 0
                notifyItemChanged(position)
                if (client.isConference) {
                    ActivityHelper.openConference(activity, client, onFollowingChanged)
                } else {
                    ActivityHelper.openClient(activity, client, onFollowingChanged)
                }
            }
            viewHolder.onFollowingChanged = { client ->
                onFollowingChanged(client)
                notifyDataSetChanged()
            }
        }

        if (viewHolder is DividerCellViewHolder) {
            if (position == 0) {
                viewHolder.titleTextView.setPadding(0, 0, 0, 0)
            } else {
                viewHolder.titleTextView.setPadding(0, activity.resources.getDimension(R.dimen.margin_15dp).toInt(), 0, 0)
            }
        }
    }

    fun onFollowingChanged(client: Client) {
        updateMembers(client)
        updateArrays()
    }

    private fun updateMembers(client: Client) {
        if (client.isConference) {
            clients[clients.indexOf(client)].following = client.following
            return
        }

        if (topClients.contains(client)) {
            (topClients[topClients.indexOf(client)] as Client).following = client.following
        }
        if (client.following) {
            members.add(client)
            if (!topClients.contains(client) && clients.contains(client)) { // remove found in clients but not from top clients
                clients.remove(client)
            }
        } else {
            members.remove(client)
            if (!topClients.contains(client) && !clients.contains(client)) { // not found in top ten and clients
                clients.add(client)
            }
        }
    }


    private fun isOwner(client: Client) = user.isOwnerOf(client.slug)

    override fun getItemCount(): Int {
        return completeArray.size
    }

    fun update(members: ArrayList<Client>, clients: ArrayList<Client>, topTen: ArrayList<Client>, resetPageIndex: Boolean = true) {
        if (resetPageIndex) {
            pageIndex = 1
        }

        this.members = members
        this.clients = clients
        this.topTen = topTen
        updateArrays()
    }

    private fun updateArrays() {
        ownerClients.clear()
        followingClients.clear()
        topClients.clear()
        otherClients.clear()
        conferenceList.clear()
        completeArray.clear()

        for (client in members) {
            if (isOwner(client)) {
                ownerClients.add(client)
            } else {
                followingClients.add(client)
            }
        }
        for (client in clients) {
            if (client.isConference) {
                conferenceList.add(client)
            } else {
                otherClients.add(client)
            }
        }
        topClients.addAll(topTen)

        if (conferenceList.isNotEmpty()) {
            conferenceList.add(0, VoctagKeys.PLACEHOLDER_DIVIDER_NEW_CONGRESSES)
        }
        if (ownerClients.isNotEmpty()) {
            ownerClients.add(0, VoctagKeys.PLACEHOLDER_DIVIDER_OWNER_CLIENTS)
        }
        if (followingClients.isNotEmpty()) {
            followingClients.add(0, VoctagKeys.PLACEHOLDER_DIVIDER_MYCLIENTS)
        }
        if (topClients.isNotEmpty()) {
            topClients.add(0, VoctagKeys.PLACEHOLDER_DIVIDER_TOP_CLIENTS)
        }
        if (otherClients.isNotEmpty()) {
            otherClients.add(0, VoctagKeys.PLACEHOLDER_DIVIDER_SUGGESTIONS)
        }

        completeArray.addAll(conferenceList)
        completeArray.addAll(ownerClients)
        completeArray.addAll(followingClients)
        completeArray.addAll(topClients)
        completeArray.addAll(otherClients)
    }

    fun incrementPage() {
        pageIndex++
    }

    fun getPageIndex(): Int = pageIndex
}
