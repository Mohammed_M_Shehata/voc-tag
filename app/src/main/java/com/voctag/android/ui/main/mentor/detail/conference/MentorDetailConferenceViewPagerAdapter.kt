package com.voctag.android.ui.main.mentor.detail.conference

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.voctag.android.R
import com.voctag.android.ui.VoctagApp
import com.voctag.android.ui.main.mentor.detail.QuestionsFragment

class MentorDetailConferenceViewPagerAdapter(fm: FragmentManager, val stageFragment: StageFragment, val questionsFragment: QuestionsFragment) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            MentorDetailConferenceActivity.STAGE_PAGE -> stageFragment
            else -> questionsFragment
        }
    }

    override fun getCount(): Int {
        return MentorDetailConferenceActivity.PAGES_COUNT
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == MentorDetailConferenceActivity.STAGE_PAGE)
            VoctagApp.getTranslation(R.string.stage)
        else
            VoctagApp.getTranslation(R.string.discussion)
    }
}

