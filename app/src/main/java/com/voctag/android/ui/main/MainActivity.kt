package com.voctag.android.ui.main

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.LocalVocsManager
import com.voctag.android.manager.PlayListHelper
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.community.CommunityFragment
import com.voctag.android.ui.main.mentor.list.MentorListFragment
import com.voctag.android.ui.main.myFeed.MyFeedFragment
import com.voctag.android.ui.main.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        val TAG: String = MainActivity::class.java.simpleName
        const val MY_FEED_INDEX = 0
        const val MENTOR_LIST_INDEX = 1
        const val COMMUNITY_INDEX = 2
        const val PROFILE_INDEX = 3
        const val FROM_REGISTRATION = "FROM_REGISTRATION"
    }

    private var currentTabIndex = 0
    private val myFeedFragment = MyFeedFragment()
    private val mentorListFragment = MentorListFragment()
    private val communityFragment = CommunityFragment()
    private val profileFragment = ProfileFragment()

    private val playListHelper = PlayListHelper(object : PlayListHelper.PlayListListener {
        override fun isAutoPlayNextEnabled(): Boolean {
            return isOnMyFeed() || isProfileViewDownloadTab()
        }

        override fun getPlayList(vocId: Int): ArrayList<Voc> {
            return getPlayingList()
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // init the bottom navigation
        bottomNavigation.isItemHorizontalTranslationEnabled = false
        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.tabbar_my_feed -> {
                    replaceFragment(myFeedFragment, MY_FEED_INDEX)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.tabbar_mentor_list -> {
                    replaceFragment(mentorListFragment, MENTOR_LIST_INDEX)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.tabbar_community -> {
                    replaceFragment(communityFragment, COMMUNITY_INDEX)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.tabbar_profile -> {
                    replaceFragment(profileFragment, PROFILE_INDEX)
                    return@setOnNavigationItemSelectedListener true
                }
            }
            return@setOnNavigationItemSelectedListener false
        }

        // for mentors: open mentor list fragment
        // for normal users: open myfeed fragment OR only after registration mentor list fragment
        val fromRegistration = intent != null && intent.getBooleanExtra(FROM_REGISTRATION, false)
        val isMentor: Boolean = PersistDataHelper.shared.loadUser()?.isOfficial ?: false

        if (fromRegistration || isMentor) {
            replaceFragment(mentorListFragment, MENTOR_LIST_INDEX, R.id.tabbar_mentor_list)
        } else {
            replaceFragment(myFeedFragment, MY_FEED_INDEX, R.id.tabbar_my_feed)
        }

        //open different screens based on the intent data
        handleOpenIntent(intent)

        RequestHelper.shared.readAllNotifications(this)
        LocalVocsManager(this).migrateSavingDownloadedVocsFromCacheToDatabase()
    }

    override fun onResume() {
        super.onResume()
        playListHelper.onResume(this)
    }

    override fun onStop() {
        super.onStop()
        playListHelper.onStop(this)
    }

    private fun isProfileViewDownloadTab(): Boolean {
        return bottomNavigation.selectedItemId == R.id.tabbar_profile && profileFragment.isDownloadsTabCurrentTab()
    }

    private fun isOnMyFeed(): Boolean {
        return bottomNavigation.selectedItemId == R.id.tabbar_my_feed
    }

    private fun handleOpenIntent(intent: Intent) {
        Handler().post {
            checkFirebaseDynamicLink(intent)
            checkOpenClientNotification(intent)
            checkOpenWebviewNotification(intent)
            checkOpenWebNotification(intent)
        }
    }

    private fun checkFirebaseDynamicLink(intent: Intent) {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(intent)
                .addOnSuccessListener(this) { pendingDynamicLinkData ->
                    var deepLink: Uri? = null
                    if (pendingDynamicLinkData != null) {
                        deepLink = pendingDynamicLinkData.link
                        Log.d(TAG, "DynamicLink> $deepLink")
                    }
                    if (deepLink != null) {
                        val vocID = deepLink.getQueryParameter(VoctagKeys.VOC_ID)
                        val clientSlug = deepLink.pathSegments[1]

                        if (vocID != null) {
                            val voc = Voc(vocID.toInt())
                            val clientThemeColor = deepLink.getQueryParameter(VoctagKeys.VOC_CLIENT_THEMECOLOR)
                            if (clientThemeColor != null) {
                                voc.parentClient = Client(clientSlug, clientThemeColor)
                            }
                            ActivityHelper.openVoc(this, voc, null, null)
                        } else {
                            loadClient(clientSlug)
                        }
                    }
                }
                .addOnFailureListener(this) { e -> Log.d(TAG, "getDynamicLink:onFailure", e) }
    }

    private fun checkOpenClientNotification(intent: Intent?) {
        intent?.let {
            val clientSlug = it.getStringExtra(VoctagKeys.EXTRA_CLIENT_SLUG)
            if (clientSlug != null) {
                loadClient(clientSlug)
            }
        }
    }

    private fun loadClient(slug: String) {
        RequestHelper.shared.loadClientDetail(slug, { client ->
            client?.let {
                var followClient = false

                if (!client.following) {
                    followClient = true
                    client.following = true
                }
                ActivityHelper.openClient(this, client, followClient = followClient, followingListener = null)
            }
        }, this@MainActivity)
    }

    private fun checkOpenWebNotification(intent: Intent?) {
        if (!intent?.getStringExtra(VoctagKeys.EXTRA_WEB_URL).isNullOrEmpty()) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(intent?.getStringExtra(VoctagKeys.EXTRA_WEB_URL)))
            browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            this.startActivity(browserIntent)
        }
    }

    private fun checkOpenWebviewNotification(intent: Intent?) {
        if (!intent?.getStringExtra(VoctagKeys.EXTRA_WEBVIEW_URL).isNullOrEmpty()) {
            val webviewIntent = Intent(this, WebViewActivity::class.java).apply {
                putExtra(VoctagKeys.EXTRA_WEBVIEW_URL, intent?.getStringExtra(VoctagKeys.EXTRA_WEBVIEW_URL))
            }
            webviewIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            this.startActivity(webviewIntent)
        }
    }

    private fun replaceFragment(fragment: Fragment, index: Int, itemId: Int? = null) {
        currentTabIndex = index
        supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit()

        if (itemId != null) {
            bottomNavigation.selectedItemId = itemId
        }
    }

    private fun getPlayingList(): ArrayList<Voc> {
        return when (bottomNavigation.selectedItemId) {
            R.id.tabbar_community -> communityFragment.getPlayList()
            R.id.tabbar_my_feed -> myFeedFragment.getPlayList()
            R.id.tabbar_profile -> profileFragment.getPlayList()
            else -> ArrayList()
        }
    }

    fun navigateToMentorsView() {
        replaceFragment(mentorListFragment, MENTOR_LIST_INDEX, R.id.tabbar_mentor_list)
    }

    fun addVisualizer(visualizerLayout: View) {
        playListHelper.addVisualizer(visualizerLayout)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        intent?.let {
            handleOpenIntent(intent)
        }
        replaceFragment(mentorListFragment, MENTOR_LIST_INDEX, R.id.tabbar_mentor_list)
    }

    override fun onBackPressed() {
        val isMentor: Boolean = PersistDataHelper.shared.loadUser()?.isOfficial ?: false
        if (isMentor && currentTabIndex != MENTOR_LIST_INDEX) {
            replaceFragment(mentorListFragment, MENTOR_LIST_INDEX, R.id.tabbar_mentor_list)
        } else if (!isMentor && currentTabIndex != MY_FEED_INDEX) {
            replaceFragment(myFeedFragment, MY_FEED_INDEX, R.id.tabbar_my_feed)
        } else {
            super.onBackPressed()
        }
    }
}
