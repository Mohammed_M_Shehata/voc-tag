package com.voctag.android.ui.main.mentor.list

import android.app.Activity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.model.Client
import org.jetbrains.anko.find

class MentorListViewHolder(val view: View) : MentorListConferenceViewHolder(view) {

    // MARK: - ClientCellViewholder

    private val clientBadge: ImageView = view.find(R.id.imageClientBadge)
    private val dividerView: View = view.find(R.id.view_divider)
    private val badge: View = view.find(R.id.badge)
    private val badgeCountTextView: TextView = view.find(R.id.badgeCountTextView)

    override fun setup(client: Client, isOwning: Boolean, activity: Activity, lastElement: Boolean) {
        super.setup(client, isOwning, activity, lastElement)

        // add client is top mentor badge
        if (client.isTopMentor) {
            clientBadge.visibility = View.VISIBLE
        } else {
            clientBadge.visibility = View.GONE
        }

        // add divider if not the last element
        if (lastElement) {
            dividerView.visibility = View.INVISIBLE
        } else {
            dividerView.visibility = View.VISIBLE
        }

        // add the badge count for all unread messages
        if (client.newVocsCount > 0) {
            badge.visibility = View.VISIBLE
            badgeCountTextView.visibility = View.VISIBLE
            badgeCountTextView.text = client.newVocsCount.toString()
        } else {
            badge.visibility = View.GONE
            badgeCountTextView.visibility = View.GONE
        }

    }

    private fun handleFollow() {
        if (client.isClosed && !client.hasAccess) {
            ActivityHelper.openClient(activity, client, onFollowingChanged)
            return
        }

        showLoading(true)
        if (client.following) {
            RequestHelper.shared.unfollowClient(client.slug, { success ->
                showLoading(false)
                if (success) {
                    client.following = false
                    onFollowingChanged?.invoke(client)
                }
            }, activity)
        } else {
            RequestHelper.shared.followClient(client.slug, { success ->
                showLoading(false)
                if (success) {
                    client.following = true
                    onFollowingChanged?.invoke(client)
                }
            }, activity)
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            followButton.visibility = View.GONE
            followProgressbar.visibility = View.VISIBLE
        } else {
            followButton.visibility = View.VISIBLE
            followProgressbar.visibility = View.GONE
        }
    }
}