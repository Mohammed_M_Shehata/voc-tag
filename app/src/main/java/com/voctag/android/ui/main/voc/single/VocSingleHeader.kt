package com.voctag.android.ui.main.voc.single

import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.MoreDialogHelper
import com.voctag.android.helper.TimeHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.AudioPlayer
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.VoctagApp
import com.voctag.android.ui.main.vote.VoteActivity
import kotlinx.android.synthetic.main.activity_voc_single.view.*
import kotlinx.android.synthetic.main.layout_voc_single_header.view.*
import org.jetbrains.anko.image
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.textColor

class VocSingleHeader(var view: View) {

    var onWillOpenVoting: (() -> Unit)? = null
    lateinit var voc: Voc
    lateinit var activity: Activity
    private var isSecret: Boolean = false
    private var runnable: Runnable? = null
    private val handler = Handler()

    init {
        view.btn_vocheader_more.setOnClickListener {
            MoreDialogHelper.newInstance(activity, voc).showMoreDialog()
        }

        view.sb_vocheader_audio.setup { setupSeekbar() }

        view.ll_vocheader_voting.setOnClickListener {
            if (!voc.didVote) {
                showVoteView()
            }
        }

        view.tv_vocheader_transcript.setOnClickListener {
            showTranscript()
        }

        runnable = Runnable { updateElapsedTime() }
    }

    fun setup(voc: Voc, activity: Activity, isSecret: Boolean = false) {
        this.voc = voc
        this.activity = activity
        this.isSecret = isSecret
        updateViews()
    }

    private fun updateViews() {

        setupColor()
        setupNickname()
        setupClientBadge()
        setupTimeAndLocation()
        setupVoting()
        setupDuration()
        setupPlays()
        setupSeekbar()

        setupSecret()

        //setup text voc
        setupTextVoc()
    }

    private fun setupColor() {
        if (voc.isNotOpenFeed()) {
            val color = Color.parseColor(voc.parentClient!!.themeColorString)
            view.cl_vocheader_background.backgroundTintList = ColorStateList.valueOf(color)
            view.iv_vocheader_private.setColorFilter(color)
            view.tv_vocheader_private.textColor = color
        }
    }

    private fun setupNickname() {
        if (voc.userNickname != null) {
            view.tv_vocheader_nickname.visibility = View.VISIBLE
            view.tv_vocheader_nickname.text = voc.userNickname!!
        } else {
            view.tv_vocheader_nickname.visibility = View.GONE
        }

        view.tv_vocheader_nickname.setOnClickListener {
            if (voc.userClientSlug != null) {
                openClientDetails()
            }
        }
    }

    private fun setupClientBadge() {
        if (voc.createdByClientOwner) {
            view.iv_vocheader_official.visibility = View.VISIBLE
        } else {
            view.iv_vocheader_official.visibility = View.GONE
        }
        view.iv_vocheader_official.imageResource = getVocBadgeImageResource()
    }

    private fun setupVoting() {
        updateLikedButton()
        view.tv_vocheader_voting.text = voc.scoreString
    }

    private fun updateLikedButton() {
        if (voc.didVote) {
            if (voc.didVoteUp) {
                view.iv_vocheader_voting.image = VoctagApp.getDrawable(R.drawable.ic_vote_up)
            } else {
                view.iv_vocheader_voting.image = VoctagApp.getDrawable(R.drawable.ic_downvoted_small_white_24dp)
            }
        } else {
            view.iv_vocheader_voting.image = VoctagApp.getDrawable(R.drawable.ic_vote)
        }
    }

    private fun setupTimeAndLocation() {
        view.tv_vocheader_datelocation.text = voc.dateLocationString
    }

    private fun setupTextVoc() {
        // update title and transcript
        if (voc.title.isNullOrEmpty() && TextUtils.isEmpty(voc.vocAudioURL)) {
            view.tv_vocheader_title.text = voc.transcript
            view.tv_vocheader_title.visibility = View.VISIBLE
            view.tv_vocheader_transcript.visibility = View.INVISIBLE
        } else {
            view.tv_vocheader_title.text = voc.title
            view.tv_vocheader_title.visibility = View.VISIBLE
            view.tv_vocheader_title.text = voc.title
        }
        view.tv_vocheader_transcript.text = voc.attributedTranscript

        // hide some elements when text is empty
        if (TextUtils.isEmpty(voc.vocAudioURL)) {
            view.sb_vocheader_audio.visibility = View.INVISIBLE
            view.tv_vocheader_playse.visibility = View.GONE
            view.tv_vocheader_duration.visibility = View.GONE
            view.tv_vocheader_progress.visibility = View.GONE
        }
    }

    private fun setupDuration() {
        view.tv_vocheader_duration.text = TimeHelper.getDurationString(voc.duration)
    }

    private fun setupPlays() {
        view.tv_vocheader_playse.text = voc.playsString
    }

    private fun setupSecret() {
        if (voc.secret && !isSecret) {
            view.ll_vocheader_private.visibility = View.VISIBLE
            view.ll_vocheader_voting.visibility = View.INVISIBLE

        } else {
            view.ll_vocheader_private.visibility = View.INVISIBLE
            view.ll_vocheader_voting.visibility = View.VISIBLE
        }
    }

    private fun setupSeekbar() {
        if (AudioPlayer.shared.isPlayingVoc(voc.vocID)) {
            val progress = AudioPlayer.shared.getProgress()
            view.sb_vocheader_audio.progress = progress
            view.sb_vocheader_audio.max = voc.duration
            view.tv_vocheader_progress.text = TimeHelper.getProgressString(progress, voc.duration)
        }
    }

    private fun getVocBadgeImageResource(): Int {
        return ActivityHelper.getVocTransparentBadgeImageResource(voc.parentClient?.isTopMentor)
    }

    private fun showVoteView() {
        VoteActivity.onVoted = { up ->
            didVote(up)
            updateLikedButton()
            view.tv_vocheader_voting.text = voc.scoreString
        }

        onWillOpenVoting?.invoke()

        val intent = Intent(activity, VoteActivity::class.java).apply {
            if (voc.isNotOpenFeed()) {
                putExtra(VoctagKeys.EXTRA_CLIENT, voc.parentClient!!.encode())
            }
        }
        activity.startActivity(intent)
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    private fun didVote(up: Boolean) {
        val clientSlug = voc.parentClient?.slug ?: VoctagKeys.COMMUNITY_SLUG
        if (up) {
            RequestHelper.shared.upVoteVoc(voc.vocID, clientSlug, activity)
        } else {
            RequestHelper.shared.downVoteVoc(voc.vocID, clientSlug, activity)
        }
        voc.voteUp(up)
    }

    private fun showTranscript() {
        if (voc.transcript.isEmpty()) {
            return
        }
        ActivityHelper.showMessageDialog(activity, voc.titleString, voc.attributedTranscript)
    }

    private fun updateElapsedTime() {
        if (AudioPlayer.shared.isPlayingVoc(voc.vocID)) {
            val progress = AudioPlayer.shared.getProgress()
            view.tv_vocsingle_downloadprogress.text = VoctagApp.getTranslation(R.string.feed_cell_progress_text).replace("%%", TimeHelper.getProgressString(progress, voc.duration, true))
            handler.postDelayed(runnable, VoctagKeys.UPDATE_INTERVAL.toLong())
        }
    }

    private fun openClientDetails() {
        val client = Client(voc.userClientSlug!!, voc.userClientThemeColor!!)
        client.following = true
        ActivityHelper.openClient(activity, client, Bundle().apply {
            putBoolean(VoctagKeys.EXTRA_RELOAD_CLIENT, true)
        })
    }
}
