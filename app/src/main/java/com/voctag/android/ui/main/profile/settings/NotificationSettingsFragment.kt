package com.voctag.android.ui.main.profile.settings

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper

class NotificationSettingsFragment : PreferenceFragmentCompat() {

    companion object {
        val TAG = NotificationSettingsFragment::class.java.simpleName
    }

    val currentUser = PersistDataHelper.shared.loadUser()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.notification_settings, rootKey)
        val isMentor: Boolean = currentUser?.isOfficial ?: false
        if (isMentor) {
            val replyVotedPreference: SwitchPreferenceCompat? = findPreference(getString(R.string.reply_voted_key))
            replyVotedPreference?.isVisible = false
            val newReplyPreference: SwitchPreferenceCompat? = findPreference(getString(R.string.new_reply_key))
            newReplyPreference?.summary = getString(R.string.settings_notifications_new_reply_summary_mentor)
        } else {
            val publicQuestionPreference: SwitchPreferenceCompat? = findPreference(getString(R.string.new_public_question_key))
            publicQuestionPreference?.isVisible = false
            val privateQuestionPreference: SwitchPreferenceCompat? = findPreference(getString(R.string.new_private_question_key))
            privateQuestionPreference?.isVisible = false
        }
    }

    private val preferencesListener: SharedPreferences.OnSharedPreferenceChangeListener =
            SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
                currentUser?.settings?.notifications?.let {
                    when (key) {
                        getString(R.string.new_podacast_key) -> {
                            it.podcast = sharedPreferences.getBoolean(key, it.podcast)
                        }
                        getString(R.string.new_speak_key) -> {
                            it.speak = sharedPreferences.getBoolean(key, it.speak)
                        }
                        getString(R.string.new_public_question_key) -> {
                            it.userPublished = sharedPreferences.getBoolean(key, it.userPublished)
                        }
                        getString(R.string.new_private_question_key) -> {
                            it.userPublishedPrivate = sharedPreferences.getBoolean(key, it.userPublishedPrivate)
                        }
                        getString(R.string.new_reply_key) -> {
                            it.reply = sharedPreferences.getBoolean(key, it.reply)
                        }
                        getString(R.string.reply_followed_key) -> {
                            it.replyFollowed = sharedPreferences.getBoolean(key, it.replyFollowed)
                        }
                        getString(R.string.reply_voted_key) -> {
                            it.replyVoted = sharedPreferences.getBoolean(key, it.replyVoted)
                        }
                    }

                    // saving current user settings preferences
                    PersistDataHelper.shared.saveUser(currentUser, false)

                    activity?.let { context ->
                        RequestHelper.shared.updateNotificationSettings(it, { error ->
                            Log.e(TAG, "notification settings error while saving in server> $error")
                        }, context)
                    }
                }
            }

    override fun onResume() {
        super.onResume()
        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(preferencesListener)
    }

    override fun onPause() {
        super.onPause()
        preferenceManager.sharedPreferences.unregisterOnSharedPreferenceChangeListener(preferencesListener)
    }
}