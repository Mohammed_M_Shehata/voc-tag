package com.voctag.android.ui.main.mentor.detail.conference

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.AppBarLayout
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.PlayListHelper
import com.voctag.android.manager.PlaylistManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.User
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.MainActivity
import com.voctag.android.ui.main.mentor.detail.QuestionsFragment
import kotlinx.android.synthetic.main.activity_mentor_detail_conference.*
import kotlinx.android.synthetic.main.layout_audio_visualizer.*
import kotlinx.android.synthetic.main.layout_mentor_detail_conference_header.*

class MentorDetailConferenceActivity : AppCompatActivity() {

    companion object {
        var onFollowingChanged: ((client: Client) -> Unit)? = null
        const val PAGES_COUNT = 2
        const val STAGE_PAGE = 0
    }

    private var currentPage: Int = STAGE_PAGE
    private lateinit var client: Client
    private lateinit var user: User
    private var mentorDetailConferenceHeader: MentorDetailConferenceHeader? = null
    private lateinit var stageFragment: StageFragment
    private val questionsFragment = QuestionsFragment()
    private var shouldReload: Boolean = false

    private val playListHelper = PlayListHelper(object : PlayListHelper.PlayListListener {
        override fun isAutoPlayNextEnabled(): Boolean {
            return isOnStageTab()
        }

        override fun getPlayList(vocId: Int): ArrayList<Voc> {
            return getVocsForCurrentTab()
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mentor_detail_conference)
        user = PersistDataHelper.shared.loadUser()!!
        updateDataFromIntentParams(intent)
        initUi()
    }

    override fun onResume() {
        super.onResume()

        if (shouldReload) {
            client.webViewLink?.let { webViewLink ->
                stageFragment.reloadData(webViewLink)
            }
            questionsFragment.reloadData()
        }

        playListHelper.onResume(this)
        playListHelper.addVisualizer(layout_visualizer)

        PlaylistManager.shared.onPlaylistDidChange = {
            mentorDetailConferenceHeader?.setupMainAudio()
            questionsFragment.updateOnPlaylistDidChange()
        }

        VocCreationManager.shared.parentClient = getCurrentClient()
        VocCreationManager.shared.parentVoc = null
    }

    override fun onStop() {
        super.onStop()
        playListHelper.onStop(this)
    }

    private fun updateDataFromIntentParams(intent: Intent?) {
        intent?.let {
            currentPage = intent.getIntExtra(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, STAGE_PAGE)

            // open client // load client
            if (intent.hasExtra(VoctagKeys.EXTRA_CLIENT)) {
                val newClient = Client.decode(intent.getStringExtra(VoctagKeys.EXTRA_CLIENT))
                if (screenAlreadyOpened()) {
                    if (newClient.slug != client.slug) {
                        ActivityHelper.openClient(this, newClient, onFollowingChanged, currentPage, extras = intent.extras, enableSingleActivityMode = false)
                    } else {
                        shouldReload = true
                    }
                } else {
                    client = newClient
                    val reloadClient = intent.getBooleanExtra(VoctagKeys.EXTRA_RELOAD_CLIENT, false)
                    if (reloadClient) {
                        loadClient(client.slug)
                    }
                }
            }
            // follow the client
            if (intent.hasExtra(VoctagKeys.EXTRA_FOLLOW_CLIENT) && intent.getBooleanExtra(VoctagKeys.EXTRA_FOLLOW_CLIENT, false)) {
                followClient()
            }

        }
    }

    private fun initUi() {
        // init the stage fragment with the webview
        stageFragment = StageFragment.newInstance(client.webViewLink)

        checkFollowingClient()
        vp_conference_content.adapter = MentorDetailConferenceViewPagerAdapter(supportFragmentManager, stageFragment, questionsFragment)
        vp_conference_content.offscreenPageLimit = PAGES_COUNT
        setupToolbar()
        setupStatusbar()
        setUpClientHeader()
        setupRecordButton()
        setupColor()

        srl_conference.setOnRefreshListener {
            questionsFragment.reloadData()
        }

        //Tabs and Viewpager
        tl_conference_tabs.setupWithViewPager(vp_conference_content)
        vp_conference_content.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                currentPage = position
                updateRecordButton()
                srl_conference.isRefreshing = false
            }
        })

        // disable swipe to refresh on viewpager swipe
        vp_conference_content.setOnTouchListener { _, event ->
            srl_conference.isEnabled = false
            when (event.action) {
                MotionEvent.ACTION_UP -> srl_conference.isEnabled = true
            }
            false
        }
    }

    private fun checkFollowingClient() {
        if (client.following) {
            rl_conference_follow.visibility = View.GONE
        } else {
            enableScrolling(false)
            rl_conference_follow.visibility = View.VISIBLE
            rl_conference_follow.setOnTouchListener { _, _ -> true }
            btn_conference_follow.setOnClickListener {
                followClient()
            }
        }
    }

    private fun followClient() {
        btn_conference_follow.isEnabled = false
        RequestHelper.shared.followClient(client.slug, { success ->
            btn_conference_follow.isEnabled = true
            if (success) {
                client.following = true
                checkFollowingClient()
                onFollowingChanged?.invoke(client)
                enableScrolling(true)
            }
        }, this)
    }

    private fun setupToolbar() {
        setSupportActionBar(tb_conference)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        abl_conference.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
            val total = abl_conference.height - tb_conference.height
            val offset = (verticalOffset + total).toFloat() / total
            srl_conference.isRefreshing = false
            srl_conference.isEnabled = verticalOffset == 0
            mentorDetailConferenceHeader?.handleOpacity(1 - offset)
        })

        btn_conference_share.setOnClickListener {
            ActivityHelper.shareVocLink(this, client = client)
        }
        btn_conference_share.visibility = View.VISIBLE
    }

    private fun setupStatusbar(){
        ActivityHelper.makeStatusBarTransparent(this)
        ViewCompat.setOnApplyWindowInsetsListener(srl_conference) { _, insets ->
            ActivityHelper.setMarginTop(tb_conference, insets.systemWindowInsetTop)
            insets.consumeSystemWindowInsets()
        }
    }

    private fun setupRecordButton() {
        btn_conference_ask.background.setTint(Color.parseColor(client.themeColorString))
        btn_conference_ask.setOnClickListener {
            if (isOnStageTab() && user.isOwnerOf(client.slug)) {
                VocCreationManager.shared.openRecordView(this, vp_conference_content.currentItem)
            } else {
                if (client.isSecretVocEnabled) {
                    ActivityHelper.openAskPlanChooser(this, client, vp_conference_content.currentItem, client.secretVocPrice)
                } else {
                    VocCreationManager.shared.openAskReplyView(this, vp_conference_content.currentItem)
                }
            }
        }

        updateRecordButton()
    }

    private fun updateRecordButton() {
        if (user.isOwnerOf(client.slug)) {
            btn_conference_ask.setText(com.voctag.android.R.string.community_client_speak_button_title)
            if (isOnStageTab()) {
                btn_conference_ask.visibility = View.VISIBLE
            } else {
                btn_conference_ask.visibility = View.GONE
            }
        } else {
            btn_conference_ask.setText(com.voctag.android.R.string.community_client_ask_button_title)
            if (isOnStageTab()) {
                btn_conference_ask.visibility = View.GONE
            } else {
                btn_conference_ask.visibility = View.VISIBLE
            }
        }
    }

    private fun setupColor(){
        val themeColor = Color.parseColor(client.themeColorString)
        srl_conference.setColorSchemeColors(themeColor)
        tl_conference_tabs.setSelectedTabIndicatorColor(themeColor)
    }

    private fun enableScrolling(enable: Boolean) {
        val params = ctl_conference.layoutParams as AppBarLayout.LayoutParams
        if (enable){
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP_MARGINS.or(AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED)
        }
        else{
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP_MARGINS
        }

        ctl_conference.layoutParams = params
    }

    private fun getVocsForCurrentTab(): ArrayList<Voc> {
        if (isOnStageTab()){
            return stageFragment.vocs
        }
        return questionsFragment.vocs
    }

    private fun isOnStageTab(): Boolean {
        return vp_conference_content.currentItem == STAGE_PAGE
    }

    private fun setUpClientHeader() {
        val clientView: View = cl_conferenceheader
        clientView.post {
            mentorDetailConferenceHeader = MentorDetailConferenceHeader(clientView)
            mentorDetailConferenceHeader?.setup(client,this)
        }
    }

    private fun screenAlreadyOpened(): Boolean {
        return this::client.isInitialized
    }

    private fun loadClient(slug: String) {
        RequestHelper.shared.loadClientDetail(slug, { client ->
            client?.let {
                updateCurrentClient(client)
            }
        }, this)
    }

    private fun updateCurrentClient(client: Client) {
        this.client = client
        setUpClientHeader()

        client.webViewLink?.let { webViewLink ->
            stageFragment.reloadData(webViewLink)
        }
    }

    fun getCurrentClient() = client

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        updateDataFromIntentParams(intent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isTaskRoot) {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
