package com.voctag.android.ui.main.mentor.detail.open

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.voctag.android.ui.main.mentor.detail.QuestionsFragment

class MentorDetailViewPagerAdapter(
        fm: FragmentManager,
        val speaksFragment: SpeaksFragment,
        val questionsFragment: QuestionsFragment
) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            MentorDetailActivity.SPEAKS_PAGE -> speaksFragment
            else -> questionsFragment
        }
    }

    override fun getCount(): Int {
        return MentorDetailActivity.PAGES_COUNT
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == MentorDetailActivity.SPEAKS_PAGE)
            "SPEAKS"
        else
            "Q&A"
    }

}

