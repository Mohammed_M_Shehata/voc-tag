package com.voctag.android.ui.auth.login

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.voctag.android.BuildConfig
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.api.firebase.AnalyticsLoggerUtils
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.UiUtils
import com.voctag.android.helper.VoctagKeys.Companion.PASSWORD_CHARACTER_MIN
import com.voctag.android.ui.auth.FacebookLoginUtils
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private fun String.isValidEmail(): Boolean = this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setSupportActionBar(tb_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.elevation = 0F

        tv_login_forget.setOnClickListener(this@LoginActivity)
        btn_login_login.setOnClickListener(this@LoginActivity)
        bindProgressButton(btn_login_login)

        // Reset Error on text change
        et_login_email.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_login_email.error = null
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        // Password Input Listener
        et_login_password.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_login_password.error = null
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        initLoginWithFacebook()
    }

    private fun initLoginWithFacebook() {
        FacebookLoginUtils.getInstance().init(this, onLoginSuccess = { isNewUser ->
            if (isNewUser) {
                ActivityHelper.openWelcomeView(this)
            } else {
                openMainView()
            }
        }, onLoginError = { errorMessage ->
            UiUtils.showSnackbar(cl_login_layout, errorMessage)
        })
    }

    private fun loginUser() {
        if (isInputValid()) {
            ActivityHelper.hideSoftKeyboard(this)
            showProgress()

            //start login request
            RequestHelper.shared.login(et_login_email.text.toString(), et_login_password.text.toString(), {
                openMainView()
            }, { errorMessageId: Int ->
                hideProgress()
                UiUtils.showSnackbar(cl_login_layout, errorMessageId)
            }, this@LoginActivity)
        } else {
            if (!et_login_email.text.toString().isValidEmail()) {
                til_login_email.error = getString(R.string.login_email_validate)
            }

            if (et_login_password.text!!.length < PASSWORD_CHARACTER_MIN) {
                til_login_password.error = getString(R.string.login_password_validate)
            }
        }
    }

    private fun openMainView() {
        RequestHelper.shared.loadUser(false, successListener = {
            ActivityHelper.openMainView(this)
        }, errorListener = {}, context = this@LoginActivity)
    }

    private fun isInputValid(): Boolean {
        return et_login_email.text.toString().isValidEmail() && et_login_password.text.toString().length >= PASSWORD_CHARACTER_MIN
    }

    private fun showProgress() {
        btn_login_login.isEnabled = false
        btn_login_login.showProgress {
            progressColorRes = R.color.blue_400
            textMarginRes = R.dimen.keyline_0
        }
    }

    private fun hideProgress() {
        btn_login_login.isEnabled = true
        btn_login_login.hideProgress(R.string.login_button)
    }

    private fun openURL(url: String) {
        val i = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(i)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        FacebookLoginUtils.getInstance().onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_login_forget -> {
                AnalyticsLoggerUtils.getInstance().logForgetPassword()
                openURL(BuildConfig.WEB_URL + getString(R.string.login_forgetpassword_link))
            }
            R.id.btn_login_login -> {
                loginUser()
            }
        }
    }
}
