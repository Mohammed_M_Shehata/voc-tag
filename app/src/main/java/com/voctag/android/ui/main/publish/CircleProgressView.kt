package com.voctag.android.ui.main.publish

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator

class CircleProgressView : View {

    private var progressBarPaint: Paint? = null
    private var backgroundPaint: Paint? = null
    private var mRadius: Float = 0.toFloat()
    private val mArcBounds = RectF()
    private var drawUpto = 0f
    private var progressColor: Int = Color.parseColor("#FFFFFF")
    private var progressBackgroundColor: Int = Color.parseColor("#22FFFFFF")
    private var strokeWidth: Float = 12.toFloat()
    private var backgroundWidth: Float = 12.toFloat()

    var max: Float = 0f
    private var progress: Float
        get() = drawUpto
        set(value) {
            drawUpto = value
            invalidate()
        }

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        setup()
    }

    fun setProgress(progress: Float, animated: Boolean) {
        if (animated) {
            val valueAnimator = ValueAnimator.ofFloat(this.progress, progress)
            valueAnimator.addUpdateListener {
                val value = it.animatedValue as Float
                this.progress = value
            }

            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.duration = 200
            valueAnimator.start()
        } else {
            this.progress = progress
        }
    }

    private fun setup() {
        progressBarPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        progressBarPaint!!.style = Paint.Style.FILL
        progressBarPaint!!.color = progressColor
        progressBarPaint!!.style = Paint.Style.STROKE
        progressBarPaint!!.strokeWidth = strokeWidth * resources.displayMetrics.density
        progressBarPaint!!.strokeCap = Paint.Cap.ROUND
        progressBarPaint!!.color = progressColor
        backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        backgroundPaint!!.style = Paint.Style.FILL
        backgroundPaint!!.color = progressBackgroundColor
        backgroundPaint!!.style = Paint.Style.STROKE
        backgroundPaint!!.strokeWidth = backgroundWidth * resources.displayMetrics.density
        backgroundPaint!!.strokeCap = Paint.Cap.SQUARE
        backgroundPaint!!.color = progressBackgroundColor
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mRadius = Math.min(w, h) / 2f
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val w = MeasureSpec.getSize(widthMeasureSpec)
        val h = MeasureSpec.getSize(heightMeasureSpec)
        val size = Math.min(w, h)
        setMeasuredDimension(size, size)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val padding = mRadius / strokeWidth
        mArcBounds.set(padding, padding, mRadius * 2 - padding, mRadius * 2 - padding)
        canvas.drawArc(mArcBounds, 0f, 360f, false, backgroundPaint!!)
        canvas.drawArc(mArcBounds, 270f, drawUpto / max * 360, false, progressBarPaint!!)
    }
}
