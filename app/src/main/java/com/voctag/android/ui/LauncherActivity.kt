package com.voctag.android.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.voctag.android.R
import com.voctag.android.api.firebase.CustomFirebaseMessagingService
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.ui.auth.intro.IntroActivity
import com.voctag.android.ui.main.MainActivity

class LauncherActivity : AppCompatActivity() {

    companion object {
        val TAG: String = LauncherActivity::class.java.simpleName
    }

    private var clientSlug: String? = null
    private var webviewUrl: String? = null
    private var webUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
        getExtras(intent)
    }

    override fun onResume() {
        super.onResume()

        //wait for 1 seconds
        val handler = Handler()
        handler.postDelayed({
            redirectUser()
        }, 1000)
    }

    private fun getExtras(intent: Intent?) {
        intent?.let {
            clientSlug = intent.extras?.getString(CustomFirebaseMessagingService.EXTRA_CLIENT_SLUG)
            webUrl = intent.extras?.getString(CustomFirebaseMessagingService.EXTRA_WEB_URL)
            webviewUrl = intent.extras?.getString(CustomFirebaseMessagingService.EXTRA_WEBVIEW_URL)
        }
    }

    private fun redirectUser() {
        val refreshToken = PersistDataHelper.shared.loadAuthentication()?.refreshToken
        if (refreshToken == null) {
            startIntroActivity()
        } else {
            startMainActivity()
        }
    }

    private fun startIntroActivity() {
        val introIntent = Intent(this, IntroActivity::class.java)
        startActivity(introIntent)
        overridePendingTransition(R.anim.fade_in_splash, R.anim.fade_out_splash)
        finish()
    }

    private fun startMainActivity() {
        val mainActivity = Intent(this, MainActivity::class.java).apply {
            putExtra(VoctagKeys.EXTRA_CLIENT_SLUG, clientSlug)
            putExtra(VoctagKeys.EXTRA_WEB_URL, webUrl)
            putExtra(VoctagKeys.EXTRA_WEBVIEW_URL, webviewUrl)
        }

        startActivity(mainActivity)
        overridePendingTransition(R.anim.fade_in_splash, R.anim.fade_out_splash)
        finish()
    }
}