package com.voctag.android.ui.main.community

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.FeedFilterManager
import com.voctag.android.model.Voc
import kotlinx.android.synthetic.main.activity_community_search.*

class CommunitySearchActivity : AppCompatActivity() {

    private lateinit var vocsAdapter: CommunityAdapter

    private var vocs: ArrayList<Voc>? = null
    private var willOpenVoting = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_community_search)

        vocs = intent.getSerializableExtra(VoctagKeys.EXTRA_VOCS) as ArrayList<Voc>
        showEmptyViewIfNecessary(vocs!!.isEmpty())
        setupToolbar()

        // Set the adapter
        vocsAdapter = CommunityAdapter(this, vocs!!)
        vocsAdapter.onWillOpenVoting = {
            willOpenVoting = true
        }
        rv_communitysearch.adapter = vocsAdapter

        //searchview settings
        //do show a done button in keyboard
        sv_communitysearch_search.imeOptions = EditorInfo.IME_ACTION_DONE
        sv_communitysearch_search.setIconifiedByDefault(false)
        sv_communitysearch_search.isFocusable = true
        sv_communitysearch_search.requestFocusFromTouch()
        sv_communitysearch_search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                if (s.length > 1) {
                    search(s)
                } else {
                    if (vocs != null) {
                        setVocs(vocs!!)
                    }
                }
                return true
            }
        })
    }

    override fun onResume() {
        super.onResume()
        if (vocs != null) {
            vocsAdapter.updateOnPlaylistDidChange()
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(tb_communitysearch)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        tb_communitysearch.setNavigationIcon(R.drawable.ic_close);
        pb_communitysearch_progress.visibility = View.GONE
    }

    private fun close() {
        finish()
        overridePendingTransition(0, 0)
    }

    private fun search(query: String) {
        if (vocs != null) {
            searchVocs(query)
        }
    }

    private fun searchVocs(query: String) {
        showLoading(true)
        val parameters = FeedFilterManager.shared.searchParametersForCurrentFilter(query)
        RequestHelper.shared.searchVocs(parameters, { vocs, errorHappened ->
            showLoading(false)
            if (vocs != null) {
                setVocs(vocs)
            }
        }, this@CommunitySearchActivity)
    }

    private fun setVocs(vocs: ArrayList<Voc>) {
        vocsAdapter.setVocs(vocs)
        showEmptyViewIfNecessary(vocs.isEmpty())
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            pb_communitysearch_progress.visibility = View.VISIBLE
        } else {
            pb_communitysearch_progress.visibility = View.GONE
        }
    }

    private fun showEmptyViewIfNecessary(empty: Boolean) {
        if (empty) {
            tv_communitysearch_empty.visibility = View.VISIBLE
        } else {
            tv_communitysearch_empty.visibility = View.GONE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        close()
    }
}
