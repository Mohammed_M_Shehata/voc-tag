package com.voctag.android.ui.main.mentor.list

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.voctag.android.R
import com.voctag.android.model.Category
import org.jetbrains.anko.layoutInflater

class CategoriesAdapter(
        private val mContext: Context,
        private val categoriesList: ArrayList<Category>,
        val onCategoryClicked: (category: Category?) -> Unit
) : RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder>(), View.OnClickListener {

    companion object {
        const val ALL_CATEGORY = 0;
    }

    var currentSelectedCategory: Category? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        val view = mContext.layoutInflater.inflate(R.layout.item_category, parent, false)
        return CategoriesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categoriesList.size + 1
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        if (position == ALL_CATEGORY) {
            holder.categoryTextView.text = mContext.getString(R.string.all)
            holder.categoryButton.tag = null
            setSelectionButton(holder.categoryButton, currentSelectedCategory == null)
        } else {
            val category = categoriesList[position - 1]
            holder.categoryTextView.text = category.getNameTranslation()
            holder.categoryButton.tag = category
            setSelectionButton(holder.categoryButton, currentSelectedCategory?.name.equals(category.name))
        }

        holder.categoryButton.setOnClickListener(this)
    }

    private fun setSelectionButton(categoryButton: View, b: Boolean) {
        categoryButton.isSelected = b
    }

    class CategoriesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val categoryButton: View = itemView.findViewById(R.id.btn_category)
        val categoryTextView: TextView = itemView.findViewById(R.id.txt_category)
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.btn_category) {
            currentSelectedCategory = v.tag as Category?
            onCategoryClicked(currentSelectedCategory)
            notifyDataSetChanged()
        }
    }
}