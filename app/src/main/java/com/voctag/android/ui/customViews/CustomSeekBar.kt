package com.voctag.android.ui.customViews

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.widget.SeekBar
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.AudioPlayer

class CustomSeekBar : androidx.appcompat.widget.AppCompatSeekBar {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var updateListener: (() -> Unit)? = null
    private var seekHandler: Handler? = null
    private var seekRunnable: Runnable? = null

    init {
        setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    AudioPlayer.shared.setProgress(progress)
                    isSeeking = false
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                isSeeking = true
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

    }

    private var isSeeking = false

    fun setup(updateListener: () -> Unit) {
        this.updateListener = updateListener
    }

    private fun setUp() {
        progress = 0
        seekHandler = Handler()
        seekRunnable = Runnable {
            updateListener?.invoke()
            seekHandler?.postDelayed(seekRunnable, VoctagKeys.UPDATE_INTERVAL.toLong())
        }
        seekHandler?.post(seekRunnable)
    }

    override fun setProgress(progress: Int) {
        if (!isSeeking) {
            super.setProgress(progress)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        seekRunnable = null
        seekHandler = null
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setUp()
    }
}