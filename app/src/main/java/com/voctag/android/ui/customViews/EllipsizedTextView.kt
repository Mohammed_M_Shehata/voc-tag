package com.voctag.android.ui.customViews

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.util.Linkify
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.voctag.android.R

class EllipsizedTextView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null
) : AppCompatTextView(context, attrs) {

    private val delimiters = (" ").toRegex()
    var ellipsis = getDefaultEllipsis().toString()
    var ellipsisColor = getDefaultEllipsisColor()
    var textValue: String = ""

    private val ellipsisSpannable: SpannableString

    init {
        if (attrs != null) {
            val typedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.EllipsizedTextView, 0, 0)
            typedArray?.let {
                ellipsis = typedArray.getString(R.styleable.EllipsizedTextView_ellipsis)
                        ?: getDefaultEllipsis().toString()
                textValue = typedArray.getString(R.styleable.EllipsizedTextView_textValue) ?: ""
                ellipsisColor = typedArray.getColor(R.styleable.EllipsizedTextView_ellipsis_color, getDefaultEllipsisColor())
                typedArray.recycle()
            }
        }
        ellipsisSpannable = SpannableString(ellipsis)
        ellipsisSpannable.setSpan(ForegroundColorSpan(ellipsisColor), 1, ellipsis.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        ellipsizeText()
    }

    private fun ellipsizeText() {
        val textLines = textValue.lines().toMutableList()
        var textToBeEllipsized: CharSequence = textValue

        var finalText = SpannableStringBuilder()
        var availableTextWidth: Float = measuredWidth - compoundPaddingLeft.toFloat() - compoundPaddingRight.toFloat()

        var lineCounter = 0
        var currentLineIndex = 0

        while (lineCounter < maxLines && textToBeEllipsized.isNotBlank()) {
            if (isLastLine(lineCounter)) {
                availableTextWidth -= paint.measureText(ellipsis)
            }

            var currentLine = textLines[currentLineIndex]
            val currentLineWidth = paint.measureText(currentLine)

            if (currentLineWidth < availableTextWidth) {
                currentLineIndex = currentLineIndex.inc()
                textToBeEllipsized = textToBeEllipsized.removePrefix(currentLine)
                textToBeEllipsized = textToBeEllipsized.trim()
                when {
                    isLastLine(lineCounter) && textToBeEllipsized.isNotBlank() -> finalText.append(currentLine).append(ellipsisSpannable)
                    isLastLine(lineCounter) || textToBeEllipsized.isBlank() -> finalText.append(currentLine)
                    !isLastLine(lineCounter) -> finalText.appendln(currentLine)
                }

            } else {
                val finalLine = when {
                    isLastLine(lineCounter) -> ellipsizeLastLine(currentLine, availableTextWidth)
                    else -> getFinalLineFromLongLine(currentLine, availableTextWidth)
                }

                textToBeEllipsized = textToBeEllipsized.removePrefix(finalLine)
                currentLine = currentLine.removePrefix(finalLine)
                textLines[currentLineIndex] = currentLine

                when {
                    isLastLine(lineCounter) -> finalText.append(finalLine).append(ellipsisSpannable)
                    else -> finalText.appendln(finalLine)
                }

                if (currentLine.isBlank() || currentLine == "\n") {
                    currentLineIndex = currentLineIndex.inc()
                }
            }

            lineCounter = lineCounter.inc()
        }

        text = finalText
        Linkify.addLinks(this, Linkify.WEB_URLS)
    }

    private fun ellipsizeLastLine(currentLine: String, availableTextWidth: Float): CharSequence {
        val ellipsizedText = TextUtils.ellipsize(currentLine, paint, availableTextWidth, ellipsize)
        val defaultEllipsisStart = ellipsizedText.indexOf(getDefaultEllipsis())
        val defaultEllipsisEnd = defaultEllipsisStart + 1

        return ellipsizedText.removeRange(defaultEllipsisStart, defaultEllipsisEnd)
    }


    private fun getFinalLineFromLongLine(currentLine: String, availableTextWidth: Float): String {
        val currentLineWords = currentLine.split(delimiters)
        var finalLine = StringBuilder()

        for (word in currentLineWords) {
            val wordsLine = StringBuilder(finalLine)
            wordsLine.append(word)
            val lineWidth = paint.measureText(wordsLine.toString())

            if (lineWidth <= availableTextWidth) {
                finalLine = finalLine.append(word).append(" ")
            } else {
                break
            }
        }

        return finalLine.toString()
    }

    private fun isLastLine(lineCounter: Int) = lineCounter == (maxLines - 1)

    private fun getDefaultEllipsis(): Char {
        return Typography.ellipsis
    }

    private fun getDefaultEllipsisColor(): Int {
        return R.color.more_color
    }
}