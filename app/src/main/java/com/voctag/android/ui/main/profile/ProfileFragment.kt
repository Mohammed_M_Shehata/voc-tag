package com.voctag.android.ui.main.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.db.relations.VocClient
import com.voctag.android.helper.UiUtils
import com.voctag.android.manager.LocalVocsManager
import com.voctag.android.manager.OnboardingManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.User
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.MainActivity
import com.voctag.android.ui.main.profile.settings.SettingsActivity
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

class ProfileFragment : Fragment() {

    companion object {
        val TAG: String = ProfileFragment::class.java.simpleName
        const val DOWNLOADS_TAB_INDEX = 0
        const val POSTS_QUESTIONS_TAB_INDEX = 1
        const val REPLIES_TAB_INDEX = 2
        const val FOLLOWING_TAB_INDEX = 3
    }

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: ProfileAdapter
    private lateinit var mainActivity: MainActivity
    private lateinit var downloadedVocs: ArrayList<Voc>
    private lateinit var user: User
    private var willOpenVoting = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity = activity as MainActivity
        downloadedVocs = ArrayList()
        LocalVocsManager(mainActivity).getDownloadedVocs().observe(this, Observer { downloadedVocs: List<VocClient> ->
            updateDownloadedVocs(downloadedVocs)
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        user = PersistDataHelper.shared.loadUser()!!
        setUser(user)

        // init the swipe refresh layout
        swipeRefreshLayout = view.swiperefreshlayoutProfile
        swipeRefreshLayout.setColorSchemeResources(R.color.main)
        swipeRefreshLayout.setOnRefreshListener {
            loadData()
        }

        // Init the adapter and recyclerview
        adapter = ProfileAdapter(mainActivity, ArrayList())
        adapter.onWillOpenVoting = {
            willOpenVoting = true
        }
        recyclerView = view.recyclerviewProfile
        recyclerView.adapter = adapter

        loadData()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbarSettingsImageView.setOnClickListener {
            mainActivity.startActivity(Intent(mainActivity, SettingsActivity::class.java))
        }
        showOnboarding()
        setupTabLayout()
    }

    override fun onResume() {
        super.onResume()

        VocCreationManager.shared.parentVoc = null
        VocCreationManager.shared.parentClient = null
        VocCreationManager.shared.onPublished = null

        adapter.updateOnPlaylistDidChange()
        mainActivity.addVisualizer(visualizerProfile)
    }

    private fun updateDownloadedVocs(vocsClients: List<VocClient>) {
        val vocsList: ArrayList<Voc> = ArrayList()
        vocsClients.forEach { vocClient ->
            vocClient.voc.parentClient = vocClient.clientList?.get(0)
            vocsList.add(vocClient.voc)
        }

        downloadedVocs.clear()
        downloadedVocs.addAll(vocsList)
    }

    private fun showOnboarding() {
        if (!OnboardingManager.shared.hasUserSeenOnboarding(OnboardingManager.OnboardingType.Profile)) {
            MaterialAlertDialogBuilder(this.context)
                    .setMessage(getString(R.string.onboarding_text_profile))
                    .setPositiveButton(getString(R.string.onboarding_confirm_title), null)
                    .show();

            OnboardingManager.shared.userSeenOnboarding(OnboardingManager.OnboardingType.Profile)
        }
    }

    private fun setupTabLayout() {
        tabsProfile.addTab(tabsProfile.newTab().setText(R.string.profile_downloaded_vocs), true)
        if (user.isOfficial) {
            tabsProfile.addTab(tabsProfile.newTab().setText(R.string.profile_myvocs_mentor_title), false)
        } else {
            tabsProfile.addTab(tabsProfile.newTab().setText(R.string.profile_myvocs_title), false)
        }

        tabsProfile.addTab(tabsProfile.newTab().setText(R.string.profile_replies_title), false)
        tabsProfile.addTab(tabsProfile.newTab().setText(R.string.profile_myfavorites_title), false)

        tabsProfile.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}
            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val vocsForTab = getVocsForTab()
                showEmptyTextViewIfNecessary(vocsForTab.isEmpty())

                when (tab?.position) {
                    POSTS_QUESTIONS_TAB_INDEX -> when {
                        user.isOfficial -> emptyviewProfile.text = getString(R.string.mentor_no_posts)
                        else -> emptyviewProfile.text = getString(R.string.client_no_questions)
                    }
                    REPLIES_TAB_INDEX -> emptyviewProfile.text = getString(R.string.no_replies)
                    FOLLOWING_TAB_INDEX -> emptyviewProfile.text = getString(R.string.no_following_data)
                    DOWNLOADS_TAB_INDEX -> emptyviewProfile.text = getString(R.string.profile_no_downloaded_vocs)
                }

                adapter.setVocs(vocsForTab)
            }
        })
    }

    private fun loadData() {
        swipeRefreshLayout.isRefreshing = true
        RequestHelper.shared.loadUser(true, successListener = {
            setUser(it)
            swipeRefreshLayout.isRefreshing = false
        }, errorListener = {
            setUser(user)
            swipeRefreshLayout.isRefreshing = false
            if (layoutProfile != null) {
                if (it.networkResponse?.statusCode == 500) {
                    UiUtils.showSnackbar(layoutProfile, getString(R.string.server_error), Snackbar.LENGTH_LONG)
                } else {
                    UiUtils.showSnackbar(layoutProfile, getString(R.string.nointernet), Snackbar.LENGTH_LONG)
                }
            }
        }, context = activity as? Activity)
    }

    private fun setUser(user: User) {
        this.user = user
        if(user.points > 0){
            textPoints?.text = user.pointsString
        }
        toolbarTitle?.text = user.nickname ?: ""
        if(this::adapter.isInitialized){
            adapter.setVocs(getVocsForTab())
            showEmptyTextViewIfNecessary(getVocsForTab().isEmpty())
        }
    }

    private fun showEmptyTextViewIfNecessary(noData: Boolean) {
        if (noData) {
            emptyviewProfile?.visibility = View.VISIBLE
        } else {
            emptyviewProfile?.visibility = View.GONE
        }
    }

    private fun getVocsForTab(): ArrayList<Voc> {
        val vocs = ArrayList<Voc>()
        when (tabsProfile?.selectedTabPosition) {
            POSTS_QUESTIONS_TAB_INDEX -> vocs.addAll(user.vocs)
            REPLIES_TAB_INDEX -> vocs.addAll(user.replies)
            FOLLOWING_TAB_INDEX -> vocs.addAll(user.favorites)
            DOWNLOADS_TAB_INDEX -> vocs.addAll(downloadedVocs)
            else -> emptyList<Voc>()
        }
        return vocs
    }

    fun getPlayList(): ArrayList<Voc> {
        return when (tabsProfile.selectedTabPosition) {
            DOWNLOADS_TAB_INDEX -> downloadedVocs
            else -> ArrayList()
        }
    }

    fun isDownloadsTabCurrentTab(): Boolean {
        return tabsProfile.selectedTabPosition == DOWNLOADS_TAB_INDEX
    }
}