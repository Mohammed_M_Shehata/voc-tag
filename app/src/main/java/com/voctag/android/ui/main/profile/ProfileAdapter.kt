package com.voctag.android.ui.main.profile

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.voctag.android.R
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.manager.PlaylistManager
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.voc.card.VocCardViewHolder
import java.util.*

open class ProfileAdapter(
        private val activity: Activity,
        private var vocs: ArrayList<Voc>
) : RecyclerView.Adapter<VocCardViewHolder>() {

    // MARK: - Reactive Variables

    var onDeletedVoc: (() -> Unit)? = null
    var onWillOpenVoting: (() -> Unit)? = null

    // MARK: - ProfileAdapter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VocCardViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_voc_card, parent, false)
        return VocCardViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: VocCardViewHolder, position: Int) {
        val voc = vocs[position]
        viewHolder.setup(voc, activity)
        viewHolder.onWillOpenVoting = onWillOpenVoting
        viewHolder.onPressedViewHolder = { isReply ->
            if (isReply) {
                val newVoc = Voc(voc.parentID!!)
                newVoc.parentClient = voc.parentClient
                ActivityHelper.openVoc(activity, newVoc, voc, onDeletedVoc)
            } else {
                voc.newRepliesCount = 0
                ActivityHelper.openVoc(activity, voc, null, onDeletedVoc)
            }
        }
    }

    fun setVocs(newVocs: ArrayList<Voc>) {
        vocs = newVocs
        notifyDataSetChanged()
    }

    fun updateOnPlaylistDidChange() {
        PlaylistManager.shared.onPlaylistDidChange = {
            activity.runOnUiThread {
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int {
        return vocs.size
    }
}
