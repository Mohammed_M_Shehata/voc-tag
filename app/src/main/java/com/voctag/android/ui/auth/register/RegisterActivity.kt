package com.voctag.android.ui.auth.register

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Patterns
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.NetworkError
import com.android.volley.ServerError
import com.android.volley.toolbox.HttpHeaderParser
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.gson.Gson
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.api.firebase.AnalyticsLoggerUtils
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.UiUtils
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Error
import com.voctag.android.model.ErrorHandler
import com.voctag.android.ui.auth.FacebookLoginUtils
import com.voctag.android.ui.VoctagApp
import kotlinx.android.synthetic.main.activity_register.*
import java.nio.charset.Charset
import java.util.*

class RegisterActivity : AppCompatActivity() {

    companion object {
        private val TAG: String = RegisterActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        setSupportActionBar(tb_register)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.elevation = 0F

        //On Click listener
        bindProgressButton(btn_register_register)
        btn_register_register.setOnClickListener { registerUser() }
        makeTermsConditionsPrivacyClickable()

        //Nickname input
        et_register_nickname.filters = arrayOf(InputFilter.LengthFilter(VoctagKeys.NICKNAME_CHARACTER_MAX))
        et_register_nickname.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_register_nickname.error = null
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        //Email Input
        et_register_email.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_register_email.error = null
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        //password Input
        et_register_password.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_register_password.error = null
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        initLoginWithFacebook()
    }

    private fun makeTermsConditionsPrivacyClickable() {
        //add terms and privacy styles and click listener
        val welcomeInfoText = getString(R.string.welcome_info)
        val ss = SpannableString(welcomeInfoText)

        val privacyClick = object : ClickableSpan() {
            override fun onClick(widget: View) {
                AnalyticsLoggerUtils.getInstance().logPrivacyClicked(AnalyticsLoggerUtils.GET_STARTED_SCREEN)
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(VoctagApp.getTranslation(R.string.voctag_url) + "privacy")))
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
                ds.typeface = Typeface.DEFAULT_BOLD
            }
        }
        val termsClick = object : ClickableSpan() {
            override fun onClick(widget: View) {
                AnalyticsLoggerUtils.getInstance().logTermsClicked(AnalyticsLoggerUtils.GET_STARTED_SCREEN)
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(VoctagApp.getTranslation(R.string.voctag_url) + "terms")))
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
                ds.typeface = Typeface.DEFAULT_BOLD
            }
        }

        val terms: String
        val privacy: String
        if (Locale.getDefault().language == "de") {
            terms = "Nutzungsbedingungen"
            privacy = "Datenschutzrichtlinien"
        } else {
            terms = "Terms of Service"
            privacy = "Privacy Policy"
        }

        ss.setSpan(termsClick, welcomeInfoText.indexOf(terms), welcomeInfoText.indexOf(terms) + terms.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        ss.setSpan(privacyClick, welcomeInfoText.indexOf(privacy), welcomeInfoText.toString().indexOf(privacy) + privacy.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        tv_register_info.linksClickable = true
        tv_register_info.highlightColor = Color.TRANSPARENT
        tv_register_info.movementMethod = LinkMovementMethod.getInstance()
        tv_register_info.setText(ss, TextView.BufferType.SPANNABLE)
    }

    private fun initLoginWithFacebook() {
        FacebookLoginUtils.getInstance().init(this, onLoginSuccess = { isNewUser ->
            if (isNewUser) {
                openWelcomeView()
            } else {
                ActivityHelper.openMainView(this)
            }
        }, onLoginError = { errorMessage ->
            UiUtils.showSnackbar(cl_register_layout, errorMessage)
        })
    }

    private fun registerUser() {
        if (isInputValid()) {
            showProgress()
            ActivityHelper.hideSoftKeyboard(this)

            //Call register endpoint
            RequestHelper.shared.register(et_register_nickname.text.toString().trim(), et_register_email.text.toString().trim(), et_register_password.text.toString(), {
                openWelcomeView()
            }, { error ->
                hideProgress()
                val statusCode = error.networkResponse?.statusCode
                if (statusCode == 401) {
                    val errors: ErrorHandler = Gson().fromJson(String(error.networkResponse?.data
                            ?: ByteArray(0), Charset.forName(HttpHeaderParser.parseCharset(error.networkResponse?.headers))), ErrorHandler::class.java)
                    errors.errors.forEach {
                        if (it.attribute.equals("nickname", true) && it.code.equals("taken")) {
                            til_register_nickname.error = getString(R.string.register_nickname_error)
                        }
                        if (it.attribute.equals("nickname", true) && it.code.equals("short")) {
                            til_register_nickname.error = getString(R.string.register_nickname_error_short)
                        }
                        if (it.attribute.equals("email", true) && it.code.equals("taken")) {
                            til_register_email.error = getString(R.string.register_email_error)
                        }
                        if (it.attribute.equals("email", true) && it.code.equals("invalid")) {
                            til_register_email.error = getString(R.string.register_email_error_invalid)
                        }
                        if (it.code == Error.ERROR_CODE_FACEBOOK) {
                            UiUtils.showSnackbar(findViewById(R.id.cl_register_layout), R.string.email_assigned_to_facebook_account)
                        }
                    }
                }
                if (error is NetworkError && statusCode != 422) {
                    UiUtils.showSnackbar(findViewById(R.id.cl_register_layout), R.string.nointernet)
                } else if (error is ServerError) {
                    UiUtils.showSnackbar(findViewById(R.id.cl_register_layout), R.string.server_error)
                }
            }, this@RegisterActivity)
        } else {
            hideProgress()
            if (et_register_nickname.text!!.length < VoctagKeys.NICKNAME_CHARACTER_MIN) {
                til_register_nickname.error = getString(R.string.register_nickname_validate)
            }
            if (!et_register_email.text.toString().isValidEmail()) {
                til_register_email.error = getString(R.string.register_email_validate)
            }
            if (et_register_password.text!!.length < VoctagKeys.PASSWORD_CHARACTER_MIN) {
                til_register_password.error = getString(R.string.register_password_validate)
            }
        }
    }

    private fun openWelcomeView() {
        ActivityHelper.openWelcomeView(this)
    }

    private fun showProgress() {
        btn_register_register.isEnabled = false
        btn_register_register.showProgress {
            progressColorRes = R.color.blue_400
            textMarginRes = R.dimen.keyline_0
        }
    }

    private fun hideProgress() {
        btn_register_register.isEnabled = true
        btn_register_register.hideProgress(R.string.register_button)
    }

    private fun isInputValid(): Boolean {
        return et_register_nickname.text!!.length >= VoctagKeys.NICKNAME_CHARACTER_MIN && et_register_nickname.text!!.length <= VoctagKeys.NICKNAME_CHARACTER_MAX && et_register_password.text!!.length >= VoctagKeys.PASSWORD_CHARACTER_MIN && et_register_email.text.toString().isValidEmail()
    }

    private fun String.isValidEmail(): Boolean = this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        FacebookLoginUtils.getInstance().onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

}