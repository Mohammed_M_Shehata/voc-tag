package com.voctag.android.ui.auth.intro

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.voctag.android.R
import com.voctag.android.helper.Utils
import com.voctag.android.model.IntroScreen
import com.voctag.android.ui.auth.login.LoginActivity
import com.voctag.android.ui.auth.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_intro.*

class IntroActivity : AppCompatActivity() {

    companion object {
        private const val INTRO_SCREENS_SIZE = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)

        setupUi()
    }

    private fun setupUi() {
        tv_intro_login.text = Utils.getTextFromRawText(getString(R.string.welcome_login_text))
        tv_intro_login.setOnClickListener { goToLoginView() }
        btn_intro_start.setOnClickListener { goToRegisterView() }

        setupViewPagerWithTabLayout()
    }

    private fun setupViewPagerWithTabLayout() {
        vp_intro.adapter = IntroScreensAdapter(getIntroScreens())

        TabLayoutMediator(tb_intro_tabs, vp_intro, TabLayoutMediator.TabConfigurationStrategy { tab, position ->
        }).attach()

        for (pos in 0 until INTRO_SCREENS_SIZE) {
            val tab: TabLayout.Tab? = tb_intro_tabs.getTabAt(pos)
            tab?.setIcon(R.drawable.selector_intro_tab)
        }
    }

    private fun getIntroScreens(): List<IntroScreen> {
        val screensList = ArrayList<IntroScreen>()
        screensList.add(IntroScreen(R.string.feature_one_text, R.drawable.illustration_onboarding_1))
        screensList.add(IntroScreen(R.string.feature_two_text, R.drawable.illustration_onboarding_2))
        screensList.add(IntroScreen(R.string.feature_three_text, R.drawable.illustration_onboarding_3))

        return screensList
    }

    private fun goToRegisterView() {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    private fun goToLoginView() {
        startActivity(Intent(this, LoginActivity::class.java))
    }
}