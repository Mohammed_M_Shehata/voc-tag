package com.voctag.android.ui.auth.intro

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.voctag.android.R
import com.voctag.android.model.IntroScreen
import kotlinx.android.synthetic.main.item_intro.view.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.textResource

class IntroScreensAdapter(private val introScreensList: List<IntroScreen>) : RecyclerView.Adapter<IntroScreensAdapter.IntroScreenViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroScreenViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_intro, parent, false)
        return IntroScreenViewHolder(view)
    }

    override fun onBindViewHolder(holder: IntroScreenViewHolder, position: Int) {
        val introScreen = introScreensList[position]
        holder.introScreenTextView.textResource = introScreen.textResId
        holder.introScreenImageView.imageResource = introScreen.imageResId
    }

    override fun getItemCount(): Int {
        return introScreensList.size
    }

    inner class IntroScreenViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val introScreenImageView: ImageView = itemView.iv_itemintro_feature
        val introScreenTextView: TextView = itemView.tv_itemintro_feature
    }
}