package com.voctag.android.ui.main.mentor.detail.conference

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Voc
import kotlinx.android.synthetic.main.fragment_stage.view.*

class StageFragment : Fragment() {

    companion object {
        fun newInstance(clientWebUrl: String?): StageFragment {
            val stageFragment = StageFragment()
            val args = Bundle()
            args.putString(VoctagKeys.CLIENT_WEB_VIEW_LINK, clientWebUrl)
            stageFragment.arguments = args
            return stageFragment
        }
    }

    private lateinit var mWebView: WebView
    private var clientWebUrl: String? = ""
    val vocs = ArrayList<Voc>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(com.voctag.android.R.layout.fragment_stage, container, false)
        clientWebUrl = arguments?.get(VoctagKeys.CLIENT_WEB_VIEW_LINK) as String?
        initView(view)
        return view
    }

    private fun initView(view: View) {
        with(view) {
            mWebView = wb_stage_website
            if (clientWebUrl != null){
                setUpWebView()
            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setUpWebView() {
        mWebView.settings.javaScriptEnabled = true
        mWebView.settings.allowFileAccess = true
        mWebView.settings.setAppCacheEnabled(false)
        mWebView.webViewClient = WebViewClient()
        mWebView.webChromeClient = FullScreenChromeClient()
        mWebView.loadUrl(clientWebUrl)
    }

    private inner class FullScreenChromeClient : WebChromeClient() {
        private var mCustomView: View? = null
        private var mCustomViewCallback: CustomViewCallback? = null
        private var mOriginalOrientation: Int = 0
        private var mOriginalSystemUiVisibility: Int = 0

        override fun onHideCustomView() {
            activity?.let {
                (it.window.decorView as FrameLayout).removeView(mCustomView)
                this.mCustomView = null
                it.window.decorView.systemUiVisibility = mOriginalSystemUiVisibility;
                it.requestedOrientation = mOriginalOrientation;
                mCustomViewCallback?.onCustomViewHidden();
                mCustomViewCallback = null;
            }
        }

        override fun onShowCustomView(paramView: View, paramCustomViewCallback: CustomViewCallback) {
            if (this.mCustomView != null) {
                onHideCustomView()
                return
            }
            activity?.let {
                this.mCustomView = paramView
                paramView.setBackgroundColor(Color.BLACK)
                (it.window.decorView as FrameLayout).addView(this.mCustomView, FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
                this.mOriginalSystemUiVisibility = it.window.decorView.systemUiVisibility
                this.mOriginalOrientation = it.requestedOrientation
                this.mCustomViewCallback = paramCustomViewCallback
                it.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                it.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE
            }
        }
    }

    override fun onStop() {
        super.onStop()
        mWebView.onPause()
    }

    override fun onResume() {
        super.onResume()
        mWebView.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mWebView.saveState(outState)
    }

    fun reloadData(webViewLink: String) {
        clientWebUrl = webViewLink
        setUpWebView()
    }
}