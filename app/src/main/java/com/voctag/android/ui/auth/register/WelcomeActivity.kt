package com.voctag.android.ui.auth.register

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.UiUtils
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        initUi()
    }

    private fun initUi() {
        val user = PersistDataHelper.shared.loadUser()
        tv_welcome_title.text = getString(R.string.welcome_user_title, user?.nickname)

        // update user only when checkbox is enabled. Otherwise it's useless
        btn_welcome_start.setOnClickListener {
            if (cb_welcome_newsletter.isChecked) {
                RequestHelper.shared.updateUser(cb_welcome_newsletter.isChecked, successListener = {
                    openMainView()
                }, errorListener = { errorMessageId ->
                    UiUtils.showSnackbar(findViewById(R.id.parent_layout), errorMessageId)
                }, context = this)
            } else {
                openMainView()
            }
        }
    }

    private fun openMainView() {
        ActivityHelper.openMainView(this, true)
    }
}