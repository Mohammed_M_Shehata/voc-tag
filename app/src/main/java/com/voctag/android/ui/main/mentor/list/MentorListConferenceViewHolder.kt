package com.voctag.android.ui.main.mentor.list

import android.app.Activity
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.model.Client
import com.voctag.android.ui.customViews.CustomButton
import com.voctag.android.ui.customViews.RoundTransform
import com.voctag.android.ui.VoctagApp
import org.jetbrains.anko.find
import org.jetbrains.anko.image

open class MentorListConferenceViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var onPressedViewHolder: (() -> Unit)? = null
    var onFollowingChanged: ((client: Client) -> Unit)? = null

    // MARK: - ClientCellViewHolder

    private val image: ImageView = view.find(R.id.icon)
    private val imageBackground: View = view.find(R.id.iconBackground)
    private val clientTitle: TextView = view.find(R.id.clientTitle)
    private val mentorForTextView: TextView = view.find(R.id.mentorForTextView)
    private val membershipsTextView: TextView = view.find(R.id.membershipsTextView)
    protected val followButton: CustomButton = view.find(R.id.followButton)
    protected val followProgressbar: ProgressBar = view.find(R.id.followProgressbar)
    lateinit var client: Client
    lateinit var activity: Activity

    init {
        view.setOnClickListener {
            onPressedViewHolder?.invoke()
        }

        followButton.setOnClickListener {
            handleFollow()
        }
        showLoading(false)
    }

    open fun setup(client: Client, isOwning: Boolean, activity: Activity, lastElement: Boolean) {
        this.client = client
        this.activity = activity

        // init the conference image with the client color as background
        imageBackground.background.setTint(Color.parseColor(client.themeColorString))
        if (client.smallIconURL.isNotEmpty()) {
            image.visibility = View.VISIBLE
            Picasso.get().load(client.smallIconURL).transform(RoundTransform(6)).into(image)
        } else {
            image.image = null
            image.visibility = View.INVISIBLE
        }

        // set client name
        clientTitle.text = client.name

        // hide membership count if it's a closed client
        if (client.isClosed) {
            membershipsTextView.visibility = View.GONE
        } else {
            membershipsTextView.text = if (client.isConference) "${client.membershipsCountString} ${VoctagApp.getTranslation(R.string.participants)}"
            else "${client.membershipsCountString} ${VoctagApp.getTranslation(R.string.community_memberships)}"

            membershipsTextView.visibility = View.VISIBLE
        }

        // set mentor_for text
        if (!client.mentorFor.isNullOrEmpty()) {
            mentorForTextView.visibility = View.VISIBLE
            mentorForTextView.text = client.mentorFor!!
        } else {
            mentorForTextView.visibility = View.GONE
        }

        // hide follow icon if user is owning the client
        if (isOwning) {
            followButton.visibility = View.GONE
        } else {
            followButton.visibility = View.VISIBLE
            updateClientIcon()
        }

    }

    private fun updateClientIcon() {
        val iconId = if (client.following) {
            if (client.isConference) R.drawable.ic_unfollow else R.drawable.ic_client_unfollow
        } else {
            if (client.isConference) R.drawable.ic_follow else R.drawable.ic_client_follow
        }

        followButton.background = VoctagApp.getDrawable(iconId)
    }

    // todo add error handling
    private fun handleFollow() {
        if (client.isClosed && !client.hasAccess) {
            ActivityHelper.openClient(activity, client, onFollowingChanged)
            return
        }

        showLoading(true)
        if (client.following) {
            RequestHelper.shared.unfollowClient(client.slug, { success ->
                showLoading(false)
                if (success) {
                    client.following = false
                    onFollowingChanged?.invoke(client)
                }
            }, activity)
        } else {
            RequestHelper.shared.followClient(client.slug, { success ->
                showLoading(false)
                if (success) {
                    client.following = true
                    onFollowingChanged?.invoke(client)
                }
            }, activity)
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            followButton.visibility = View.GONE
            followProgressbar.visibility = View.VISIBLE
        } else {
            followButton.visibility = View.VISIBLE
            followProgressbar.visibility = View.GONE
        }
    }
}