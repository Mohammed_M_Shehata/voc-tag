package com.voctag.android.ui

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.IntentFilter
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import com.downloader.PRDownloader
import com.voctag.android.api.firebase.AnalyticsLoggerUtils
import com.voctag.android.manager.AudioNotificationManager
import com.voctag.android.manager.LocalVocsManager
import com.voctag.android.manager.PlayListHelper
import com.voctag.android.ui.main.MainActivity

class VoctagApp : Application() {

    companion object {
        private lateinit var app: VoctagApp
        val TAG: String = VoctagApp::class.java.simpleName
        val context: Context
            get() = app.applicationContext

        // resource helper methods
        fun getTranslation(resId: Int): String {
            return context.getString(resId)
        }

        fun getDrawable(resId: Int): Drawable {
            return context.getDrawable(resId)
        }

        fun getColor(resId: Int): Int {
            return ContextCompat.getColor(context, resId)
        }

    }

    override fun onCreate() {
        super.onCreate()

        AnalyticsLoggerUtils.getInstance().init(this)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        app = this

        val intentFilter = IntentFilter().apply {
            addAction(AudioNotificationManager.NOTIFY_PLAY)
            addAction(AudioNotificationManager.NOTIFY_PAUSE)
            addAction(AudioNotificationManager.NOTIFY_BACKWARD)
            addAction(AudioNotificationManager.NOTIFY_FORWARD)
        }
        val receiver = AudioNotificationManager.NotificationBroadcast()
        registerReceiver(receiver, intentFilter)

        listenForActivities();
        PRDownloader.initialize(applicationContext)

        PlayListHelper(null).loadCachedPlayList(this)
    }

    private fun listenForActivities() {
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity?) {}

            override fun onActivityResumed(activity: Activity?) {}

            override fun onActivityStarted(activity: Activity?) {}

            override fun onActivityDestroyed(activity: Activity?) {
                Log.d(TAG, "on activity destroyed> ${activity}")
                if (activity is MainActivity){
                    LocalVocsManager.onDestroy(this@VoctagApp);
                }
            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {}

            override fun onActivityStopped(activity: Activity?) {}

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                Log.d(TAG, "on activity created> ${activity}")
            }

        })
    }
}
