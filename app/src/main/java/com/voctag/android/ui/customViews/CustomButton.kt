package com.voctag.android.ui.customViews

import android.content.Context
import android.util.AttributeSet

/**
 * Created by Jan on 05.11.17.
 */

class CustomButton : androidx.appcompat.widget.AppCompatButton {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setEnabled(enabled: Boolean) {
        alpha = if (enabled) 1f else 0.5f
        super.setEnabled(enabled)
    }
}
