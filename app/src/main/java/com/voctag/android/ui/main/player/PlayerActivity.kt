package com.voctag.android.ui.main.player

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.target.CustomViewTarget
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.MoreDialogHelper
import com.voctag.android.helper.TimeHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.AudioPlayer
import com.voctag.android.manager.DownloadVocsHelper
import com.voctag.android.manager.PlaylistManager
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.vote.VoteActivity
import jp.wasabeef.blurry.Blurry
import kotlinx.android.synthetic.main.activity_player.*
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.imageResource
import java.util.*

class PlayerActivity : AppCompatActivity(), View.OnClickListener, Observer {

    companion object {
        val TAG: String = PlayerActivity::class.java.simpleName
    }

    private lateinit var downloadVocsHelper: DownloadVocsHelper
    private var mediaSpeed: Float = 1.25f
    private val playlistManager = PlaylistManager.shared
    private var currentVoc: Voc? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)

        playlistManager.getCurrentVoc()?.let { setImage(it) }
        setupStatusBar()
        seekbar.setup { updateSeekBar() }

        // init download vocs helper. This is needed for downloadprogress and updating of the download button and text
        downloadVocsHelper = DownloadVocsHelper(this, buttonDownload, textviewDownloadProgress)

        // setup some Click listeners
        buttonPrev.setOnClickListener(this@PlayerActivity)
        buttonPlay.setOnClickListener(this@PlayerActivity)
        buttonNext.setOnClickListener(this@PlayerActivity)
        buttonReplies.setOnClickListener(this@PlayerActivity)
        buttonSpeed.setOnClickListener(this@PlayerActivity)
        buttonLike.setOnClickListener(this@PlayerActivity)
        buttonClose.setOnClickListener(this@PlayerActivity)
        buttonShare.setOnClickListener(this@PlayerActivity)
        buttonMore.setOnClickListener(this@PlayerActivity)
        buttonBackTen.setOnClickListener(this@PlayerActivity)
        buttonNextThirty.setOnClickListener(this@PlayerActivity)
        imagePhoto.setOnClickListener(this@PlayerActivity)
        buttonDownload.setOnClickListener(this@PlayerActivity)
    }

    override fun onResume() {
        super.onResume()
        setupPlaylist()
        updateViews()
    }

    override fun onStop() {
        super.onStop()
        downloadVocsHelper.onStop()
        playlistManager.deleteObserver(this)
    }

    private fun setupStatusBar() {
        ActivityHelper.makeStatusBarTransparent(this)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.layoutPlayer)) { _, insets ->
            ActivityHelper.setMarginTop(toolbar, insets.systemWindowInsetTop)
            insets.consumeSystemWindowInsets()
        }
    }

    private fun setupPlaylist() {
        playlistManager.addObserver(this)
    }

    private fun updateViews() {
        val prev = currentVoc
        currentVoc = playlistManager.getCurrentVoc()
        val voc = currentVoc ?: return
        updatePlayButton(voc)

        if (prev == null || prev.vocID != voc.vocID) {
            onVocChanged(voc)
        }
    }

    private fun onVocChanged(voc: Voc) {
        setSpeed()
        setImage(voc)
        textviewNickname.text = voc.userNickname
        textTitle.text = voc.titleString

        seekbar.max = voc.duration
        updateSeekBar()

        downloadVocsHelper.onResume(voc)

        // update like button: set if user has liked + update color
        updateLikeButton()

        buttonReplies.text = voc.repliesString

        // enable or disable some buttons
        enableButton(buttonReplies, !(voc.parentID != null && voc.secret))
        enableButton(buttonShare, !(voc.secret))
        enableButton(buttonPrev, playlistManager.isPreviousAvailable())
        enableButton(buttonNext, playlistManager.isNextAvailable())

        // If it's a main voc disable options
        if (voc.vocID < 0) {
            layoutOptions.visibility = View.INVISIBLE
        } else {
            layoutOptions.visibility = View.VISIBLE
        }
    }

    // -- Audio Speed Helper Methods -- //

    private fun setSpeed() {
        mediaSpeed = AudioPlayer.shared.getCurrentSpeed()
        setSpeedBtnText()
    }

    private fun incSpeed() {
        mediaSpeed += .25f
        if (mediaSpeed > 2)
            mediaSpeed = 1f

        setSpeedBtnText()
        AudioPlayer.shared.setSpeed(mediaSpeed)
    }

    private fun setSpeedBtnText() {
        val mediaSpeedString = "${mediaSpeed}x"
        val ss = SpannableString(mediaSpeedString)
        ss.setSpan(RelativeSizeSpan(.85f), mediaSpeedString.length - 1, mediaSpeedString.length, SpannableString.SPAN_COMPOSING)
        buttonSpeed.text = ss
    }

    // -- Profile and Background Image Helper Methods -- //

    private fun setImage(voc: Voc) {
        val profileUrl: String?
        if (voc.userClientSlug == voc.parentClient?.slug) {
            profileUrl = voc.parentClient?.mediumIconURL
        } else if (!voc.isNotOpenFeed() && voc.userClientLogoMedium != null) {
            profileUrl = voc.userClientLogoMedium
        } else {
            profileUrl = null
        }

        if (profileUrl != null) {
            setBackgroundImage(profileUrl)
            setProfileImage(profileUrl)
        } else {
            imagePhoto.setImageDrawable(getDrawable(R.drawable.illustration_placeholder_user))
        }

    }

    private fun setProfileImage(profileUrl: String) {
        Glide.with(this)
                .load(profileUrl)
                .into(imagePhoto)
    }

    private fun setBackgroundImage(profileUrl: String) {
        Glide.with(this)
                .load(profileUrl)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(object : CustomViewTarget<LinearLayout, Drawable>(layoutBackground) {
                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        // error handling
                    }

                    override fun onResourceCleared(placeholder: Drawable?) {
                        // clear all resources
                    }

                    override fun onResourceReady(resource: Drawable, transition: com.bumptech.glide.request.transition.Transition<in Drawable>?) {
                        Blurry.delete(layoutBackground)
                        layoutBackground.backgroundDrawable = resource
                        blurBackground()
                    }
                })
    }

    private fun blurBackground() {
        Blurry.with(layoutBackground.context)
                .radius(10)
                .sampling(15)
                .color(Color.argb(98, 0, 0, 0))
                .onto(layoutBackground);
    }

    // -- Button Helper Methods -- //

    private fun enableButton(button: View, enabled: Boolean) {
        button.isEnabled = enabled
        if (enabled) {
            button.alpha = 1f
        } else {
            button.alpha = .5f
        }
    }

    private fun updateLikeButton() {
        currentVoc?.let {
            buttonLike.isEnabled = !it.didVote
            if (!it.didVote)
                buttonLike.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_voted_player, 0, 0)
            else if (it.didVoteUp) {
                buttonLike.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_voted_up_player, 0, 0)
            } else {
                buttonLike.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_voted_down, 0, 0)
            }
            buttonLike.text = getString(R.string.number_likes, it.score)
        }
    }

    private fun goToReplies() {
        val voc = currentVoc ?: return
        ActivityHelper.openVoc(this, voc, null, null)
    }

    // -- Seekbar Helper Methods -- //
    private fun updateSeekBar() {
        val progress = AudioPlayer.shared.getProgress()
        seekbar.progress = progress
        setProgressTextView(progress)
    }

    private fun setProgressTextView(progress: Int) {
        val voc = currentVoc ?: return

        textviewProgressStart.text = TimeHelper.getProgressString(0, progress)

        var duration: Int = voc.duration
        if (voc.isMain()) { // main voc
            duration = AudioPlayer.shared.getDuration()
            seekbar.max = duration
        }
        textviewProgressEnd.text = TimeHelper.getProgressString(progress, duration)
        if (duration - progress > 0)
            textviewProgressEnd.text = "-${textviewProgressEnd.text}"
    }

    // Play Button

    private fun updatePlayButton(voc: Voc) {
        if (AudioPlayer.shared.isPreparingVoc(voc.vocID)) {
            showDownloadAnimation()
        } else {
            hideDownloadAnimation()
            if (AudioPlayer.shared.isPlaying()) {
                buttonPlay.imageResource = R.drawable.ic_pause_player
            } else {
                buttonPlay.imageResource = R.drawable.ic_play_player
            }
        }
    }

    private fun showDownloadAnimation() {
        progressbar.visibility = View.VISIBLE
        buttonPlay.visibility = View.INVISIBLE
    }

    private fun hideDownloadAnimation() {
        progressbar.visibility = View.INVISIBLE
        buttonPlay.visibility = View.VISIBLE
    }

    private fun didVote(up: Boolean) {
        currentVoc?.let {
            val clientSlug = it.parentClient?.slug ?: VoctagKeys.COMMUNITY_SLUG
            if (up) {
                RequestHelper.shared.upVoteVoc(it.vocID, clientSlug, this)
            } else {
                RequestHelper.shared.downVoteVoc(it.vocID, clientSlug, this)
            }
            it.voteUp(up)
            updateLikeButton()
        }
    }

    private fun showVoteView() {
        currentVoc?.let {
            VoteActivity.onVoted = { up ->
                didVote(up)
            }
            val intent = Intent(this, VoteActivity::class.java).apply {
                if (it.isNotOpenFeed()) {
                    putExtra(VoctagKeys.EXTRA_CLIENT, it.parentClient!!.encode())
                }
            }
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }

    fun onDownloadBtnClicked() {
        downloadVocsHelper.onDownloadBtnClicked()
    }

    override fun update(o: Observable?, arg: Any?) {
        if (o is PlaylistManager) {
            if (arg == PlaylistManager.PLAY_LIST_CHANGES) {
                updateViews()
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.buttonPrev -> playlistManager.playPrevious()
            R.id.buttonPlay -> {
                currentVoc?.let { playlistManager.playVoc(it) }
            }
            R.id.buttonNext -> playlistManager.playNext()
            R.id.buttonReplies -> goToReplies()
            R.id.buttonSpeed -> incSpeed()
            R.id.buttonLike -> showVoteView()
            R.id.buttonClose -> finish()
            R.id.buttonShare -> ActivityHelper.shareVocLink(this, voc = currentVoc)
            R.id.buttonMore -> {
                currentVoc?.let { MoreDialogHelper.newInstance(this, it).showMoreDialog() }
            }
            R.id.buttonBackTen -> AudioPlayer.shared.backward()
            R.id.buttonNextThirty -> AudioPlayer.shared.forward()
            R.id.imagePhoto -> {
                currentVoc?.let {
                    if (it.userClientSlug != null) {
                        val client = Client(it.userClientSlug!!, it.userClientThemeColor!!)
                        client.following = true
                        ActivityHelper.openClient(this, client, Bundle().apply {
                            putBoolean(VoctagKeys.EXTRA_RELOAD_CLIENT, true)
                        })
                    }
                }
            }
        }
    }
}
