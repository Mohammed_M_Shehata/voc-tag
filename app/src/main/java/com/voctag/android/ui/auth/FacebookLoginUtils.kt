package com.voctag.android.ui.auth

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.util.Log
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.ConnectivityHelper
import kotlinx.android.synthetic.main.layout_login_with_facebook.*

class FacebookLoginUtils {

    companion object {
        private const val PERMISSION_EMAIL = "email"
        private var instance: FacebookLoginUtils? = null
        fun getInstance(): FacebookLoginUtils {
            if (instance == null) {
                instance = FacebookLoginUtils()
            }
            return instance!!
        }
    }

    private lateinit var activity: Activity
    private lateinit var callbackManager: CallbackManager

    fun init(activity: Activity, onLoginSuccess: (isNewUser: Boolean) -> Unit, onLoginError: (error: String) -> Unit) {
        this.activity = activity
        callbackManager = CallbackManager.Factory.create()

        val loginManager = LoginManager.getInstance()
        loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                result?.let { loginRes ->
                    val emailPermissionGranted = loginRes.recentlyGrantedPermissions.contains(PERMISSION_EMAIL)
                    if (emailPermissionGranted) {
                        Log.d("com.test", "fbToken> ${loginRes.accessToken.token}")
                        loginUsingFacebook(activity, loginRes.accessToken, onLoginSuccess, { errorMessageId ->
                            hideProgress()
                            onLoginError(activity.getString(errorMessageId))
                        })
                    } else {
                        hideProgress()
                        onLoginError(activity.getString(R.string.facebook_email_permission_denied))
                    }
                }
            }

            override fun onCancel() {
                hideProgress()
            }

            override fun onError(error: FacebookException?) {
                hideProgress()
                onLoginError(error?.message + "")
            }
        })

        activity.buttonFacebookLogin.setOnClickListener {
            if (ConnectivityHelper.isOnline(activity)) {
                showProgress()
                loginManager.logInWithReadPermissions(activity, listOf(PERMISSION_EMAIL))
            } else {
                onLoginError(activity.getString(R.string.nointernet))
            }
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun loginUsingFacebook(activity: Activity, accessToken: AccessToken, onLoginSuccess: (isNewUser: Boolean) -> Unit, onLoginError: (errorMessageId: Int) -> Unit) {
        RequestHelper.shared.loginWithFacebook(accessToken.token, onLoginSuccess, onLoginError, activity)
    }

    private fun showProgress() {
        activity.buttonFacebookLogin.showProgress {
            buttonTextRes = R.string.register_btn_facebook_text
            progressColor = Color.WHITE
        }
    }

    private fun hideProgress() {
        activity.buttonFacebookLogin.hideProgress(R.string.register_btn_facebook_text)
    }
}

