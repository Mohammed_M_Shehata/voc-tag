package com.voctag.android.ui.main.profile.settings

import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.voctag.android.R
import com.voctag.android.ui.VoctagApp
import org.jetbrains.anko.find

class NotificationSettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_settings)
        setupToolbar()
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.notifications_settings_container, NotificationSettingsFragment())
                .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupToolbar() {
        val toolbar: Toolbar = find(R.id.toolbar)
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val title: TextView = toolbar.find(R.id.toolbarTitle)
        title.text = VoctagApp.getTranslation(R.string.settings_notifications_title)
    }
}