package com.voctag.android.ui.main.mentor.detail

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.PlaylistManager
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.customViews.CustomButton
import com.voctag.android.ui.customViews.PlayButton
import com.voctag.android.ui.VoctagApp
import org.jetbrains.anko.find
import org.jetbrains.anko.image
import org.jetbrains.anko.textColor

class MentorAccessActivity : AppCompatActivity() {

    // MARK: - Reactive Variables

    companion object {
        var onFollowingChanged: ((client: Client) -> Unit)? = null
    }

    // MARK: - ClientAccessActivity

    private lateinit var accessCodeTextInput: EditText
    private lateinit var background: View
    private lateinit var overlay: RelativeLayout
    private lateinit var playButton: PlayButton
    private lateinit var logo: ImageView
    private lateinit var nameLabel: TextView
    private lateinit var descriptionLabel: TextView
    private lateinit var redeemButton: CustomButton
    private lateinit var communityLabel: TextView
    private lateinit var infoLabel: TextView

    private lateinit var getAccessLabel: TextView
    private lateinit var getAccessCodeLabel: TextView
    private lateinit var getAccessView: RelativeLayout

    private lateinit var client: Client
    private var fakeVoc: Voc? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mentor_access)

        accessCodeTextInput = find(R.id.accessCodeTextInput)
        background = find(R.id.background)
        overlay = find(R.id.overlay)
        playButton = find(R.id.buttonPlay)
        logo = find(R.id.logo)
        nameLabel = find(R.id.nameLabel)
        descriptionLabel = find(R.id.descriptionLabel)
        redeemButton = find(R.id.redeemButton)
        communityLabel = find(R.id.communityLabel)
        infoLabel = find(R.id.infoLabel)
        getAccessLabel = find(R.id.getAccessLabel)
        getAccessCodeLabel = find(R.id.getAccessCodeLabel)
        getAccessView = find(R.id.getAccessView)

        // get params from intent
        client = Client.decode(intent.getStringExtra(VoctagKeys.EXTRA_CLIENT))

        setupToolbar()

        val metrics = Resources.getSystem().displayMetrics
        val density = metrics.density.toInt()
        val screenHeight = metrics.heightPixels
        if (screenHeight <= 1280) {
            val logoParams = logo.layoutParams
            logoParams.height = 50 * density
            logo.layoutParams = logoParams

            val params = overlay.layoutParams
            params.height = 350 * density
            overlay.layoutParams = params
        } else if (screenHeight >= 2560) {
            val params = overlay.layoutParams
            params.height = 470 * density
            overlay.layoutParams = params
        } else {
            val params = overlay.layoutParams
            params.height = 370 * density
            overlay.layoutParams = params
        }

        nameLabel.text = client.name
        descriptionLabel.text = client.description

        if (client.mediumIconURL.isNotEmpty()) {
            logo.visibility = View.VISIBLE
            Glide.with(this)
                    .load(client.mediumIconURL)
                    .transform(RoundedCorners(30))
                    // Alternative: .transforms(CenterCrop(), RoundedCorners(radius))
                    .transition(DrawableTransitionOptions.withCrossFade()).into(logo)

        } else {
            logo.image = null
            logo.visibility = View.GONE
        }

        fakeVoc = client.mainAudioFakeVoc

        if (fakeVoc != null) {
            playButton.visibility = View.VISIBLE
            playButton.setVoc(fakeVoc!!)
        } else {
            playButton.visibility = View.GONE
        }

        PlaylistManager.shared.onPlaylistDidChange = {
            if (fakeVoc != null) {
                runOnUiThread {
                    playButton.setVoc(fakeVoc!!)
                }
            }
        }

        redeemButton.setOnClickListener {
            redeemAccessCode()
        }

        if (client.salesURL.isNotEmpty()) {
            getAccessCodeLabel.text = client.salesURL

            getAccessView.visibility = View.VISIBLE
            getAccessView.setOnClickListener {
                if (validateEmail(client.salesURL)) {
                    val url = Uri.parse("mailto:${client.salesURL}")
                    startActivity(Intent(Intent.ACTION_VIEW, url))
                } else {
                    try {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(client.salesURL)))
                    } catch (e: ActivityNotFoundException) {
                    }
                }
            }
        } else {
            getAccessView.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        onFollowingChanged = null
    }

    private fun setupToolbar() {
        val color = Color.parseColor(client.themeColorString)
        window.statusBarColor = color

        val toolbar: Toolbar = find(R.id.toolbar)
        toolbar.title = ""
        toolbar.setBackgroundColor(color)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        background.setBackgroundColor(color)
        redeemButton.background.setTint(color)
    }

    private fun validateEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun redeemAccessCode() {
        redeemButton.isEnabled = false
        RequestHelper.shared.redeemAccessCode(client.slug, accessCodeTextInput.text.toString(), {
            client.hasAccess = true
            client.following = true
            onFollowingChanged?.invoke(client)

            finish()
            ActivityHelper.openClient(this, client, onFollowingChanged)
        }, { invalid ->
            redeemButton.isEnabled = true
            if (invalid) {
                infoLabel.text = VoctagApp.getTranslation(R.string.community_closed_wrong_code)
                infoLabel.textColor = Color.RED
            }
        }, this@MentorAccessActivity)
    }
}
