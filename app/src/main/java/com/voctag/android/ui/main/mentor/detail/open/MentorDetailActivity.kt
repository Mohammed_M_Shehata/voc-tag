package com.voctag.android.ui.main.mentor.detail.open

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.AppBarLayout
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.PlayListHelper
import com.voctag.android.manager.PlaylistManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.User
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.MainActivity
import com.voctag.android.ui.main.mentor.detail.QuestionsFragment
import kotlinx.android.synthetic.main.activity_mentor_detail.*
import kotlinx.android.synthetic.main.layout_audio_visualizer.*
import kotlinx.android.synthetic.main.layout_mentor_detail_header.*

class MentorDetailActivity : AppCompatActivity() {
    companion object {
        var onFollowingChanged: ((client: Client) -> Unit)? = null
        const val PAGES_COUNT = 2
        const val SPEAKS_PAGE = 0
        const val QUESTIONS_PAGE = 1
    }

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var client: Client
    private lateinit var user: User
    private var mentorDetailHeader: MentorDetailHeader? = null
    private lateinit var viewPager: ViewPager
    private val speaksFragment = SpeaksFragment()
    private val questionsFragment = QuestionsFragment()
    private var currentPage: Int = SPEAKS_PAGE
    private var shouldReload: Boolean = false

    private val playListHelper = PlayListHelper(object : PlayListHelper.PlayListListener {
        override fun isAutoPlayNextEnabled(): Boolean {
            return isOnSpeaksTab()
        }

        override fun getPlayList(vocId: Int): ArrayList<Voc> {
            return getVocsForCurrentTab()
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mentor_detail)

        // set current user
        user = PersistDataHelper.shared.loadUser()!!

        // update data from intent
        updateDataFromIntentParams(intent)

        if(client.themeColor.isNullOrEmpty()){
            loadClient(client.slug)
        }

        initUi()
    }

    override fun onResume() {
        super.onResume()

        if (shouldReload) {
            if (isOnSpeaksTab()){
                speaksFragment.reloadData()
            }else{
                questionsFragment.reloadData()
            }
        }

        playListHelper.onResume(this)
        playListHelper.addVisualizer(layout_visualizer)

        PlaylistManager.shared.onPlaylistDidChange = {
            mentorDetailHeader?.setupMainAudio()
            questionsFragment.updateOnPlaylistDidChange()
            speaksFragment.updateOnPlaylistDidChange()
        }

        VocCreationManager.shared.parentClient = getCurrentClient()
        VocCreationManager.shared.parentVoc = null
    }

    override fun onStop() {
        super.onStop()
        playListHelper.onStop(this)
    }

    /**
     * this is used to navigate either to the question or speaks tab
     * Also to set if the client is beeing followed automatically
     */
    private fun updateDataFromIntentParams(intent: Intent?) {
        intent?.let {
            currentPage = intent.getIntExtra(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, QUESTIONS_PAGE)

            if (intent.hasExtra(VoctagKeys.EXTRA_CLIENT)) {
                val newClient = Client.decode(intent.getStringExtra(VoctagKeys.EXTRA_CLIENT))
                // check if client is already open
                if (this::client.isInitialized) {
                    if (newClient.slug != client.slug) {
                        ActivityHelper.openClient(this, newClient, onFollowingChanged, currentPage, extras = intent.extras, enableSingleActivityMode = false)
                    } else {
                        shouldReload = true
                    }
                } else {
                    client = newClient
                    val reloadClient = intent.getBooleanExtra(VoctagKeys.EXTRA_RELOAD_CLIENT, false)
                    if (reloadClient) {
                        loadClient(client.slug)
                    }
                }
            }
            if (intent.hasExtra(VoctagKeys.EXTRA_FOLLOW_CLIENT) && intent.getBooleanExtra(VoctagKeys.EXTRA_FOLLOW_CLIENT, false)) {
                followClient()
            }
            if (intent.hasExtra(VoctagKeys.EXTRA_CLIENT_SLUG)) {
                if(!this::client.isInitialized){
                    client = Client()
                    client.slug = VoctagKeys.EXTRA_CLIENT_SLUG
                }else{
                    client.slug = VoctagKeys.EXTRA_CLIENT_SLUG
                }
            }
        }
    }

    private fun initUi() {
        // init the swipe refresh layout
        swipeRefreshLayout = srl_mentordetail
        swipeRefreshLayout.setOnRefreshListener {
            loadClient(client.slug)
            if (currentPage == SPEAKS_PAGE) {
                speaksFragment.reloadData()
            } else {
                questionsFragment.reloadData()
            }
        }
        swipeRefreshLayout.setColorSchemeColors(Color.parseColor(client.themeColorString))

        // init the viewpager
        viewPager = vp_mentordetail_content
        viewPager.adapter = MentorDetailViewPagerAdapter(supportFragmentManager, speaksFragment, questionsFragment)
        viewPager.offscreenPageLimit = PAGES_COUNT
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                if (client.following) {
                    currentPage = position
                    updateRecordButton()
                    swipeRefreshLayout.isRefreshing = false
                }
            }
        })

        //navigate to current page
        viewPager.currentItem = currentPage

        // disable swipe to refresh to prevent reloading
        vp_mentordetail_content.setOnTouchListener { _, event ->
            srl_mentordetail.isEnabled = false
            when (event.action) {
                MotionEvent.ACTION_UP -> srl_mentordetail.isEnabled = true
            }
            false
        }

        // init the tabs with icons
        tl_mentordetail_tabs.setSelectedTabIndicatorColor(Color.parseColor(client.themeColorString))
        tl_mentordetail_tabs.setupWithViewPager(viewPager)
        tl_mentordetail_tabs.getTabAt(0)?.setIcon(R.drawable.ic_tab_list)
        tl_mentordetail_tabs.getTabAt(1)?.setIcon(R.drawable.ic_tab_qa)

        //check to show or hide the following overlay
        checkFollowingClient()

        setupStatusbar()
        setupToolbar()
        setupMentorDetailHeader()
        setupRecordButton()
    }

    private fun setupStatusbar() {
        ActivityHelper.makeStatusBarTransparent(this)
        ViewCompat.setOnApplyWindowInsetsListener(swipeRefreshLayout) { _, insets ->
            ActivityHelper.setMarginTop(tb_mentordetail, insets.systemWindowInsetTop)
            insets.consumeSystemWindowInsets()
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(tb_mentordetail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        abl_mentordetail.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
            val total = abl_mentordetail.height - tb_mentordetail.height
            val offset = (verticalOffset + total).toFloat() / total
            swipeRefreshLayout.isRefreshing = false
            swipeRefreshLayout.isEnabled = verticalOffset == 0
            mentorDetailHeader?.handleOpacity(1 - offset)
        })

        btn_mentordetail_share.setOnClickListener {
            ActivityHelper.shareVocLink(this, client = client)
        }
        btn_mentordetail_share.visibility = View.VISIBLE
    }

    private fun setupMentorDetailHeader() {
        val mentorHeader: View = cl_mentorheader
        mentorHeader.post {
            mentorDetailHeader = MentorDetailHeader(mentorHeader)
            mentorDetailHeader?.setup(client, this)
        }
    }

    private fun setupRecordButton() {
        btn_mentordetail_ask.background.setTint(Color.parseColor(client.themeColorString))
        btn_mentordetail_ask.setOnClickListener {
            if (isOnSpeaksTab() && user.isOwnerOf(client.slug)) {
                VocCreationManager.shared.openRecordView(this, viewPager.currentItem)
            } else {
                if (client.isSecretVocEnabled) {
                    ActivityHelper.openAskPlanChooser(this, client, viewPager.currentItem, client.secretVocPrice)
                } else {
                    VocCreationManager.shared.openAskReplyView(this, viewPager.currentItem)
                }
            }
        }
        updateRecordButton()
    }

    private fun updateRecordButton() {
        if (user.isOwnerOf(client.slug)) {
            btn_mentordetail_ask.setText(R.string.community_client_speak_button_title)
            if (isOnSpeaksTab()) {
                btn_mentordetail_ask.visibility = View.VISIBLE
            } else {
                btn_mentordetail_ask.visibility = View.GONE
            }
        } else {
            btn_mentordetail_ask.setText(R.string.community_client_ask_button_title)
            if (isOnSpeaksTab()) {
                btn_mentordetail_ask.visibility = View.GONE
            } else {
                btn_mentordetail_ask.visibility = View.VISIBLE
            }
        }
    }

    private fun checkFollowingClient() {
        if (client.following) {
            rl_mentordetail_follow.visibility = View.GONE
            updateRecordButton()
        } else {
            enableScrolling(false)
            rl_mentordetail_follow.visibility = View.VISIBLE
            btn_mentordetail_ask.visibility = View.GONE
            rl_mentordetail_follow.setOnTouchListener { _, _ -> true }
            btn_mentordetail_follow.setOnClickListener {
                followClient()
            }
        }
    }

    private fun followClient() {
        btn_mentordetail_follow.isEnabled = false
        RequestHelper.shared.followClient(client.slug, { success ->
            btn_mentordetail_follow.isEnabled = true
            if (success) {
                client.following = true
                checkFollowingClient()
                onFollowingChanged?.invoke(client)
                enableScrolling(true)
            }
        }, this)
    }

    private fun enableScrolling(enable: Boolean) {
        val params = ctl_mentordetail.layoutParams as AppBarLayout.LayoutParams
        if (enable) {
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL.or(AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED)
        } else {
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
        }

        ctl_mentordetail.layoutParams = params
    }

    private fun loadClient(slug: String) {
        RequestHelper.shared.loadClientDetail(slug, { client ->
            client?.let {
                updateCurrentClient(client)
            }
        }, this)
    }

    private fun updateCurrentClient(client: Client) {
        this.client = client
        setupMentorDetailHeader()
    }

    private fun getVocsForCurrentTab(): ArrayList<Voc> {
        if (isOnSpeaksTab()) {
            return speaksFragment.vocs
        }
        return questionsFragment.vocs
    }

    private fun isOnSpeaksTab(): Boolean {
        return viewPager.currentItem == SPEAKS_PAGE
    }

    fun getCurrentClient() = client

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        updateDataFromIntentParams(intent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isTaskRoot) {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // call fragment if its a secret voc published
        if( data?.getBooleanExtra(VoctagKeys.EXTRA_SHOW_CONGRATULATIONS_DIALOG, false) == true){
            questionsFragment.onActivityResult(requestCode, resultCode, data)
        }
    }
}