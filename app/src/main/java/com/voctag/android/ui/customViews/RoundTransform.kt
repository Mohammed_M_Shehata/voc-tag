package com.voctag.android.ui.customViews

import android.content.res.Resources
import android.graphics.*
import com.squareup.picasso.Transformation

class RoundTransform(private val radius: Int) : Transformation {

    override fun transform(source: Bitmap): Bitmap {
        val paint = Paint()
        paint.isAntiAlias = true
        paint.shader = BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)

        val density = Resources.getSystem().displayMetrics.density
        val radius = (radius - 3) * density //radius is not equal densityPixels

        val bitmap = Bitmap.createBitmap(source.width, source.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawRoundRect(0f, 0f, source.width.toFloat(), source.height.toFloat(), radius, radius, paint)

        if (source != bitmap) {
            source.recycle()
        }

        return bitmap
    }

    override fun key() = "rounded"
}