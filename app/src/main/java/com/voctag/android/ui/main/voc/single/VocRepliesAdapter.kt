package com.voctag.android.ui.main.voc.single

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.voctag.android.R
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.manager.PlaylistManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.voc.card.VocCardViewHolder
import java.util.*

class VocRepliesAdapter(private val activity: Activity, private var vocs: ArrayList<Voc>, private var client: Client? = null) : RecyclerView.Adapter<VocCardViewHolder>() {

    // MARK: - Reactive Variables

    var onDeletedVoc: (() -> Unit)? = null
    var onWillOpenVoting: (() -> Unit)? = null

    // MARK: - FeedDetailAdapter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VocCardViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_voc_card, parent, false)
        return VocCardViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: VocCardViewHolder, position: Int) {
        val voc = vocs[position]
        voc.parentClient = client

        val holder = (viewHolder as VocCardViewHolder)
        holder.setup(voc, activity, removeBadge = position != 0)
        holder.onWillOpenVoting = onWillOpenVoting
        holder.onPressedViewHolder = { _ ->
            val parentVoc = VocCreationManager.shared.parentVoc
            voc.newRepliesCount = 0
            if (!(parentVoc != null && parentVoc.secret)) {
                ActivityHelper.openVoc(activity, voc, null, onDeletedVoc)
            }
        }
    }

    override fun getItemCount(): Int {
        return vocs.size
    }

    // MARK: - Public

    fun update(vocs: ArrayList<Voc>) {
        this.vocs = vocs
        notifyDataSetChanged()
    }

    fun updateOnPlaylistDidChange() {
        PlaylistManager.shared.onPlaylistDidChange = {
            activity.runOnUiThread {
                notifyDataSetChanged()
            }
        }
    }
}
