package com.voctag.android.ui.main.mentor.list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.paginate.Paginate
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.ConnectivityHelper
import com.voctag.android.helper.PaginateHelper
import com.voctag.android.helper.UiUtils
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.FeedFilterManager
import com.voctag.android.manager.OnboardingManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Category
import com.voctag.android.model.Client
import com.voctag.android.ui.VoctagApp
import com.voctag.android.ui.main.MainActivity
import kotlinx.android.synthetic.main.fragment_mentor_list.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class MentorListFragment : Fragment(), Paginate.Callbacks {

    companion object {
        val TAG: String = MentorListFragment::class.java.simpleName
    }

    private lateinit var mentorsAdapter: MentorListAdapter
    private lateinit var categoriesAdapter: CategoriesAdapter
    private lateinit var mainActivity: MainActivity

    private var clients = ArrayList<Client>()
    private var members = ArrayList<Client>()
    private var topTen = ArrayList<Client>()
    private var shouldReload = false
    private var loading: Boolean = false
    private var allItemsLoaded: Boolean = false
    private val categoriesList = ArrayList<Category>()
    private var errorHappened = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity = activity as MainActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mentor_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showOnboarding()

        allItemsLoaded = false

        toolbarButtonSearch.setOnClickListener {
            showSearchView()
        }

        emptyViewMentors.visibility = View.GONE

        // Init Mentors Adapter, Recyclerview and load Data
        mentorsAdapter = MentorListAdapter(mainActivity)
        mentorsAdapter.update(members, clients, topTen)
        mentorsAdapter.onFollowingChanged = { followedClient ->
            shouldReload = true
            mentorsAdapter.onFollowingChanged(followedClient)
        }

        recyclerviewMentors.adapter = mentorsAdapter
        val paginate = Paginate.with(recyclerviewMentors, this)
                .setLoadingTriggerThreshold(2)
                .setLoadingListItemCreator(PaginateHelper.getLoadingListCreatorMentorList())
                .build()
        paginate.setHasMoreDataToLoad(!allItemsLoaded)

        loadData()

        // Init Categories Adapter and Recyclerview and load categories
        categoriesAdapter = CategoriesAdapter(requireActivity(), categoriesList) {
            loadDataWithAnimation()
        }
        recyclerviewCategories.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        recyclerviewCategories.adapter = categoriesAdapter
        appbarlayoutCategories.setExpanded(true, true)
        fetchCategories()

        // Init Swipe to refresh layout
        swipeRefreshLayout.setColorSchemeColors(VoctagApp.getColor(R.color.main))
        swipeRefreshLayout.setOnRefreshListener {
            loadDataWithAnimation()
        }
    }

    override fun onResume() {
        super.onResume()

        VocCreationManager.shared.parentClient = null
        VocCreationManager.shared.parentVoc = null

        if (shouldReload) {
            mentorsAdapter.notifyDataSetChanged()
        }

        mainActivity.addVisualizer(visualizerMentorList)
    }

    private fun showOnboarding() {
        if (!OnboardingManager.shared.hasUserSeenOnboarding(OnboardingManager.OnboardingType.Mentors)) {
            MaterialAlertDialogBuilder(context)
                    .setMessage(getString(R.string.onboarding_text_mentors))
                    .setPositiveButton(getString(R.string.onboarding_confirm_title), null)
                    .show();

            OnboardingManager.shared.userSeenOnboarding(OnboardingManager.OnboardingType.Mentors)
        }
    }

    private fun showSearchView() {
        val intent = Intent(mainActivity, MentorSearchActivity::class.java).apply {
            putExtra(VoctagKeys.EXTRA_MEMBER_CLIENTS, members)
            putExtra(VoctagKeys.EXTRA_CLIENTS, clients)
            putExtra(VoctagKeys.EXTRA_TOP_CLIENTS, topTen)
        }
        mainActivity.startActivity(intent)
        mainActivity.overridePendingTransition(0, 0)
    }

    private fun setClients(resetPageIndex: Boolean) {
        mentorsAdapter.update(members, clients, topTen, resetPageIndex)
        mentorsAdapter.incrementPage()
        loading = errorHappened
        activity?.runOnUiThread {
            Handler().postDelayed({
                activity?.let { mentorsAdapter.notifyDataSetChanged() }
            }, 100)
        }
    }

    private fun loadDataWithAnimation() {
        allItemsLoaded = false
        swipeRefreshLayout.isRefreshing = true
        loadData()
    }

    private fun loadMore(pageIndex: Int) {
        if (pageIndex == 1) {
            return
        }

        loading = true
        errorHappened = false
        GlobalScope.launch {
            val clientsList = searchClients(FeedFilterManager.SearchClientScope.EXPLORE, pageIndex = pageIndex)
            clientsList?.let {
                clients.addAll(clientsList)
            }
            onSearchClientCompleteResponse(clientsList) { loadMore(pageIndex) }
            setClients(false)
        }
    }

    private fun loadData() {
        loading = true
        errorHappened = false
        GlobalScope.launch {
            val conferenceList = async {
                searchClients(null, true)
            }
            val memberList = async {
                searchClients(FeedFilterManager.SearchClientScope.MEMBER)
            }
            val topTenList = async {
                searchClients(FeedFilterManager.SearchClientScope.TOP_TEN)
            }
            val clientsList = async {
                searchClients(FeedFilterManager.SearchClientScope.EXPLORE)
            }

            clients.clear()
            members.clear()
            topTen.clear()

            memberList.await().let { it?.let { members.addAll(it) } }
            topTenList.await().let { it?.let { topTen.addAll(it) } }
            conferenceList.await().let { it?.let { clients.addAll(it) } }
            clientsList.await().let { it?.let { clients.addAll(it) } }

            onSearchClientCompleteResponse(clientsList.getCompleted()) { loadData() }
            activity?.runOnUiThread { recyclerviewMentors.layoutManager?.scrollToPosition(0) }
            setClients(true)
        }
    }

    private fun onSearchClientCompleteResponse(clientsList: ArrayList<Client>?, onRetryFunction: () -> Unit) {
        activity?.runOnUiThread {
            allItemsLoaded = errorHappened || clientsList.isNullOrEmpty()
            swipeRefreshLayout.isRefreshing = false
            showEmptyHolder()

            if (allItemsLoaded) {
                mentorsAdapter.notifyDataSetChanged()
            }

            if (errorHappened) {
                if (ConnectivityHelper.isOnline(mainActivity.applicationContext)) {
                    UiUtils.showSnackbar(swipeRefreshLayout, R.string.server_error)
                }else{
                    UiUtils.showSnackbar(swipeRefreshLayout, R.string.nointernet)
                }
            }
        }
    }

    private suspend fun searchClients(searchClientScope: FeedFilterManager.SearchClientScope? = null, fetchConferene: Boolean = false, pageIndex: Int = 1): ArrayList<Client>? = suspendCoroutine { continuation ->
        val parameters = FeedFilterManager.shared.searchParametersForSearchClients(null, categoriesAdapter.currentSelectedCategory?.name, searchClientScope, pageIndex, fetchConferene)
        RequestHelper.shared.searchClients(parameters, { response, errorHappened, categoryName ->
            if (isNotCurrentCategory(categoryName)) {
                continuation.resume(null)
                return@searchClients
            }
            if (errorHappened) {
                this.errorHappened = true
            }
            continuation.resume(response)
        }, context = activity as? Activity, categoryName = categoriesAdapter.currentSelectedCategory?.name)
    }

    private fun showEmptyHolder() {
        if (this.clients.size + members.size + topTen.size == 0)
            emptyViewMentors.visibility = View.VISIBLE
        else
            emptyViewMentors.visibility = View.GONE
    }

    //todo proper error handling. eventually only load once a day?
    private fun fetchCategories() {
        RequestHelper.shared.fetCategories({ categories, errorHappened ->
            if (categories != null) {
                categoriesList.clear()
                categoriesList.addAll(categories)
                categoriesAdapter.notifyDataSetChanged()
            }
        }, context = activity as? Activity)
    }

    private fun isNotCurrentCategory(categoryName: String?) =
            (categoryName == null && categoriesAdapter.currentSelectedCategory != null) ||
                    (categoryName != null && categoryName != categoriesAdapter.currentSelectedCategory?.name)

    override fun onLoadMore() {
        loadMore(mentorsAdapter.getPageIndex())
    }

    override fun isLoading(): Boolean {
        return loading
    }

    override fun hasLoadedAllItems(): Boolean {
        return allItemsLoaded
    }
}
