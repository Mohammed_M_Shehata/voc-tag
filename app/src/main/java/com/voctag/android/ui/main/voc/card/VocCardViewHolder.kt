package com.voctag.android.ui.main.voc.card

import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.api.firebase.AnalyticsLoggerUtils
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.TimeHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.AudioPlayer
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.VoctagApp
import com.voctag.android.ui.main.vote.VoteActivity
import kotlinx.android.synthetic.main.item_voc_card.view.*
import org.jetbrains.anko.image
import org.jetbrains.anko.imageResource


open class VocCardViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    companion object {
        const val AUDIO_VOC_MAX_LINES = 2
    }

    // MARK: - Reactive Variables

    var onPressedViewHolder: ((isReply: Boolean) -> Unit)? = null
    var onWillOpenVoting: (() -> Unit)? = null


    // MARK: - VocCellViewHolder

    lateinit var voc: Voc
    lateinit var activity: Activity
    private var removePrivateBadge: Boolean = false
    private val handler = Handler()
    private var runnable: Runnable? = null

    init {
        view.cv_voccard.setOnClickListener {
            onPressedViewHolder?.invoke(isMyReply())
        }

        view.cv_voccard.setOnLongClickListener {
            //long pressed on transcript
            val clientSlug = voc.parentClient?.slug ?: VoctagKeys.COMMUNITY_SLUG
            AnalyticsLoggerUtils.getInstance().logVocLongTab(clientSlug, voc.vocID)
            showTranscript()
            false
        }

        view.tv_voccard_transcript.setOnClickListener {
            showTranscript()
        }

        view.btn_voccard_reply.setOnClickListener {
            VocCreationManager.shared.parentVoc = voc
            VocCreationManager.shared.parentClient = voc.parentClient

            // to open voc details before ask/reply view
            onPressedViewHolder?.invoke(isMyReply())
            Handler().post {
                VocCreationManager.shared.openAskReplyView(activity, finishDetetailsOnBack = true)
            }
        }

        view.cl_voccard_vote.setOnClickListener {
            if (!voc.didVote) {
                showVoteView()
            }
        }

        runnable = Runnable { updateElapsedTime() }
    }

    fun setup(voc: Voc, activity: Activity, removeBadge: Boolean = false) {
        this.voc = voc
        this.activity = activity
        this.removePrivateBadge = removeBadge
        updateViews()
    }

    fun updateViews() {
        updateNickname()
        updateMentorBadge()
        updateDateAndLocation()
        updateVoting()
        updateTitleAndTranscript()
        updateDuration()
        updateReplies()
        //updates for text voc cards
        updateTextVocCard()
        // updates for secret vocs
        updateSecretVocCard()

        if (AudioPlayer.shared.isPlayingVoc(voc.vocID)) {
            updateElapsedTime()
        }
    }

    private fun updateNickname() {
        if (voc.userNickname != null) {
            view.tv_voccard_nickname.visibility = View.VISIBLE
            view.tv_voccard_nickname.text = voc.userNickname!!
        } else {
            view.tv_voccard_nickname.visibility = View.GONE
        }
        view.tv_voccard_nickname.setOnClickListener {
            if (voc.userClientSlug != null) {
                openClientDetails()
            }
        }
    }

    private fun updateMentorBadge() {
        if (voc.createdByClientOwner) {
            view.iv_voccard_official.visibility = View.VISIBLE
        } else {
            view.iv_voccard_official.visibility = View.GONE
        }
        view.iv_voccard_official.imageResource = ActivityHelper.getVocBadgeImageResource(voc.parentClient?.isTopMentor)
    }

    private fun updateDateAndLocation() {
        view.tv_voccard_datelocation.text = voc.dateLocationString
    }

    private fun updateVoting() {
        view.tv_voccard_vote.text = voc.scoreString
        updateLikedButton()
    }

    private fun updateLikedButton() {
        if (voc.didVote) {
            if (voc.didVoteUp) {
                view.iv_voccard_vote.image = VoctagApp.getDrawable(R.drawable.ic_vote_up)
            } else {
                view.iv_voccard_vote.image = VoctagApp.getDrawable(R.drawable.ic_downvoted_small_main_light_24dp)
            }
        } else {
            view.iv_voccard_vote.image = VoctagApp.getDrawable(R.drawable.ic_vote)
        }
        if (voc.isNotOpenFeed()) {
            val color = Color.parseColor(voc.parentClient!!.themeColorString)
            if (voc.didVote) {
                view.iv_voccard_vote.image?.setTint(color)
            }
        } else {
            if (voc.didVote) {
                view.iv_voccard_vote.image?.setTint(VoctagApp.getColor(R.color.main))
            }
        }
    }

    private fun updateTitleAndTranscript() {
        if (voc.title.isNullOrEmpty() && TextUtils.isEmpty(voc.vocAudioURL)) {
            view.tv_voccard_transcript.visibility = View.GONE
            view.tv_voccard_title.text = voc.attributedTranscript
            val typeface = ResourcesCompat.getFont(view.context, R.font.roboto_medium)
            view.tv_voccard_title.typeface = typeface;
        } else {
            view.tv_voccard_transcript.visibility = View.VISIBLE
            view.tv_voccard_transcript.text = voc.attributedTranscript
            view.tv_voccard_title.text = voc.title
            val typeface = ResourcesCompat.getFont(view.context, R.font.roboto_medium)
            view.tv_voccard_title.typeface = typeface;
        }
    }

    private fun updateDuration() {
        view.tv_voccard_duration.text = TimeHelper.getDurationString(voc.duration)
    }

    private fun updateReplies() {
        //replies
        when {
            voc.repliesCount == 0 -> {
                view.tv_voccard_replies.visibility = View.GONE
            }
            voc.repliesCount == 1 -> {
                view.tv_voccard_replies.visibility = View.VISIBLE
                view.tv_voccard_replies.text = view.context.getString(R.string.voc_card_replies_text_one, voc.repliesCount)
            }
            else -> {
                view.tv_voccard_replies.visibility = View.VISIBLE
                view.tv_voccard_replies.text = view.context.getString(R.string.voc_card_replies_text_multiple, voc.repliesCount)
            }
        }

        when {
            voc.newRepliesCount == 0 -> {
                view.tv_voccard_replies_new.visibility = View.GONE
            }
            voc.newRepliesCount >= 1 -> {
                view.tv_voccard_replies_new.visibility = View.VISIBLE
                view.tv_voccard_replies_new.text = "·   " + view.context.getString(R.string.voc_card_replies_new_text_one, voc.newRepliesCount)
            }
        }
    }

    private fun updateTextVocCard() {
        view.btn_voccard_play.setVoc(voc)

        if (voc.title.isNullOrEmpty() && TextUtils.isEmpty(voc.vocAudioURL)) {
            view.btn_voccard_play.visibility = View.GONE
            view.tv_voccard_duration.visibility = View.GONE
            view.tv_voccard_title.maxLines = Int.MAX_VALUE
        } else {
            view.btn_voccard_play.visibility = View.VISIBLE
            view.tv_voccard_duration.visibility = View.VISIBLE
            view.tv_voccard_title.maxLines = AUDIO_VOC_MAX_LINES
        }
        val params = view.btn_voccard_reply.layoutParams as ConstraintLayout.LayoutParams
        if (TextUtils.isEmpty(voc.vocAudioURL)) {
            params.marginEnd = activity.resources.getDimension(R.dimen.margin_8dp).toInt()
        } else {
            params.marginEnd = activity.resources.getDimension(R.dimen.margin_75dp).toInt()
        }
    }

    private fun updateSecretVocCard() {
        if (voc.secret) {
            view.tv_voccard_private.backgroundTintList = ColorStateList.valueOf(Color.parseColor(voc.parentClient?.themeColorString))
            view.btn_voccard_reply.visibility = View.GONE
            view.cl_voccard_vote.visibility = View.GONE
            view.tv_voccard_private.visibility = View.VISIBLE

        } else {
            view.cl_voccard_vote.visibility = View.VISIBLE
            view.btn_voccard_reply.visibility = View.VISIBLE
            view.tv_voccard_private.visibility = View.GONE
        }
    }

    private fun openClientDetails() {
        val client = Client(voc.userClientSlug!!, voc.userClientThemeColor!!)
        client.following = true
        ActivityHelper.openClient(activity, client, Bundle().apply {
            putBoolean(VoctagKeys.EXTRA_RELOAD_CLIENT, true)
        })
    }

    private fun didVote(up: Boolean) {
        val clientSlug = voc.parentClient?.slug ?: VoctagKeys.COMMUNITY_SLUG
        if (up) {
            RequestHelper.shared.upVoteVoc(voc.vocID, clientSlug, activity)
        } else {
            RequestHelper.shared.downVoteVoc(voc.vocID, clientSlug, activity)
        }
        voc.voteUp(up)
    }

    private fun showVoteView() {
        VoteActivity.onVoted = { up ->
            didVote(up)
            updateLikedButton()
            view.tv_voccard_vote.text = voc.scoreString
        }

        onWillOpenVoting?.invoke()

        val intent = Intent(activity, VoteActivity::class.java).apply {
            if (voc.isNotOpenFeed()) {
                putExtra(VoctagKeys.EXTRA_CLIENT, voc.parentClient!!.encode())
            }
        }
        activity.startActivity(intent)
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    private fun isMyReply(): Boolean {
        val user = PersistDataHelper.shared.loadUser() ?: return false
        return voc.userID == user.id && voc.parentID != null
    }

    private fun updateElapsedTime() {
        if (AudioPlayer.shared.isPlayingVoc(voc.vocID)) {
            val progress = AudioPlayer.shared.getProgress()
            view.tv_voccard_duration.text = VoctagApp.getTranslation(R.string.feed_cell_progress_text).replace("%%", TimeHelper.getProgressString(progress, voc.duration, true))
            handler.postDelayed(runnable, VoctagKeys.UPDATE_INTERVAL.toLong())
        }
    }

    private fun showTranscript() {
        if (voc.transcript.isEmpty()) {
            return
        }
        ActivityHelper.showMessageDialog(activity, voc.titleString, voc.attributedTranscript)
    }
}