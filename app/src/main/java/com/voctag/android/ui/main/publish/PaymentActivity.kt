package com.voctag.android.ui.main.publish

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.voctag.android.BuildConfig
import com.voctag.android.R

class PaymentActivity : AppCompatActivity() {

    companion object {
        const val AMOUNT: String = "EXTRA_AMOUNT"
        const val PAYMENT_NONCE: String = "PAYMENT_NONCE"
        val TAG = PaymentActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        val webView: WebView = findViewById(R.id.webview)
        webView.settings.javaScriptEnabled = true
        webView.addJavascriptInterface(MobilePaymentInterface(this), "MobilePaymentInterface")
        webView.webViewClient = WebViewClient()
        val amount = intent.getStringExtra(AMOUNT)
        webView.loadUrl(getString(R.string.payment_url, BuildConfig.WEB_URL, amount))
        Log.d(TAG, "amount $amount")
    }

    class MobilePaymentInterface(private val mActivity: PaymentActivity) {
        @JavascriptInterface
        fun setNonce(nonce: String) {
            val data = Intent()
            data.putExtra(PAYMENT_NONCE, nonce)
            mActivity.setResult(Activity.RESULT_OK, data)
            mActivity.finish()
        }
    }
}
