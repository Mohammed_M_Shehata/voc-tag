package com.voctag.android.ui.main.myFeed

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.paginate.Paginate
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.PaginateHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.FeedFilterManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Banner
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.VoctagApp
import com.voctag.android.ui.main.MainActivity
import com.voctag.android.ui.main.mentor.detail.open.MentorDetailActivity
import kotlinx.android.synthetic.main.fragment_my_feed.*
import kotlinx.android.synthetic.main.fragment_my_feed.view.*
import kotlinx.android.synthetic.main.layout_audio_visualizer.*

open class MyFeedFragment : Fragment(), View.OnClickListener, Paginate.Callbacks {

    companion object {
        private val TAG: String = MyFeedFragment::class.java.simpleName
    }

    private lateinit var mainActivity: MainActivity
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: MyFeedAdapter
    private var items = ArrayList<Any>()
    private var vocs = ArrayList<Voc>()
    private var banner = ArrayList<Banner>()
    private lateinit var paginate: Paginate
    private var loading = false
    private var allItemsLoaded = false
    private var shouldReload = false
    private var pageIndex: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity = activity as MainActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_my_feed, container, false)

        // init the swipe to refresh
        swipeRefreshLayout = view.srl_myfeed
        swipeRefreshLayout.setColorSchemeColors(VoctagApp.getColor(R.color.main))
        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            reloadData()
        }

        // init adapter and recyclerview for vocs
        linearLayoutManager = LinearLayoutManager(mainActivity, LinearLayoutManager.VERTICAL, false)
        adapter = MyFeedAdapter(mainActivity, this, items, { client, isClientReply ->
            if (isClientReply) {
                openClientDetails(client, true)
            } else {
                openClientDetails(client, false)
            }
        }, { client ->
            openClientDetails(client, true)
        })
        recyclerView = view.rv_myfeed
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter
        recyclerView.setPadding(recyclerView.paddingLeft, recyclerView.paddingTop, recyclerView.paddingRight, 0)

        //pagination
        paginate = Paginate.with(recyclerView, this)
                .setLoadingTriggerThreshold(2)
                .setLoadingListItemCreator(PaginateHelper.getLoadingListCreatorMyFeed())
                .build()
        paginate.setHasMoreDataToLoad(!allItemsLoaded)

        // init click listener
        view.btn_myfeed_empty.setOnClickListener(this@MyFeedFragment)

        return view
    }

    override fun onResume() {
        super.onResume()
        adapter.updateOnPlaylistDidChange()

        // show the empty view
        showEmptyViewIfNecessary()

        VocCreationManager.shared.parentClient = null
        VocCreationManager.shared.parentVoc = null
        VocCreationManager.shared.onPublished = {
            shouldReload = true
        }

        if (banner.isNullOrEmpty()) {
            loadBanner()
        }

        if (shouldReload) {
            reloadData()
        }

        mainActivity.addVisualizer(layout_visualizer)
    }

    private fun loadBanner() {
        RequestHelper.shared.getBanner({ response, errorHappened ->
            if (response != null) {
                banner.addAll(response)
                items.add(banner)
                adapter.setItems(items)
            }
        }, context = activity)
    }

    private fun loadMore(isCompleteReload: Boolean) {
        loading = true
        val parameters = FeedFilterManager.shared.searchParametersForCurrentFilter(null, pageIndex)
        RequestHelper.shared.searchVocs(parameters, { response, errorHappened ->
            swipeRefreshLayout.isRefreshing = false
            loading = false
            showEmptyViewIfNecessary()
            if (cl_myfeed_layout != null) {
                if (response != null) {
                    if (isCompleteReload) {
                        pageIndex++
                        items.removeAll(vocs)
                        vocs = response
                        items.addAll(response)
                        adapter.setItems(items)
                    } else {
                        pageIndex++
                        vocs.addAll(response)
                        items.addAll(response)
                        adapter.setItems(items)
                    }
                }
                if (!errorHappened && response.isNullOrEmpty()) {
                    allItemsLoaded = true
                    adapter.notifyDataSetChanged()
                }
                if (errorHappened) {
                    Snackbar.make(cl_myfeed_layout, getString(R.string.nointernet), Snackbar.LENGTH_LONG).show()
                }
            }
        }, context = activity as? Activity, myFeeds = true)
    }

    private fun reloadData() {
        pageIndex = 1
        allItemsLoaded = false
        vocs.clear()
        items.clear()
        loadMore(true)

        // add or load banner
        if (!banner.isNullOrEmpty()) {
            items.add(banner)
        } else {
            loadBanner()
        }
    }

    private fun showEmptyViewIfNecessary() {
        if (vocs.isEmpty() && allItemsLoaded) {
            cl_myfeed_empty?.visibility = View.VISIBLE
        } else {
            cl_myfeed_empty?.visibility = View.GONE
        }
    }

    private fun openClientDetails(client: Client?, openQuestionTab: Boolean = false) {
        client?.following = true
        client?.let {
            ActivityHelper.openClient(activity as Activity, client, Bundle().apply {
                putBoolean(VoctagKeys.EXTRA_RELOAD_CLIENT, true)
                if (openQuestionTab) {
                    putInt(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, MentorDetailActivity.QUESTIONS_PAGE)
                } else {
                    putInt(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, MentorDetailActivity.SPEAKS_PAGE)
                }
            })
        }
    }

    fun getPlayList(): ArrayList<Voc> {
        return vocs
    }

    override fun onLoadMore() {
        loadMore(false)
    }

    override fun isLoading(): Boolean {
        return loading
    }

    override fun hasLoadedAllItems(): Boolean {
        return allItemsLoaded
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_myfeed_empty -> (activity as MainActivity).navigateToMentorsView()
        }
    }
}