package com.voctag.android.ui.main.profile

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter.LengthFilter
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.NetworkError
import com.android.volley.toolbox.HttpHeaderParser
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.ErrorHandler
import kotlinx.android.synthetic.main.activity_nickname_edit.*
import java.nio.charset.Charset

class NicknameEditActivity : AppCompatActivity(), View.OnClickListener {

    var nickname: String = PersistDataHelper.shared.loadUser()?.nickname.toString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nickname_edit)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        edittextNickname.setText(nickname)

        bindProgressButton(buttonUpdateNickname)
        buttonUpdateNickname.setOnClickListener(this@NicknameEditActivity)

        //Nickname validator
        edittextNickname.filters = arrayOf(LengthFilter(VoctagKeys.NICKNAME_CHARACTER_MAX))
        edittextNickname.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(editable: Editable) {
                //show validation message
                if (edittextNickname.text?.length!! < 3) {
                    inputlayoutNickname.error = getString(R.string.nickname_validate)
                    buttonUpdateNickname.isEnabled = false
                } else {
                    inputlayoutNickname.error = null
                    buttonUpdateNickname.isEnabled = true
                }
                //Disable the button if no change
                if (edittextNickname.text!!.toString().equals(nickname)) {
                    buttonUpdateNickname.isEnabled = false
                }
            }
        })
    }

    private fun updateNickname() {
        ActivityHelper.hideSoftKeyboard(this)
        showProgress()

        RequestHelper.shared.updateNickname(edittextNickname.text.toString().trim(), {
            hideProgress()
            Snackbar.make(layoutNicknameEdit, getString(R.string.nickname_success), Snackbar.LENGTH_LONG).show()
            nickname = edittextNickname.text.toString()
            buttonUpdateNickname.isEnabled = false
        }, {
            hideProgress()
            if (it.networkResponse?.statusCode == 422) {
                val errors: ErrorHandler = Gson().fromJson(String(it.networkResponse?.data
                        ?: ByteArray(0), Charset.forName(HttpHeaderParser.parseCharset(it.networkResponse?.headers))), ErrorHandler::class.java)
                errors.errors.forEach {
                    if (it.attribute.equals("nickname", true) && it.code.equals("taken")) {
                        inputlayoutNickname.error = getString(R.string.register_nickname_error)
                    }
                    if (it.attribute.equals("nickname", true) && it.code.equals("short")) {
                        inputlayoutNickname.error = getString(R.string.register_nickname_error_short)
                    }
                }
            }
            if (it is NetworkError && it.networkResponse?.statusCode != 422) {
                Snackbar.make(layoutNicknameEdit, getString(R.string.nointernet), Snackbar.LENGTH_LONG).show()
            }
        }, this@NicknameEditActivity)
    }

    private fun showProgress() {
        buttonUpdateNickname.isEnabled = false
        buttonUpdateNickname.showProgress {
            progressColorRes = R.color.white
            textMarginRes = R.dimen.keyline_0
        }
    }

    private fun hideProgress() {
        buttonUpdateNickname.isEnabled = true
        buttonUpdateNickname.hideProgress(R.string.nickname_button_cta)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.buttonUpdateNickname -> updateNickname()
        }
    }
}