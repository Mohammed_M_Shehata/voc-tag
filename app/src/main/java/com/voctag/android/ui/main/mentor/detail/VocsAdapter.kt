package com.voctag.android.ui.main.mentor.detail

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.voctag.android.R
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.voc.card.VocCardViewHolder
import java.util.*

class VocsAdapter(
        private val activity: Activity,
        private var vocsList: List<Voc>
) : RecyclerView.Adapter<VocCardViewHolder>() {

    var onDeletedVoc: (() -> Unit)? = null
    var onWillOpenVoting: (() -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VocCardViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_voc_card, parent, false)
        return VocCardViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: VocCardViewHolder, position: Int) {
        val voc = vocsList[position]

        viewHolder.setup(voc, activity)
        viewHolder.onWillOpenVoting = onWillOpenVoting
        viewHolder.onPressedViewHolder = { _ ->
            voc.newRepliesCount = 0
            ActivityHelper.openVoc(activity, voc, null, onDeletedVoc)
        }
    }

    override fun getItemCount() = vocsList.size

    fun setVocs(newVocs: ArrayList<Voc>) {
        vocsList = newVocs
        notifyDataSetChanged()
    }
}
