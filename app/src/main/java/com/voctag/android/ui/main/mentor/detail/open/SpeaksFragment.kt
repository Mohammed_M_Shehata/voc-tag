package com.voctag.android.ui.main.mentor.detail.open

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.paginate.Paginate
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.PaginateHelper
import com.voctag.android.manager.FeedFilterManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.mentor.detail.VocsAdapter
import kotlinx.android.synthetic.main.fragment_speaks.*
import kotlinx.android.synthetic.main.fragment_speaks.view.*


class SpeaksFragment : Fragment(), Paginate.Callbacks {

    companion object {
        private val TAG: String = SpeaksFragment::class.java.simpleName
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: VocsAdapter
    var vocs = ArrayList<Voc>()
    private lateinit var client: Client
    private lateinit var paginate: Paginate
    private var loading = false
    private var allItemsLoaded = false
    private var shouldReload = false
    private var pageIndex: Int = 1
    private var willOpenVoting = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vocs = ArrayList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_speaks, container, false)

        // init adapter and recyclerview for speaks
        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        adapter = VocsAdapter(activity as Activity, vocs)
        recyclerView = view.rv_speaks
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter

        // view should reload when voc is beeing deleted
        adapter.onDeletedVoc = {
            shouldReload = true
        }
        adapter.onWillOpenVoting = {
            willOpenVoting = true
        }

        // set current client
        client = getCurrentClient()

        // pagination
        paginate = Paginate.with(recyclerView, this)
                .setLoadingTriggerThreshold(2)
                .setLoadingListItemCreator(PaginateHelper.getLoadingListCreatorVocs())
                .build()
        paginate.setHasMoreDataToLoad(!allItemsLoaded)

        return view
    }

    override fun onResume() {
        super.onResume()
        VocCreationManager.shared.onPublished = {
            shouldReload = true
        }

        if (shouldReload) {
            reloadData()
        }
    }

    private fun loadMore() {
        loading = true
        val parameters = FeedFilterManager.shared.searchParametersForClientVocs(0, pageIndex, 0)
        RequestHelper.shared.loadClientVocs(client.slug, parameters, { response, errorHappened ->
            loading = false
            showEmptyViewIfNecessary()
            if (cl_speaksfragment != null) {
                if (response != null) {
                    pageIndex++
                    vocs.addAll(response)
                    adapter.setVocs(vocs)
                }
                if (!errorHappened && response.isNullOrEmpty()) {
                    allItemsLoaded = true
                    adapter.notifyDataSetChanged()
                }
                if (errorHappened) {
                    Snackbar.make(cl_speaksfragment, getString(R.string.nointernet), Snackbar.LENGTH_LONG).show()
                }
            }
        }, activity as Activity)
    }

    private fun getCurrentClient(): Client {
        return (activity as MentorDetailActivity).getCurrentClient()
    }

    private fun showEmptyViewIfNecessary() {
        if (vocs.isEmpty() && allItemsLoaded && tv_speaks_emptyview != null) {
            tv_speaks_emptyview.visibility = View.VISIBLE
            val user = PersistDataHelper.shared.loadUser()!!
            tv_speaks_emptyview.text = if (user.isOwnerOf(client.slug)) {
                getString(R.string.speaks_mentor_no_data)
            } else {
                getString(R.string.speaks_client_no_data)
            }
        } else {
            tv_speaks_emptyview?.visibility = View.GONE
        }
    }

    fun reloadData() {
        pageIndex = 1
        allItemsLoaded = false
        vocs.clear()
        loadMore()
    }

    fun updateOnPlaylistDidChange() {
        adapter.notifyDataSetChanged()
    }

    override fun onLoadMore() {
        loadMore()
    }

    override fun isLoading(): Boolean {
        return loading
    }

    override fun hasLoadedAllItems(): Boolean {
        return allItemsLoaded
    }
}