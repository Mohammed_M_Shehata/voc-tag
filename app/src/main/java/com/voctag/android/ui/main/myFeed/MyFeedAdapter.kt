package com.voctag.android.ui.main.myFeed

import android.app.Activity
import android.graphics.Typeface
import android.os.Handler
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.voctag.android.R
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.manager.PlaylistManager
import com.voctag.android.model.Banner
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.VoctagApp
import com.voctag.android.ui.main.voc.card.VocCardViewHolder
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import kotlinx.android.synthetic.main.item_my_feed.view.*
import org.jetbrains.anko.image
import java.util.*
import kotlin.math.roundToInt

class MyFeedAdapter(
        val activity: Activity,
        val fragment: Fragment,
        private var items: ArrayList<Any>,
        val onClientClickedListener: (client: Client?, isClientReply: Boolean) -> Unit,
        val onAskMonitorClicked: (client: Client?) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), View.OnClickListener {

    private val BANNER = 1
    private val VOC = 2

    val roundedDimen: Float = activity.resources.getDimension(R.dimen.dimen_6dp)
    lateinit var bannerRecyclerView: RecyclerView

    // MARK: - Reactive Variables
    var onDeletedVoc: (() -> Unit)? = null
    var onWillOpenVoting: (() -> Unit)? = null

    // MARK: - CommunityAdapter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(activity)
        val view: View
        when (viewType) {
            BANNER -> {
                view = inflater.inflate(R.layout.layout_banner, parent, false)
                return BannerViewHolder(view)
            }
            VOC -> {
                view = inflater.inflate(R.layout.item_my_feed, parent, false)
                return MyFeedViewHolder(view)
            }
            else -> {
                view = inflater.inflate(R.layout.item_my_feed, parent, false)
                return MyFeedViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        // only update the view when it's not already initialized. It's to prevent redrawing the banner which causes the flickering
        if (viewHolder.itemViewType == BANNER && !this::bannerRecyclerView.isInitialized) {
            fragment as MyFeedFragment
            viewHolder as BannerViewHolder
            val banner = getBannerList(position)
            val adapter = BannerItemAdapter(activity, banner)

            // add a snaphelper to make the scrolling to the next banner smooth
            val snapHelper: SnapHelper = PagerSnapHelper()
            val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            bannerRecyclerView = viewHolder.recyclerView

            // update right padding if it's only one banner
            if (banner.size == 1) {
                bannerRecyclerView.setPadding(24, 8, 0, 8)
            }

            bannerRecyclerView.layoutManager = layoutManager
            bannerRecyclerView.onFlingListener = null;
            snapHelper.attachToRecyclerView(bannerRecyclerView)
            bannerRecyclerView.adapter = adapter

            // automatic sroll to next item every 4 seconds
            val handler = Handler()
            handler.postDelayed(object : Runnable {
                override fun run() {
                    val totalItemCount: Int = adapter.itemCount
                    if (totalItemCount <= 1) return
                    val lastVisibleItemIndex = layoutManager.findLastVisibleItemPosition()
                    // when last position scroll to first
                    if (lastVisibleItemIndex + 1 >= totalItemCount) {
                        bannerRecyclerView.smoothScrollToPosition(1)
                    } else {
                        bannerRecyclerView.smoothScrollToPosition(lastVisibleItemIndex + 1)
                    }
                    handler.postDelayed(this, 4000)
                }
            }, 4000)
        } else if (viewHolder.itemViewType == VOC) {
            //related to voc card
            val voc = getVoc(position)
            viewHolder as VocCardViewHolder
            viewHolder.setup(voc, activity)
            viewHolder.onWillOpenVoting = onWillOpenVoting
            viewHolder.onPressedViewHolder = { isReply ->
                if (isReply) {
                    val newVoc = Voc(voc.parentID!!)
                    newVoc.parentClient = voc.parentClient
                    ActivityHelper.openVoc(activity, newVoc, voc, onDeletedVoc)
                } else {
                    voc.newRepliesCount = 0
                    ActivityHelper.openVoc(activity, voc, null, onDeletedVoc)
                }
            }

            // related to my feed item
            val holder = (viewHolder as MyFeedViewHolder)
            holder.timeLocationTextView.text = voc.dateLocationString
            holder.photoImageView.image = null
            voc.parentClient?.smallIconURL?.let {
                if (it.isNotEmpty())
                    Glide.with(activity)
                            .load(it)
                            .apply(RequestOptions().transforms(RoundedCornersTransformation(roundedDimen.roundToInt(), 0)))
                            .into(holder.photoImageView)
            }
            holder.layoutHeader.tag = voc
            holder.layoutHeader.setOnClickListener(this)

            val actionText = when {
                voc.isClientReply -> activity.getString(R.string.feed_reply)
                voc.isPodcast -> activity.getString(R.string.feed_podcast)
                else -> activity.getString(R.string.feed_speak)
            }

            val myTypeface = Typeface.create(ResourcesCompat.getFont(activity, R.font.roboto_regular), Typeface.NORMAL)
            val textName = "${voc.clientUserNickname} $actionText"
            val formattedTextName = SpannableString(textName)
            formattedTextName.setSpan(CustomTypefaceSpan(myTypeface), voc.clientUserNickname?.length!!, textName.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            formattedTextName.setSpan(ForegroundColorSpan(VoctagApp.getColor(R.color.mid)), voc.clientUserNickname?.length!!, textName.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            holder.clientNameTextView.text = formattedTextName
            holder.timeLocationTextView.text = voc.replyDateLocationString

            holder.askMonitorTextView.text = activity.getString(R.string.ask_monitor_question, voc.clientUserNickname)
            holder.askMonitorTextView.tag = voc
            holder.askMonitorTextView.setOnClickListener(this)

            // hide the devider when its the last item
            if (isLastPosition(position)) {
                holder.dividerView.visibility = View.GONE
            } else {
                holder.dividerView.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (items[position] is ArrayList<*>) {
            return BANNER
        } else {
            return VOC
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun getVoc(position: Int): Voc {
        return items[position] as Voc
    }

    private fun getBannerList(position: Int): ArrayList<Banner> {
        return items[position] as ArrayList<Banner>
    }

    private fun isLastPosition(position: Int): Boolean = items.size == position + 1

    fun setItems(newItems: ArrayList<Any>) {
        items = newItems
        notifyDataSetChanged()
    }

    fun updateOnPlaylistDidChange() {
        PlaylistManager.shared.onPlaylistDidChange = {
            activity.runOnUiThread {
                notifyDataSetChanged()
            }
        }
    }

    override fun onClick(v: View?) {
        val voc = v?.tag as Voc
        when (v.id) {
            R.id.cl_itemmyfeed_header -> onClientClickedListener(voc.parentClient, voc.isClientReply)
            R.id.tv_itemmyfeed_ask -> onAskMonitorClicked(voc.parentClient)
        }
    }

    class BannerViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var recyclerView: RecyclerView = itemView.findViewById<View>(R.id.rv_layoutbanner) as RecyclerView
    }

    class MyFeedViewHolder(view: View) : VocCardViewHolder(view) {
        val photoImageView: ImageView = view.iv_itemmyfeed_mentor
        val clientNameTextView: TextView = view.tv_itemmyfeed_name
        val timeLocationTextView: TextView = view.tv_itemmyfeed_timeandlocation
        val askMonitorTextView: TextView = view.tv_itemmyfeed_ask
        val layoutHeader: View = view.cl_itemmyfeed_header
        val dividerView: View = view.view_itemmyfeed_devider
    }
}

