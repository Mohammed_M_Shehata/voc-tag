package com.voctag.android.ui.customViews

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.voctag.android.R
import com.voctag.android.manager.AudioPlayer
import com.voctag.android.manager.PlaylistManager
import com.voctag.android.model.Voc
import com.voctag.android.ui.VoctagApp
import kotlinx.android.synthetic.main.view_main_audio.view.*

class MainAudio : FrameLayout {

    private var voc: Voc? = null

    constructor(context: Context) : super(context) {
        setup()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setup()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setup()
    }

    private fun setup() {
        val view = View.inflate(context, R.layout.view_main_audio, this)
        view.setOnClickListener {
            if (voc == null || voc?.vocAudioURL.isNullOrEmpty()){
                return@setOnClickListener
            }
            LocalBroadcastManager.getInstance(context).sendBroadcast(PlaylistManager.getPlayIntent(voc!!))
        }
    }

    fun setVoc(voc: Voc) {
        this.voc = voc
        updateButtonState()
    }

    private fun updateButtonState() {
        if (voc == null) {
            hideDownloadAnimation()
            iv_mainaudio_icon.background = VoctagApp.getDrawable(R.drawable.ic_play)
            return
        }

        if (AudioPlayer.shared.isPreparingVoc(voc!!.vocID)) {
            showDownloadAnimation()
        } else {
            hideDownloadAnimation()
            if (AudioPlayer.shared.isPlayingVoc(voc!!.vocID)) {
                iv_mainaudio_icon.background = VoctagApp.getDrawable(R.drawable.ic_pause)
            } else {
                iv_mainaudio_icon.background = VoctagApp.getDrawable(R.drawable.ic_play)
            }
        }
    }

    private fun showDownloadAnimation() {
        pb_mainaudio_progress.visibility = View.VISIBLE
        iv_mainaudio_icon.visibility = View.GONE
    }

    private fun hideDownloadAnimation() {
        pb_mainaudio_progress.visibility = View.GONE
        iv_mainaudio_icon.visibility = View.VISIBLE
    }

}
