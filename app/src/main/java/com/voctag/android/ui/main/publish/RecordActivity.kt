package com.voctag.android.ui.main.publish

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.voctag.android.BuildConfig
import com.voctag.android.R
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.TimeHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.AudioPlayer
import com.voctag.android.manager.AudioRecorder
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.User
import kotlinx.android.synthetic.main.activity_record.*
import org.jetbrains.anko.backgroundColor
import java.util.*

class RecordActivity : AppCompatActivity(), ActivityCompat.OnRequestPermissionsResultCallback {

    private val RECORD_PERMISSIONS_REQUEST_CODE = 100

    private var startTime: Date? = null
    private lateinit var progressHandler: Handler
    private lateinit var progressRunnable: Runnable
    private var maxDuration: Long = 0
    private var user: User? = null
    private var isSpeak = false
    private var isSecret = false
    private var amount: String? = null
    var duration: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record)

        //get params from intent
        if (intent.hasExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION)) isSpeak = intent.getBooleanExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION, false)
        if (intent.hasExtra(VoctagKeys.EXTRA_SECRET)) isSecret = intent.getBooleanExtra(VoctagKeys.EXTRA_SECRET, false)
        if (intent.hasExtra(VoctagKeys.EXTRA_AMOUNT)) amount = intent.getStringExtra(VoctagKeys.EXTRA_AMOUNT)

        user = PersistDataHelper.shared.loadUser()

        setupToolbar()
        setupColor()
        setupTexts()
        setupRecordDuration()

        // init Progressbar
        progressHandler = Handler()
        progressRunnable = Runnable { updateElapsedTime() }
        cpv_record.max = maxDuration.toFloat()

        resetRecording()

        if (!isSpeak) {
            onClickRecordButton()
        }
        btn_record_record.setOnClickListener { onClickRecordButton() }
    }

    private fun setupToolbar() {
        setSupportActionBar(tb_record)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.elevation = 0F
        btn_record_forward.setOnClickListener { openPublish() }
        btn_record_forward.visibility = View.GONE
    }

    private fun setupColor() {
        if (VocCreationManager.shared.isNotOpenFeed()) {
            val client = VocCreationManager.shared.parentClient as Client
            val color = Color.parseColor(client.themeColorString)
            window.statusBarColor = color
            cl_record_layout.backgroundColor = color
            btn_record_record.imageTintList = ColorStateList.valueOf(color)
        }
    }

    private fun setupTexts() {
        val parentVoc = VocCreationManager.shared.parentVoc
        if (parentVoc != null) {
            if (isSecret) {
                tv_record_title.text = getString(R.string.record_title_private_reply)
                tv_record_instruction.text = getString(R.string.record_instructions_private_reply)
            } else {
                tv_record_title.text = getString(R.string.record_title_reply)
                tv_record_instruction.text = getString(R.string.record_instructions)
            }
            return
        }

        if (VocCreationManager.shared.isNotOpenFeed()) {
            val parentClient = VocCreationManager.shared.parentClient as Client
            if (parentClient.isConference) {
                tv_record_title.text = getString(R.string.conference_ask_question)
                tv_record_instruction.text = getString(R.string.record_instructions)
            } else if (user!!.isOwnerOf(parentClient.slug)) {
                if (isSpeak) {
                    tv_record_title.text = getString(R.string.record_title)
                    tv_record_instruction.text = getString(R.string.record_instructions_mentor_text)
                } else {
                    tv_record_title.text = getString(R.string.record_mentor_title)
                    tv_record_instruction.text = getString(R.string.record_instructions)
                }
            } else {
                if (isSecret) {
                    tv_record_title.text = getString(R.string.ask_mentor_privately)
                    tv_record_instruction.text = getString(R.string.private_record_instructions)
                } else {
                    tv_record_title.text = getString(R.string.record_title_ask)
                    tv_record_instruction.text = getString(R.string.record_instructions)
                }
            }
        } else {
            tv_record_title.text = getString(R.string.ask_the_community)
            tv_record_instruction.text = getString(R.string.record_instructions)
        }
    }

    private fun setupRecordDuration() {
        if (user != null && VocCreationManager.shared.isNotOpenFeed()) {
            val client = VocCreationManager.shared.parentClient as Client
            if (user!!.isOwnerOf(client.slug)) {
                maxDuration = AudioRecorder.recordingMaxTimeForOwner
                return
            }
        }
        // if private question then 5 mins for a user
        if (isSecret && VocCreationManager.shared.parentVoc == null) {
            maxDuration = AudioRecorder.recordingSecretMaxTime
        } else {
            maxDuration = AudioRecorder.recordingMaxTime
        }
    }

    private fun onClickRecordButton() {
        if (AudioPlayer.shared.isPlaying()) {
            AudioPlayer.shared.pause()
        }

        if (VocCreationManager.shared.isRecordingAudio()) {
            stopRecording()
        } else {
            if (hasPermission(this)) {
                startRecording()
            } else {
                requestPermission(this)
            }
        }
    }

    private fun startRecording() {
        if (VocCreationManager.shared.didRecordVoc()) {
            discard(true)
            return
        }

        cpv_record.setProgress(0.0f, true)
        VocCreationManager.shared.onStartRecording = {
            startTime = Calendar.getInstance().time
            setupViews(true)
            updateElapsedTime()
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
        VocCreationManager.shared.startRecordingVoc()
    }

    private fun stopRecording() {
        setupViews(false)
        window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        if (startTime != null) {
            val end = Calendar.getInstance().time.time
            val start = startTime!!.time + 200 // ms approx delay of mediarecorder class
            duration = end - start
            VocCreationManager.shared.stopRecordingVoc(duration.toInt())
            val minTime = AudioRecorder.recordingMinTime
            if (duration > minTime) {
                btn_record_forward.visibility = View.VISIBLE
                openPublish()
                return
            }
        }

        resetRecording()
    }

    private fun setupViews(recording: Boolean) {
        if (recording) {
            btn_record_record.setImageDrawable(getDrawable(R.drawable.ic_stop))
            tv_record_record.text = getString(R.string.stop)
        } else {
            btn_record_record.setImageDrawable(getDrawable(R.drawable.ic_mic))
            tv_record_record.text = getString(R.string.start)
        }
    }

    private fun discardNoReset() {
        discard(false)
    }

    private fun discard(reset: Boolean) {
        if (VocCreationManager.shared.didRecordVoc()) {
            MaterialAlertDialogBuilder(this)
                    .setTitle(R.string.record_discard_title)
                    .setMessage(getString(R.string.record_discard_text))
                    .setPositiveButton(getString(R.string.record_discard_button_discard)) { dialog, which ->
                        if (reset) {
                            resetRecording()
                        } else {
                            VocCreationManager.shared.reset()
                            finish()
                        }
                    }
                    .setNegativeButton(R.string.record_discard_button_cancel, null)
                    .show();
            return
        }

        if (VocCreationManager.shared.isRecordingAudio()) {
            stopRecording()
        }

        VocCreationManager.shared.reset()
        finish()
    }

    private fun updateElapsedTime() {
        if (VocCreationManager.shared.isRecordingAudio()) {
            val elapsedTime = Calendar.getInstance().timeInMillis - startTime!!.time
            if (elapsedTime <= maxDuration) {
                setElapsedTime(elapsedTime.toInt())
                progressHandler.postDelayed(progressRunnable, VoctagKeys.UPDATE_INTERVAL.toLong())
            } else {
                setElapsedTime(maxDuration.toInt())
                stopRecording()
            }
        }
    }

    private fun setElapsedTime(elapsedTime: Int) {
        tv_record_progress.text = TimeHelper.getProgressString(elapsedTime, maxDuration.toInt())
        cpv_record.setProgress(elapsedTime.toFloat(), false)
    }

    private fun resetRecording() {
        VocCreationManager.shared.discardRecording()
        btn_record_forward.visibility = View.GONE
        setupViews(false)
        setElapsedTime(0)
        cpv_record.setProgress(maxDuration.toFloat(), true)
    }

    private fun openPublish() {
        PublishActivity.onPublished = { showCongratulationsDialog ->
            val secretQuestionIntent = Intent().apply {
                putExtra(VoctagKeys.EXTRA_SHOW_CONGRATULATIONS_DIALOG, showCongratulationsDialog)
            }
            setResult(Activity.RESULT_OK, secretQuestionIntent)
            finish()
        }

        val intent = Intent(this, PublishActivity::class.java).apply {
            putExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION, isSpeak)
            putExtra(VoctagKeys.EXTRA_SECRET, isSecret)
            putExtra(VoctagKeys.EXTRA_AMOUNT, amount)
            putExtra(VoctagKeys.EXTRA_AUDIO_DURATION, duration / 1000f)
        }
        startActivity(intent)
    }

    // --- Permission --- //

    private fun hasPermission(activity: Activity): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(activity: Activity) {
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.RECORD_AUDIO), RECORD_PERMISSIONS_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == RECORD_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
            } else {
                // Permission denied.
                MaterialAlertDialogBuilder(this)
                        .setCancelable(false)
                        .setTitle(R.string.record_permission_title)
                        .setMessage(getString(R.string.record_permission_message))
                        .setPositiveButton(getString(R.string.record_permission_button_settings)) { dialog, which ->
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        }
                        .setNegativeButton(R.string.record_permission_button_cancel, null)
                        .show();
            }
        }
    }

    override fun onBackPressed() {
        discardNoReset()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            discardNoReset()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}
