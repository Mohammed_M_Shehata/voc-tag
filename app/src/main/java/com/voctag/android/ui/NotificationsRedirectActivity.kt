package com.voctag.android.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.voctag.android.api.firebase.AnalyticsLoggerUtils
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.WebViewActivity
import com.voctag.android.ui.main.voc.single.VocSingleActivity

class NotificationsRedirectActivity : AppCompatActivity() {

    companion object {
        val TAG: String = NotificationsRedirectActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent != null) {
            openNewIntent(intent)
        }

    }

    private fun openNewIntent(intent: Intent) {
        var openDetailIntent: Intent? = null
        var fcmNotification: Boolean
        var notificationType = ""
        var notificationMessage = ""

        intent.let {
            when {
                //redirect to voc
                intent.hasExtra(VoctagKeys.EXTRA_VOC) -> {
                    openDetailIntent = Intent(this, VocSingleActivity::class.java).apply {
                        val voc = Voc.decode(intent.getStringExtra(VoctagKeys.EXTRA_VOC))
                        if (voc.parentID != null && voc.secret) {
                            val newVoc = Voc(voc.parentID!!)
                            newVoc.parentClient = voc.parentClient
                            putExtra(VoctagKeys.EXTRA_VOC, newVoc.encode())
                        } else {
                            putExtra(VoctagKeys.EXTRA_VOC, voc.encode())
                        }
                        flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                    }

                    startActivity(openDetailIntent)
                }
                // redirect to client
                intent.hasExtra(VoctagKeys.EXTRA_CLIENT) -> {
                    val client = Client.decode(intent.getStringExtra(VoctagKeys.EXTRA_CLIENT))
                    openDetailIntent = Intent(this, ActivityHelper.getActivityToOpen(client.isConference)).apply {
                        putExtras(intent)
                        flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                    }

                    startActivity(openDetailIntent)
                }
                //redirect to web
                intent.hasExtra(VoctagKeys.EXTRA_WEB_URL) -> {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(intent.getStringExtra(VoctagKeys.EXTRA_WEB_URL)))
                    browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(browserIntent)
                }
                //redirect to webview
                intent.hasExtra(VoctagKeys.EXTRA_WEBVIEW_URL) -> {
                    val webviewIntent = Intent(this, WebViewActivity::class.java).apply {
                        putExtra(VoctagKeys.EXTRA_WEBVIEW_URL, intent.getStringExtra(VoctagKeys.EXTRA_WEBVIEW_URL))
                    }
                    webviewIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    startActivity(webviewIntent)
                }
            }
            fcmNotification = isFCMNotification(intent)
            if (fcmNotification) {
                notificationType = intent.getStringExtra(VoctagKeys.NOTIFICATION_TYPE)
                notificationMessage = intent.getStringExtra(VoctagKeys.NOTIFICATION_MESSAGE)
            }
        }

        if (fcmNotification) {
            AnalyticsLoggerUtils.getInstance().logUserOpenNotification(notificationType, notificationMessage)
        }

        finish()
    }

    private fun isFCMNotification(intent: Intent) = intent.hasExtra(VoctagKeys.NOTIFICATION_TYPE)
}