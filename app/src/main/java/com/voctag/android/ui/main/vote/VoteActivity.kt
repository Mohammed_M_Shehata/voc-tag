package com.voctag.android.ui.main.vote

import android.animation.Animator
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.voctag.android.R
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Client
import com.voctag.android.ui.VoctagApp
import kotlinx.android.synthetic.main.activity_vote.*
import org.jetbrains.anko.image

class VoteActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        var onVoted: ((up: Boolean) -> Unit)? = null
    }

    private var client: Client? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vote)
        if (intent.hasExtra(VoctagKeys.EXTRA_CLIENT)) {
            client = Client.decode(intent.getStringExtra(VoctagKeys.EXTRA_CLIENT))
        }
        background.setOnClickListener(this@VoteActivity)
        downVoteView.setOnClickListener(this@VoteActivity)
        upVoteView.setOnClickListener(this@VoteActivity)
    }

    override fun onDestroy() {
        super.onDestroy()
        upVoteImageView.image!!.setTintList(null)
        downVoteImageView.image!!.setTintList(null)
        onVoted = null
    }

    private fun close() {
        finish()
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    private fun didVote(up: Boolean) {
        onVoted?.invoke(up)
        background.isClickable = false
        downVoteView.isClickable = false
        upVoteView.isClickable = false
        if (up) {
            if (client != null && client?.isNotOpenFeed()!!) {
                upVoteImageView.image!!.setTint(Color.parseColor(client!!.themeColorString))
            } else {
                upVoteImageView.image!!.setTint(VoctagApp.getColor(R.color.lightMain))
            }

            upVoteImageView.animate().scaleX(1.3f).scaleY(1.3f).setDuration(250).setListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator?) {}
                override fun onAnimationRepeat(animation: Animator?) {}
                override fun onAnimationEnd(animation: Animator?) {
                    upVoteImageView.animate().scaleX(1f).scaleY(1f).setDuration(250).setListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator?) {}
                        override fun onAnimationRepeat(animation: Animator?) {}
                        override fun onAnimationEnd(animation: Animator?) {
                            close()
                        }

                        override fun onAnimationCancel(animation: Animator?) {}
                    })
                }

                override fun onAnimationCancel(animation: Animator?) {}
            })
        } else {
            if (client != null && client?.isNotOpenFeed()!!) {
                downVoteImageView.image!!.setTint(Color.parseColor(client!!.themeColorString))
            } else {
                downVoteImageView.image!!.setTint(VoctagApp.getColor(R.color.lightMain))
            }

            downVoteImageView.animate().scaleX(1.3f).scaleY(1.3f).setDuration(250).setListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator?) {}
                override fun onAnimationRepeat(animation: Animator?) {}
                override fun onAnimationEnd(animation: Animator?) {
                    downVoteImageView.animate().scaleX(1f).scaleY(1f).setDuration(250).setListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator?) {}
                        override fun onAnimationRepeat(animation: Animator?) {}
                        override fun onAnimationEnd(animation: Animator?) {
                            close()
                        }

                        override fun onAnimationCancel(animation: Animator?) {}
                    })
                }

                override fun onAnimationCancel(animation: Animator?) {}
            })
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.background -> {
                close()
            }
            R.id.downVoteView -> {
                didVote(false)
            }
            R.id.upVoteView -> {
                didVote(true)
            }
        }
    }
}