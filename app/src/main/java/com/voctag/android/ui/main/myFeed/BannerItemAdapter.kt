package com.voctag.android.ui.main.myFeed

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Banner
import com.voctag.android.ui.main.WebViewActivity
import com.voctag.android.ui.main.mentor.detail.open.MentorDetailActivity
import com.voctag.android.ui.main.voc.single.VocSingleActivity
import kotlinx.android.synthetic.main.item_banner.view.*

class BannerItemAdapter(
        val activity: Activity,
        private var banner: ArrayList<Banner>
) : RecyclerView.Adapter<BannerItemAdapter.BannerItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannerItemViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_banner, parent, false)
        return BannerItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: BannerItemViewHolder, position: Int) {
        val banner = getItem(position)
        Glide.with(activity)
                .load(banner.imageUrl)
                .transform(RoundedCorners(6))
                .into(holder.buttonImage)

        when (banner.type) {
            (Banner.BannerType.CLIENT) -> {
                holder.buttonImage.setOnClickListener {
                    showProgressBar(holder.progressbar, holder.buttonImage)
                    RequestHelper.shared.loadClientDetail(banner.extras.clientSlug, { client ->
                        client?.let {
                            hideProgressBar(holder.progressbar, holder.buttonImage)
                            ActivityHelper.openClient(activity, client, Bundle().apply {
                                putBoolean(VoctagKeys.EXTRA_RELOAD_CLIENT, true)
                                putInt(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, MentorDetailActivity.SPEAKS_PAGE)
                            })
                        }
                    }, activity)
                }
            }
            (Banner.BannerType.CONFERENCE) -> {
                holder.buttonImage.setOnClickListener {
                    showProgressBar(holder.progressbar, holder.buttonImage)
                    RequestHelper.shared.loadClientDetail(banner.extras.clientSlug, { client ->
                        client?.let {
                            hideProgressBar(holder.progressbar, holder.buttonImage)
                            ActivityHelper.openClient(activity, client, Bundle().apply {
                                putBoolean(VoctagKeys.EXTRA_RELOAD_CLIENT, true)
                                putInt(VoctagKeys.EXTRA_CLIENT_TAB_TO_OPEN, MentorDetailActivity.SPEAKS_PAGE)
                            })
                        }
                    }, activity)
                }
            }
            (Banner.BannerType.VOC) -> {
                holder.buttonImage.setOnClickListener {
                    showProgressBar(holder.progressbar, holder.buttonImage)
                    RequestHelper.shared.loadClientVocDetail(banner.extras.clientSlug, Integer.parseInt(banner.extras.vocId), { client ->
                        client?.let {
                            hideProgressBar(holder.progressbar, holder.buttonImage)
                            val intent = Intent(activity, VocSingleActivity::class.java).apply {
                                putExtra(VoctagKeys.EXTRA_VOC, client.encode())
                            }
                            activity.startActivity(intent)
                        }
                    }, activity)

                }
            }
            (Banner.BannerType.WEB) -> {
                holder.buttonImage.setOnClickListener {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(banner.extras.webUrl))
                    browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    activity.baseContext.startActivity(browserIntent)
                }
            }
            (Banner.BannerType.WEBVIEW) -> {
                holder.buttonImage.setOnClickListener {
                    val webviewIntent = Intent(activity, WebViewActivity::class.java).apply {
                        putExtra(VoctagKeys.EXTRA_WEBVIEW_URL, banner.extras.webviewUrl)
                    }
                    activity.startActivity(webviewIntent)
                }
            }
        }
    }

    private fun showProgressBar(progressbar: ProgressBar, imageButton: ImageButton) {
        progressbar.visibility = View.VISIBLE
    }

    private fun hideProgressBar(progressbar: ProgressBar, imageButton: ImageButton) {
        progressbar.visibility = View.INVISIBLE
    }

    private fun getItem(position: Int): Banner {
        return banner[position]
    }

    override fun getItemCount(): Int {
        return banner.size
    }

    class BannerItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val buttonImage: ImageButton = itemView.ib_banner
        val progressbar: ProgressBar = itemView.pb_banner
    }
}