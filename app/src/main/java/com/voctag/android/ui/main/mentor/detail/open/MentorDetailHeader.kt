package com.voctag.android.ui.main.mentor.detail.open

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.voctag.android.R
import com.voctag.android.model.Client
import com.voctag.android.ui.customViews.MainAudio
import com.voctag.android.ui.customViews.TouchTextView
import jp.wasabeef.blurry.Blurry
import kotlinx.android.synthetic.main.layout_mentor_detail_header.view.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.dimen
import org.jetbrains.anko.image
import org.jetbrains.anko.padding

class MentorDetailHeader(view: View) {

    private val mainAudio: MainAudio = view.ma_mentorheader
    private val largeIcon: ImageView = view.iv_mentorheader_logo
    private val nameTextView: TextView = view.tv_mentorheader_name
    private val descriptionTextView: TextView = view.tv_mentorheader_description
    private val controlsContainer: ConstraintLayout = view.cl_mentorheader_controls
    private val backgroundContainer: ConstraintLayout = view.cl_mentorheader
    private val gradientOverlay: ImageView = view.iv_mentorheader_gradient
    private val blurOverlay: ImageView = view.iv_mentorheader_overlay

    private lateinit var client: Client
    private lateinit var activity: Activity

    private var target = object : Target {
        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
        }

        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            if (bitmap == null) {
                return
            }
            largeIcon.setImageBitmap(bitmap)
            largeIcon.invalidate()
            Blurry.with(activity).radius(12).sampling(2).from(bitmap).into(blurOverlay)
        }
    }

    fun setup(client: Client, activity: Activity) {
        this.client = client
        this.activity = activity

        nameTextView.text = client.name

        //description
        val spanText = SpannableString(client.description)
        descriptionTextView.setOnTouchListener(TouchTextView(spanText));
        descriptionTextView.text = spanText
        descriptionTextView.setOnClickListener {
            showMoreDescription()
        }

        setupMentorBadge()
        setupClientColor()
        setupMainAudio()
        handleOpacity(0f) // init with full toolbar

        // background image
        if (client.mediumIconURL.isNotEmpty()) {
            val width = activity.window.decorView.width
            Picasso.get().load(client.mediumIconURL).resize(width, 0).into(target)
        } else {
            largeIcon.image = null
        }
    }

    private fun setupMentorBadge() {
        if (client.isTopMentor) {
            val drawable = ContextCompat.getDrawable(activity.applicationContext, R.drawable.official_badge_client_detail)
            val bitmap = (drawable as BitmapDrawable).bitmap
            val topIcon: Drawable = BitmapDrawable(activity.resources, Bitmap.createScaledBitmap(bitmap, 45, 45, true))
            nameTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, topIcon, null)
            nameTextView.compoundDrawablePadding = 15
        }
    }

    private fun setupClientColor() {
        val color = Color.parseColor(client.themeColorString)
        backgroundContainer.backgroundColor = color
    }

    fun setupMainAudio() {
        val voc = client.mainAudioFakeVoc
        if (voc != null) {
            mainAudio.visibility = View.VISIBLE
            mainAudio.setVoc(voc)
        } else {
            mainAudio.visibility = View.GONE
        }
    }

    fun handleOpacity(offset: Float) {
        controlsContainer.alpha = 1f - (offset + 0.125f)
        gradientOverlay.alpha = Math.min(1f, (1.7f - offset))
        blurOverlay.alpha = offset
    }

    private fun showMoreDescription() {
        val messageSpannable = SpannableStringBuilder(client.description)

        val builder = AlertDialog.Builder(activity)
        val messageTextView = TextView(activity)
        messageTextView.text = messageSpannable
        Linkify.addLinks(messageTextView, Linkify.WEB_URLS)
        messageTextView.movementMethod = LinkMovementMethod.getInstance()
        messageTextView.padding = activity.dimen(R.dimen.dialog_message_padding)

        builder.setView(messageTextView)
        builder.setPositiveButton(R.string.feed_cell_action_done, null)
        builder.create()

        val dialog: AlertDialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor(client.themeColorString))
        }

        dialog.show()
    }

}