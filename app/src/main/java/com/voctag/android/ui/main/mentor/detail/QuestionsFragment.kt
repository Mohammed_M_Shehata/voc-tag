package com.voctag.android.ui.main.mentor.detail

import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.paginate.Paginate
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.PaginateHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.FeedFilterManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.User
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.mentor.detail.conference.MentorDetailConferenceActivity
import com.voctag.android.ui.main.mentor.detail.open.MentorDetailActivity
import kotlinx.android.synthetic.main.fragment_questions.*
import kotlinx.android.synthetic.main.fragment_questions.view.*

class QuestionsFragment : Fragment(), Paginate.Callbacks {

    companion object {
        const val PUBLIC_QUESTIONS_TAB_INDEX: Int = 0
        const val PRIVATE_QUESTIONS_TAB_INDEX: Int = 1
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: VocsAdapter
    private lateinit var client: Client
    private lateinit var user: User
    var vocs = ArrayList<Voc>()
    private lateinit var paginate: Paginate
    private var loading = false
    private var allItemsLoaded = false
    private var shouldReload = false
    private var pageIndex: Int = 1
    private var willOpenVoting = false
    private var showCongratulationsDialog: Boolean = false
    private var currentQuestionsTabIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vocs = ArrayList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_questions, container, false)

        // init adapter and recyclerview for vocs
        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        adapter = VocsAdapter(activity as Activity, vocs)
        recyclerView = view.rv_questions
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter

        // view should reloaded when voc is beeing deleted
        adapter.onDeletedVoc = {
            shouldReload = true
        }
        adapter.onWillOpenVoting = {
            willOpenVoting = true
        }

        // set the current client
        client = getCurrentClient()
        user = PersistDataHelper.shared.loadUser()!!

        //show chip layout to filter between private and public vocs
        if (client.isSecretVocEnabled && user.isOwnerOf(client.slug)) {

            // show chip layout
            view.cl_fragmentquestion_chips.visibility = View.VISIBLE
            val publicChip = view.chip_fragmentquestion_public
            val privateChip = view.chip_fragmentquestion_private

            // color current active chip with the client theme color
            if (currentQuestionsTabIndex == 0) {
                publicChip.chipBackgroundColor = ColorStateList.valueOf(Color.parseColor(client.themeColorString))
            } else {
                privateChip.chipBackgroundColor = ColorStateList.valueOf(Color.parseColor(client.themeColorString))
            }

            // init the public chip
            publicChip.setOnClickListener {
                currentQuestionsTabIndex = 0
                publicChip.isChecked = true
                publicChip.chipBackgroundColor = ColorStateList.valueOf(Color.parseColor(client.themeColorString))

                privateChip.isChecked = false
                privateChip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(R.color.gray_300))

                reloadData()
            }

            // init the private chip
            privateChip.setOnClickListener {
                currentQuestionsTabIndex = 1
                privateChip.isChecked = true
                privateChip.chipBackgroundColor = ColorStateList.valueOf(Color.parseColor(client.themeColorString))

                publicChip.isChecked = false
                publicChip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(R.color.gray_300))
                reloadData()
            }
        }

        // pagination
        paginate = Paginate.with(recyclerView, this)
                .setLoadingTriggerThreshold(2)
                .setLoadingListItemCreator(PaginateHelper.getLoadingListCreatorVocs())
                .build()
        paginate.setHasMoreDataToLoad(!allItemsLoaded)

        return view
    }

    override fun onResume() {
        super.onResume()
        VocCreationManager.shared.onPublished = {
            shouldReload = true
        }

        if (showCongratulationsDialog) {
            showCongratulationsDialog = false
            ActivityHelper.showCongratulationDialog(activity as Activity
                    , getString(R.string.congratulation_received_your_question, client.name)
                    , getString(R.string.question_received_description)
                    , getString(R.string.ok))
        }

        if (shouldReload) {
            reloadData()
        }

        // show Onboarding
        if (!PersistDataHelper.shared.loadUser()!!.isOwnerOf(client.slug) && client.following) {
            ActivityHelper.showQuestionsAnswersHint(this.requireContext(), client.isSecretVocEnabled, client.slug, client.themeColorString)
        }
    }

    private fun loadMore() {
        loading = true
        val parameters = FeedFilterManager.shared.searchParametersForClientVocs(MentorDetailActivity.QUESTIONS_PAGE, pageIndex, currentQuestionsTabIndex)
        RequestHelper.shared.loadClientVocs(client.slug, parameters, { response, errorHappened ->
            loading = false
            showEmptyViewIfNecessary()
            if (cl_questionsfragment != null) {
                if (response != null) {
                    pageIndex++
                    vocs.addAll(response)
                    adapter.setVocs(vocs)
                }
                if (!errorHappened && response.isNullOrEmpty()) {
                    allItemsLoaded = true
                    adapter.notifyDataSetChanged()
                }
                if (errorHappened) {
                    Snackbar.make(cl_questionsfragment, getString(R.string.nointernet), Snackbar.LENGTH_LONG).show()
                }
            }
        }, activity as Activity)
    }

    private fun getCurrentClient(): Client {
        if (activity is MentorDetailConferenceActivity) {
            return (activity as MentorDetailConferenceActivity).getCurrentClient()
        } else if (activity is MentorDetailActivity) {
            return (activity as MentorDetailActivity).getCurrentClient()
        } else {
            return getCurrentClient()
        }
    }

    private fun showEmptyViewIfNecessary() {
        if (tv_emptyview != null) {
            if (vocs.isEmpty() && allItemsLoaded) {
                val user = PersistDataHelper.shared.loadUser()!!
                tv_emptyview.text = if (user.isOwnerOf(client.slug)) {
                    if (client.isSecretVocEnabled && currentQuestionsTabIndex == PRIVATE_QUESTIONS_TAB_INDEX) {
                        getString(R.string.mentor_no_private_questions)
                    } else if (client.isSecretVocEnabled && currentQuestionsTabIndex == PUBLIC_QUESTIONS_TAB_INDEX) {
                        getString(R.string.mentor_no_public_questions)
                    } else {
                        getString(R.string.questions_answers_mentor_no_data)
                    }
                } else {
                    getString(R.string.questions_answers_client_no_data)
                }

                tv_emptyview.visibility = View.VISIBLE
            } else {
                tv_emptyview?.visibility = View.GONE
            }
        }
    }

    fun reloadData() {
        pageIndex = 1
        allItemsLoaded = false
        vocs.clear()
        loadMore()
    }

    fun updateOnPlaylistDidChange() {
        if (::adapter.isInitialized) {
            adapter.notifyDataSetChanged()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        showCongratulationsDialog = data?.getBooleanExtra(VoctagKeys.EXTRA_SHOW_CONGRATULATIONS_DIALOG, false)
                ?: false
    }

    override fun onLoadMore() {
        loadMore()
    }

    override fun isLoading(): Boolean {
        return loading
    }

    override fun hasLoadedAllItems(): Boolean {
        return allItemsLoaded
    }
}