package com.voctag.android.ui.main.publish

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.material.snackbar.Snackbar
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.manager.LocationManager
import com.voctag.android.manager.VocCreationManager
import com.voctag.android.model.Client
import com.voctag.android.model.User
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.mentor.detail.open.MentorDetailActivity
import kotlinx.android.synthetic.main.activity_ask_reply.*
import org.jetbrains.anko.backgroundColor


class AskReplyActivity : AppCompatActivity(), ActivityCompat.OnRequestPermissionsResultCallback, View.OnClickListener {

    companion object {
        var onPublished: (() -> Unit)? = null
    }

    private var user: User? = null
    private var isSpeak = false
    private var isSecret = false
    private var amount: String? = null
    private var textQuestionLength: Int = 400
    private var finishDetailsOnBack = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ask_reply)

        if (intent.hasExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION)) isSpeak = intent.getBooleanExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION, false)
        if (intent.hasExtra(VoctagKeys.EXTRA_SECRET)) isSecret = intent.getBooleanExtra(VoctagKeys.EXTRA_SECRET, false)
        if (intent.hasExtra(VoctagKeys.EXTRA_AMOUNT)) amount = intent.getStringExtra(VoctagKeys.EXTRA_AMOUNT)
        finishDetailsOnBack = intent.getBooleanExtra(VoctagKeys.EXTRA_FINISH_DETAILS_ON_BACK, false)

        user = PersistDataHelper.shared.loadUser()

        // Setup UI
        setupToolbar()
        setupColor()
        setupTexts()

        // init click listeners
        bindProgressButton(btn_askreply_publish)
        btn_askreply_publish.setOnClickListener(this@AskReplyActivity)
        btn_askreply_publish.alpha = 0.5F
        btn_askreply_record.setOnClickListener(this@AskReplyActivity)

        // listen to Text Changes
        et_askreply_question.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(editable: Editable?) {
                setQuestionSize()
                btn_askreply_publish.isEnabled = editable.toString().length >= 3
                if (btn_askreply_publish.isEnabled) {
                    btn_askreply_publish.alpha = 1F
                } else {
                    btn_askreply_publish.alpha = 0.5F
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        onPublished = null
    }

    private fun setupToolbar() {
        setSupportActionBar(tb_askreply)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.elevation = 0F
    }

    private fun setupColor() {
        // set correct colors if not open feed
        if (VocCreationManager.shared.isNotOpenFeed()) {
            val client = VocCreationManager.shared.parentClient as Client
            val color = Color.parseColor(client.themeColorString)
            window.statusBarColor = color
            btn_askreply_publish.setTextColor(resources.getColor(R.color.white))
            cl_askreply_layout.backgroundColor = color
            btn_askreply_publish.backgroundTintList = ColorStateList.valueOf(color)

            // color state list
            btn_askreply_record.imageTintList = ColorStateList.valueOf(color)
            view_askreply_separator.backgroundColor = color
        }
    }

    private fun setupTexts() {
        val parentVoc = VocCreationManager.shared.parentVoc
        if (parentVoc != null) {
            tv_askreply_title.text = getString(R.string.reply_title)
            tv_askreply_heading.text = getString(R.string.type_reply)
            et_askreply_question.hint = getString(R.string.type_your_reply_here)
            tv_askreply_record.text = getString(R.string.record_reply)
        } else {
            if (VocCreationManager.shared.isNotOpenFeed()) {
                val parentClient = VocCreationManager.shared.parentClient as Client
                if (parentClient.isConference) {
                    tv_askreply_title.text = getString(R.string.conference_ask_question)
                } else if (user!!.isOwnerOf(parentClient.slug)) {
                    tv_askreply_title.text = getString(R.string.record_mentor_title)
                } else {
                    if (isSecret) {
                        tv_askreply_title.text = getString(R.string.ask_mentor_privately)
                        btn_askreply_publish.text = getString(R.string.send_question_pay)
                    } else {
                        tv_askreply_title.text = getString(R.string.record_title_ask)
                    }
                }
            } else {
                tv_askreply_title.text = getString(R.string.ask_the_community)
            }

            textQuestionLength = if (isSecret) {
                resources.getInteger(R.integer.private_question_length)
            } else {
                resources.getInteger(R.integer.public_question_length)
            }
            et_askreply_question.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(textQuestionLength))

            tv_askreply_heading.text = getString(R.string.type_question)
            et_askreply_question.hint = getString(R.string.type_your_question_here)
            tv_askreply_record.text = getString(R.string.record_question)
        }

        setQuestionSize()
    }

    private fun setQuestionSize() {
        val currentLength: Int = et_askreply_question.text.length
        tv_askreply_length.text = String.format(getString(R.string.question_text_size, currentLength, textQuestionLength))
    }

    private fun publishOrPayTextVoc() {
        btn_askreply_publish.setOnClickListener(null)
        showProgress()
        if (isSecret) {
            ActivityHelper.payForPublishQuestion(this, amount)
        } else {
            publish(null)
        }
    }

    private fun publish(nonce: String?) {
        ActivityHelper.hideSoftKeyboard(this)
        publishTextQuestion(nonce) { success ->
            if (success) {
                VocCreationManager.shared.onPublished?.invoke()
                val secretQuestionIntent = Intent().apply {
                    val showCongratulationsDialog: Boolean = isSecret
                    putExtra(VoctagKeys.EXTRA_SHOW_CONGRATULATIONS_DIALOG, showCongratulationsDialog)
                }
                setResult(Activity.RESULT_OK, secretQuestionIntent)
                finish()
            } else {
                hideProgress()
                btn_askreply_publish.setOnClickListener(this@AskReplyActivity)
                Snackbar.make(cl_askreply_layout, getString(R.string.pulishing_question_error_message), Snackbar.LENGTH_LONG).show()
            }
        }
    }

    private fun publishTextQuestion(nonce: String?, completionListener: (success: Boolean) -> Unit) {
        val params = VocCreationManager.shared.publishingTextQuestionParameters(isSecret, isSpeak, et_askreply_question.text.toString(), nonce)
        if (params != null) {
            val successListener: (voc: Voc) -> Unit = { voc ->
                VocCreationManager.shared.reset()
                completionListener(true)
            }
            if (VocCreationManager.shared.isNotOpenFeed()) {
                val client = VocCreationManager.shared.parentClient as Client
                RequestHelper.shared.uploadClientVoc(client.slug, params, successListener, {
                    completionListener(false)
                }, this@AskReplyActivity)
            } else {
                RequestHelper.shared.uploadVoc(params, successListener, {
                    completionListener(false)
                }, this@AskReplyActivity)
            }
        } else {
            completionListener(false)
        }
    }

    private fun showProgress() {
        btn_askreply_publish.showProgress {
            progressColor = Color.WHITE
            textMarginRes = R.dimen.keyline_0
        }
    }

    private fun hideProgress() {
        btn_askreply_publish.hideProgress(R.string.publish)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LocationManager.LOCATION_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                LocationManager.shared.getLocationIfPossible(this) {
                    publishOrPayTextVoc()
                }
            } else {
                publishOrPayTextVoc()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ActivityHelper.PAYMENT_REQUEST_CODE) {
            ActivityHelper.onPaymentActivityResult(requestCode, resultCode, data, ::publish)
        } else {
            setResult(Activity.RESULT_OK, data)
            finish()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_askreply_publish -> {
                LocationManager.shared.getLocationIfPossible(this) {
                    publishOrPayTextVoc()
                }
            }
            R.id.btn_askreply_record -> {
                val tabIndex = if (isSpeak) MentorDetailActivity.SPEAKS_PAGE else MentorDetailActivity.QUESTIONS_PAGE
                VocCreationManager.shared.openRecordView(this, tabIndex, isSecret, amount)
            }
        }
    }
}