package com.voctag.android.db.converter

import androidx.room.TypeConverter
import java.util.*


class DataTypesConverter {

    @TypeConverter
    fun fromTimestap(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date): Long {
        return date.time
    }
}