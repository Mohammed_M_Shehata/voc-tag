package com.voctag.android.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class PlayList(
        @PrimaryKey var id: Int,
        var lastVocIndex: Int,
        var lastVocSeekTime: Int,
        var autoPlayNextEnabled: Boolean
) {
    companion object {
        const val CURRENT_PLAY_LIST = 1
    }
}