package com.voctag.android.db.daos

import android.util.Log
import androidx.room.*
import com.voctag.android.db.entities.PlaListVocJoin
import com.voctag.android.db.relations.VocClient

@Dao
interface PlaListVocsJoinDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: List<PlaListVocJoin>)

    @Query("DELETE FROM plalistvocjoin where playListId = :playListId")
    fun deletePlayList(playListId: Int): Int

    @Query("DELETE FROM voc where id IN (SELECT plalistvocjoin.vocId from plalistvocjoin where playListId = :playListId) AND (voc.id NOT IN (SELECT downloadedvoc.vocId FROM downloadedvoc))")
    fun deleteVocs(playListId: Int): Int

    @Query("SELECT * From Voc INNER JOIN plalistvocjoin ON Voc.id = plalistvocjoin.vocId WHERE plalistvocjoin.playListId = :playListId  ORDER BY plalistvocjoin.vocOrder")
    fun getCurrentVocsOfPlayList(playListId: Int): List<VocClient>

    @Transaction
    fun clear(playListId: Int) {
        val deleteVocs = deleteVocs(playListId)
        deletePlayList(playListId)
        Log.d("com.test", "PlaListVocsJoinDao> -- deleteVocsCount>$deleteVocs")
    }

}