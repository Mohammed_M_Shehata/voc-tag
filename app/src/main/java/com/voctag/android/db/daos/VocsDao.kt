package com.voctag.android.db.daos

import android.util.Log
import androidx.room.*
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.repository.PlayListRepository

@Dao
interface VocsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vocsList: List<Voc>): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(voc: Voc): Long

    @Query("DELETE FROM voc WHERE voc.id = :vocId AND (:vocId NOT IN (SELECT plalistvocjoin.vocId FROM plalistvocjoin))")
    fun deleteVoc(vocId: Int): Int

    @Query("SELECT * FROM voc WHERE vocID IN (:vocsIds)")
    fun getVocsByIds(vocsIds: List<Int>): List<Voc>

    @Query("SELECT * FROM voc WHERE vocID = :vocId")
    fun getVocById(vocId: Int): Voc?

    @Transaction
    fun insertVocsIntoDatabase(vocsList: List<Voc>, clientDao: ClientDao): List<Long> {
        val vocsIds = ArrayList<Int>()
        val clients = ArrayList<Client>()

        vocsList.forEach { voc ->
            vocsIds.add(voc.vocID)
            voc.parentClient?.let {
                clients.add(it)
                voc.clientSlug = it.slug
            }
        }
        clientDao.insert(clients)

        val vocsInDB = getVocsByIds(vocsIds)
        val vocsHashMap: HashMap<Int, Voc> = HashMap()

        vocsInDB.forEach { voc ->
            vocsHashMap[voc.vocID] = voc
        }

        vocsList.forEach { voc ->
            if (vocsHashMap.contains(voc.vocID)) {
                voc.id = vocsHashMap[voc.vocID]?.id!!
            }
        }
        val vocsRowsIds = insert(vocsList)

        Log.d(PlayListRepository.TAG, "vocs id> ${vocsRowsIds}")
        return vocsRowsIds
    }
}