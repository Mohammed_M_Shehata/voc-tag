package com.voctag.android.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.voctag.android.model.Client

@Dao
interface ClientDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(clientsList: List<Client>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(client: Client)
}