package com.voctag.android.db.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.voctag.android.db.entities.DownloadedVoc
import com.voctag.android.db.relations.VocClient

@Dao
interface DownloadedVocsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: List<DownloadedVoc>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(voc: DownloadedVoc)

    @Query("DELETE FROM downloadedVoc where vocId = :vocId")
    fun deleteDownloadedVoc(vocId: Int): Int

    @Transaction
    @Query("SELECT * FROM voc INNER JOIN downloadedvoc ON downloadedvoc.vocId = voc.id ORDER BY downloadTime DESC")
    fun getDownloadedVocsList(): LiveData<List<VocClient>>
}