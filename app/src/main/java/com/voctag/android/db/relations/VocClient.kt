package com.voctag.android.db.relations

import androidx.room.Embedded
import androidx.room.Relation
import com.voctag.android.model.Client
import com.voctag.android.model.Voc

class VocClient(@Embedded var voc: Voc) {

    @Relation(entity = Client::class, parentColumn = "clientSlug", entityColumn = "slug")
    var clientList: List<Client>? = null
}