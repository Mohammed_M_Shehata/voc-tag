package com.voctag.android.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DownloadedVoc(
        @PrimaryKey val vocId: Int,
        val downloadTime: Long = System.currentTimeMillis()
)