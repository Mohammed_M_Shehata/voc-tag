package com.voctag.android.db

import android.content.Context
import android.os.AsyncTask
import androidx.annotation.MainThread
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.voctag.android.BuildConfig
import com.voctag.android.db.converter.DataTypesConverter
import com.voctag.android.db.daos.*
import com.voctag.android.db.entities.DownloadedVoc
import com.voctag.android.db.entities.PlaListVocJoin
import com.voctag.android.db.entities.PlayList
import com.voctag.android.model.Client
import com.voctag.android.model.Voc

@Database(entities = [PlayList::class, Voc::class, Client::class, PlaListVocJoin::class, DownloadedVoc::class], version = BuildConfig.DATABASE_VERSION_CODE)
@TypeConverters(DataTypesConverter::class)
public abstract class DatabaseRoomManager : RoomDatabase() {

    abstract fun playListDao(): PlayListDao
    abstract fun vocsDao(): VocsDao
    abstract fun clientDao(): ClientDao
    abstract fun playListVocJoinDao(): PlaListVocsJoinDao
    abstract fun downloadedVocsDao(): DownloadedVocsDao

    companion object {
        private lateinit var sInstance: DatabaseRoomManager

        @MainThread
        fun get(context: Context, databaseListener: DatabaseListener?): DatabaseRoomManager {
            if (!Companion::sInstance.isInitialized) {
                sInstance = Room.databaseBuilder(context, DatabaseRoomManager::class.java
                        , BuildConfig.DATABASE_NAME)
                        .addCallback(object : RoomDatabase.Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                databaseListener?.onDatabaseCreated()
                            }
                        }).addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4).build()
            }
            return sInstance
        }

        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE Client ADD isConference INTEGER NOT NULL DEFAULT 0")
                database.execSQL("ALTER TABLE Client ADD webViewLink TEXT")
            }
        }

        val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE Voc ADD isPodcast INTEGER NOT NULL DEFAULT 0")
            }
        }

        val MIGRATION_3_4 = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS DownloadedVoc (vocId INTEGER PRIMARY KEY NOT NULL, downloadTime INTEGER NOT NULL DEFAULT 0)")
            }
        }
    }

    fun singOut() {
        SignOutAsyncTask(this).execute()
    }

    interface DatabaseListener {
        fun onDatabaseCreated()
    }

    class SignOutAsyncTask(val roomManager: DatabaseRoomManager) : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {
            roomManager.clearAllTables()
            return null
        }
    }
}