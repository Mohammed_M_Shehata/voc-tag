package com.voctag.android.db.entities

import androidx.room.Entity

@Entity(primaryKeys = ["playListId", "vocId"])
class PlaListVocJoin(
        val playListId: Int, val vocId: Int, val vocOrder: Int
)

