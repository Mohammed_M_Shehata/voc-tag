package com.voctag.android.db

import android.app.Activity
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.voctag.android.R
import com.voctag.android.api.AuthenticationHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.*
import com.voctag.android.ui.auth.intro.IntroActivity
import com.voctag.android.ui.VoctagApp

/**
 * Created by jan.maron on 28.08.17.
 */

class PersistDataHelper {

    companion object {
        val shared = PersistDataHelper()
    }

    fun saveAuthentication(auth: Authentication) {
        if (auth.authToken != null) {
            AuthenticationHelper.shared.saveAuthToken(auth.authToken!!)
            auth.authToken = null
        }
        val editor = getEditor()
        editor.putString(VoctagKeys.AUTHENTICATION, auth.encode())
        editor.apply()
    }

    fun loadAuthentication(): Authentication? {
        val prefs = sharedPreferences()
        val json = prefs.getString(VoctagKeys.AUTHENTICATION, VoctagKeys.EMPTY)
        return if (json != null && json.isNotEmpty()) {
            val auth = Authentication.decode(json)
            auth.authToken = AuthenticationHelper.shared.getAuthToken()
            auth
        } else {
            null
        }
    }

    fun saveFilter(filter: Filter) {
        val editor = getEditor()
        editor.putString(VoctagKeys.CURRENT_FILTER, filter.encode())
        editor.apply()
    }

    fun loadFilter(): Filter? {
        val prefs = sharedPreferences()
        val json = prefs.getString(VoctagKeys.CURRENT_FILTER, VoctagKeys.EMPTY)
        return if (json != null && json.isNotEmpty()) {
            Filter.decode(json)
        } else {
            null
        }
    }

    fun saveUser(user: User, saveSettings: Boolean = true) {
        val editor = getEditor()
        editor.putString(VoctagKeys.USER, user.encode())
        editor.apply()

        if (saveSettings)
            saveSettings(user.settings)
    }

    private fun saveSettings(settings: Settings?) {
        settings?.notifications?.let { notificationsSettings ->

            val editor = PreferenceManager.getDefaultSharedPreferences(VoctagApp.context).edit()

            editor.putBoolean(VoctagApp.context.getString(R.string.reply_voted_key), notificationsSettings.replyVoted)
            editor.putBoolean(VoctagApp.context.getString(R.string.reply_followed_key), notificationsSettings.replyFollowed)
            editor.putBoolean(VoctagApp.context.getString(R.string.new_reply_key), settings.notifications.reply)
            editor.putBoolean(VoctagApp.context.getString(R.string.new_public_question_key), settings.notifications.userPublished)
            editor.putBoolean(VoctagApp.context.getString(R.string.new_private_question_key), settings.notifications.userPublishedPrivate)
            editor.putBoolean(VoctagApp.context.getString(R.string.new_speak_key), settings.notifications.speak)
            editor.putBoolean(VoctagApp.context.getString(R.string.new_podacast_key), settings.notifications.podcast)

            editor.apply()
        }
    }

    fun loadUser(): User? {
        val prefs = sharedPreferences()
        val json = prefs.getString(VoctagKeys.USER, VoctagKeys.EMPTY)
        return if (json != null && json.isNotEmpty()) {
            User.decode(json)
        } else {
            null
        }
    }

    fun saveDownloadedVoc(voc: Voc) {

        val downloadedVocs = getDownloadedVocs()
        downloadedVocs[voc.vocID] = voc
        saveDownloadedVocs(downloadedVocs)
    }

    fun removeDownloadedVoc(vocId: Int) {

        val downloadedVocs = getDownloadedVocs()
        downloadedVocs.remove(vocId)
        saveDownloadedVocs(downloadedVocs)
    }

    private fun saveDownloadedVocs(downloadedVocs: HashMap<Int, Voc>) {
        getEditor().putString(VoctagKeys.DOWNLOADED_VOCS, Gson().toJson(downloadedVocs)).apply()
    }

    fun hasUserDownloadVocs(): Boolean {
        return getDownloadedVocs().isNotEmpty()
    }

    fun getDownloadedVocs(): HashMap<Int, Voc> {
        val json = sharedPreferences().getString(VoctagKeys.DOWNLOADED_VOCS, null)
        val type = object : TypeToken<HashMap<Int, Voc>>() {}.type

        try {
            if (json != null) return Gson().fromJson<HashMap<Int, Voc>>(json, type)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return HashMap()
    }

    fun clearDownloadedVocsFromCache() {
        sharedPreferences().edit().remove(VoctagKeys.DOWNLOADED_VOCS).apply()
    }

    fun saveDownloadedVocsIds(vocs: List<Voc>) {
        val downloadedVocsIds = getDownloadedVocsIds()
        vocs.forEach { voc -> downloadedVocsIds.add(voc.vocID) }
        saveDownloadedVocsIds(downloadedVocsIds)
    }

    fun removeDownloadedVocId(vocId: Int) {

        val downloadedVocsIds = getDownloadedVocsIds()
        downloadedVocsIds.remove(vocId)
        saveDownloadedVocsIds(downloadedVocsIds)
    }

    private fun saveDownloadedVocsIds(downloadedVocsIds: HashSet<Int>) {
        getEditor().putString(VoctagKeys.DOWNLOADED_VOCS_IDS, Gson().toJson(downloadedVocsIds)).apply()
    }

    fun getDownloadedVocsIds(): HashSet<Int> {
        val json = sharedPreferences().getString(VoctagKeys.DOWNLOADED_VOCS_IDS, null)
        val type = object : TypeToken<HashSet<Int>>() {}.type

        try {
            if (json != null) return Gson().fromJson<HashSet<Int>>(json, type)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return HashSet()
    }

    fun savePlayerSpeed(speed: Float) {
        getEditor().putFloat(VoctagKeys.PLAYER_SPEED, speed).apply()
    }

    fun getPlayerSpeed(): Float {
        return sharedPreferences().getFloat(VoctagKeys.PLAYER_SPEED, 1f)
    }


    fun clearAllData(context: Activity) {
        VoctagApp.context.getSharedPreferences(VoctagKeys.PREFERENCES_VOC_COUNTS, MODE_PRIVATE).edit().clear().apply()
        getEditor().clear().apply()
        val intent = Intent(context, IntroActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.finish()
        context.startActivity(intent)
        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun cacheBoolean(key: String, value: Boolean) {
        getEditor().putBoolean(key, value).apply()
    }

    fun getCachedBoolean(key: String): Boolean {
        return sharedPreferences().getBoolean(key, false)
    }

    private fun getEditor() =
            sharedPreferences().edit()

    private fun sharedPreferences() =
            VoctagApp.context.getSharedPreferences(VoctagKeys.PREFERENCES, MODE_PRIVATE)
}
