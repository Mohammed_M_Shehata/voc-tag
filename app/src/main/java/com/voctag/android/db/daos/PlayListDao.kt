package com.voctag.android.db.daos

import androidx.room.*
import com.voctag.android.db.entities.PlayList

@Dao
interface PlayListDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(playList: PlayList)

    @Query("DELETE FROM playlist where id = :playListId")
    fun deletePlayList(playListId: Int)

    @Update
    fun updateCurrentPlayList(playList: PlayList)

    @Query("SELECT * FROM playlist WHERE id = :playListId")
    fun getCurrentPlayList(playListId: Int): PlayList
}