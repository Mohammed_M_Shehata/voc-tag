package com.voctag.android.manager

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.voctag.android.R
import com.voctag.android.model.Voc
import com.voctag.android.repository.PlayListRepository
import com.voctag.android.ui.main.player.PlayerActivity
import io.gresse.hugo.vumeterlibrary.VuMeterView
import org.jetbrains.anko.find
import java.util.*
import kotlin.collections.ArrayList

class PlayListHelper(val playListListener: PlayListListener?) : Observer {

    companion object {
        val TAG: String = PlayListHelper::class.java.simpleName
    }

    private var visualizer: Visualizer? = null

    private val playBroadCastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                Log.d(TAG, "play voc")
                if (playListListener == null){
                    return
                }
                val voc = intent.getSerializableExtra(PlaylistManager.PLAY_VOC) as Voc
                val vocsList = playListListener.getPlayList(voc.vocID)
                if (vocsList.isEmpty() && !voc.isMain()){
                    vocsList.add(voc)
                }

                PlaylistManager.shared.playVocFomPlayList(context, getAudioVocs(vocsList), voc, playListListener.isAutoPlayNextEnabled())
            }
        }
    }

    private fun getAudioVocs(vocs: ArrayList<Voc>): ArrayList<Voc> {
        val audioVocs = ArrayList<Voc>()
        vocs.forEach { voc ->
            voc.takeUnless { voc.vocAudioURL.isNullOrEmpty() }?.let {
                audioVocs.add(it)
            }
        }
        return audioVocs
    }

    fun onResume(context: Context) {
        Log.d(TAG, "on resume")
        unRegister(context)
        LocalBroadcastManager.getInstance(context).registerReceiver(playBroadCastReceiver, IntentFilter(PlaylistManager.PLAY_ACTION))
        AudioPlayer.shared.addObserver(this)
    }

    fun onStop(context: Context) {
        unRegister(context)
        visualizer?.stop()
    }

    private fun unRegister(context: Context) {
        try {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(playBroadCastReceiver)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        AudioPlayer.shared.deleteObserver(this)
    }

    interface PlayListListener {
        fun getPlayList(vocId: Int): ArrayList<Voc>
        fun isAutoPlayNextEnabled(): Boolean
    }

    // call it when resuming the fragment or activity
    fun addVisualizer(visualizerLayout: View) {
        visualizer = null
        visualizer = Visualizer(visualizerLayout)
        visualizer?.notifyVisualizer()
    }

    override fun update(o: Observable?, arg: Any?) {
        if (o is AudioPlayer) {
            visualizer?.notifyVisualizer()
        }
    }

    fun loadCachedPlayList(context: Context) {
        if (PlaylistManager.shared.playQueue.isNotEmpty()) {
            return
        }

        PlayListRepository(context).getCurrentPlayList { vocsList, lastVocIndex, lastVocProgress, autoPlayNextEnabled ->
            PlaylistManager.shared.playVocFomPlayList(vocsList, lastVocIndex, lastVocProgress, autoPlayNextEnabled)
        }
    }

    class Visualizer(private val visualizerLayout: View) {
        private val vuMeterView: VuMeterView = visualizerLayout.find(R.id.visualizer_bar)
        private val visualizerImageView: ImageView = visualizerLayout.find(R.id.img_equalizer)

        init {
            visualizerLayout.setOnClickListener {
                it.context.startActivity(Intent(it.context, PlayerActivity::class.java))
            }
        }

        fun stop() {
            if (PlaylistManager.shared.playQueue.isNotEmpty()) {
                visualizerLayout.visibility = View.VISIBLE
                vuMeterView.visibility = View.INVISIBLE
                visualizerImageView.visibility = View.VISIBLE
            } else {
                visualizerLayout.visibility = View.GONE
            }
        }

        fun notifyVisualizer() {
            if (AudioPlayer.shared.isPlaying()) {
                visualizerLayout.visibility = View.VISIBLE
                vuMeterView.visibility = View.VISIBLE
                visualizerImageView.visibility = View.INVISIBLE
                vuMeterView.resume(true)
            } else {
                stop()
            }
        }
    }
}