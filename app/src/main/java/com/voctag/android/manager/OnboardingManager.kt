package com.voctag.android.manager

import android.content.Context
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.ui.VoctagApp

class OnboardingManager {

    companion object {
        val shared = OnboardingManager()
    }

    enum class OnboardingType {
        Community, Mentors, Profile;

        val key: String
            get() {
                return when (this) {
                    Community -> VoctagKeys.ONBOARDING_TYPE_COMMUNITY
                    Mentors -> VoctagKeys.ONBOARDING_TYPE_MENTORS
                    Profile -> VoctagKeys.ONBOARDING_TYPE_PROFILE
                }
            }
    }

    fun hasUserSeenOnboarding(type: OnboardingType): Boolean {
        val prefs = VoctagApp.context.getSharedPreferences(VoctagKeys.PREFERENCES, Context.MODE_PRIVATE)
        return prefs.getBoolean(type.key, false)
    }

    fun userSeenOnboarding(type: OnboardingType) {
        val editor = VoctagApp.context.getSharedPreferences(VoctagKeys.PREFERENCES, Context.MODE_PRIVATE).edit()
        editor.putBoolean(type.key, true)
        editor.apply()
    }
}