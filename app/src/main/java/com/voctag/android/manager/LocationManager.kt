package com.voctag.android.manager

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import android.location.Location
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationServices
import com.voctag.android.db.PersistDataHelper

/**
 * Created by jan.maron on 26.09.17.
 */

class LocationManager {

    companion object {
        val LOCATION_PERMISSIONS_REQUEST_CODE = 400
        val shared = LocationManager()
    }

    // This function is used in all cases where it would be good
    // to additionally have the location e.g. uploading a voc
    fun getLocationIfPossible(activity: Activity, onLocationFetched: (() -> Unit)? = null) {
        if (checkPermission(activity)) {
            getLastLocation(activity, onLocationFetched)
        } else {
            saveLocation(null) // reset location
            requestPermissions(activity)
        }
    }

    /**
     * This method should be called after location permission has been granted
     */
    @SuppressLint("MissingPermission")
    private fun getLastLocation(activity: Activity, onLocationFetched: (() -> Unit)?) {
        val locationClient = LocationServices.getFusedLocationProviderClient(activity)
        locationClient.lastLocation.addOnSuccessListener(activity) { location ->
            // Got last known location. In some rare situations this can be null.
            saveLocation(location)
            onLocationFetched?.invoke()
        }
    }

    private fun saveLocation(location: Location?) {
        val user = PersistDataHelper.shared.loadUser()
        user?.let {
            if (location != null) {
                user.latitude = location.latitude
                user.longitude = location.longitude
            } else {
                user.latitude = 0.0
                user.longitude = 0.0
            }
            PersistDataHelper.shared.saveUser(user, false)
        }
    }

    // --- Permission --- //

    private fun checkPermission(activity: Activity): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions(activity: Activity) {
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSIONS_REQUEST_CODE)
    }
}
