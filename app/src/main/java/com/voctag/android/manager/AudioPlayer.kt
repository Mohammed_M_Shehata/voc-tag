package com.voctag.android.manager

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.util.Log
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.model.Voc
import com.voctag.android.ui.VoctagApp
import org.jetbrains.anko.doAsync
import java.util.*
import java.util.concurrent.TimeUnit

class AudioPlayer : Observable() {

    companion object {
        val shared = AudioPlayer()
        val TAG: String = AudioPlayer::class.java.simpleName
    }

    fun forward() {
        mediaPlayer?.let { mediaPlayer ->
            val currentProgress = mediaPlayer.currentPosition.plus(TimeUnit.SECONDS.toMillis(30))
            if (currentProgress < mediaPlayer.duration) {
                mediaPlayer.seekTo(currentProgress.toInt())
            }
        }
    }

    fun backward() {
        mediaPlayer?.let { mediaPlayer ->
            val currentProgress = mediaPlayer.currentPosition.minus(TimeUnit.SECONDS.toMillis(10))
            if (currentProgress > 0) {
                mediaPlayer.seekTo(currentProgress.toInt())
            }
        }
    }

    fun pause() {
        isPlayingAudio = true
        toggle()
    }

    fun play() {
        isPlayingAudio = false
        toggle()
    }

    var onDidFinishPlaying: (() -> Unit)? = null
    var onPlayPauseChanged: (() -> Unit)? = null
    var onMediaPlayerPrepared: (() -> Unit)? = null

    private var mediaPlayer: MediaPlayer? = null
    private var currentVocID = 0
    private lateinit var currentVoc: Voc
    private var isPreparing = false
    private var isPlayingAudio = false
        set(playing) {
            field = playing
            onPlayPauseChanged?.invoke()

            setChanged()
            notifyObservers()
        }
    private var speed: Float = PersistDataHelper.shared.getPlayerSpeed()

    fun getProgress(): Int {
        return if (isPlayerSetUp()) {
            mediaPlayer!!.currentPosition
        } else {
            0
        }
    }

    fun setProgress(progress: Int) {
        if (isPlayerSetUp()) {
            mediaPlayer!!.seekTo(progress)
        }
    }

    fun handlePlayingVoc(voc: Voc): Boolean {
        currentVoc = voc
        if (currentVocID != voc.vocID || !isPlayerSetUp()) {
            setupMediaPlayer(voc)
            return true
        }
        return toggle()
    }

    fun isPlaying(): Boolean {
        return if (isPlayerSetUp()) {
            isPlayingAudio
        } else {
            false
        }
    }

    fun isPreparingVoc(vocID: Int): Boolean {
        return isPreparing && currentVocID == vocID
    }

    fun isPlayingVoc(vocID: Int): Boolean {
        return isPlaying() && currentVocID == vocID
    }

    fun stopAndReset() {
        if (isPlayerSetUp()) {
            updateAudioServiceState(true)
            stop()
        }
    }

    fun stop() {
        mediaPlayer?.stop()
        mediaPlayer?.release()
        mediaPlayer = null
    }

    private fun isPlayerSetUp(): Boolean {
        return mediaPlayer != null
    }

    private var audioRequestGranted = false
    private var wasPlaying = false
    private fun requestAudioFocus() {
        val audioManager = VoctagApp.context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val audioFocusChangeListener = AudioManager.OnAudioFocusChangeListener { focusChange ->
            when (focusChange) {
                AudioManager.AUDIOFOCUS_GAIN -> {
                    if (isPlayerSetUp() && wasPlaying) {
                        wasPlaying = false
                        play()
                    }
                }
                AudioManager.AUDIOFOCUS_LOSS -> {
                    audioRequestGranted = false
                    handleAudioLossFocus()
                }
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                    handleAudioLossFocus()
                }
            }
        }
        val focusRequest = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val audioAttributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()

            val audioFocusRequest = AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                    .setAudioAttributes(audioAttributes)
                    .setAcceptsDelayedFocusGain(true)
                    .setOnAudioFocusChangeListener(audioFocusChangeListener)
                    .build()

            audioManager.requestAudioFocus(audioFocusRequest)

        } else {
            audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
        }

        audioRequestGranted = focusRequest == AudioManager.AUDIOFOCUS_REQUEST_GRANTED
    }

    private fun handleAudioLossFocus() {
        if (mediaPlayer?.isPlaying!!) {
            wasPlaying = true
            pause()
        }
    }

    private fun toggle(): Boolean {
        if (!isPlayerSetUp()){
            handlePlayingVoc(currentVoc)
        }

        return if (isPlaying()) {
            if (!isPreparing) {
                mediaPlayer?.pause()
            }
            updateAudioServiceState(true)
            false
        } else {
            if (!audioRequestGranted) {
                requestAudioFocus()
            }
            if (!isPreparing && audioRequestGranted) {
                startMediaPlayer()
            }
            updateAudioServiceState(false)

            true
        }
    }

    private fun setupMediaPlayer(voc: Voc) {
        stopAndReset()
        currentVocID = voc.vocID
        isPreparing = true

        doAsync {
            mediaPlayer = MediaPlayer()
            mediaPlayer?.isLooping = false

            val downloadedVocFile = LocalVocsManager.newInstance(VoctagApp.context).getDownloadedVoc(voc)
            if (downloadedVocFile != null) {
                mediaPlayer?.setDataSource(downloadedVocFile.path)
                Log.d(TAG, "run offline file> ${voc.vocID}")
            } else {
                mediaPlayer?.setDataSource(voc.vocAudioURL)
            }

            mediaPlayer?.setOnPreparedListener {
                if (!audioRequestGranted) {
                    requestAudioFocus()
                }
                if (isPreparing && isPlayerSetUp() && audioRequestGranted) {
                    isPreparing = false
                    onMediaPlayerPrepared?.invoke()
                }

                mediaPlayer?.setOnCompletionListener {
                    Log.d(TAG, "media player on complete listener")
                    updateAudioServiceState(true)
                    onDidFinishPlaying?.invoke()
                }
            }
            mediaPlayer?.prepare()
        }
    }

    private fun startMediaPlayer() {
        Log.d(TAG, " start media player")
        mediaPlayer?.start()
        setMediaPlayerSpeed()
    }

    private fun updateAudioServiceState(stopService: Boolean) {
        if (stopService) {
            isPlayingAudio = false
            AudioService.stopAudioService(VoctagApp.context, currentVoc)
        } else {
            isPlayingAudio = true
            AudioService.startAudioService(VoctagApp.context, currentVoc)
        }
    }

    fun setSpeed(speed: Float) {
        this.speed = speed
        PersistDataHelper.shared.savePlayerSpeed(speed)

        setMediaPlayerSpeed()
    }

    private fun setMediaPlayerSpeed() {
        if (mediaPlayer?.isPlaying == true) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Log.d(TAG, "setting the speed")
                mediaPlayer?.playbackParams = mediaPlayer?.playbackParams?.setSpeed(speed)
            }
        }
    }

    fun getCurrentSpeed(): Float {
        return speed
    }

    fun getDuration(): Int {
        if (mediaPlayer != null) {
            return mediaPlayer!!.duration
        }
        return 0
    }
}
