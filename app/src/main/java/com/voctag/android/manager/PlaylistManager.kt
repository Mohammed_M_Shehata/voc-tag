package com.voctag.android.manager

import android.content.Context
import android.content.Intent
import android.util.Log
import com.voctag.android.api.RequestHelper
import com.voctag.android.helper.LoggingHelper
import com.voctag.android.model.Voc
import com.voctag.android.repository.PlayListRepository
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by jan.maron on 26.09.17.
 */

class PlaylistManager private constructor() : Observable() {

    companion object {
        val PLAY_VOC: String = "PLAY_VOC"

        val shared = PlaylistManager()
        val PLAY_LIST_CHANGES = "PLAY_LIST_CHANGES"
        val PLAY_ACTION = "PLAY_ACTION"
        val TAG: String = PlaylistManager::class.java.simpleName

        fun getPlayIntent(voc: Voc): Intent {
            return Intent(PLAY_ACTION).apply {
                putExtra(PLAY_VOC, voc)
            }
        }
    }

    var autoPlayNextEnabled: Boolean = false
    var onPlaylistDidChange: (() -> Unit)? = null

    val playQueue = ArrayList<Voc>()
    var currentIndex = 0
        private set

    private val didCountPlayArray = ArrayList<Int>() //vocIDs

    fun isPreviousAvailable(): Boolean {
        return currentIndex > 0
    }

    fun isNextAvailable(): Boolean {
        return currentIndex < playQueue.size - 1
    }

    fun playPrevious() {
        currentIndex -= 1
        val voc = getCurrentVoc() ?: return
        playVoc(voc)
    }

    fun playNext() {
        Log.d(TAG, "next play voc>")
        currentIndex += 1
        val voc = getCurrentVoc() ?: return
        playVoc(voc)
    }

    fun getCurrentVoc(): Voc? {
        return if (playQueue.size >= 1 && currentIndex < playQueue.size) {
            playQueue[currentIndex]
        } else {
            LoggingHelper.logErrorForClass(PlaylistManager::class.java, "index for voc not found")
            null
        }
    }

    fun playVoc(voc: Voc, vocProgress: Int = -1) {
        Log.d(TAG, "play voc> ${voc.vocID} > ${voc.title}")
        AudioPlayer.shared.onPlayPauseChanged = {
            notifyPlayListChanges()
        }

        AudioPlayer.shared.onDidFinishPlaying = {
            if (didCountPlayArray.contains(voc.vocID)) {
                didCountPlayArray.remove(voc.vocID)
            }

            if (isNextAvailable() && autoPlayNextEnabled) {
                playNext()
            } else {
                notifyPlayListChanges()
            }
        }

        AudioPlayer.shared.onMediaPlayerPrepared = {
            if (vocProgress != -1) {
                AudioPlayer.shared.setProgress(vocProgress)
            } else {
                AudioPlayer.shared.play()
            }
        }
        if (AudioPlayer.shared.handlePlayingVoc(voc)) {
            if (!didCountPlayArray.contains(voc.vocID)) {
                didCountPlayArray.add(voc.vocID)

                if (!voc.isMain()) {
                    RequestHelper.shared.playedVoc(voc, null)
                }

                RequestHelper.shared.logPlayVocEvent(voc)
            }
        }
        notifyPlayListChanges()
    }

    private fun notifyPlayListChanges() {
        onPlaylistDidChange?.invoke()
        setChanged()
        notifyObservers(PLAY_LIST_CHANGES)
    }

    fun playVocFomPlayList(context: Context?, list: ArrayList<Voc>, voc: Voc, autoPlayNextEnabled: Boolean) {

        val playListChanged = playQueue != list
        if (playListChanged) {
            playQueue.clear()
            this.autoPlayNextEnabled = autoPlayNextEnabled

            if (voc.vocID > 0) { // not a header voc in client details activity
                playQueue.addAll(list)
            }
        }
        currentIndex = playQueue.indexOf(voc)
        if (currentIndex == -1) {
            playQueue.add(0, voc)
            currentIndex = 0
        }
        Log.d(TAG, "play voc is changed $playListChanged from list> index ${currentIndex} - ${playQueue.size}")
        playVoc(voc)

        if (playListChanged) {
            context?.let {
                PlayListRepository(it).savePlayList(playQueue, currentIndex, AudioPlayer.shared.getProgress(), autoPlayNextEnabled)
            }
        }
    }


    fun playVocFomPlayList(list: ArrayList<Voc>, vocIndex: Int, vocProgress: Int, autoPlayNextEnabled: Boolean) {
        playQueue.clear()
        playQueue.addAll(list)
        this.autoPlayNextEnabled = autoPlayNextEnabled
        currentIndex = vocIndex
        if (currentIndex < playQueue.size) {
            val voc = playQueue[currentIndex]
            playVoc(voc, vocProgress)
        }
    }

    fun reset() {
        AudioPlayer.shared.stop()
        playQueue.clear()
    }
}
