package com.voctag.android.manager

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.voctag.android.R
import com.voctag.android.helper.ActivityHelper
import com.voctag.android.model.Voc
import com.voctag.android.ui.VoctagApp
import org.jetbrains.anko.image
import org.jetbrains.anko.toast

class DownloadVocsHelper(val context: Context, val downloadVocImageView: View, val downloadProgressTexView: TextView) {

    private val localVocsManager: LocalVocsManager = LocalVocsManager.newInstance(context)
    private var currentVoc: Voc? = null

    private fun updateDownloadVocContainerViews(downloadInProgress: Boolean, vocDownloaded: Boolean) {
        if (downloadInProgress) {
            downloadVocImageView.visibility = View.INVISIBLE
            downloadProgressTexView.visibility = View.VISIBLE
        } else {
            downloadVocImageView.visibility = View.VISIBLE
            downloadProgressTexView.visibility = View.INVISIBLE
            updateDownloadVocDrawable(vocDownloaded)
        }
    }

    private fun updateDownloadVocDrawable(vocDownloaded: Boolean) {
        if (vocDownloaded) {
            if (downloadVocImageView is ImageView) {
                downloadVocImageView.image = context.getDrawable(R.drawable.ic_downloaded_player)
            } else if (downloadVocImageView is TextView) {
                downloadVocImageView.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_downloaded_player, 0, 0)
                downloadVocImageView.text = VoctagApp.getTranslation(R.string.downloaded)
            }
        } else {
            if (downloadVocImageView is ImageView) {
                downloadVocImageView.image = context.getDrawable(R.drawable.ic_download_player)
            } else if (downloadVocImageView is TextView) {
                downloadVocImageView.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_download_player, 0, 0)
                downloadVocImageView.text = VoctagApp.getTranslation(R.string.download)
            }
        }
    }

    fun onDownloadBtnClicked() {
        if (currentVoc == null) {
            return
        }

        val voc = currentVoc!!
        if (localVocsManager.isVocDownloaded(voc.vocID)) {
            ActivityHelper.showConfirmationDialog(context, R.string.remove_downloaded_voc, R.string.remove_downloaded_voc_message, R.string.delete) {
                localVocsManager.deleteVoc(voc.vocID)
                updateDownloadVocContainerViews(downloadInProgress = false, vocDownloaded = false)
            }
        } else {
            updateDownloadVocContainerViews(downloadInProgress = true, vocDownloaded = false)
            downloadProgressTexView.text = "0%"
            localVocsManager.downloadVoc(voc)
        }
    }

    private fun setupDownloadVocButton() {
        if (currentVoc == null) {
            return
        }

        val voc = currentVoc!!
        updateDownloadVocContainerViews(downloadInProgress = false, vocDownloaded = localVocsManager.isVocDownloaded(voc.vocID))
        downloadVocImageView.setOnClickListener {
            onDownloadBtnClicked()
        }
        try {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(downloadBroadCastReceiver)
            LocalBroadcastManager.getInstance(context).registerReceiver(downloadBroadCastReceiver, IntentFilter(localVocsManager.getVocKey(voc.vocID)))
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
    }

    private val downloadBroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                if (intent.hasExtra(LocalVocsManager.DOWNLOAD_PROGRESS)) {
                    val percentage = intent.getIntExtra(LocalVocsManager.DOWNLOAD_PROGRESS, 0)

                    if (percentage == 100) { // download completed
                        updateDownloadVocContainerViews(downloadInProgress = false, vocDownloaded = true)
                    } else {
                        if (downloadProgressTexView.visibility != View.VISIBLE)
                            updateDownloadVocContainerViews(downloadInProgress = true, vocDownloaded = false)
                        downloadProgressTexView.text = "$percentage%"
                    }
                } else if (intent.hasExtra(LocalVocsManager.DOWNLOAD_ERROR)) {
                    updateDownloadVocContainerViews(downloadInProgress = false, vocDownloaded = false)
                    context?.toast(intent.getStringExtra(LocalVocsManager.DOWNLOAD_ERROR))
                }
            }
        }
    }

    fun onStop() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(downloadBroadCastReceiver)
    }

    fun onResume(voc: Voc) {
        currentVoc = voc
        setupDownloadVocButton()
    }
}