package com.voctag.android.manager

import android.media.MediaRecorder
import com.voctag.android.helper.LoggingHelper
import com.voctag.android.ui.VoctagApp
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import java.util.*

class AudioRecorder {

    companion object {
        val recordingMinTime: Long = 1000
        val recordingMaxTime: Long = 90000
        val recordingSecretMaxTime: Long = 5 * 60 * 1000
        val recordingMaxTimeForOwner: Long = 15 * 60 * 1000 // 10 mins in milliseconds
    }

    // MARK: - Reactive Variables

    var onStartRecording: (() -> Unit)? = null

    // MARK: - AudioRecorder

    private val recordingAudioChannels = 2
    private val recordingBitRate = 192000
    private val recordingAudioSamplingRate = 44100

    private var audioRecorder: MediaRecorder? = null
    var isRecording = false
    var recordAudioURL: String? = null

    // MARK: - Public

    fun didRecordAudio(): Boolean {
        return audioRecorder != null
    }

    fun startRecording() {
        doAsync {
            // Create audio file path
            val timestamp = Calendar.getInstance().time
            recordAudioURL = urlForNewVoc(timestamp)

            audioRecorder = MediaRecorder()
            audioRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
            audioRecorder?.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS)
            audioRecorder?.setOutputFile(recordAudioURL)
            audioRecorder?.setAudioEncodingBitRate(recordingBitRate)
            audioRecorder?.setAudioChannels(recordingAudioChannels)
            audioRecorder?.setAudioSamplingRate(recordingAudioSamplingRate)
            audioRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

            audioRecorder?.prepare()
            audioRecorder?.start()
            isRecording = true
            uiThread {
                onStartRecording?.invoke()
            }
        }
    }

    fun stopRecording() {
        audioRecorder?.stop()
        audioRecorder?.release()
        isRecording = false
    }

    fun discardRecording() {
        if (audioRecorder != null) {
            if (isRecording) {
                stopRecording()
            }

            deleteFile(recordAudioURL)
            audioRecorder = null
            recordAudioURL = null
        }
    }

    // MARK: - Private

    private fun deleteFile(audioFileURL: String?) {
        val url = audioFileURL ?: return

        val f = File(url)
        if (!f.delete()) {
            LoggingHelper.logErrorForClass(AudioRecorder::class.java, "File could not be removed.")
        }
    }

    private fun urlForNewVoc(timestamp: Date): String {
        val cacheDir = VoctagApp.context.cacheDir.path
        val fileName = "voc_audio_${timestamp.time}.aac"
        return cacheDir + fileName
    }
}
