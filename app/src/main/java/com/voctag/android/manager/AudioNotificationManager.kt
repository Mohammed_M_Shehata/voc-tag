package com.voctag.android.manager

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.View
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.NotificationTarget
import com.voctag.android.R
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Voc
import com.voctag.android.ui.NotificationsRedirectActivity


class AudioNotificationManager {

    companion object {

        const val NOTIFY_PLAY = "NOTIFY_PLAY"
        const val NOTIFY_PAUSE = "NOTIFY_PAUSE"
        const val NOTIFY_BACKWARD = "NOTIFY_BACKWARD"
        const val NOTIFY_FORWARD = "NOTIFY_FORWARD"
        const val CHANNEL_ID: String = "CHANNEL_ID"
        private const val CHANNEL_NAME: String = "UPSPEAK_CHANNEL"

        fun generateNotification(context: Context, ongoing: Boolean, voc: Voc): Notification {
            createNotificationChannel(context)
            val notificationView = RemoteViews(context.packageName, R.layout.layout_audio_notification)
            initNotificationView(context, notificationView, voc)

            val openVocDetailsIntent = Intent(context, NotificationsRedirectActivity::class.java).apply {
                if (voc.isNotOpenFeed() && voc.parentClient?.mainAudioFakeVoc != null) {
                    putExtra(VoctagKeys.EXTRA_CLIENT, voc.parentClient?.encode())
                } else {
                    putExtra(VoctagKeys.EXTRA_VOC, voc.encode())
                }
            }

            // the request code need to be changed each time when generating new notification
            val pendingVocDetails = PendingIntent.getActivity(context, System.currentTimeMillis().toInt(), openVocDetailsIntent, PendingIntent.FLAG_ONE_SHOT)

            val notification = NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.notification_logo)
                    .setContent(notificationView)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setContentIntent(pendingVocDetails)
                    .setAutoCancel(true)
                    .setSound(null)
                    .setOngoing(ongoing)
                    .build()

            val notificationTarget = NotificationTarget(context, R.id.iv_audionotification, notificationView, notification, AudioService.FOREGROUND_SERVICE_ID)

            val notificationImage: Any
            if (voc.isNotOpenFeed()) {
                voc.parentClient.let { parentClient ->
                    if (parentClient!!.smallIconURL.isEmpty()) {
                        notificationImage = getBitmapFromColor(context, ColorDrawable(Color.parseColor(voc.parentClient?.themeColorString))) as Any
                    } else {
                        notificationImage = parentClient.smallIconURL
                    }
                }
            } else {
                notificationImage = (context.getDrawable(R.drawable.logo) as BitmapDrawable).bitmap
            }

            Glide.with(context)
                    .asBitmap()
                    .apply(RequestOptions.bitmapTransform(RoundedCorners(8)))
                    .load(notificationImage)
                    .into(notificationTarget)

            return notification
        }

        private fun initNotificationView(context: Context, notificationView: RemoteViews, voc: Voc) {
            val playIntent = Intent(NOTIFY_PLAY)
            val pauseIntent = Intent(NOTIFY_PAUSE)
            val backwardIntent = Intent(NOTIFY_BACKWARD)
            val forwardIntent = Intent(NOTIFY_FORWARD)

            val pendingPlay = PendingIntent.getBroadcast(context, 0, playIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            notificationView.setOnClickPendingIntent(R.id.btn_audionotification_play, pendingPlay)

            val pendingPause = PendingIntent.getBroadcast(context, 0, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            notificationView.setOnClickPendingIntent(R.id.btn_audionotification_pause, pendingPause)

            val pendingBackward = PendingIntent.getBroadcast(context, 0, backwardIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            notificationView.setOnClickPendingIntent(R.id.btn_audionotification_backward, pendingBackward)

            val pendingForward = PendingIntent.getBroadcast(context, 0, forwardIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            notificationView.setOnClickPendingIntent(R.id.btn_audionotification_forward, pendingForward)

            if (!AudioPlayer.shared.isPlaying()) {
                notificationView.setViewVisibility(R.id.btn_audionotification_play, View.VISIBLE)
                notificationView.setViewVisibility(R.id.btn_audionotification_pause, View.GONE)
            } else {
                notificationView.setViewVisibility(R.id.btn_audionotification_play, View.GONE)
                notificationView.setViewVisibility(R.id.btn_audionotification_pause, View.VISIBLE)
            }
            var notificationTitle = context.getString(R.string.no_title)
            voc.title?.let { title ->
                if (title.isNotEmpty()) {
                    notificationTitle = title
                }
            }
            notificationView.setTextViewText(R.id.tv_audionotification_title, notificationTitle)

            var notificationDescription = voc.userNickname
            if (voc.parentClient?.mainAudioFakeVoc != null) {
                notificationDescription = voc.parentClient?.name
            }
            notificationView.setTextViewText(R.id.tv_audionotification_nickname, notificationDescription)
        }

        private fun getBitmapFromColor(context: Context, colorDrawable: ColorDrawable): Bitmap? {
            val width = context.resources.getDimension(R.dimen.width_notification_image).toInt()
            val bitmap = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            colorDrawable.setBounds(0, 0, canvas.width, canvas.height)
            colorDrawable.draw(canvas)
            return bitmap
        }

        fun createNotificationChannel(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val importance = NotificationManager.IMPORTANCE_LOW
                val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance).apply {
                    setShowBadge(false)
                }
                val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }
        }
    }

    class NotificationBroadcast : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                when (intent.action) {
                    NOTIFY_PLAY -> {
                        AudioPlayer.shared.play()
                    }
                    NOTIFY_PAUSE -> {
                        AudioPlayer.shared.pause()
                    }
                    NOTIFY_BACKWARD -> {
                        AudioPlayer.shared.backward()
                    }
                    NOTIFY_FORWARD -> {
                        AudioPlayer.shared.forward()
                    }
                }
            }
        }
    }
}