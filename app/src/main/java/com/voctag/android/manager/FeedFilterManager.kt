package com.voctag.android.manager

import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Filter
import com.voctag.android.ui.main.mentor.detail.QuestionsFragment
import com.voctag.android.ui.main.mentor.detail.open.MentorDetailActivity
import java.util.*

class FeedFilterManager {

    companion object {
        val shared = FeedFilterManager()
    }

    enum class SearchClientScope { MEMBER, TOP_TEN, EXPLORE }

    fun indexForCurrentlySelectedOrderType(): Int {
        return currentFilter().orderType.ordinal
    }

    fun searchParametersForCurrentFilter(query: String?, page: Int = 1): HashMap<String, String> {
        val filter = currentFilter()

        val parameters = HashMap<String, String>()
        parameters.put(VoctagKeys.FILTER_ORDER, filter.orderType.rawValue)
        parameters.put(VoctagKeys.FILTER_PAGE, "$page")

        if (query != null) {
            parameters.put(VoctagKeys.FILTER_QUERY, query)
        }

        return parameters
    }

    fun searchParametersForClientVocs(tabIndex: Int, page: Int = 1, currentQuestionsTabIndex: Int = 0): HashMap<String, String> {
        val parameters = HashMap<String, String>()

        if (tabIndex == MentorDetailActivity.SPEAKS_PAGE) {
            parameters.put(VoctagKeys.FILTER_SCOPE, "feed")
        } else if (tabIndex == MentorDetailActivity.QUESTIONS_PAGE && currentQuestionsTabIndex == QuestionsFragment.PUBLIC_QUESTIONS_TAB_INDEX) {
            parameters.put(VoctagKeys.FILTER_SCOPE, "community")
        } else {
            parameters.put(VoctagKeys.FILTER_SCOPE, "private_community")
        }

        parameters.put(VoctagKeys.FILTER_PAGE, "$page")

        return parameters
    }

    fun searchParametersForSearchClients(query: String?, category: String? = null, searchClientScope: SearchClientScope? = null, page: Int = 1, fetchConference: Boolean? = null): HashMap<String, String> {
        val parameters = HashMap<String, String>()

        if (query != null) {
            parameters[VoctagKeys.FILTER_QUERY] = query
        }

        if (fetchConference != null && fetchConference) {
            parameters[VoctagKeys.FILTER_SCOPE] = "conferences"
        }

        when (searchClientScope) {
            SearchClientScope.MEMBER -> parameters[VoctagKeys.FILTER_SCOPE] = "member"
            SearchClientScope.TOP_TEN -> parameters[VoctagKeys.FILTER_SCOPE] = "top_10"
            SearchClientScope.EXPLORE -> parameters[VoctagKeys.FILTER_SCOPE] = "explore_without_top_10"
        }

        if (category != null) {
            parameters[VoctagKeys.FILTER_CATEGORY] = category
        }

        parameters[VoctagKeys.FILTER_PAGE] = "$page"

        return parameters
    }

    fun updateFilter(orderType: Filter.OrderType) {
        val filter = currentFilter()
        filter.orderType = orderType
        PersistDataHelper.shared.saveFilter(filter)
    }

    // MARK: - Private

    private fun currentFilter(): Filter {
        var filter = PersistDataHelper.shared.loadFilter()
        if (filter == null) {
            filter = Filter()
            PersistDataHelper.shared.saveFilter(filter)
        }

        return filter
    }
}
