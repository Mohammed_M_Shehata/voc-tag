package com.voctag.android.manager

import android.app.Activity
import android.content.Intent
import android.util.Base64
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.helper.LoggingHelper
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Client
import com.voctag.android.model.Voc
import com.voctag.android.ui.main.mentor.detail.open.MentorDetailActivity
import com.voctag.android.ui.main.publish.AskReplyActivity
import com.voctag.android.ui.main.publish.RecordActivity
import java.io.File
import java.util.*

/**
 * Created by jan.maron on 26.09.17.
 */

class VocCreationManager {

    companion object {
        val shared = VocCreationManager()
    }

    // MARK: - Reactive Variables

    var onPublished: (() -> Unit)? = null
    var onStartRecording: (() -> Unit)? = null

    // MARK: - VocCreationManager

    val audioRecorder = AudioRecorder()
    var parentVoc: Voc? = null
    var parentClient: Client? = null
    var title = ""
    var recordDuration: Int = 0

    // MARK: - Public

    fun isNotOpenFeed(): Boolean {
        return parentClient != null && parentClient?.isNotOpenFeed()!!
    }

    fun didRecordVoc(): Boolean {
        return audioRecorder.didRecordAudio()
    }

    fun isRecordingAudio(): Boolean {
        return audioRecorder.isRecording
    }

    fun publishingParameters(fileUrl: String?, audioDuration: Float, secret: Boolean, reply: Boolean, chargeUser: Boolean, nonce: String?, isClientSPEAKSSection: Boolean): HashMap<String, String> {
        val parameters = HashMap<String, String>()
        if (fileUrl != null) {
            parameters.put(VoctagKeys.VOC_EXTERNAL_AUDIO_URL, fileUrl)
            parameters.put(VoctagKeys.VOC_AUDIO_DURATION, audioDuration.toString())
        } else {
            val base64EncodedAudioFileString = base64EncodedStringForCurrentRecord()
            if (!base64EncodedAudioFileString.isNullOrEmpty()) {
                val audioBase64DataURIScheme = "data:audio/aac;base64," + base64EncodedAudioFileString
                parameters.put(VoctagKeys.AUDIO, audioBase64DataURIScheme)
            }
        }

        val user = PersistDataHelper.shared.loadUser()
        if (user != null) {
            if (user.hasLocation) {
                parameters.put(VoctagKeys.LATITUDE, user.latitude.toString())
                parameters.put(VoctagKeys.LONGITUDE, user.longitude.toString())
            }
        }

        if (!title.isEmpty()) {
            parameters.put(VoctagKeys.TITLE, title)
        }

        if (parentVoc != null) {
            parameters.put(VoctagKeys.PARENT_ID, parentVoc!!.vocID.toString())
        }

        if (secret && !reply && nonce != null) {
            parameters.put(VoctagKeys.PAYMENT_METHOD_NONCE, nonce)
        }

        parameters.put(VoctagKeys.SECRET, secret.toString())

        if (reply) {
            parameters.put(VoctagKeys.CHARGE_USER, chargeUser.toString())
        }

        if (isClientSPEAKSSection) {
            parameters.put(VoctagKeys.SHOW_IN_FEED, true.toString())
        }

        return parameters
    }

    fun publishingTextQuestionParameters(secret: Boolean, isClientSPEAKSSection: Boolean, textQuestion: String, nonce: String?): HashMap<String, String>? {

        val parameters = HashMap<String, String>()
        parameters.put(VoctagKeys.VOC_TRANSCRIPT, textQuestion)

        val user = PersistDataHelper.shared.loadUser()
        if (user != null) {
            if (user.hasLocation) {
                parameters.put(VoctagKeys.LATITUDE, user.latitude.toString())
                parameters.put(VoctagKeys.LONGITUDE, user.longitude.toString())
            }
        }

        if (secret && nonce != null) {
            parameters.put(VoctagKeys.PAYMENT_METHOD_NONCE, nonce)
        }
        parameters.put(VoctagKeys.SECRET, secret.toString())

        if (parentVoc != null) {
            parameters.put(VoctagKeys.PARENT_ID, parentVoc!!.vocID.toString())
        }

        if (isClientSPEAKSSection) {
            parameters.put(VoctagKeys.SHOW_IN_FEED, true.toString())
        }
        return parameters
    }

    fun recordedAudioFileURL(): String? {
        return audioRecorder.recordAudioURL
    }

    fun openAskReplyView(activity: Activity, currentTabIndex: Int = 1, secret: Boolean = false, amount: String? = null, finishDetetailsOnBack: Boolean = false) {
        AskReplyActivity.onPublished = {
            activity.finish()
        }
        val intent = Intent(activity, AskReplyActivity::class.java).apply {
            if (currentTabIndex == 0) {
                putExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION, true)
            } else {
                putExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION, false)
                putExtra(VoctagKeys.EXTRA_SECRET, secret)
                putExtra(VoctagKeys.EXTRA_AMOUNT, amount)
            }
            if (finishDetetailsOnBack)
                putExtra(VoctagKeys.EXTRA_FINISH_DETAILS_ON_BACK, true)
        }
        //TODO
        activity.startActivityForResult(intent, 1)
    }

    fun openRecordView(activity: Activity, currentTabIndex: Int = 1, secret: Boolean = false, amount: String? = null) {
        val intent = Intent(activity, RecordActivity::class.java).apply {
            if (currentTabIndex == MentorDetailActivity.SPEAKS_PAGE) {
                putExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION, true)
            } else {
                putExtra(VoctagKeys.EXTRA_CLIENTSPEAKSSECTION, false)
                putExtra(VoctagKeys.EXTRA_SECRET, secret)
                putExtra(VoctagKeys.EXTRA_AMOUNT, amount)
            }
        }

        activity.startActivityForResult(intent, 1)
    }

    // MARK: - Record

    fun startRecordingVoc() {
        audioRecorder.onStartRecording = this.onStartRecording
        audioRecorder.startRecording()
    }

    fun stopRecordingVoc(recordDuration: Int) {
        audioRecorder.stopRecording()
        this.recordDuration = recordDuration
    }

    // MARK: - Reset

    fun discardRecording() {
        audioRecorder.discardRecording()
    }

    fun reset() {
        discardRecording()
        parentVoc = null
        title = ""
        recordDuration = 0
    }

    // MARK: - Private

    private fun base64EncodedStringForCurrentRecord(): String? {
        val recordURL = audioRecorder.recordAudioURL
        if (!recordURL.isNullOrEmpty()) {
            val audioFileData = File(recordURL).readBytes()
            if (audioFileData.isNotEmpty()) {
                return Base64.encodeToString(audioFileData, Base64.DEFAULT)
            }
        }

        LoggingHelper.logErrorForClass(VocCreationManager::class.java, "Error encoding audio fil; no record audio Url.")
        return null
    }
}
