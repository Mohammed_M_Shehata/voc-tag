package com.voctag.android.manager

import android.app.Activity
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.Progress
import com.google.gson.Gson
import com.voctag.android.R
import com.voctag.android.api.RequestHelper
import com.voctag.android.db.PersistDataHelper
import com.voctag.android.db.relations.VocClient
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Voc
import com.voctag.android.repository.VocRepository
import com.voctag.android.ui.NotificationsRedirectActivity
import java.io.File

class LocalVocsManager(val mContext: Context) {

    private var percentage: Int = 0
    private var notificationManager: NotificationManager
    private var notificationBuilder: NotificationCompat.Builder

    init {
        AudioNotificationManager.createNotificationChannel(mContext)

        notificationBuilder = NotificationCompat.Builder(mContext)
        notificationBuilder.setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.notification_logo)
                .setContentTitle(mContext.getString(R.string.app_name))
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setContentText(mContext.getString(R.string.downloading))
                .setProgress(100, 0, false)
                .setChannelId(AudioNotificationManager.CHANNEL_ID)

        notificationManager = mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    fun downloadVoc(voc: Voc) {

        val notificationId = DOWNLOAD_NOTIFICATION_ID++
        val intent = Intent(mContext, NotificationsRedirectActivity::class.java).apply {
            putExtra(VoctagKeys.EXTRA_VOC, voc.encode())
        }

        val pendingIntent = PendingIntent.getActivity(mContext, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        notificationBuilder.setContentIntent(pendingIntent)
        notificationBuilder.setContentText(mContext.getString(R.string.downloading))
        val deleteIntent = Intent(mContext, DeleteDownloadNotificationReceiver::class.java).apply {
            putExtra(DOWNLOAD_TAG, getVocKey(voc.vocID))
        }
        val pendingDeleteIntent = PendingIntent.getBroadcast(mContext, notificationId, deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        notificationBuilder.setDeleteIntent(pendingDeleteIntent)

        val downloadRequest = PRDownloader.download(voc.vocAudioURL, getVocsDirPath(), getVocFileName(voc.vocID))
                .build()
        downloadRequest.tag = getVocKey(voc.vocID)
        downloadRequest
                .setOnProgressListener { progress: Progress ->
                    percentage = ((progress.currentBytes * 1.0 / progress.totalBytes) * 100).toInt()
                    broadCastDownloadProgress(voc.vocID, percentage)
                }.setOnCancelListener {
                    broadCastDownloadError(voc.vocID, mContext.getString(R.string.download_cancelled))
                    cancelDownloadingNotification(notificationId)
                }.start(object : OnDownloadListener {
                    override fun onDownloadComplete() {

                        saveDownloadedVoc(voc)
//                        PersistDataHelper.shared.saveDownloadedVoc(voc)
                    }

                    override fun onError(error: Error) {

                        var message = mContext.getString(R.string.server_error)
                        if (error.isConnectionError) {
                            message = mContext.getString(R.string.nointernet)
                        }
                        broadCastDownloadError(voc.vocID, message)
                        cancelDownloadingNotification(notificationId)
                    }
                })
        Downloader(notificationId, downloadRequest.downloadId).execute()

        Log.d("com.test", "downloaded ID > ${downloadRequest.downloadId}")
    }

    private fun saveDownloadedVoc(voc: Voc) {
        VocRepository(mContext).saveDownloadedVoc(voc)
        PersistDataHelper.shared.saveDownloadedVocsIds(listOf(voc))
    }

    fun saveDownloadedVocs(vocs: List<Voc>) {
        VocRepository(mContext).saveDownloadedVocs(vocs)
        PersistDataHelper.shared.saveDownloadedVocsIds(vocs)
    }

    fun isVocDownloaded(vocId: Int): Boolean {
//        val file = File(getVocsDirPath(), getVocFileName(vocId))
//        return file.exists()

        return PersistDataHelper.shared.getDownloadedVocsIds().contains(vocId)
    }

    fun getDownloadedVocs(): LiveData<List<VocClient>> {
        return VocRepository(mContext).getDownloadedVocs()
    }

    fun deleteVoc(vocId: Int) {
        val file = File(getVocsDirPath(), getVocFileName(vocId))

        if (file.exists()) {
            file.delete()
        }

//        PersistDataHelper.shared.removeDownloadedVoc(vocId)
        VocRepository(mContext).deleteDownloadedVoc(vocId)
        PersistDataHelper.shared.removeDownloadedVocId(vocId)
    }

    private fun getVocsDirPath() = File(mContext.filesDir, VOCS_PATH).path

    private fun getVocFileName(vocId: Int): String {

        return FILE_PREFIX.format(vocId)
    }

    private fun cancelDownloadingNotification(notificationId: Int) {

        val notificationManager = mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(notificationId)
    }

    private fun broadCastDownloadProgress(vocId: Int, progress: Int) {

        val intent = Intent(getVocKey(vocId))
        intent.putExtra(DOWNLOAD_PROGRESS, progress)
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
    }

    fun broadCastDownloadError(vocId: Int, message: String) {

        val intent = Intent(getVocKey(vocId))
        intent.putExtra(DOWNLOAD_ERROR, message)
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
    }

    fun getVocKey(vocId: Int): String = VOC_KEY.format(vocId)

    fun getDownloadedVoc(voc: Voc): File? {
        if (isVocDownloaded(voc.vocID)) {
            val file = File(getVocsDirPath(), getVocFileName(voc.vocID))
            if (file.exists())
                return file
        }
        return null
    }

    fun migrateSavingDownloadedVocsFromCacheToDatabase() {
        val downloadedVocsInCache = PersistDataHelper.shared.getDownloadedVocs()
        if (downloadedVocsInCache.isNotEmpty()) {
            searchForDownloadedVocs(downloadedVocsInCache.keys) { downloadedVocs ->
                if (downloadedVocs != null) {
                    LocalVocsManager(mContext).saveDownloadedVocs(downloadedVocs)
                } else {
                    LocalVocsManager(mContext).saveDownloadedVocs(ArrayList<Voc>(downloadedVocsInCache.values))
                }

                PersistDataHelper.shared.clearDownloadedVocsFromCache()
            }
        }
    }

    private fun searchForDownloadedVocs(vocsIds: MutableSet<Int>, onVocsFetched: (vocs: List<Voc>?) -> Unit) {
        val downloadedVocsIds = HashMap<String, MutableSet<Int>>()
        downloadedVocsIds[VOCS_IDS] = vocsIds
        val params = HashMap<String, String>()
        params[VOCS_IDS_KEY] = Gson().toJson(downloadedVocsIds)

        RequestHelper.shared.searchForDownloadedVocs(params, { vocs: ArrayList<Voc>?, errorHappened: Boolean ->
            if (errorHappened) {
                onVocsFetched(null)
            } else {
                onVocsFetched(vocs)
            }
        }, mContext as Activity)
    }

    fun getVocById(vocID: Int, onFetchVocListener: (voc: Voc?) -> Unit) {
        VocRepository(mContext).getVocById(vocID, onFetchVocListener)
    }

    fun updateCurrentVocIfInDatabase(voc: Voc) {
        VocRepository(mContext).updateCurrentVocIfInDatabase(voc)
    }

    companion object {

        const val VOCS_IDS_KEY: String = "VOCS_IDS_KEY"
        private const val VOCS_IDS = "voc_ids"
        private const val VOCS_PATH = "vocs"
        private const val FILE_PREFIX = "voc_%d.mp3"
        private const val VOC_KEY = "VOC_KEY_%d"
        const val DOWNLOAD_PROGRESS = "DOWNLOAD_PROGRESS"
        const val DOWNLOAD_ERROR = "DOWNLOAD_ERROR"
        const val DOWNLOAD_TAG = "DOWNLOAD_TAG"
        private var DOWNLOAD_NOTIFICATION_ID = 500

        fun newInstance(context: Context): LocalVocsManager {

            return LocalVocsManager(context)
        }

        // cancel all downloads and remove all notifications
        fun onDestroy(context: Context) {

            PRDownloader.cancelAll()

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.cancelAll()
        }
    }

    class DeleteDownloadNotificationReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {

            intent?.let {
                val tag = intent.getStringExtra(DOWNLOAD_TAG)
                Log.d("Local Vocs Manager", "cancel download...$tag")

                PRDownloader.cancel(tag)
            }
        }

    }

    private inner class Downloader(val notificationId: Int, val downloadId: Int) : AsyncTask<Void, Int, Boolean>() {

        override fun onPreExecute() {
            super.onPreExecute()

            notificationBuilder.setProgress(100, 0, false)
            notificationManager.notify(notificationId, notificationBuilder.build())
        }


        override fun onProgressUpdate(vararg values: Int?) {
            // Update progress
            notificationBuilder.setProgress(100, values[0] as Int, false)
            notificationManager.notify(notificationId, notificationBuilder.build())

            super.onProgressUpdate(values[0])
        }

        override fun doInBackground(vararg params: Void): Boolean {
            while (PRDownloader.getStatus(downloadId) == com.downloader.Status.RUNNING) {

                publishProgress(percentage)
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
            return true
        }

        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)

            Log.d("com.test", "complete> $result")
            if (percentage == 100) {
                notificationBuilder.setContentText(mContext.getString(R.string.downloaded))
                notificationBuilder.setProgress(0, 0, false)
                notificationManager.notify(notificationId, notificationBuilder.build())
            }
        }
    }
}