package com.voctag.android.manager

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import com.voctag.android.helper.VoctagKeys
import com.voctag.android.model.Voc
import com.voctag.android.repository.PlayListRepository

class AudioService : Service() {
    companion object {

        const val START_AUDIO_SERVICE: String = "START_AUDIO_SERVICE"
        const val STOP_AUDIO_SERVICE: String = "STOP_AUDIO_SERVICE"
        const val SAVE_PLAYLIST: String = "SAVE_PLAYLIST"
        const val SIGN_OUT_SERVICE: String = "SIGN_OUT_SERVICE"

        const val FOREGROUND_SERVICE_ID: Int = 1
        var playListManager: PlaylistManager? = null

        fun startAudioService(context: Context, voc: Voc) {
            val startIntent = Intent(context, AudioService::class.java).apply {
                putExtra(VoctagKeys.EXTRA_VOC, voc.encode())
                action = START_AUDIO_SERVICE
            }
            playListManager = PlaylistManager.shared
            context.startService(startIntent)
        }

        fun stopAudioService(context: Context, voc: Voc) {
            val startIntent = Intent(context, AudioService::class.java).apply {
                putExtra(VoctagKeys.EXTRA_VOC, voc.encode())
                action = STOP_AUDIO_SERVICE
            }
            context.startService(startIntent)
        }

        fun signOut(context: Context) {
            val startIntent = Intent(context, AudioService::class.java).apply {
                action = SIGN_OUT_SERVICE
            }
            context.startService(startIntent)
        }

        fun savePlayList(context: Context) {
            val startIntent = Intent(context, AudioService::class.java).apply {
                action = SAVE_PLAYLIST
            }
            context.startService(startIntent)
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            when (intent.action) {
                START_AUDIO_SERVICE -> {
                    val voc = Voc.decode(intent.getStringExtra(VoctagKeys.EXTRA_VOC))
                    startForeground(voc)
                }
                STOP_AUDIO_SERVICE -> {
                    val voc = Voc.decode(intent.getStringExtra(VoctagKeys.EXTRA_VOC))
                    stopAudio(voc)
                }
                SIGN_OUT_SERVICE -> {
                    playListManager?.reset()
                    stopForeground(true)
                    stopSelf()
                }
                SAVE_PLAYLIST -> {
                    savePlayList()
                }
            }
        }

        return START_NOT_STICKY
    }

    private fun startForeground(voc: Voc) {
        startForeground(FOREGROUND_SERVICE_ID, AudioNotificationManager.generateNotification(this, true, voc))
    }

    private fun stopAudio(voc: Voc) {
        PlayListRepository(this).updateCurrentPlayList(playListManager?.currentIndex, AudioPlayer.shared.getProgress()
                , playListManager?.autoPlayNextEnabled ?: false)

        stopForeground(false)
        AudioNotificationManager.generateNotification(this, false, voc)
    }

    private fun savePlayList() {
        playListManager?.let {
            PlayListRepository(this).savePlayList(it.playQueue, it.currentIndex, AudioPlayer.shared.getProgress())
        }
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            stopForeground(true)
        }
        super.onTaskRemoved(rootIntent)
    }
}